function mobiletoggle()
{
	if($('#mobile-menu-toggle').hasClass('on') == false){
		$('#mobile-menu-toggle > span:first-child').addClass('rotate-45');
		$('#mobile-menu-toggle > span:nth-child(2)').hide();
		$('#mobile-menu-toggle > span:last-child').addClass('rotate-135');

		$('.global-navigation').removeClass('hidden-xs hidden-sm hidden-md');
		$('.global-navigation').addClass('mobile-menu');

	}else{
		$('#mobile-menu-toggle > span:first-child').removeClass('rotate-45');
		$('#mobile-menu-toggle > span:nth-child(2)').show();
		$('#mobile-menu-toggle > span:last-child').removeClass('rotate-135');
		$('.global-navigation').addClass('hidden-xs hidden-sm hidden-md');
		$('.global-navigation').removeClass('mobile-menu');
	}
	$('#mobile-menu-toggle').toggleClass('on');
}

$('#mobile-menu-toggle').click(function(){
	mobiletoggle();
});

$('#mobile-search-close').click(function(){
	$('.global-search').addClass('hidden-xs hidden-sm hidden-md');
	$('#mobile-search-toggle').show();
	$('#mobile-search-close').hide();
});

$('#mobile-search-toggle').click(function(){
	$('.global-search').removeClass('hidden-xs hidden-sm hidden-md');
	$('#mobile-search-toggle').hide();
	$('#mobile-search-close').show();
	$('.global-search input[type=text]').focus();
});

$(window).resize(function(){
	if($(this).width() > 1200){
		if($('#mobile-menu-toggle').hasClass('on') == true){
			$('#mobile-menu-toggle > span:first-child').removeClass('rotate-45');
			$('#mobile-menu-toggle > span:nth-child(2)').show();
			$('#mobile-menu-toggle > span:last-child').removeClass('rotate-135');
			$('#mobile-menu-toggle').removeClass('on');
			$('.global-navigation').addClass('hidden-xs hidden-sm hidden-md');
			$('.global-navigation').removeClass('mobile-menu');

			$('.global-search').addClass('hidden-xs hidden-sm hidden-md');
		}
		
		$('#all-sites-container').show();
	}
	else {
		$('#all-sites-container').hide();
	}
});

$('.share-this.share-min a').click(function(){
	if($(this).attr('data-toggle') != 'modal'){
		var url = $(this).attr('data-url');

		if($(this).hasClass('button-pinterest-p')){
			if($(this).attr('data-image')){
				url += '&media=' + $(this).attr('data-image');
			}else{
				url += '&media=' + $('#og-image').attr('content');
			}
		}

		window.open(this.href + url,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=850px');
		return false;
	}else{
		var url = $(this).attr('data-url');


		if($(this).hasClass('button-pinterest-p')){
			url += '&media=' + $('#og-image').attr('content');
		}

		if($(this).hasClass('button-share')){
			url += '&media=' + $(this).attr('data-image');
		}

		$('#share-url').val(url);
	}
});

$('.share-this.share-all a').click(function(){
	var url = $('#share-url').val();

	if($(this).hasClass('button-pinterest-p')){
		if(url.search("&media") < 0){
			url += '&media=' + $('#og-image').attr('content') ;
		}
	}

	window.open(this.href + url,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=850px');
	return false;
});

$('.share-url').click(function(){
	var url = $(this).attr('data-url');

	if($(this).attr('data-image')){
		url += '&media=' + $(this).attr('data-image');
	}

	$('#share-url').val(url);
});

$('.modal-location').click(function(){
	$('#modal-map iframe').attr('src', $(this).attr('data-location'));
});

$('.modal-email').click(function(){
	$('#modal-email input[name=message]').val($(this).attr('data-message'));
});

$('[data-toggle="tooltip"]').tooltip();

// $('ul.nav li.dropdown').hover(function() {
//   $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
// }, function() {
//   $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
// });
