<?php

class Business extends CI_Model{

	var $table = 'phuketnews_business';
	var $category = 'phuketnews_business_category';
	var $photo = 'phuketnews_business_photo';

	function sitemap($page)
	{
		$limit = 500;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM $this->table ";
		$sql .= "WHERE status = 1 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function slug($str)
	{
		$text = $str;
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = @iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if(empty($text)) {
			return urlencode($str);
		}

		return $text;
	}

	function get_md5_slug($slug)
	{
		$this->db->where('md5(slug)', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function insert_photo($arr)
	{
		$this->db->insert($this->photo, $arr);
		return $this->db->insert_id();
	}

	function get_photo($id)
	{

		$this->db->order_by('num', 'asc');
		$this->db->where('id',$id);
		$query = $this->db->get($this->photo);
		return $query->result();
	}

	function clear_photo($id, $num){
		$this->db->where('id', $id);
		$this->db->where('num', $num);
		$this->db->delete($this->photo);
	}

	function update_photo($photo_id, $arr)
	{
		$this->db->where('photo_id', $photo_id);
		$this->db->update($this->photo, $arr);
	}

	function get_category()
	{
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_list()
	{
		$this->db->order_by('category_title','asc');
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_title($category_title)
	{
		$this->db->where('category_title', $category_title);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_province_cate($province, $category_id, $page)
	{

		$limit = 20;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND state = '$province' ";
		$sql .= "AND category_id = '$category_id' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND state = '$province' ";
		$sql .= "AND category_id = '$category_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_in_category($category_id, $latest)
	{

		$items = array();

		foreach($latest as $index=>$value){
			$items[] = $value->id;
		}

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND category_id = '$category_id' ";
		$sql .= "AND id NOT IN (".implode(',', $items).")";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 5";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_business_active_category($page, $category_id)
	{
		if($page == false){
			$page = 1;
		}
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->category.".category_id = '$category_id' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".status = 1 ";
		$sql .= "ORDER BY ".$this->table.".id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE category_id = '$category_id' ";
		$sql .= "AND status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function get_rand($limit)
	{
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY RAND() ";
		$sql .= "LIMIT 0, $limit";
		$query = $this->db->query($sql);
		return $query->result();
	}


	function get_latest($limit)
	{
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY RAND() ";
		$sql .= "LIMIT 0, $limit";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_active($page)
	{
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY ".$this->table.".id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = round($rs[0]->num/$limit,0);

		return $data;

	}

	function get_map_active($page)
	{
		$category_id = $this->input->get('category_id');
		$keyword = $this->input->get('keyword');
		$province = $this->input->get('province');
		$geo = $this->input->get('geo');

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "AND ".$this->table.".lat != '' ";
		$sql .= "AND ".$this->table.".lng != '' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}
		if($keyword){
			$sql .= "AND (";
			$sql .= "title LIKE '%$keyword%' OR ";
			$sql .= "keywords LIKE '%$keyword%' OR ";
			$sql .= "description LIKE '%$keyword%' ";
			$sql .= ")";
		}
		if($province){
			$sql .= "AND state = '$province' ";
		}
		if($geo){
			$sql .= "AND city = '$geo' ";
		}
		$sql .= "ORDER BY ".$this->table.".id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		//Paging
		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".lat != '' ";
		$sql .= "AND ".$this->table.".lng != '' ";

		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}
		if($keyword){
			$sql .= "AND (";
			$sql .= "title LIKE '%$keyword%' OR ";
			$sql .= "keywords LIKE '%$keyword%' OR ";
			$sql .= "description LIKE '%$keyword%' ";
			$sql .= ")";
		}
		if($province){
			$sql .= "AND state = '$province' ";
		}
		if($geo){
			$sql .= "AND city = '$geo' ";
		}
		$sql .= "ORDER BY ".$this->table.".id DESC ";

		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = round($rs[0]->num/$limit,0);

		return $data;

	}

	function get_search()
	{
		$category_id = $this->input->get('category_id');
		$keyword = $this->input->get('keyword');
		$page = $this->input->get('page');
		$province = $this->input->get('province');
		if($page == false){ $page = 1;}
		$geo = $this->input->get('geo');

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		// if($geo){
		// 	list($lat, $lng) = explode(',',$geo);
		// 	$sql .= ",( 3959 * acos( cos( radians('$lat') ) *
		// 	cos( radians( ".$this->table.".lat ) ) *
		// 	cos( radians( ".$this->table.".lng ) -
		// 	radians('$lng') ) +
		// 	sin( radians('$lat') ) *
		// 	sin( radians(  ".$this->table.".lat ) ) ) )
		// 	AS distance ";
		// }

		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}
		if($keyword){
			$sql .= "AND (";
			$sql .= "title LIKE '%$keyword%' OR ";
			$sql .= "keywords LIKE '%$keyword%' OR ";
			$sql .= "description LIKE '%$keyword%' ";
			$sql .= ")";
		}
		if($province){
			$sql .= "AND state = '$province' ";
		}

		// if($geo){
		// 	$sql .= "ORDER BY distance ";
		// }else{
		// 	$sql .= "ORDER BY ".$this->table.".id DESC ";
		// }



		if($geo){
			$sql .= "AND city = '$geo' ";
		}
		$sql .= "ORDER BY ".$this->table.".id DESC ";

		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		if($keyword){
			$sql .= "AND (";
			$sql .= "title LIKE '%$keyword%' OR ";
			$sql .= "keywords LIKE '%$keyword%' OR ";
			$sql .= "description LIKE '%$keyword%' ";
			$sql .= ")";
		}
		if($category_id){
			$sql .= "AND category_id = '$category_id' ";
		}
		if($province){
			$sql .= "AND state = '$province' ";
		}
		if($geo){
			$sql .= "AND city = '$geo' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = round($rs[0]->num/$limit,0);

		return $data;

	}

	function get_category_id($category_id)
	{
		$this->db->where('category_id', $category_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_slug($slug)
	{
		$this->db->where('category_slug', $slug);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_province()
	{
		$sql = "SELECT distinct state From " . $this->table . " where state != '' Order By state";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_city($province)
	{
		$sql = "SELECT distinct city From " . $this->table;
		$sql .= " Where city != ''";

		if($province != '')
		{
			$sql .= " and state = '" . $province . "'";
		}
		$sql .= " Order By city";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_closest_business($lat, $lng, $category_id, $id)
	{

		$sql = "SELECT *,
			( 3959 * acos( cos( radians('$lat') ) *
			cos( radians( lat ) ) *
			cos( radians( lng ) -
			radians('$lng') ) +
			sin( radians('$lat') ) *
			sin( radians( lat ) ) ) )
			AS distance ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE 1 ";
		$sql .= "AND ".$this->table.".id != '$id' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		$sql .= "AND status = 1 ";
		$sql .= "ORDER BY distance ASC, sponsor DESC ";
		$sql .= "LIMIT 0, 3 ";
		$query = $this->db->query($sql);
		return $query->result();

	}

	function views($id)
	{

		$sql = "UPDATE ".$this->table." ";
		$sql .= "SET views = views + 1 ";
		$sql .= "WHERE id = '$id' ";
		$this->db->query($sql);

	}

	function get_email_id($email, $id)
	{

		$this->db->where('email', $email);
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();

	}

	function get_email($email)
	{
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}

		$limit = 30;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE email = '$email' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE email = '$email' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = round($rs[0]->num/$limit,0);

		return $data;

	}

	function get_business_id_by_member($member_id, $id)
	{
		$this->db->where('member_id', $member_id);
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_member_id($member_id, $status)
	{
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}
		$q = $this->input->get('q');

		$limit = 12;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE 1 ";
		$sql .= "AND member_id = '$member_id' ";
		if($q){
			$sql .= "AND  ".$this->table.".title LIKE '%$q%' ";
		}
		$sql .= "AND status = '$status' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE 1 ";
		if($q){
			$sql .= "AND  ".$this->table.".title LIKE '%$q%' ";
		}
		$sql .= "AND status = '$status' ";
		$sql .= "AND member_id = '$member_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

}
