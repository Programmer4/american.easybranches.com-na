<?php

class Share extends CI_Model{

	function get()
	{
	
		$n = 0;

		$items[$n]['envelope']['icon'] = 'envelope';
		$items[$n]['envelope']['url'] = base_url().'email/share?u=';
		$n++;

		$items[$n]['facebook']['icon'] = 'facebook';
		$items[$n]['facebook']['url'] = 'https://www.facebook.com/sharer/sharer.php?u=';
		$n++;
		
		$items[$n]['twitter']['icon'] = 'twitter';
		$items[$n]['twitter']['url'] = 'https://twitter.com/intent/tweet?url=';
		$n++;

		$items[$n]['google-plus']['icon'] = 'google-plus';
		$items[$n]['google-plus']['url'] = 'https://plus.google.com/share?url=';
		$n++;

		$items[$n]['pinterest-p']['icon'] = 'pinterest-p';
		$items[$n]['pinterest-p']['url'] = 'https://www.pinterest.com/pin/create/button/?url=';
		$n++;

		$items[$n]['linkedin']['icon'] = 'linkedin';
		$items[$n]['linkedin']['url'] = 'https://www.linkedin.com/cws/share?url=';
		$n++;
		
		$items[$n]['reddit-alien']['icon'] = 'reddit-alien';
		$items[$n]['reddit-alien']['url'] = 'https://www.reddit.com/submit?url=';
		$n++;
		
		$items[$n]['digg']['icon'] = 'digg';
		$items[$n]['digg']['url'] = 'http://digg.com/submit?phase=2&amp;url=';
		$n++;

		$items[$n]['foursquare']['icon'] = 'foursquare';
		$items[$n]['foursquare']['url'] = '';
		$n++;

		$items[$n]['delicious']['icon'] = 'delicious';
		$items[$n]['delicious']['url'] = 'https://del.icio.us/save?provider=easybranches.com&title=ShareThis&v=5&url=';
		$n++;

		$items[$n]['whatsapp']['icon'] = 'whatsapp';
		$items[$n]['whatsapp']['url'] = 'whatsapp://send?text=';
		$n++;

		$items[$n]['weibo']['icon'] = 'weibo';
		$items[$n]['weibo']['url'] = 'http://service.weibo.com/staticjs/weiboshare.html?url=';
		$n++;

		$items[$n]['tumblr']['icon'] = 'tumblr';
		$items[$n]['tumblr']['url'] = 'http://www.tumblr.com/share/link?url';
		$n++;

		$items[$n]['xing']['icon'] = 'xing';
		$items[$n]['xing']['url'] = 'https://www.xing.com/spi/shares/new?url=';
		$n++;

		return $items;
	
	}
	
	function push($url, $media){	
		$items = $this->get();
				
		$i = 0;
		$code = '<ul class="share-this share-min">';
		foreach($items as $index=>$value){
			
			if($index > 6){
				$add_class = 'hidden-xs';
			}else{
				$add_class = '';
			}
			
			foreach($value as $ii=>$vv){
				$code .= '<li class="'.$add_class.'">';
				$code .= '<a href="'.$value[$ii]['url'].'" data-url="'.$url.'" data-image="'.$media.'" class="button-'.$ii.'">';
				$code .= '<i class="fa fa-fw fa-'.$ii.'"></i>';
				$code .= '</a>';
				$code .= '</li>';
			}
			
			if($i > 5){
				$code .= '<li>';
				$code .= '<a href="#" data-toggle="modal" data-target="#modal-share" data-url="'.$url.'" data-image="'.$media.'" class="button-share">';
				$code .= '<i class="fa fa-fw fa-share-alt"></i>';
				$code .= '</a>';
				$code .= '</li>';
				break;
			}
			$i++;
		}
		$code .= '</ul>';
	
		return $code;
	}
	
	function all($url, $media)
	{
		$items = $this->get();
				
		$i = 0;
		$code = '<ul class="share-this share-all">';
		foreach($items as $index=>$value){
						
			foreach($value as $ii=>$vv){
				$code .= '<li>';
				$code .= '<a href="'.$value[$ii]['url'].'" class="button-'.$ii.'" data-image="'.$media.'" onClick="$(this).attr($(\'#share-url\').val())">';
				$code .= '<i class="fa fa-fw fa-'.$ii.'"></i>';
				$code .= '</a>';
				$code .= '</li>';
			}
			
			$i++;
		}
		$code .= '</ul>';
	
		return $code;	
	}


}