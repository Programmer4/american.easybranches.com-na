<?php

class Yacht extends CI_Model{

	var $table = 'phuketnews_yacht';
	var $category = 'phuketnews_yacht_category';
	var $photo = 'phuketnews_yacht_photo';
	var $location = 'phuketnews_yacht_location';

	function get_rand_index($page)
	{
		if($page == false){ $page = 1;}
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY rand() DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_rand($limit)
	{
		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY rand() ";
		$sql .= "LIMIT 0, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_latest($limit)
	{
		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY rand() ";
		$sql .= "LIMIT 0, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_member_items($member_id, $page)
	{
		if($page == false){
			$page = 1;
		}
		$limit = 16;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND member_id = '$member_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_md5_slug($slug)
	{
		$this->db->where('md5(slug)', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_category_array(){

		$query = $this->db->get($this->category);
		$rs = $query->result();
		foreach($rs as $value){
			$category[$value->category_id] = $value ;
		}

		return $category;

	}

	function clear_photo($id, $num){
		$this->db->where('id', $id);
		$this->db->where('num', $num);
		$this->db->delete($this->photo);
	}

	function get_th_province_list()
	{
		$this->db->order_by('title', 'asc');
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_th_province($title)
	{
		$this->db->where('title', $title);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_location()
	{
		$this->db->order_by('title','asc');
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_location_id($location_id)
	{
		$this->db->where('location_id', $location_id);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function insert_photo($arr){
		$this->db->insert($this->photo, $arr);
	}

	function get_items($category_id, $page)
	{
		if($page == false){ $page = 1;}
		$limit = 12;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND ".$this->category.".category_id = '$category_id' ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$sql .= "AND category_id = '$category_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_search()
	{

		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}
		$q = $this->input->get('q');
		$q = addslashes($q);
		$category_id = $this->input->get('category_id');
		$want_to = $this->input->get('want_to');
		$location_id = $this->input->get('location_id');

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}
		// if($want_to){
		// 	$sql .= "AND ".$this->table.".want_to = '$want_to' ";
		// }

		if($want_to == 'charter'){
			$sql .= "AND " .$this->table. ".want_to = 'charter' ";
		} else if ($want_to == 'sale') {
			$sql .= "AND " .$this->table. ".want_to = 'sale' ";
		}

		if($location_id){
			$sql .= "AND ".$this->table.".location_id = '$location_id' ";
		}
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		if($q){
			$sql .= "AND (".$this->table.".title LIKE '%$q%' OR ".$this->table.".description LIKE '%$1%' OR detail LIKE '%$q%') ";
		}
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";

		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}

		if ($want_to == 'charter') {
			$sql .= "AND " .$this->table. ".want_to = 'charter' ";
		} else if ($want_to == 'sale') {
			$sql .= "AND " .$this->table. ".want_to = 'sale' ";
		}

		if($location_id){
			$sql .= "AND ".$this->table.".location_id = '$location_id' ";
		}
		if($q){
			$sql .= "AND (title LIKE '%$q%' OR description LIKE '%$1%' OR detail LIKE '%$q%')";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_category($parent_id)
	{
		$this->db->where('parent_id', $parent_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_id($category_id)
	{
		$this->db->where('category_id', $category_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_slug($slug)
	{
		$this->db->where('category_slug', $slug);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_location_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_category_list($parent_id)
	{
		$this->db->where('parent_id', $parent_id);
		$this->db->order_by('category_title', 'asc');
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function get_id_by_member($id, $member_id)
	{
		$this->db->where('id',$id);
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_active_all()
	{

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;

		return $data;
	}
	function get_want_to($category_id)
	{
		$this->db->where('category_id',$category_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_length($length)
	{
		$this->db->where('length',$length);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_bath($bathrooms)
	 {
	 	$this->db->where('bathrooms',$bathrooms);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_cabin($cabin)
	 {
	 	$this->db->where('cabin',$cabin);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_accommodates($accommodates)
	 {
	 	$this->db->where('accommodates',$accommodates);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_price($price)
	 {
	 	$this->db->where('price',$price);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_note($note)
	 {
	 	$this->db->where('note',$note);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_detail($detail)
	 {
	 	$this->db->where('detail',$detail);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_activity($activity)
	 {
	 	$this->db->where('activity',$activity);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_amenities($amenities)
	 {
	 	$this->db->where('amenities',$amenities);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	 function get_conditions($conditions)
	 {
	 	$this->db->where('conditions',$conditions);
		$query = $this->db->get($this->table);
		return $query->result();
	 }

	function get_active($page)
	{
		if($page == false){ $page = 1;}
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province, ";
		$sql .= $this->table.".lat AS lat, ";
		$sql .= $this->table.".lng AS lng, ";
		$sql .= $this->table.'.want_to ';
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_photo($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->photo);
		return $query->result();
	}

	function get_member($member_id, $status)
	{
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}
		$limit = 12;
		$start = $page * $limit - $limit;
		$q = $this->input->get('q');
		$q = addslashes($q);

		$sql = "SELECT ".$this->table.".*, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->location.title AS location ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND status = '$status' ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		if($q){
			$sql .= "AND ".$this->table.".title LIKE '%".$q."%' ";
		}
		$sql .= "AND member_id = '$member_id' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND ".$this->table.".title LIKE '%".$q."%' ";
		}
		$sql .= "AND member_id = '$member_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_nearly($lat, $lng, $limit, $current_id)
	{
			$sql = "SELECT *, ";
			$sql .= $this->category.".category_title AS category, ";
			$sql .= $this->table.".title AS title, ";
			$sql .= $this->table.".slug AS slug, ";
			$sql .= $this->location.".title AS province, ";
			$sql .= "( 3959 * acos( cos( radians('$lat') ) *
			cos( radians( lat ) ) *
			cos( radians( lng ) -
			radians('$lng') ) +
			sin( radians('$lat') ) *
			sin( radians( lat ) ) ) )
			AS distance ";
			$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
			$sql .= "WHERE status = 1 ";
			$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
			$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
			$sql .= "AND ".$this->table.".id != " . $current_id . " ";
			$sql .= "ORDER BY distance ";
			$sql .= "LIMIT 0, $limit";

			$query = $this->db->query($sql);
			return $query->result();
	}

	function get_by_filters($page, $provinceId, $categoryId, $want_to)
	{
		if($page == false){ $page = 1;}
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->category.".category_title AS category, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->location.".title AS province ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND status = '1' ";

		if($provinceId != '')
		{
			$sql .= "AND ".$this->table.".location_id = " . $provinceId . ' ';
		}

		if($categoryId != '')
		{
			$sql .= "AND ".$this->table.".category_id = '" . $categoryId . "' ";
		}

		if($want_to != '')
		{
			$sql .= "AND ".$this->table.".want_to = '" . $want_to . "' ";
		}

		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = '1' ";
		if($provinceId != '')
		{
			$sql .= "AND ".$this->table.".location_id = " . $provinceId . ' ';
		}

		if($categoryId != '')
		{
			$sql .= "AND ".$this->table.".category_id = '" . $categoryId . "'";
		}

		if($want_to != '')
		{
			$sql .= "AND ".$this->table.".want_to = '" . $want_to . "'";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

function check_existing_want_to($want_to){
	$sql = "SELECT COUNT(*) AS num ";
	$sql .= "FROM ".$this->table." ";
	$sql .= "WHERE want_to = '".$want_to."'";
	$query = $this->db->query($sql);
	$rs = $query->result();
	return $rs[0]->num;
}

}
