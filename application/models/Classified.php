<?php

class Classified extends CI_Model{

	var $table = 'phuketnews_classified';
	var $category = 'phuketnews_classified_category';
	var $photo = 'phuketnews_classified_photo';
	var $location = 'phuketnews_classified_location';

	function get_rand($page)
	{

		$limit =21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->table.slug AS slug ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		$sql .= "ORDER BY rand() ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;

	}

	function get_member_items($member_id, $page)
	{
		if($page == false){
			$page = 1;
		}
		$limit = 16;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND member_id = '$member_id' ";
		$sql .= "ORDER BY id DESC ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND member_id = '$member_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_md5_slug($slug)
	{
		$this->db->where('md5(slug)', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_category_array(){

		$query = $this->db->get($this->category);
		$rs = $query->result();
		foreach($rs as $value){
			$category[$value->category_id] = $value ;
		}

		return $category;

	}

	function clear_photo($id, $num){
		$this->db->where('id', $id);
		$this->db->where('num', $num);
		$this->db->delete($this->photo);
	}

	function get_th_province_list()
	{
		$this->db->order_by('title', 'asc');
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_location_id($location_id)
	{
		$this->db->where('location_id', $location_id);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function insert_photo($arr){
		$this->db->insert($this->photo, $arr);
	}

	function get_lastest($page)
	{
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->table.slug AS slug ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;
	}

	function get_items($category_id, $child_id, $page)
	{
		if($page == false){
			$page = 1;
		}
		$limit = 16;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND category_id = '$category_id' ";
		if($child_id){
			$sql .= "AND child_id = '$child_id' ";
		}
		$sql .= "ORDER BY id DESC ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND category_id = '$category_id' ";
		if($child_id){
			$sql .= "AND child_id = '$child_id' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_search()
	{

		$location_id = $this->input->get('location_id');
		$category_id = $this->input->get('category_id');
		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}
		$q = $this->input->get('keyword');
		$q = addslashes($q);

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->table.slug AS slug ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		if($location_id){
			$sql .= "AND $this->table.location_id = '$location_id' ";
		}
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}
		if($q){
			$sql .= "AND (title LIKE '%$q%' OR description LIKE '%$1%' OR detail LIKE '%$q%')";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		if($location_id){
			$sql .= "AND $this->table.location_id = '$location_id' ";
		}
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}
		if($q){
			$sql .= "AND (title LIKE '%$q%' OR description LIKE '%$1%' OR detail LIKE '%$q%')";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_category($parent_id)
	{
		$this->db->where('parent_id', $parent_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_id($category_id)
	{
		$this->db->where('category_id', $category_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_slug($slug)
	{
		$this->db->where('category_slug', $slug);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_list($parent_id)
	{
		$this->db->where('parent_id', $parent_id);
		$this->db->order_by('category_title', 'asc');
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_location_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function get_id_by_member($id, $member_id)
	{
		$this->db->where('id',$id);
		$this->db->where('member_id',$member_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_active($page)
	{
		if($page == false){ $page = 1;}
		$limit = 12;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND status = '1' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND status = '1' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

	}

	function get_photo($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->photo);
		return $query->result();
	}

	function get_member($member_id)
	{
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}
		$limit = 12;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= ",$this->table.title as classified_title ";
		$sql .= ",$this->location.title as location_title ";
		$sql .= "FROM ".$this->table.", ".$this->category.", ".$this->location." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		$sql .= "AND member_id = '$member_id' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND member_id = '$member_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_by_member_id($member_id, $status)
	{
		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}

		$limit = 20;
		$start = $page * $limit - $limit;
		$q = $this->input->get('keyword');


		$sql = "SELECT *, ".$this->location .".title AS location_title ".", ".$this->table .".title AS classified_title ";
		$sql .= "FROM ".$this->table. ", ".$this->category. ", ".$this->location ." ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		$sql .= "AND ".$this->table. ".category_id =  ".$this->category.".category_id ";
		$sql .= "AND ".$this->table.".location_id = ".$this->location.".location_id ";
		if($q){
			$sql .= "AND ".$this->table .".title LIKE '%$q%' ";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table . " " ;
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND ".$this->table .".title LIKE '%$q%' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_by_filters($page, $location_id, $category_id )
	{
		if($this->input->get('location_id') != '')
		{
			$location_id = $this->input->get('location_id');
		}

		if($this->input->get('category_id') != '')
		{
			$category_id = $this->input->get('category_id');
		}

		if($this->input->get('page') != '')
		{
			$page = $this->input->get('page');
		}

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		$q = $this->input->get('keyword');
		$q = addslashes($q);

		$sql = "SELECT *, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->table.slug AS slug ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		if($q){
			$sql .= "AND ($this->table.title LIKE '%$q%' OR $this->table.description LIKE '%$1%' OR $this->table.detail LIKE '%$q%') ";
		}
		if($location_id){
			$sql .= "AND $this->table.location_id = '$location_id' ";
		}
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		if($q){
			$sql .= "AND ($this->table.title LIKE '%$q%' OR $this->table.description LIKE '%$1%' OR $this->table.detail LIKE '%$q%') ";
		}
		if($location_id){
			$sql .= "AND $this->table.location_id = '$location_id' ";
		}
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;
	}


	function get_related_item($category_id, $location_id, $limit, $current_id)
	{
		$sql = "SELECT *, ";
		$sql .= "$this->table.title AS title, ";
		$sql .= "$this->table.slug AS slug ";
		$sql .= "FROM $this->table, $this->category, $this->location ";
		$sql .= "WHERE 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.location_id = $this->location.location_id ";
		$sql .= "AND $this->table.status = 1 ";
		if($location_id){
			$sql .= "AND $this->table.location_id = '$location_id' ";
		}
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}

		if($current_id){
			$sql .= "AND $this->table.id != '$current_id' ";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, $limit";
		$query = $this->db->query($sql);
		return $query->result();
	}


}
