<?php

class Apple extends CI_Model{

	var $table = 'phuketnews_applemovie';

	function movie()
	{
		$api = 'https://itunes.apple.com/th/rss/topmovies/limit=50/genre=4401/xml';		
		return file_get_contents($api);
	}
	
	function movie_get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		$rs = $query->result();
		if(isset($rs[0]->movie_id)){
			return $rs[0]->movie_id;
		}else{
			return false;
		}
	}
	
	function movie_insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

}