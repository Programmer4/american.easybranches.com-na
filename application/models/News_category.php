<?php

class News_category extends CI_Model{

	var $table = 'phuketnews_news_category';

	function get_id($category_id)
	{
		$this->db->where('category_id', $category_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get()
	{
		$this->db->order_by('category_title', 'ASC');
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get_slug($slug)
	{
		$this->db->where('category_slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

}