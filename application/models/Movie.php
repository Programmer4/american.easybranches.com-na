<?php

class Movie extends CI_Model{

	var $table = 'phuketnews_applemovie';

	function get($page)
	{
		$limit = 30;
		$start = $page * $limit - $limit;
		
		$sql = "SELECT * ";
		$sql .= "FROM $this->table ";		
		$sql .= "ORDER BY movie_id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();
		
		$sql = "SELECT COUNT(*) num ";
		$sql .= "FROM $this->table ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);
		
		return $data;
	}
	
	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

}