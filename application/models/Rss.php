<?php
class Rss extends CI_Model{

	var $table = 'phuketnews_rss_news';

	function insert($arr)
	{
	
		$this->db->where('guid', $arr['guid']);
		$query = $this->db->get($this->table);
		$rs = $query->result();
		
		if(empty($rs[0]->id)){
			
			$this->db->insert($this->table, $arr);
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

}