<?php

class Session extends CI_Model{
	
	var $table = 'phuketnews_session';

	function getcode(){
		
		if(empty($_SESSION['mysession_id'])){

			$str = 'abcdefghijklmnopqrstuvwxyz';
			$str .= strtoupper($str);
			$str .= '0123456789';
			
			$ss = '';
			
			for($i=1; $i<= 15; $i++){
				$ss .= substr($str,rand(0,strlen($str)),1);
			}
			
			$_SESSION['mysession_id'] = $ss;
									
		}else{
			$ss = $_SESSION['mysession_id'];
		}
		
		return $ss;
		
	}
	
	function sess($mysession_id)
	{
		
		$sql = "SELECT * FROM ".$this->table." WHERE mysession_id = '$mysession_id' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
				
		return $rs;	
	}
	
	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}
	
	function update($mysession_id, $arr)
	{
		$this->db->where('mysession_id', $mysession_id);
		$this->db->update($this->table, $arr);
	}	
	
	function delete($mysession_id)
	{
		$this->db->where('mysession_id', $mysession_id);
		$this->db->delete($this->table);
	}
	
	
}