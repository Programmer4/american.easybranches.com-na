<?php

class Comment extends CI_Model{

	var $table = 'phuketnews_comments';

	function approve($id)
	{
		$this->db->where('md5(id)', $id);
		$this->db->update($this->table, array('status'=>1));
	}
	
	function reject($id){
		$this->db->where('md5(id)', $id);
		$this->db->delete($this->table);
	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}
	
	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}
	
	function get_slug($uri, $page)
	{
		$limit = 10;

		if($page == false){ $page = 1;}
		$start = $page * $limit - $limit;
		
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND uri = '$uri' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();
		
		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND uri = '$uri' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);
		
		return $data;
	}

}