<?php

class Event extends CI_Model{

	var $table = 'phuketnews_events';
	var $category = 'phuketnews_events_category';

	function get_rand($page)
	{

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->category.".title AS category, ";
		$sql .= $this->category.".slug AS category_slug ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d')+1,date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$sql .= "ORDER BY rand() ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		return $data;

	}

	function get_active_all()
	{
		$sql = "SELECT *, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->category.".title AS category, ";
		$sql .= $this->category.".slug AS category_slug ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d')+1,date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$sql .= "ORDER BY id DESC ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_nearly($lat, $lng, $limit, $current_event_id)
	{
			$sql = "SELECT *, ";
			$sql .= $this->table.".slug AS slug, ";
			$sql .= $this->table.".title AS title, ";
			$sql .= $this->category.".title AS category, ";
			$sql .= $this->category.".slug AS category_slug, ";
			$sql .= "( 3959 * acos( cos( radians('$lat') ) *
			cos( radians( lat ) ) *
			cos( radians( lng ) -
			radians('$lng') ) +
			sin( radians('$lat') ) *
			sin( radians( lat ) ) ) )
			AS distance ";
			$sql .= "FROM ".$this->table. ", ".$this->category." ";
			$sql .= "WHERE status = 1 ";
			$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
			$sql .= "AND ".$this->table.".id != " . $current_event_id . " ";
			$sql .= "ORDER BY distance ";
			$sql .= "LIMIT 0, $limit";

			$query = $this->db->query($sql);
			return $query->result();

	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_by_member_id($member_id, $status)
	{
		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}

		$limit = 20;
		$start = $page * $limit - $limit;
		$q = $this->input->get('q');

		$sql = "SELECT *, " .$this->table.".title AS event_title ,". $this->category.".title AS category_title ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		$sql .= "AND ".$this->table. ".type_id =  ".$this->category.".type_id ";
		if($q){
			$sql .= "AND " . $this->table. ".title LIKE '%$q%' ";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table . " " ;
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND ".$this->table.".title LIKE '%$q%' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_category()
	{
		$this->db->order_by('title','asc');
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_id($type_id){
		$this->db->where('type_id',$type_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_category_slug($slug){
		$this->db->where('slug',$slug);
		$query = $this->db->get($this->category);
		return $query->result();
	}


	function get_slug_md5($slug)
	{
		$this->db->where('md5(slug) = ', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_latest()
	{
		$this->db->order_by('id', 'desc');
		$this->db->where('status',1);
                $this->db->where('time_end >', date('Y-m-d H:i:s'));
		$query = $this->db->get($this->table, 5, 0);
		return $query->result();
	}

	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_id_member($member_id, $id)
	{
		$this->db->where('member_id', $member_id);
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_type_id($type_id)
	{
		$this->db->where('type_id', $type_id);
		$query = $this->db->get($this->category);
		return $query->result();
	}

	function get_active($page)
	{

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->category.".title AS category, ";
		$sql .= $this->category.".slug AS category_slug ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d')+1,date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d')+1,date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_active_category($slug, $page)
	{

		$query = $this->db->get_where($this->category, array('slug'=>$slug));
		$category = $query->result();

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->category.".title AS category, ";
		$sql .= $this->category.".slug AS category_slug ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$sql .= "AND ".$this->table.".type_id = '".$category[0]->type_id."' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		$sql .= "AND ".$this->table.".type_id = '".$category[0]->type_id."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_search()
	{

		$page = $this->input->get('page');
		$type_id = $this->input->get('type_id');
		//$geo = $this->input->get('geo');
		$province = $this->input->get('province');
		$time_start = $this->input->get('time_start');

		$query = $this->db->get_where($this->category, array('type_id'=>$type_id));
		$category = $query->result();

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		//if(empty($geo)){
			$sql = "SELECT *, ";
			$sql .= $this->table.".slug AS slug, ";
			$sql .= $this->table.".title AS title, ";
			$sql .= $this->category.".title AS category, ";
			$sql .= $this->category.".slug AS category_slug ";
			$sql .= "FROM ".$this->table. ", ".$this->category." ";
			$sql .= "WHERE status = 1 ";
			if($time_start){
				$sql .= "AND ".$this->table.".time_start >= '$time_start' ";
				$sql .= "AND ".$this->table.".time_end > '$time_start' ";
			}else{
				$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
			}
			if($province != ''){
				$sql .= "AND ".$this->table.".province = '$province' ";
			}
			$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
			if(isset($category[0]->type_id)){
				$sql .= "AND ".$this->table.".type_id = '".$category[0]->type_id."' ";
			}



			$sql .= "ORDER BY id DESC ";
		// }else{
		//
		// 	list($lat,$lng) = explode(',', $geo);
		//
		// 	$sql = "SELECT *, ";
		// 	$sql .= $this->table.".slug AS slug, ";
		// 	$sql .= $this->table.".title AS title, ";
		// 	$sql .= $this->category.".title AS category, ";
		// 	$sql .= $this->category.".slug AS category_slug, ";
		// 	$sql .= "( 3959 * acos( cos( radians('$lat') ) *
		// 	cos( radians( lat ) ) *
		// 	cos( radians( lng ) -
		// 	radians('$lng') ) +
		// 	sin( radians('$lat') ) *
		// 	sin( radians( lat ) ) ) )
		// 	AS distance ";
		// 	$sql .= "FROM ".$this->table. ", ".$this->category." ";
		// 	$sql .= "WHERE status = 1 ";
		// 	if($time_start){
		// 		$sql .= "AND ".$this->table.".time_start >= '$time_start' ";
		// 		$sql .= "AND ".$this->table.".time_end > '$time_start' ";
		// 	}else{
		// 		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		// 	}
		// 	$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		// 	if(isset($category[0]->type_id)){
		// 		$sql .= "AND ".$this->table.".type_id = '".$category[0]->type_id."' ";
		// 	}
		// 	$sql .= "ORDER BY distance ";
		// }
		$sql .= "LIMIT $start, $limit ";

		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		if($time_start){
			$sql .= "AND ".$this->table.".time_start >= '$time_start' ";
			$sql .= "AND ".$this->table.".time_end > '$time_start' ";
		}else{
			$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		}
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";
		if(isset($category[0]->type_id)){
			$sql .= "AND ".$this->table.".type_id = '".$category[0]->type_id."' ";
		}
		if($province != ''){
			$sql .= "AND ".$this->table.".province = '$province' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_province()
	{
		$sql = "SELECT distinct province From " . $this->table . " where province != '' Order By province";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_by_filters($page, $type_id, $province, $city)
	{

		if($page == false){
			$page = 1;
		}

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT *, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->category.".title AS category, ";
		$sql .= $this->category.".slug AS category_slug ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";

		if($type_id != '')
		{
			$sql .= "AND ".$this->table.".type_id = " . $type_id . ' ';
		}

		if($province != '')
		{
			$sql .= "AND ".$this->table.".province = '" . $province . "'";
		}

		if($city != '')
		{
			$sql .= "AND ".$this->table.".city = '" . $city . "'";
		}

		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";

		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".time_end >= '".date('Y-m-d H:i:s', mktime(0,0,0,date('m'),date('d'),date('Y')))."' ";
		$sql .= "AND ".$this->table.".type_id = ".$this->category.".type_id ";

		if($type_id != '')
		{
			$sql .= "AND ".$this->table.".type_id = " . $type_id . ' ';
		}

		if($province != '')
		{
			$sql .= "AND ".$this->table.".province = '" . $province . "'";
		}

		if($city != '')
		{
			$sql .= "AND ".$this->table.".city = '" . $city . "'";
		}

		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}


}
