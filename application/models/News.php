<?php

class News extends CI_Model{

	var $table = 'phuketnews_news';
	var $category = 'phuketnews_news_category';

	function sitemap($page)
	{
		$limit = 500;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function google_sitemap($page)
	{
		$limit = 500;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND category_id = 8 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_member_id($member_id, $status, $page)
	{
		$limit = 20;
		$start = $page * $limit - $limit;
		$q = $this->input->get('q');
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table. ", ".$this->category." ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		$sql .= "AND ".$this->table. ".category_id =  ".$this->category.".category_id ";
		if($q){
			$sql .= "AND title LIKE '%".addslashes($q)."%' ";
		}
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table. " ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND title LIKE '%".addslashes($q)."%' ";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function md5_slug($slug){
		$sql = "SELECT * FROM ".$this->table." WHERE md5(slug) = '$slug' ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function slug($str)
	{
		$text = $str;
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = @iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if(empty($text)) {
			return urlencode($str);
		}

		return $text;
	}

	function insert($arr)
	{
		$arr['entered'] = date('Y-m-d H:i:s');
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete($this->table);
	}

	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_slug($slug)
	{
		$this->db->where('slug', $slug);
		$this->db->where('status',1);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_related($category_id, $id)
	{
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE id < '$id' ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		$sql .= "AND $this->table.category_id = '$category_id' ";
		$sql .= "AND status = 1 ";
		$sql .= "ORDER BY $this->table.id DESC ";
		$sql .= "LIMIT 0, 12 ";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function news_clips($page)
	{
		if($page == false){
			$page = 1;
		}
		$limit = 22;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND video != '' ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND video != '' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_active($page)
	{

		if($page == false){
			$page = 1;
		}
		$limit = 22;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_latest()
	{
		$start = 0;
		$limit = 5;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function get_latest_10()
	{
		$start = 0;
		$limit = 10;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function get_categories($page, $category_ids)
	{
		if($page == false){
			$page = 1;
		}

		$limit = 22;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id in ($category_ids) ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id in ($category_ids) ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;
	}

	function get_category($page, $category_id)
	{
		if($page == false){
			$page = 1;
		}

		$limit = 22;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;
	}

	function get_topstory($category_id)
	{

		$timestamp = time();
		$arr = getdate($timestamp);
		$wday = $arr['wday'];

		$start_date = date('Y-m-d', mktime(0,0,0,date('m'),date('d')-$wday,date('Y')));
		$end_date = date('Y-m-d', mktime(0,0,0,date('m'),date('d')+1,date('Y')));

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE 1 ";
		$sql .= "AND status = '1' ";
		if($category_id){
			$sql .= "AND ".$this->table.".category_id = '$category_id' ";
		}
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND ( entered BETWEEN '".$start_date."' AND '".$end_date."' ) ";
		$sql .= "ORDER BY views DESC ";
		$sql .= "LIMIT 0, 5 ";
		$query = $this->db->query($sql);
		return $query->result();

	}

	function views($id)
	{

		$sql = "UPDATE ".$this->table." ";
		$sql .= "SET views = views + 1 ";
		$sql .= "WHERE id = '$id' ";
		$this->db->query($sql);

	}


	function get_email($email)
	{

		$q = $this->input->get('q');
		$q = addslashes($q);

		$page = $this->input->get('page');
		if($page == 0){ $page = 1; }
		$limit = 12;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE email = '$email' ";
		if($q){
			$sql .= "AND (keywords LIKE '%$q%' OR title LIKE '%$q%' OR description LIKE '%$q%' OR detail LIKE '%$q%') \n";
		}
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE email = '$email' ";
		if($q){
			$sql .= "AND (keywords LIKE '%$q%' OR title LIKE '%$q%' OR description LIKE '%$q%' OR detail LIKE '%$q%') \n";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_email_and_id($email, $id)
	{
		$this->db->where('id', $id);
		$this->db->where('email', $email);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_member_and_id($member_id, $id)
	{
		$this->db->where('id', $id);
		$this->db->where('member_id', $member_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_tag($tag, $page)
	{

		$tag = addslashes($tag);

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id  ";
		$sql .= "AND (hashtags LIKE '%$tag%' OR title LIKE '%$tag%' OR description LIKE '%$tag%' or keywords LIKE '%$tag%')";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND ".$this->table.".category_id = ".$this->category.".category_id ";
		$sql .= "AND (hashtags LIKE '%$tag%' OR title LIKE '%$tag%' OR description LIKE '%$tag%' or keywords LIKE '%$tag%')";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}



}
