<?php
class Eventbrite extends CI_Model{

	var $token = 'HQP3T7BWZNY7TMAD2GH7';
	
	function search($location, $page)
	{
	
		$url = 'https://www.eventbriteapi.com/v3/events/search/?token='.$this->token.'&location.address='.$location.'&page='.$page;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		curl_close($ch);

		$data = json_decode($result,true);
		$contents = $data['events'];
		
		
		foreach($contents as $index=>$value){
						
			$arr['title'] = $value['name']['text'];			
			$arr['description'] = substr($value['description']['text'],0,250);
			if(isset($value['description']['html'])){
				$arr['contentdata'] = $value['description']['html'];
			}
			if(isset($value['logo']['original']['url'])){
				$arr['thumbnail'] = $value['logo']['original']['url'];			
			}
			$arr['guid'] = $value['url'];

			$id = $this->Rss->insert($arr);
			unset($arr);
						
			if($id){
			
				$arr2['title'] = $value['name']['text'];	
				$arr2['member_id'] = 0;
				if(isset($value['category_id'])){
					$arr2['type_id'] = $value['category_id'];
				}

				$arr2['description'] = substr($value['description']['text'],0,250);
				if(isset($value['description']['html'])){
					$arr2['detail'] = $value['description']['html'];
				}
				$arr2['time_start'] = $value['start']['local'];
				$arr2['time_end'] = $value['end']['local'];
				$arr2['entered'] = date('Y-m-d H:i:s');
				$arr2['venue_id'] = $value['venue_id'];
				$arr2['zoom'] = '15';
				$arr2['status'] = 0;
				
				$id = $this->Event->insert($arr2);
				if(isset($value['logo']['original']['url'])){
					$thumbnail = 'uploads/event/phuket-event-'.$id.'.jpg';
					$img = file_get_contents($value['logo']['original']['url']);
					file_put_contents($thumbnail, $img);
				}else{
					$thumbnail = 'assets/images/default-event.png';
				}

				$slug = 'phuket-event-'.sprintf('%06d',$id);
				$this->Event->update($id, array('slug'=>$slug,'thumbnail'=>$thumbnail));
				
				unset($arr);
			
			}
		}   
	}
	
	function venues($venue_id)
	{
		
		$url = 'https://www.eventbriteapi.com/v3/venues/'.$venue_id.'/?token=' . $this->token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		curl_close($ch);
		
		$data = json_decode($result,true);
		$arr['venue_id'] = $data['id'];
		$arr['address_1'] = $data['address']['address_1'];
		$arr['address_2'] = $data['address']['address_2'];
		$arr['city'] = $data['address']['city'];
		$arr['region'] = $data['address']['region'];
		$arr['postal_code'] = $data['address']['postal_code'];
		$arr['country'] = $data['address']['country'];
		$arr['latitude'] = $data['address']['latitude'];
		$arr['longitude'] = $data['address']['longitude'];
		$arr['localized_address_display'] = $data['address']['localized_address_display'];
		$arr['localized_area_display'] = $data['address']['localized_area_display'];
		$arr['localized_multi_line_address_display'] = $data['address']['localized_multi_line_address_display'][0].','.$data['address']['localized_multi_line_address_display'][0];
		$arr['resource_uri'] = $data['resource_uri'];
		$arr['address_name'] = $data['name'];
	
		return $arr;		
	}
}