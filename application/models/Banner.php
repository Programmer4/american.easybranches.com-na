<?php

class Banner extends CI_Model{

	var $table = 'phuketnews_news';
	var $category = 'phuketnews_news_category';

	function get($lat, $lng, $limit)
	{
		$category_id = $this->input->get('category_id');
		$category_name = $this->input->get('category_name');
		if($category_name != '')
		{
			$sql = "SELECT category_id ";
			$sql .= "FROM ".$this->category . " " ;
			$sql .= "WHERE category_slug = '$category_name'";
			$query = $this->db->query($sql);
			$rs = $query->result();

			if(count($rs) > 0)
			{
				$category_id = $rs[0]->category_id;
			}
		}

		$sql = "SELECT *,
			( 3959 * acos( cos( radians('$lat') ) *
			cos( radians( lat ) ) *
			cos( radians( lng ) -
			radians('$lng') ) +
			sin( radians('$lat') ) *
			sin( radians( lat ) ) ) )
			AS distance ";
		$sql .= "FROM ".$this->table.", ".$this->category." ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND $this->table.category_id = $this->category.category_id ";
		if($category_id){
			$sql .= "AND $this->table.category_id = '$category_id' ";
		}
		$sql .= "AND sponsor > 0 ";
		$sql .= "ORDER BY distance ASC, id DESC, sponsor DESC ";
		$sql .= "LIMIT 0, $limit ";

		$query = $this->db->query($sql);
		$rs = $query->result();
		if(count($rs) == 0){

			$sql = "SELECT *,
				( 3959 * acos( cos( radians('$lat') ) *
				cos( radians( lat ) ) *
				cos( radians( lng ) -
				radians('$lng') ) +
				sin( radians('$lat') ) *
				sin( radians( lat ) ) ) )
				AS distance ";
			$sql .= "FROM ".$this->table.", ".$this->category." ";
			$sql .= "WHERE status = 1 ";
			$sql .= "AND $this->table.category_id = $this->category.category_id ";
			// if($category_id){
			// 	$sql .= "AND category_id = '$category_id' ";
			// }
			$sql .= "ORDER BY distance ASC, views DESC ";
			$sql .= "LIMIT 0, $limit ";
			$query = $this->db->query($sql);
			$rs = $query->result();
			return $rs;
		}else{
			return $rs;
		}
	}

	function track($arr)
	{
		$this->db->insert('phuketnews_banner_callback', $arr);
	}

	function get_latest()
	{
		$sql = "SELECT * FROM $this->table,$this->category WHERE status = 1 AND $this->table.category_id = $this->category.category_id AND thumbnail != '' ORDER BY ID DESC LIMIT 0, 5";
		$query = $this->db->query($sql);
		return $query->result();
	}

}
