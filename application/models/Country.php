<?php

class Country extends CI_Model{

	var $table = 'phuketnews_country';

	function get()
	{
		$this->db->order_by('name', 'asc');
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get_code($code)
	{
		$this->db->where('code', $code);
		$query = $this->db->get($this->table);
		return $query->result();
	}

}