<?php
class Paginate extends CI_Model{

	function loadmore($url, $page, $pages)
	{
		$limit = 10;

		$rows = ceil($page/$limit);
		$start = $rows * $limit - $limit;

    $dis = '<div id="pages" class="text-center">';
    $dis .= '<ul class="pagination">';

		if($start-1 > 0){
    	$dis .= '<li><a href="'.$url.'&page='.($start).'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>';
		}

		$i = $start;
		for($i=1; $i<=$limit;$i++ ){

			if($start+$i == $page){
				$style = 'active';
			}else{
				$style = '';
			}

			if($start+$i <= $pages){
				$dis .= '<li class="'.$style.'"><a href="'.$url.'&page='.($start+$i).'">'.($start+$i)."</a></li>";
			}
		}

		if($start+$limit+1 < $pages){
     	$dis .= '<li> <a href="'.$url.'&page='.($start+$limit+1).'" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>';
    }
    $dis .= '</ul>';
    $dis .= '</div>';

		$dis .= '<script>

		$("#pages a").click(function(){
			$("#comments").load(this.href);
			return false;
		});
		</script>';

		return $dis;

	}


	function loadmorestr($url, $page, $pages)
	{
		$limit = 10;

		$rows = ceil($page/$limit);
		$start = $rows * $limit - $limit;

    $dis = '<div id="pages" class="text-center">';
    $dis .= '<ul class="pagination">';

		if($start-1 > 0){
    	$dis .= '<li><a href="'.$url.'&page='.($start).'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>';
		}

		$i = $start;
		for($i=1; $i<=$limit;$i++ ){

			if($start+$i == $page){
				$style = 'active';
			}else{
				$style = '';
			}

			if($start+$i <= $pages){
				$dis .= '<li class="'.$style.'"><a href="'.$url.'&page='.($start+$i).'">'.($start+$i)."</a></li>";
			}
		}

		if($start+$limit+1 < $pages){
     	$dis .= '<li> <a href="'.$url.'&page='.($start+$limit+1).'" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>';
    }
    $dis .= '</ul>';
    $dis .= '</div>';


		return $dis;

	}


	function pages($url, $page, $pages, $query=''){
		$limit = 10;

		$rows = ceil($page/$limit);
		$start = $rows * $limit - $limit;

    $dis = '<div id="pages" class="text-center">';
    $dis .= '<ul class="pagination">';

		if($start-1 > 0){
    	$dis .= '<li><a href="'.$url.'/'.($start).'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a> </li>';
		}

		$i = $start;
		for($i=1; $i<=$limit;$i++ ){

			if($start+$i == $page){
				$style = 'active';
			}else{
				$style = '';
			}

			if($start+$i <= $pages){
				$dis .= '<li class="'.$style.'"><a href="'.$url.'/'.($start+$i).$query.'">'.($start+$i)."</a></li>";
			}
		}

		if($start+$limit+1 < $pages){
     	$dis .= '<li> <a href="'.$url.'/'.($start+$limit+1).$query.'" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a> </li>';
    }
    $dis .= '</ul>';
    $dis .= '</div>';

		return $dis;
	}
 }
