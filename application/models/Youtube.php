<?php

class Youtube extends CI_Model{
	
	var $apikey = 'AIzaSyB1EJJFs5Dc1mh8E08jlpSJwQrXUi_b-so';
	var $maxResults = 50;
	var $hl = 'en_US';
	
	function video($keyword){
		
		$published = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-1, date('Y')));
		$published = str_replace(' ','T',$published);
		
		$url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($keyword).'&key='.$this->apikey.'&maxResults='.$this->maxResults.'&order=viewCount';
		$url .= '&publishedAfter='.$published.'Z';
		$content = file_get_contents($url);
		
		return $content;
		
	}
	
	function channel($channelId){

		$published = date('Y-m-d H:i:s',mktime(0,0,0,date('m'),date('d')-1, date('Y')));
		$published = str_replace(' ','T',$published);

		$url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&key='.$this->apikey.'&maxResults='.$this->maxResults.'&order=viewCount';
		$url .= '&channelId='.$channelId;
		$url .= '&publishedAfter='.$published.'Z';

		$content = file_get_contents($url);
		return $content;
	}

}
