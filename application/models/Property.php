<?php

class Property extends CI_Model{

	var $table = 'phuketnews_property';
	var $location = 'phuketnews_property_location';
	var $photo = 'phuketnews_property_photo';
	var $member = 'phuketnews_member';

	function get_latest($page)
	{
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT ";
		$sql .= $this->table.".id AS id, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".address AS address, ";
		$sql .= $this->table.".city AS city, ";
		$sql .= $this->table.".state AS state, ";
		$sql .= $this->table.".zipcode AS zipcode, ";
		$sql .= $this->table.".type AS type, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".beds AS beds, ";
		$sql .= $this->table.".baths AS baths, ";
		$sql .= $this->table.".parking AS parking, ";
		$sql .= $this->table.".building_size AS building_size, ";
		$sql .= $this->table.".land_size AS land_size, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.'.want_to, ';
		$sql .= $this->table.".thumbnail AS thumbnail, ";
		$sql .= $this->table.".entered AS entered, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".property_price AS property_price, ";
		$sql .= $this->table.".lat AS lat, ";
		$sql .= $this->table.".lng AS lng, ";
		$sql .= $this->location.".title AS location ";
		$sql .= "FROM ".$this->table .", " . $this->location." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		#$sql .= "AND ".$this->member.".member_id = ".$this->table.".member_id ";
		$sql .= "AND ".$this->table .".location_id = " . $this->location.".location_id ";
		$sql .= "ORDER BY rand() ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		return $data;
	}

	function get_nearly($lat, $lng, $limit, $current_property_id)
	{

			$sql = "SELECT *, ";
			$sql .= "( 3959 * acos( cos( radians('$lat') ) *
			cos( radians( lat ) ) *
			cos( radians( lng ) -
			radians('$lng') ) +
			sin( radians('$lat') ) *
			sin( radians( lat ) ) ) )
			AS distance ";
			$sql .= "FROM $this->table ";
			$sql .= "WHERE status = 1 ";
			$sql .= "and id !=" . $current_property_id . "  ";


			$sql .= "ORDER BY distance ";
			$sql .= "LIMIT 0, $limit";

			$query = $this->db->query($sql);
			return $query->result();

	}

	function get_active($page)
	{

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT ";
		$sql .= $this->table.".id AS id, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".address AS address, ";
		$sql .= $this->table.".city AS city, ";
		$sql .= $this->table.".state AS state, ";
		$sql .= $this->table.".zipcode AS zipcode, ";
		$sql .= $this->table.".type AS type, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".beds AS beds, ";
		$sql .= $this->table.".baths AS baths, ";
		$sql .= $this->table.".parking AS parking, ";
		$sql .= $this->table.".building_size AS building_size, ";
		$sql .= $this->table.".land_size AS land_size, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.'.want_to, ';
		$sql .= $this->table.".thumbnail AS thumbnail, ";
		$sql .= $this->table.".entered AS entered, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".property_price AS property_price, ";
		$sql .= $this->table.".lat AS lat, ";
		$sql .= $this->table.".lng AS lng ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_province_active($province, $page)
	{
		$province = urldecode($province);

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT ";
		$sql .= $this->table.".id AS id, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".address AS address, ";
		$sql .= $this->table.".city AS city, ";
		$sql .= $this->table.".state AS state, ";
		$sql .= $this->table.".zipcode AS zipcode, ";
		$sql .= $this->table.".type AS type, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".beds AS beds, ";
		$sql .= $this->table.".baths AS baths, ";
		$sql .= $this->table.".parking AS parking, ";
		$sql .= $this->table.".building_size AS building_size, ";
		$sql .= $this->table.".land_size AS land_size, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.'.want_to, ';
		$sql .= $this->table.".thumbnail AS thumbnail, ";
		$sql .= $this->table.".entered AS entered, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".property_price AS property_price, ";
		$sql .= $this->table.".lat AS lat, ";
		$sql .= $this->table.".lng AS lng ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "AND state = '".$province."' ";
		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";

		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND state = '".$province."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}

	function get_province_city_active($province, $city, $page)
	{
		$province = urldecode($province);
		$city = urldecode($city);

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT ";
		$sql .= $this->table.".id AS id, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".address AS address, ";
		$sql .= $this->table.".city AS city, ";
		$sql .= $this->table.".state AS state, ";
		$sql .= $this->table.".zipcode AS zipcode, ";
		$sql .= $this->table.".type AS type, ";
		$sql .= $this->table.".description AS description, ";
		$sql .= $this->table.".beds AS beds, ";
		$sql .= $this->table.".baths AS baths, ";
		$sql .= $this->table.".parking AS parking, ";
		$sql .= $this->table.".building_size AS building_size, ";
		$sql .= $this->table.".land_size AS land_size, ";
		$sql .= $this->table.".slug AS slug, ";
		$sql .= $this->table.'.want_to, ';
		$sql .= $this->table.".thumbnail AS thumbnail, ";
		$sql .= $this->table.".entered AS entered, ";
		$sql .= $this->table.".title AS title, ";
		$sql .= $this->table.".property_price AS property_price, ";
		$sql .= $this->table.".lat AS lat, ";
		$sql .= $this->table.".lng AS lng ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		$sql .= "AND state = '".$province."' ";
		$sql .= "AND city = '".$city."' ";
		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";

		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND state = '".$province."' ";
		$sql .= "AND city = '".$city."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_search_active()
	{
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}

		$type = $this->input->get('type');
		$province = $this->input->get('province');
		$want_to = $this->input->get('want_to');
		$price_from = $this->input->get('price_from');

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		if($province){
			$sql .= "AND state = '$province' ";
		}
		if($want_to == 'rentout'){
			$sql .= "AND ".$this->table.".want_to = 'rentout' ";
		}else if($want_to == 'sale'){
			$sql .= "AND ".$this->table.".want_to = 'sale' ";
		}
		if($type){
			$sql .= "AND ".$this->table.".type = '$type' ";
		}

		switch($price_from)
		{
			case'less than 2M';
				$sql .= "AND property_price < '2000000' ";
			break;
			case'between 2M and 5M';
				$sql .= "AND property_price BETWEEN '2000000' AND '5000000' ";
			break;
			case'between 5M and 10M';
				$sql .= "AND property_price BETWEEN '5000000' AND '10000000' ";
			break;
			case'between 10M and 20M';
				$sql .= "AND property_price BETWEEN '10000000' AND '20000000' ";
			break;
			case'between 20M and 50M';
				$sql .= "AND property_price BETWEEN '20000000' AND '50000000' ";
			break;
			case'more than 50M';
				$sql .= "AND property_price >= '50000000' ";
			break;
		}

		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";
		if($province){
			$sql .= "AND state = '$province' ";
		}
		if($want_to == 'rentout'){
			$sql .= "AND ".$this->table.".want_to = 'rentout' ";
		}else if($want_to == 'sale'){
			$sql .= "AND ".$this->table.".want_to = 'sale' ";
		}
		if($type){
			$sql .= "AND ".$this->table.".type = '$type' ";
		}

		switch($price_from)
		{
			case'less than 2M';
				$sql .= "AND property_price < '2000000' ";
			break;
			case'between 2M and 5M';
				$sql .= "AND property_price BETWEEN '2000000' AND '5000000' ";
			break;
			case'between 5M and 10M';
				$sql .= "AND property_price BETWEEN '5000000' AND '10000000' ";
			break;
			case'between 10M and 20M';
				$sql .= "AND property_price BETWEEN '10000000' AND '20000000' ";
			break;
			case'between 20M and 50M';
				$sql .= "AND property_price BETWEEN '20000000' AND '50000000' ";
			break;
			case'more than 50M';
				$sql .= "AND property_price >= '50000000' ";
			break;
		}

		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

	function get_province_city_active_type($province, $city, $want_to, $page)
	{
		$province = urldecode($province);
		$city = urldecode($city);

		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND want_to = '".$want_to."' ";
		$sql .= "AND state = '".$province."' ";
		$sql .= "AND city = '".$city."' ";
		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND want_to = '".$want_to."' ";
		$sql .= "AND state = '".$province."' ";
		$sql .= "AND city = '".$city."' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;
	}


	function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_slug($slug)
	{
		$this->db->where('slug',$slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function md5_slug($slug)
	{
		$this->db->where('md5(slug)', $slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function insert($arr)
	{
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}

	function update($id, $arr){
		$this->db->where('id', $id);
		$this->db->update($this->table, $arr);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_location()
	{
		$this->db->order_by('title','asc');
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_location_id($location_id)
	{
		$this->db->where('location_id', $location_id);
		$query = $this->db->get($this->location);
		return $query->result();
	}

	function get_member_id($member_id, $status)
	{
		$q = $this->input->get('q');

		$limit = 21;
		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND ".$this->table.".title LIKE '%$q%' ";
		}
		$sql .= "ORDER BY ".$this->table.".id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE member_id = '$member_id' ";
		$sql .= "AND status = '$status' ";
		if($q){
			$sql .= "AND ".$this->table.".title LIKE '%$q%'";
		}
		$query = $this->db->query($sql);
		$rs = $query->result();

		$data['pages'] = ceil($rs[0]->num/$limit);
		return $data;

	}

	function get_id_by_member($member_id, $id)
	{
		$this->db->where('member_id', $member_id);
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result();
	}


	function clear_photo($id, $num)
	{
		$this->db->where('id',$id);
		$this->db->where('num',$num);
		$this->db->delete($this->photo);
	}

	function insert_photo($arr)
	{
		$this->db->insert($this->photo, $arr);
		return $this->db->insert_id();
	}

	function get_photo($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->photo);
		return $query->result();
	}

	function get_active_by_type($page, $want_to)
	{


		$type = $this->input->get('type');
		$limit = 21;
		$start = $page * $limit - $limit;

		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";

		if($want_to == 'rentout'){
			$sql .= "AND ".$this->table.".want_to = 'rentout' ";
		}else if($want_to == 'sale'){
			$sql .= "AND ".$this->table.".want_to = 'sale' ";
		}

		$sql .= "ORDER BY sponsor, id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();

		$sql = "SELECT COUNT(*) AS num ";
		$sql .= "FROM ".$this->table ." \n";
		$sql .= "WHERE ".$this->table.".status = 1 ";

		if($want_to == 'rentout'){
			$sql .= "AND ".$this->table.".want_to = 'rentout' ";
		}else if($want_to == 'sale'){
			$sql .= "AND ".$this->table.".want_to = 'sale' ";
		}

		$query = $this->db->query($sql);
		$rs = $query->result();
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);

		return $data;

	}

}
