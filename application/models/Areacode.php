<?php

class Areacode extends CI_Model{

	var $table = 'areacode';
	
	public function get()
	{
		$this->db->order_by('country', 'asc');
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	public function get_code($code)
	{
		$this->db->where('code', $code);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
}