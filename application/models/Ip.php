<?php
class Ip extends CI_Model
{
	public function Dot2LongIP ($IPaddr)
	{
		if ($IPaddr == ""){
			return 0;
		} else {
			$ip = explode(".", $IPaddr);
			return ($ip[3] + $ip[2] * 256 + $ip[1] * 256 * 256 + $ip[0] * 256 * 256 * 256);
		}
	}

	public function LookUp($ip_addr)
	{
		$dec = $this->Dot2LongIP($ip_addr);
		$this->db->cache_on();
		$sql = "SELECT * ";
		$sql .= "FROM ip2location_db11 ";
		$sql .= "WHERE '$dec' BETWEEN ip_from AND ip_to ";
		$sql .= "ORDER BY ip_from LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->result();
	}
}
