<?php

class Member extends CI_Model{

	var $table = 'phuketnews_member';
	
	function get_active($page)
	{
	
		$limit = 20;
		$start = $page * $limit - $limit;
	
		$sql = "SELECT * ";
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE 1 ";
		$sql .= "AND firstname != '' ";
		$sql .= "ORDER BY member_id DESC ";
		$sql .= "LIMIT $start, $limit ";
		$query = $this->db->query($sql);
		$data['rows'] = $query->result();
	
		$sql = "SELECT COUNT(*) AS num ";	
		$sql .= "FROM ".$this->table." ";
		$sql .= "WHERE 1 ";
		$sql .= "AND firstname != '' ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		
		$data['items'] = $rs[0]->num;
		$data['pages'] = ceil($rs[0]->num/$limit);
		
		return $data;
	}
	
	function get_slug($slug)
	{
		$this->db->where('slug',$slug);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function insert($arr){
	
		$this->db->insert($this->table, $arr);
		return $this->db->insert_id();
	}
	
	function update($member_id, $arr){
	
		$this->db->where('member_id', $member_id);
		$this->db->update($this->table, $arr);
	}
	
	function get_id($member_id)
	{
		$this->db->where('member_id', $member_id);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get_email($email)
	{
		$this->db->where('email', $email);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get_email_password($email, $password)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	function get_social()
	{
		
		$items['facebook'] = 'facebook';
		$items['twitter'] = 'twitter';
		$items['instagram'] = 'instagram';
		$items['pinterest'] = 'pinterest';
		
		return $items;
	}

}