<?php

class ThaiDate extends CI_Model{

	function convert($strDate, $inclTime = TRUE)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];

		$thaiDate =  "$strDay $strMonthThai $strYear";
		if($inclTime == TRUE)
		{
			$thaiDate =  "$strDay $strMonthThai $strYear, $strHour:$strMinute";
		}
			return $thaiDate;
	}

}
