<?php

class Page_event_api extends CI_Controller{

	function index($location, $page)
	{
	
		$this->Eventbrite->search($location, $page);
		
	}
	
	function venue()
	{
	
		$sql = "SELECT * "; 
		$sql .= "FROM phuketnews_events ";
		$sql .= "WHERE 1 ";
		$sql .= "AND venue_id != '0' ";
		$sql .= "AND lat = '' ";
		$sql .= "AND lng = '' ";
		$sql .= "LIMIT 0, 5 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
	
		foreach($rs as $value){
			$arr = $this->Eventbrite->venues($value->venue_id);
			
			$arr2['address'] = $arr['address_1'];
			$arr2['city'] = $arr['city'];
			$arr2['province'] = $arr['region'];
			$arr2['zipcode'] = $arr['postal_code'];
			$arr2['email'] = 'pana@easybranches.com';
			$arr2['lat'] = $arr['latitude'];
    	$arr2['lng'] = $arr['longitude'];
			$arr2['status'] = 1;
			
			$this->Event->update($value->id, $arr2);
		}		
	}
}