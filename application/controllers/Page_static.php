<?php

class Page_static extends CI_Controller{

	var $module = 'static';
	var $cache = 15;

	public function advertise()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;
		$data['module'] = $this->module;
		$data['topstory_footer'] = $this->News->get_topstory('');
		//$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.advertise.php', $data);

	}
	public function rss()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;
		$data['module'] = $this->module;
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['topstory'] = $data['topstory_footer'];
		///$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.rss.php', $data);

	}


	public function about()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;
		$data['module'] = $this->module;
		$data['topstory_footer'] = $this->News->get_topstory('');
		///$data['event_footer'] = $this->Event->get_latest();

		$this->output->cache($this->cache);
		$this->load->view('page.about.php', $data);
	}

	public function terms()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['meta_title'] = 'ข้อกำหนด และเงื่อนไขของเว็บไซต';
		$data['meta_description'] = 'ข้อกำหนด และเงื่อนไขของเว็บไซต์ เพื่อเป็นการสร้างความเชื่อมั่น และทําความเข้าใจเกี่ยวกับการใช้บริการของบริษัท บริษัทขอเรียนว่าบริษัทมีนโยบายในการคุ้มครองข้อมูลส่วนบุคคลของผู้ใช้บริการทุกท่านโดยสังเขปดังนี้.';

		$data['sess'] = $sess;
		$data['module'] = $this->module;
		$data['topstory_footer'] = $this->News->get_topstory('');
		///$data['event_footer'] = $this->Event->get_latest();

		$this->output->cache($this->cache);
		$this->load->view('page.terms.php', $data);
	}

}
