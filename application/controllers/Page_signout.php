<?php

class Page_signout extends CI_Controller{

	function index()
	{
	
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);	
		$this->Session->delete($session_id);	
		
		header('location:'.base_url().'signin');
		exit;
	
	}

}