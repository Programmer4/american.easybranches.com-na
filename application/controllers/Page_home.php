<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_home extends CI_Controller {

	var $module = 'home';
	var $cache = 60;

	public function index()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		//$business_latest = $this->Business->get_rand(4);
		$topstory = $this->News->get_topstory('');

		$meta_title = 'News | Always up to date with the latest news with Easybranches.com';
		$meta_description = 'America.Easybranches.com keeping you up to date with the latest news from america';
		$meta_keywords = 'america news international breaking newest information';
		if(isset($topstory[0]->thumbnail)){
			$meta_image = $topstory[0]->thumbnail;
			$data['meta_image'] = base_url().$meta_image;
		}

		$data['sess'] = $sess;
		$data['module'] = $this->module;
		$data['meta_title'] = $meta_title;
		$data['meta_description'] = $meta_description;
		$data['meta_keywords'] = $meta_keywords;
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		$data['news'] = $this->News->get_latest();
		$data['topstory'] = $topstory;

		$data['new_york'] = $this->News->get_category(1, 32);
		$data['california'] = $this->News->get_category(1, 6);
		$data['florida'] = $this->News->get_category(1, 10);
		$data['texas'] = $this->News->get_category(1, 42);
		$data['hawaii'] = $this->News->get_category(1, 12);
		$data['washington'] = $this->News->get_category(1, 47);
		$data['virginia'] = $this->News->get_category(1, 45);
		$data['colorado'] = $this->News->get_category(1, 7);
		$data['arizona'] = $this->News->get_category(1, 4);

		$data['google_api'] = $this->Google->api();

		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.home.php', $data);
	}
}
