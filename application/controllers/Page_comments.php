<?php

class Page_comments extends CI_Controller{

	function index()
	{

		$uri = $this->input->get('uri');
		$uri = addslashes($uri);
		$page = $this->input->get('page');

		if($page == false){
			$page = 1;
		}

		$data['comments'] = $this->Comment->get_slug($uri, $page);
		$data['page'] = $page;
		$data['uri'] = $uri;

		$this->load->view('tpl.comment.php', $data);

	}

	function submit()
	{

		if($this->input->post('comment')){


			$uri =  $this->input->post('uri');

			$arr['uri']	 = urldecode($uri);
			$arr['name']	 = $this->input->post('name');
			$arr['email']	 = $this->input->post('email');
			$arr['message']	 = $this->input->post('message');
			$arr['member_id'] = $this->input->post('member_id');

			$id = $this->Comment->insert($arr);

			$email = 'pana@easybranches.com';

			$subject = '[America.Easybranches.com] - New comment comming ';

			$approve = base_url().'comments/approve?id='.md5($id);
			$reject = base_url().'comments/reject?id='.md5($id);

			$message = 'Detail of comment below '."\n";
			$message .= '---------------------------------------------' . "\n";
			$message .= 'URL: ' . base_url().$arr['uri']. "\n";
			$message .= 'Name: ' . $arr['name'] . "\n";
			$message .= 'Email: ' . $arr['email'] . "\n";
			$message .= 'Message: ' . $arr['message'] . "\n";
			$message .= '---------------------------------------------' . "\n";
			$message .= 'Approve: ' . $approve . "\n";
			$message .= 'Reject: ' . $reject . "\n";

			mail($email, $subject, $message);

			echo 'ok';

			exit;
		}
	}

	function approve()
	{
		$id = $this->input->get('id');
		$this->Comment->approve($id);
		echo 'Comment has been approved';

	}

	function reject()
	{

		$id = $this->input->get('id');
		$this->Comment->reject($id);
		echo 'Comment has been removed';
	}


	function form()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(isset($sess[0]->member_id)){
			$member = $this->Member->get_id($sess[0]->member_id);
			$data['member'] = $member;
		}

		$uri = $this->input->get('uri');
		$data['uri'] = $uri;
		$data['sess'] = $sess;

		$this->load->view('tpl.comment.form.php', $data);

	}

}
