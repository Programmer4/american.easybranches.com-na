<?php

class Page_news extends CI_Controller{

	var $module = 'news';
	var $cache = 60;

	function tag_index(){
		$this->tag_index_pages(1);
	}

	function tag_index_pages($page)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['meta_title'] = 'America.Easybranches.com keeping you up to date with the latest news from america';
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['news'] = $this->News->get_active($page);
		$data['topstory_footer'] = $this->News->get_topstory('');
		//$data['event_footer'] = $this->Event->get_latest();
		$data['page'] = $page;
		$data['sess'] = $sess;

		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.news.tag.page.php',$data);

	}

	function editors(){
		$this->editors_pages(1);
	}

	function editors_pages($page)
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$editor = $this->Member->get_active($page);

		$data['meta_title'] = 'America News Editors | America News Online, Phuket Events, Phuket Classifieds';
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['topstory_footer'] = $this->News->get_topstory('');
		//$data['event_footer'] = $this->Event->get_latest();
		$data['sess'] = $sess;

		$data['editor'] = $editor;

		if(isset($editor[0]->member_id)){
			$this->Cachecontrol->output($this->cache);
			$this->load->view('page.news.editors.php',$data);
		}
	}

	function editor($slug)
	{
		$this->editor_pages($slug, 1);
	}

	function editor_pages($slug, $page)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$member = $this->Member->get_slug($slug);
		if(isset($member[0]->member_id)){
			$news = $this->News->get_member_id($member[0]->member_id, 1, $page);

			$data['sess'] = $sess;
			$data['meta_title'] = $member[0]->firstname. ' '.$member[0]->lastname.' - easybranches News';
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$data['news'] = $news;
			$data['member'] = $member;
			$data['page'] = $page;
			$data['slug'] = $slug;

			$this->Cachecontrol->output($this->cache);
			$this->load->view('page.news.editor.php',$data);
		}

	}

	function clips()
	{
		$this->clips_pages(1);
	}

	function clips_pages($page)
	{
		$news = $this->News->news_clips($page);

		$data['meta_title'] = 'คลิปข่าว คลิปกระแส คลิปล่าสุด - easybranches News';
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['news'] = $news;
		$data['topstory'] = $this->News->get_topstory(0);
		$data['page'] = $page;
		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.news.clips.php',$data);
	}

	function approve($slug){
		$rs = $this->News->md5_slug($slug);

		if(isset($rs[0]->id)){
			if($rs[0]->status == 0){

				echo '<h1 align="center">'.$rs[0]->title.' has been approve</h1>';
				echo '<p align="center"><a href="'.base_url().'story/'.$rs[0]->slug.'" target="_blank">View</a>';

				$this->News->update($rs[0]->id, array('status'=>1));

				$subject = '[phuketnews.easybranches.com] - Your story has been approved!';

				$message = 'Hi '.$rs[0]->email.','."\n";
				$message .= "Your story has been approved on America.Easybranches.com. \n";
				$message .= "You can view it online here: \n\n";
				$message .= base_url().'story/'.$rs[0]->slug."\n\n";
				$message .= "Thanks for your high quality submission. Keep up the awesome work! \n\n";
				$message .= "Regards,\n";
				$message .= "Easy Branches Team";

				mail($rs[0]->email, $subject, $message);

			}
		}
	}

	function delete($slug)
	{
		$rs = $this->News->md5_slug($slug);
		if(isset($rs[0]->id)){
			if($rs[0]->status == 0){

				echo '<h1 align="center">'.$rs[0]->title.' has been delete</h1>';

				$this->News->delete($rs[0]->id);

				$subject = '[America.Easybranches.com] - Je verhaal is afgewezen!';

				$message = 'Dear '.$rs[0]->email.','."\n";
				$message .= "Uw verhaal is niet goedgekeurd in America.Easybranches.com \n";
				$message .= "Bedankt voor het indienen van de inhoud. \n\n";
				$message .= "Best regards,\n";
				$message .= "Easy Branches Team";

				mail($rs[0]->email, $subject, $message);

			}
		}
	}

	function index()
	{
		$this->pages(1);
	}

	function pages($page)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;

		$data['module'] = $this->module;
		$data['page'] = $page;

		$news = $this->News->get_active($page);
		$topstory = $this->News->get_topstory(0);

		if($page == 1){
			$data['meta_title'] = 'America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_description'] = 'America.Easybranches.com keeping you up to date with the latest news from america"';
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}else{
			$data['meta_title'] = 'America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_description'] = 'America.Easybranches.com keeping you up to date with the latest news from america - Page '.$page;
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}
		$data['news'] = $news;
		$data['topstory'] = $topstory;

		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.news.all.php',$data);

	}

	function category($slug)
	{
		$this->category_pages($slug, 1);
	}

	function category_pages($slug, $page)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;

		if($page == false){
			$page = 1;
		}

		$category = $this->News_category->get_slug($slug);

		if(isset($category[0]->category_id)){
			$news = $this->News->get_category($page, $category[0]->category_id);

				switch ($category[0]->category_slug) {
					case 'algemeen':
						$news = $this->News->get_categories($page, '17,1,2,3,13');
						break;
					case 'economie':
						$news = $this->News->get_categories($page, '4,18,19,20,21,22');
						break;
					case 'sports':
							$news = $this->News->get_categories($page, '6,23,25,26,27,40,41');
						break;
					case 'technology':
							$news = $this->News->get_categories($page, '12,14,15,28,29,30');
						break;
					case 'entertainment':
							$news = $this->News->get_categories($page, '5,10,16,24,31,32,33,34');
						break;
					case 'lifestyle':
							$news = $this->News->get_categories($page, '7,35,36,37');
						break;
					case 'overig':
							$news = $this->News->get_categories($page, '38,39');
						break;
				}

			$topstory = $this->News->get_topstory('');
			$topstory_footer = $topstory;

			$data['news'] = $news;
			$data['category'] = $category;
			$data['topstory'] = $topstory;
			$data['slug'] = $slug;
			$data['page'] = $page;

			$data['meta_title'] = $category[0]->category_title.' | America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_description'] = 'laatste '.$category[0]->category_title.' news | America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_keywords'] = $category[0]->category_title.', america news international breaking newest information';
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

			$data['topstory_footer'] = $topstory_footer;
			//$data['event_footer'] = $this->Event->get_latest();

			$this->Cachecontrol->output($this->cache);
			$this->load->view('page.news.category.php', $data);

		}else{

			$topstory = $this->News->get_topstory('');
			$topstory_footer = $topstory;

			$data['topstory_footer'] = $topstory_footer;
			//$data['event_footer'] = $this->Event->get_latest();

			$this->load->view('page.404.php', $data);
		}

	}

	function tag($tag)
	{
		$this->tag_pages($tag, 1);
	}

	function tag_pages($tag, $page)
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$tag = urldecode($tag);

		$data['sess'] = $sess;

		if($page == 1){
			$data['meta_title'] = ucfirst($tag) . ' | America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}else{
			$data['meta_title'] = ucfirst($tag) . ' Page '.$page.' | America.Easybranches.com keeping you up to date with the latest news from america';
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}
		$data['news'] = $this->News->get_tag($tag, $page);
		$data['tag'] = $tag;
		$data['page'] = $page;
		$data['topstory_footer'] = $this->News->get_topstory('');
		//$data['event_footer'] = $this->Event->get_latest();

		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.news.tag.php', $data);
	}

function content_news($categorySlug, $slug)
{
	$content = $this->News->get_slug(urldecode($slug));
	if(isset($content[0])){
		$category = $this->News_category->get_id($content[0]->category_id);
		if($category[0]->category_slug == urldecode($categorySlug))
		{
			$this->content($slug);
		}
		else {
			header('location:'.base_url());
		}
	}
	else {
		header('location:'.base_url());
	}
}

function content($slug)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$slug = urldecode($slug);
		$content = $this->News->get_slug($slug);

		$data['sess'] = $sess;
		$data['content'] = $content;

		$data['share'] = $this->Share->get();
		$data['topstory_footer'] = $this->News->get_topstory('');
		//$data['event_footer'] = $this->Event->get_latest();

		if(isset($content[0])){
			$category = $this->News_category->get_id($content[0]->category_id);
			$related = $this->News->get_related($category[0]->category_id, $content[0]->id);
			$topstory = $this->News->get_topstory('');
			$editor = $this->Member->get_id($content[0]->member_id);

			$data['module'] = $this->module;
			$data['related'] = $related;
			$data['topstory'] = $topstory;
			$data['slug'] = $slug;
			$data['editor'] = $editor;

			$data['category'] = $category;
			$data['meta_title'] = $content[0]->title;
			$data['meta_keywords'] = $this->gen_keywords($content[0]->description);
			$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			
			if($content[0]->description){
				$data['meta_description'] = $content[0]->description;
			}

			$data['meta_image'] = base_url().$content[0]->thumbnail;
			//$this->Cachecontrol->output($this->cache);
			$this->Cachecontrol->output(180);
			$this->load->view('page.news.content.php', $data);
		}else{
			header('location:'.base_url());
			exit;
			#$this->load->view('page.404.php', $data);
		}
	}

	function views()
	{
		$id = $this->input->get('id');
		$content = $this->News->get_id($id);
		if(isset($content[0]->id)){
			$this->News->views($content[0]->id);
			echo number_format($content[0]->views);
		}
	}

	function gen_keywords($str){

		$keywords = array();

		$symbol = array("\"","\"","'","-", "..");
		$str = strip_tags($str);
		$str = str_replace($symbol, ' ', $str);
		$data = explode(' ', $str);
		foreach($data as $value){
			if(empty($keywords[$value])){
				$keywords[$value] = $value;
			}
		}

		$str = implode(',',$keywords);
		$str = trim($str);

		return $str;
	}

}
