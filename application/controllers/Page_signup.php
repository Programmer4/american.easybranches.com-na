<?php

class Page_signup extends CI_Controller{

	var $module = 'signup';
	var $approve_email = 'pana@easybranches.com';

	function index()
	{

		$data['module'] = $this->module;

		if($this->input->post('save')){

			$email = $this->input->post('email');
			$rs = $this->Member->get_email($email);

			if(empty($rs[0]->member_id)){

				$arr['email'] = $this->input->post('email');
				$arr['password'] = $this->input->post('password');
				$arr['firstname'] = $this->input->post('firstname');
				$arr['lastname'] = $this->input->post('lastname');
				$arr['entered'] = date('Y-m-d H:i:s');
				$arr['status'] = 0;
				$member_id = $this->Member->insert($arr);

				header('location:'.base_url().'signup/paypal/'.$member_id);
				exit;
			}else{

				header('location:'.base_url().'signup?error=1');
				exit;
			}
		}

		$this->load->view('page.signup.php', $data);
	}

	function complete()
	{
		$this->load->view('page.singup.complete.php');
	}

	public function paypal($id)
	{
		$member = $this->Member->get_id(urldecode($id));
		$data['member'] = $member;
		$this->load->view('page.signup.paypal.php', $data);
	}

	public function paypal_ipn()
	{
		 if($this->input->get('id') && $this->input->post('payment_status') == 'Completed'){
		 	$id = $this->input->get('id');
		 	$member = $this->Member->get_id($id);

		 	$name = $member[0]->firstname . ' ' . $member[0]->lastname;
		 	$email = $member[0]->email;
		 	$arr['status'] = 1;
		 	$arr['transectionId'] = $this->input->post('txn_id');
		 	$expiredDate = new DateTime();
		 	$expiredDate->add(new DateInterval('P1Y'));
		 	$arr['expired'] = $expiredDate->format('Y-m-d H:i:s');
		 	$this->Business->update($id, $arr);

			$content = $member;
			$headers = 'MIME-Version: 1.0'."\n";
			$headers .= 'Content-type: text/html; charset=utf-8'."\n";
			$message = '<table width="650" align="center">';
			$message .= '<tr>';
			$message .= '<td valign="top">Member:</td>';
			$message .= '<td>'.$content[0]->firstname. ' '. $content[0]->firstname .'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Email:</td>';
			$message .= '<td>'.$content[0]->email.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Expired Date:</td>';
			$message .= '<td>'.$expiredDate->format('Y-m-d H:i:s').'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">PayPal TransectionId</td>';
			$message .= '<td>' . $content[0]->transectionId . '</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '</table>';

			mail($this->approve_email, '[America.Easybranches.com] - paid new member submited', $message, $headers);

			$text = '<!doctype html>';
			$text .= '<html>';
			$text .= '<head>';
			$text .= '<meta content="width=device-width,initial-scale=1" name=viewport>';
			$text .= '<meta charset="utf-8"><title>Untitled Document</title>';
			$text .= '</head>';
			$text .= '<body style="background:#f9f9f9;padding-top: 60px;padding-bottom: 60px;">';
			$text .= '<center>';
			$text .= '<a href="http://America.Easybranches.com" target="_blank"><img src="'.base_url().'assets/images/logo-email.png"></a>';
			$text .= '</center>';
			$text .= '<br>';
			$text .= '<div style="background: #ffffff;margin:auto;border-radius:5px;padding-top:15px;padding-left:30px; padding-right:30px;">';
			$text .= '<h1 align="center" style="margin-top:15px;font-size: 16px;font-family: Verdana, \'Microsoft Sans Serif\'"> Lidmaatschap bevestiging </h1>';

			$text .= '<div style="margin-top:15px;font-size: 16px;font-family: Verdana, \'Microsoft Sans Serif\'; align: center">
									Dear  '.$name.'<br><br>
									Your account has been verified.<br><br>
										<b>Email: </b> '.$content[0]->email.' </br>
										<b>Password: </b> '.$content[0]->password.'</br>
									 <a href="'.base_url() .'signin">Sign in</a></td>
								 </div>';
			$text .= '<p>&nbsp;</p>';
			$text .= '</div>';
			$text .= '<p align="center" style="font-size: 11px; color: #999; margin-top: 30px; font-family: Verdana, \'Microsoft Sans Serif\'">2017 Easy Branches Co., Ltd. All rights reserved.</p>';
			$text .= '</body>';
			$text .= '</html>';
			$subject = '[America.Easybranches.com] - Membership confirmation!';

			 mail($email, $subject, $text, $headers);
		}
	}

}
