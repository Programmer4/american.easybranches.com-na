<?php

class Page_news_api extends CI_Controller{

	public function index()
	{
		$api[] = 'newsapi/thelocal_at';
		$api[] = 'newsapi/thelocal_dk';
		$api[] = 'newsapi/thelocal_fr';
		$api[] = 'newsapi/thelocal_de';
		$api[] = 'newsapi/thelocal_it';
		$api[] = 'newsapi/thelocal_se';


		$api[] = 'newsapi/metropole_culture';
		$api[] = 'newsapi/metropole_food_drink';
		$api[] = 'newsapi/metropole_explore_vienna';
		$api[] = 'newsapi/metropole_stories_trends';
		$api[] = 'newsapi/metropole_stories_ideas';
		$api[] = 'newsapi/xpats';
		$api[] = 'newsapi/nytimes_belgium';
		$api[] = 'newsapi/novinite';

		$api[] = 'newsapi/nytimes_bulgaria';
		$api[] = 'newsapi/nytimes_croatia';
		$api[] = 'newsapi/nytimes_cyprus';
		$api[] = 'newsapi/nytimes_czech_republic';
		$api[] = 'newsapi/nytimes_denmark';
		$api[] = 'newsapi/nytimes_estonia';
		$api[] = 'newsapi/nytimes_finland';
		$api[] = 'newsapi/nytimes_france';
		$api[] = 'newsapi/nytimes_germany';
		$api[] = 'newsapi/nytimes_greece';
		$api[] = 'newsapi/nytimes_hungary';
		$api[] = 'newsapi/nytimes_ireland';
		$api[] = 'newsapi/nytimes_italy';
		$api[] = 'newsapi/nytimes_latvia';
		$api[] = 'newsapi/nytimes_lithuania';
		$api[] = 'newsapi/nytimes_luxembourg';
		$api[] = 'newsapi/nytimes_malta';
		$api[] = 'newsapi/nytimes_netherlands';
		$api[] = 'newsapi/nytimes_poland';
		$api[] = 'newsapi/nytimes_portugal';
		$api[] = 'newsapi/nytimes_romania';

		$api[] = 'newsapi/nytimes_slovakia';
		$api[] = 'newsapi/nytimes_slovenia';
		$api[] = 'newsapi/nytimes_spain';
		$api[] = 'newsapi/nytimes_sweden';
		$api[] = 'newsapi/nytimes_united_kingdom';
		$api[] = 'newsapi/croatiaweek';
		$api[] = 'newsapi/total_croatia_news';


		$api[] = 'newsapi/cyprusmail_crime';
		$api[] = 'newsapi/cyprusmail_cyprustalks';
		$api[] = 'newsapi/cyprusmail_education';
		$api[] = 'newsapi/cyprusmail_property';

		 $api[] = 'newsapi/incyprus_local';
		 $api[] = 'newsapi/incyprus_local_business';
		 $api[] = 'newsapi/incyprus_investing_in_cyprus';
		 $api[] = 'newsapi/incyprus_local_sports';

		 $api[] = 'newsapi/praguepost';
		 $api[] = 'newsapi/cphpost';
		 $api[] = 'newsapi/murmur';

		 $api[] = 'newsapi/postimees';
		 $api[] = 'newsapi/balticreports';
		 $api[] = 'newsapi/estonianworld';

		 $api[] = 'newsapi/cyprusmail_britain';
		 $api[] = 'newsapi/deutschland';
		 $api[] = 'newsapi/spiegel';
		 $api[] = 'newsapi/thejournal_germany';
		 $api[] = 'newsapi/ekathimerini';
		 $api[] = 'newsapi/greekreporter';
		 $api[] = 'newsapi/thenationalherald_greece';
		 $api[] = 'newsapi/apokoronasnews';
		 $api[] = 'newsapi/helsinkitimes';
		 $api[] = 'newsapi/finlandtoday';
		 $api[] = 'newsapi/goodnewsfinland';
		 $api[] = 'newsapi/dailyfinland';
		 $api[] = 'newsapi/france24';
		 $api[] = 'newsapi/rfi';
		 $api[] = 'newsapi/economist';
		 $api[] = 'newsapi/dailynewshungary';
		 $api[] = 'newsapi/hungarytoday';
		 $api[] = 'newsapi/politics';
		 $api[] = 'newsapi/budapestbeacon';
		 $api[] = 'newsapi/budapesttimes';
		 $api[] = 'newsapi/independent';
		 $api[] = 'newsapi/irishmirror';
		 $api[] = 'newsapi/breakingnews';

		 $api[] = 'newsapi/abcnews_malta';
		 $api[] = 'newsapi/abcnews_luxembourg';
		 $api[] = 'newsapi/abcnews_netherlands';
		 $api[] = 'newsapi/abcnews_poland';
		 $api[] = 'newsapi/abcnews_portugal';
		 $api[] = 'newsapi/abcnews_romania';
		 $api[] = 'newsapi/abcnews_slovakia';
		 $api[] = 'newsapi/abcnews_slovenia';
		 $api[] = 'newsapi/abcnews_spain';
		 $api[] = 'newsapi/abcnews_sweden';
		 $api[] = 'newsapi/abcnews_united_kingdom';
		 $api[] = 'newsapi/thenews_national';
		 $api[] = 'newsapi/thenews_business';
		 $api[] = 'newsapi/thenews_culture';
		 $api[] = 'newsapi/thenews_sport';
		 $api[] = 'newsapi/krakowpost';
		 $api[] = 'newsapi/politico_economy';
		 $api[] = 'newsapi/politico_defense';
		 $api[] = 'newsapi/politico_energy';
		 $api[] = 'newsapi/politico_healthcare';
		 $api[] = 'newsapi/algarvedailynews';
		 $api[] = 'newsapi/econews';
		 $api[] = 'newsapi/romania_insider';
		 $api[] = 'newsapi/romaniajournal';
		 $api[] = 'newsapi/nineoclock';
		 $api[] = 'newsapi/spectator_sme';
		 $api[] = 'newsapi/economist_britain';
		 $api[] = 'newsapi/rtvslo';
		 $api[] = 'newsapi/theolivepress';
		 $api[] = 'newsapi/typicallyspanish';
		 $api[] = 'newsapi/dutchnews';
		 $api[] = 'newsapi/wort';
		 $api[] = 'newsapi/luxembourgforfinance';
		 $api[] = 'newsapi/timesofmalta';
		 $api[] = 'newsapi/maltaToday';
		 $api[] = 'newsapi/independent_malta';
		 $api[] = 'newsapi/sofiaglobe';
		 $api[] = 'newsapi/football_italia';
		 $api[] = 'newsapi/bolognapress';
		 $api[] = 'newsapi/huffingtonpost';
		 $api[] = 'newsapi/calciomercato';
		 $api[] = 'newsapi/g7italy';
		 $api[] = 'newsapi/bnn_news_latvia';
		 $api[] = 'newsapi/bnn_news_lithuania';
		 $api[] = 'newsapi/bnn_news_estonia';
		 $api[] = 'newsapi/lsm';


		$data['api'] = $api;

		$this->load->view('page.newsapi.php', $data);
	}

}
