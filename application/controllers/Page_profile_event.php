<?php
class Page_profile_event extends CI_Controller{

	var $module = 'events';
	var $meta_title = 'Events';
	var $approve_email = 'pana@easybranches.com';

	public function index()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$page = $this->input->get('page');
		if($page == false){$page = 1;}

		$status = $this->input->get('status');
		if($status == '' || $status == 1){
			$status = '1';
		}else{
			$status = '0';
		}

		$member = $this->Member->get_id($sess[0]->member_id);

		$data['page'] = $page;
		$data['sess'] = $sess;
		$data['meta_title'] = $this->meta_title;
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['event_footer'] = $this->Event->get_latest();
		$data['status'] = $status;
		$data['event'] = $this->Event->get_by_member_id($sess[0]->member_id, $status);
		$data['searchKeyword'] = $this->input->get('q');

		$this->load->view('page.profile.event.php', $data);

	}

	public function preview()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();

		$id = $this->input->get('id');
		$event = $this->Event->get_id_member($sess[0]->member_id, $id);

		if(isset($event[0]->type_id)){
			$category = $this->Event->get_type_id($event[0]->type_id);
			$data['category'] = $category;
		}

		$data['sess'] = $sess;
		$data['google_api'] = $google_api;
		$data['content'] = $event;
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['event_footer'] = $this->Event->get_latest();
		$data['meta_title'] = 'Your Event';

		$this->load->view('page.profile.event.preview.php', $data);

	}

	public function submit()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		if($this->input->post('save')){

			$id = $this->input->post('id');

			$arr['member_id'] = $sess[0]->member_id;
			$arr['type_id'] = $this->input->post('type_id');
			$arr['title'] = $this->input->post('title');
			$arr['address']  = $this->input->post('address');
			$arr['city']  = $this->input->post('city');
			$arr['province']  = $this->input->post('province');
			$arr['zipcode']  = $this->input->post('zipcode');
			$arr['description'] = $this->input->post('description');
			$arr['detail'] = $this->input->post('detail');
			$arr['lat'] = $this->input->post('lat');
			$arr['lng'] = $this->input->post('lng');
			$arr['zoom'] = $this->input->post('zoom');
			$arr['time_start'] = $this->input->post('time_start');
			$arr['time_end'] = $this->input->post('time_end');
			$arr['website'] = $this->input->post('website');
			$arr['free'] = $this->input->post('free');
			$arr['price'] = $this->input->post('price');
			$arr['fax'] = $this->input->post('fax');
			$arr['phone'] = $this->input->post('phone');
			$arr['email'] = $this->input->post('email');
			$arr['geo'] = $this->input->post('geo');
			#$arr['paypal'] = $this->input->post('paypal');
			#$arr['status'] = $this->input->post('status');

			if($id == false){
				$arr['entered'] = date('Y-m-d H:i:s');
				$id = $this->Event->insert($arr);

				if(is_file($_FILES['thumbnail']['tmp_name'])){

					$filename = 'uploads/event/'.$this->News->slug($arr['title']).'-'.$id.'.jpg';
					move_uploaded_file($_FILES['thumbnail']['tmp_name'], $filename);

					$arr2['thumbnail'] = $filename;
					$arr2['slug'] = $this->News->slug($arr['title']).'-'.$id;

					$this->Event->update($id, $arr2);
				}

				$approve = base_url().'event/approve/'.md5($arr2['slug']);
				$delete = base_url().'event/delete/'.md5($arr2['slug']);
				$mapimg = 'https://maps.googleapis.com/maps/api/staticmap?center='.$arr['geo'].'&markers=color:red|'.$arr['lat'].','.$arr['lng'].'&zoom='.$arr['zoom'].'&size=450x200&key='.$google_api;

				# approve email
				$headers = "Content-type:text/html; charset=utf-8\r\n";
				$subject = '[America.Easybranches.com] - New event has been submit';

				$message = '<div style="width:650px; margin:auto;">';
				$message .= '<h3 align="center">New Event Submit</h3>';
				$message .= '<p>';
				$message .= '<img src="'.base_url().$filename.'" style="width:100%;">';
				$message .= '</p>';
				$message .= '<table>';
				$message .= '<tr>';
				$message .= '<td width="100" valign="top" nowrap><strong>Title:</strong> </td>';
				$message .= '<td>'.$arr['title'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Description:</strong> </td>';
				$message .= '<td>'.$arr['description'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Detail:</strong> </td>';
				$message .= '<td>'.$arr['detail'].'</td>';
				$message .= '</tr>';

				if($arr['free'] == 1){
					$message .= '<tr>';
					$message .= '<td valign="top" nowrap><strong>Free event:</strong> </td>';
					$message .= '<td> Yes </td>';
					$message .= '</tr>';
				}else{
					$message .= '<tr>';
					$message .= '<td valign="top" nowrap><strong>Price:</strong> </td>';
					$message .= '<td> '.$arr['price'].' </td>';
					$message .= '</tr>';
				}


				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Address:</strong> </td>';
				$message .= '<td>'.$arr['address'].' '.$arr['city'].' '.$arr['province'].' ' .$arr['zipcode']. ' </td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Map:</strong> </td>';
				$message .= '<td> <img src="'.$mapimg.'"> </td>';
				$message .= '</tr>';


				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Phone:</strong> </td>';
				$message .= '<td> ';
				if($arr['phone']){
					$message .= '<a href="tel:'.$arr['phone'].'">'.$arr['phone']. '</a>';
				}else{
					$message .= ' - ';
				}
				$message .= '</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Fax:</strong> </td>';
				$message .= '<td> ';
				if($arr['fax']){
					$message .= $arr['fax'];
				}else{
					$message .= '-';
				}
				$message .= '</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Website:</strong> </td>';
				$message .= '<td> ';
				if($arr['website']){
					$message .= $arr['website'];
				}else{
					$message .= '-';
				}
				$message .= '</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Email:</strong> </td>';
				$message .= '<td> ';
				if($arr['email']){
					$message .= '<a href="email:'.$arr['email'].'">'.$arr['email']. '</a>';
				}else{
					$message .= ' - ';
				}
				$message .= '</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td valign="top" nowrap><strong>Date:</strong> </td>';
				$message .= '<td> ' . $arr['time_start'] .' - ' . $arr['time_end']. '</td>';
				$message .= '</tr>';

				$message .= '</table>';
				$message .= '<br>';
				$message .= '<center>';
				$message .= 'What you want to do with this event? <br><br>';
				$message .= '<a href="'.$approve.'" style="display:block; padding:15px; background:#64dd17; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Approve</strong> </a>';
				$message .= '<br>';
				$message .= '<br>';
				$message .= '<a href="'.$delete.'" style="display:block; padding:15px; background:#d50000; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Delete</strong> </a>';
				$message .= '</center>';

				$message .= '</div>';

				mail($this->approve_email, $subject, $message, $headers);

				header('location:'.base_url().'account/event/preview?id='. $id);
				exit;

			}else{

				if(isset($_FILES['thumbnail']['tmp_name'])){
					$filename = 'uploads/event/'.$this->News->slug($arr['title']).'-'.$id.'.jpg';
					move_uploaded_file($_FILES['thumbnail']['tmp_name'], $filename);
				}

				$this->Event->update($id, $arr);

				header('location:'.base_url().'account/event/preview?save=1&id='.$id);
				exit;
			}
		}

		$topstory = $this->News->get_topstory('');
		$type = $this->Event->get_category();
		$member = $this->Member->get_id($sess[0]->member_id);
		$id = $this->input->get('id');

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['meta_title'] = $this->meta_title;
		$data['topstory_footer'] = $topstory;
		$data['google_api'] = $google_api;
		$data['type'] = $type;
		$data['event_footer'] = $this->Event->get_latest();
		$data['content'] = $this->Event->get_id_member($member[0]->member_id, $id);
		$data['meta_title'] = 'Your Event';

		$this->load->view('page.profile.event.form.php', $data);

	}

}
