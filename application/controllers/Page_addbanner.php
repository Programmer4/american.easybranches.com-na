<?php
class Page_addbanner extends CI_Controller{

  var $cache = 5;

  public function index()
  {
    $news = $this->Banner->get_latest();
    $this->output->cache($this->cache);

    $data['news'] = $news;

    $this->load->view('page.addbanner.php', $data);
  }
}
