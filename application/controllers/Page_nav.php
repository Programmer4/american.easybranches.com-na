<?php

class Page_nav extends CI_Controller{

	function index()
	{
		echo '<div class="container">
	 <div class="row">
		<div class="col-lg-12 text-right" id="other-sites-container">
			<a href="javascript:void(0);" id="other-sites">Easybranches News</a>
		</div>
		<div class="col-lg-12 text-right" id="all-sites-container">
      <ul id="globalnews" class="list-inline">
        <li class="dropdown"><a href="http://www.worldnews.easybranches.com" target="_blank">Daily World News</a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="http://www.worldnews.easybranches.com/regions" target="_blank">Regions</a></li>
            <li><a href="http://www.worldnews.easybranches.com/travel" target="_blank">Travel</a></li>
            <li><a href="http://www.worldnews.easybranches.com/financial" target="_blank">Financial</a></li>
            <li><a href="http://www.worldnews.easybranches.com/entertainment" target="_blank">Entertainment</a></li>
            <li><a href="http://www.worldnews.easybranches.com/technology" target="_blank">Technology</a></li>
            <li><a href="http://www.worldnews.easybranches.com/sport" target="_blank">Sport</a></li>
            <li><a href="http://www.worldnews.easybranches.com/lifestyle" target="_blank">Lifestyle</a></li>
            <li><a href="http://www.worldnews.easybranches.com/photography" target="_blank">Photos</a></li>
          </ul>
        </li>
        <li><a href="http://www.america.easybranches.com" target="_blank">America News</a></li>
        <li class="dropdown"><a href="http://www.nederland.easybranches.com/" target="_blank">Nederlands Nieuws</a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
            <li><a href="http://www.nederland.easybranches.com/nieuws/algemeen" target="_blank">Algemeen</a></li>
            <li><a href="http://www.nederland.easybranches.com/nieuws/politiek" target="_blank">Politiek</a></li>
            <li><a href="http://www.nederland.easybranches.com/economie" target="_blank">Economie</a></li>
            <li><a href="http://www.nederland.easybranches.com/sports" target="_blank">Sports</a></li>
            <li><a href="http://www.nederland.easybranches.com/technology" target="_blank">Tech</a></li>
            <li><a href="http://www.nederland.easybranches.com/entertainment" target="_blank">Entertainment</a></li>
            <li><a href="http://www.nederland.easybranches.com/lifestyle" target="_blank">Lifestyle</a></li>
            <li><a href="http://www.nederland.easybranches.com/overig" target="_blank">Overig</a></li>
          </ul>
        </li>
        <li><a href="http://www.america.easybranches.com" target="_blank">U.S.A. News</a></li>
        <li><a href="http://www.easybranches.asia" target="_blank">中国新闻</a></li>
        <li><a href="http://www.easybranches.biz" target="_blank">أخبار موقع</a></li>
        <li class="dropdown"><a href="http://www.phuketnews.easybranches.com/" target="_blank">Phuket News</a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu4">
            <li><a href="http://www.phuketnews.easybranches.com/news" target="_blank">News</a></li>
            <li><a href="http://www.phuketnews.easybranches.com/business" target="_blank">Business</a></li>
            <li><a href="http://www.phuketnews.easybranches.com/property" target="_blank">Property</a></li>
            <li><a href="http://www.phuketnews.easybranches.com/event" target="_blank">Events</a></li>
            <li><a href="http://www.phuketnews.easybranches.com/yachts" target="_blank">Yachts</a></li>
            <li><a href="http://www.phuketnews.easybranches.com/classified" target="_blank">Classified</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="http://www.thainews.easybranches.com/" target="_blank">ข่าวไทย</a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu5">
            <li><a href="http://www.thainews.easybranches.com/news" target="_blank">ข่าวล่าสุด</a></li>
            <li><a href="http://www.thainews.easybranches.com/business" target="_blank">ธุรกิจ</a></li>
            <li><a href="http://www.thainews.easybranches.com/property" target="_blank">อสังหาริมทรัพย์</a></li>
            <li><a href="http://www.thainews.easybranches.com/event" target="_blank">กิจกรรม</a></li>
            <li><a href="http://www.thainews.easybranches.com/yachts" target="_blank">เรือยอชท์</a></li>
            <li><a href="http://www.thainews.easybranches.com/classified" target="_blank">อยากซื้อ อยากขาย</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="http://www.football-soccer.easybranches.com" target="_blank">Football</a>
          <ul class="dropdown-menu">
            <li><a href="http://www.football-soccer.easybranches.com/news/premier-peague-news">Premier League News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/la-liga-news">La Liga News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/bundesliga-news">Bundesliga News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/serie-a-news">Serie A News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/ligue-1-news">Ligue 1 News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/eredivisie-news">Eredivisie News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/europa-league-news">Europa League News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/champions-league-news">Champions League News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/efl-championship-news">EFL Championsship News</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/news/clip-football-news">Clip Football News &amp; Highlight</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="http://www.football-soccer.easybranches.com/th" target="_blank">ฟุตบอล</a>
          <ul class="dropdown-menu">
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/ไฮไลท์ฟุตบอล" target="_blank">ไฮไลท์ฟุตบอล</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/พรีเมียร์ลีก อังกฤษ/" target="_blank">พรีเมียร์ลีก อังกฤษ</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/บุนเดสลีกา" target="_blank">บุนเดสลีกา เยอรมัน</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/ลาลีกา" target="_blank">ลาลีกา สเปน</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/กัลโช่เซเรียอา" target="_blank">กัลโช่ เซเรียอา อิตาลี</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/ลีกเอิง ฝรั่งเศส" target="_blank">ลีกเอิง ฝรั่งเศส</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/บอลไทย" target="_blank">ฟุตบอลไทย</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/คลิป" target="_blank">คลิปข่าว</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/ย้าย" target="_blank">ตลาดซื้อ-ขายนักเตะ</a></li>
            <li><a href="http://www.football-soccer.easybranches.com/th/news/tag/ข่าวกีฬาเด็ด" target="_blank">กีฬาเด็ด</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>';
	}

}
