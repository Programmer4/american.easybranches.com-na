<?php

class Page_signin extends CI_Controller{

	function index()
	{

		$session_id = $this->Session->getcode();

		if($this->input->post('save')){

			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$rs = $this->Member->get_email_password($email, $password);

			if(isset($rs[0]->member_id)){

				$arr['member_id'] = $rs[0]->member_id;
				$arr['mysession_id'] = $session_id;
				$arr['remote_addr'] = $_SERVER['REMOTE_ADDR'];
				$arr['entered'] = date('Y-m-d H:i:s');

				$this->Session->insert($arr);

				header('location:'.base_url().'account/profile');
				exit;

			}else{
				header('location:'.base_url().'signin?error=1');
			}
			exit;
		}

		$data['error'] = $this->input->get('error');

		$this->load->view('page.signin.php', $data);
	}

	function forgot()
	{

		if($this->input->post('save')){

			$email = $this->input->post('email');
			$rs = $this->Member->get_email($email);

			if(isset($rs[0]->email)){

				$headers = 'From: no-reply@easybranches.com';
				$subject = '[America.Easybranches.com] - Password benodigd';
				$message = 'Dear '.$rs[0]->firstname . ' '.$rs[0]->lastname.', '."\n\n";
				$message .= 'Email: '.$rs[0]->email." \n";
				$message .= 'Password: '.$rs[0]->password." \n";
				$message .= '----------------------------------------------------'."\n";
				$message .= "U kunt zich aanmelden bij: ".base_url().'signin'."\n\n";
				$message .= "Best regards, \n";
				$message .= "Easy Branches Team. ";

				mail($rs[0]->email, $subject, $message, $headers);

				header('location:'.base_url().'forgot?send=true');
				exit;
			}
		}

		$data['send'] = $this->input->get('send');
		$data['error'] = $this->input->get('error');
		$this->load->view('page.signin.forgot.php', $data);

	}


}
