<?php

class Page_news_sitemap extends CI_Controller{
	
	var $cache = 15;
	
	function index()
	{
		$this->index_pages(1);
	}
	
	function index_pages($page){


		header('Content-type: text/xml');

		$items = $this->News->sitemap($page);
		$data['items'] = $items;
		
		#$this->output->cache($this->cache);
		$this->load->view('sitemap.php', $data);
	}
	
	function google()
	{
		$this->google_pages(1);
	}
	
	function google_pages($page)
	{
		$items = $this->News->google_sitemap($page);
		$data['items'] = $items;
		
		#$this->output->cache($this->cache);
		$this->load->view('sitemap.googlenews.php', $data);
	}

}