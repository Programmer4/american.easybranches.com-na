<?php

class Page_apple extends CI_Controller{

	function movie()
	{
		$content = $this->Apple->movie();
		$content = str_replace('im:','im',$content);
		$xml = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA);
		$json = json_encode($xml);
		$data = json_decode($json,TRUE);
				
		foreach($data['entry'] as $value){

			$id = $this->Apple->movie_get_id($value['id']); 
			
			if($id == false){
				
				$arr['title'] = $value['title'];
				$arr['summary'] = $value['summary'];
				$arr['id'] = $value['id'];
				$arr['link'] = json_encode($value['link']);
				$arr['category'] = json_encode($value['category']);
				$arr['content'] = $value['content'];
				$arr['slug'] = $this->News->slug($value['title']);
				if(isset($value['rights'])){
					$arr['rights'] = $value['rights'];
				}
				$arr['imartist'] = $value['imartist'];
				if(isset($value['imprice'])){
					$arr['imprice'] = $value['imprice'];
				}
				if(isset($value['imrentalPrice'])){
					$arr['imrentalPrice'] = $value['imrentalPrice'];
				}
				$arr['imreleaseDate'] = $value['imreleaseDate'];
				$arr['thumbnail'] = $value['imimage'][2];
				
				$this->Apple->movie_insert($arr);
				unset($arr);
			}
		}
	}
}