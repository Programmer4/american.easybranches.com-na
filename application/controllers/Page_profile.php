<?php

class Page_profile extends CI_Controller{

	var $meta_title = 'Your Account';

	function index()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		if($this->input->post('save')){

			$arr['slug'] = '';
			if($this->input->post('firstname')){
				$arr['slug'] .= $this->input->post('firstname').'-';
			}
			if($this->input->post('lastname')){
				$arr['slug'] .= $this->input->post('lastname').'-';
			}
			$arr['slug'] .= $sess[0]->member_id;
			$arr['slug'] = strtolower($arr['slug']);

			$arr['title'] = $this->input->post('title');
			$arr['firstname'] = $this->input->post('firstname');
			$arr['lastname'] = $this->input->post('lastname');
			$arr['address'] = $this->input->post('address');
			$arr['city'] = $this->input->post('city');
			$arr['state'] = $this->input->post('state');
			$arr['country'] = $this->input->post('country');
			$arr['zipcode'] = $this->input->post('zipcode');
			$arr['email'] = $this->input->post('email');
			$arr['password'] = $this->input->post('password');
			$arr['phone'] = $this->input->post('phone');
			$arr['fax'] = $this->input->post('fax');
			$arr['mobile'] = $this->input->post('mobile');
			$arr['website'] = $this->input->post('website');
			$arr['socialmedia'] = json_encode($this->input->post('social'));

			if($_FILES['thumbnail']['tmp_name']){

				$filename = 'uploads/profile/'.sprintf('%05d',$sess[0]->member_id).'.jpg';
				$arr['thumbnail'] = $filename;

				move_uploaded_file($_FILES['thumbnail']['tmp_name'], $filename);
			}

			$this->Member->update($sess[0]->member_id, $arr);
			header('location:'.base_url().'account/profile?save=1');
			exit;
		}

		$member = $this->Member->get_id($sess[0]->member_id);
		$country = $this->Country->get();

		$data['save'] = $this->input->get('save');
		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['country'] = $country;
		$data['social'] = $this->Member->get_social();
		$data['meta_title'] = 'Your Profile';

		$this->load->view('page.profile.php',$data);

	}

	function dashboard()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$member = $this->Member->get_id($sess[0]->member_id);
		$topstory = $this->News->get_topstory('');

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['topstory_footer'] = $topstory;
		$data['meta_title'] = $this->meta_title;
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.dashboard.php',$data);

	}

	function sign()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

				if(isset($sess[0]->id)){
					echo '<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
					echo 'Account</a>';
					echo '<ul class="dropdown-menu pull-right">';
					echo '<li><a href="'. base_url().'account/dashboard">Dashboard</a></li>';
					echo '<li role="separator" class="divider"></li>';
					echo '<li><a href="'.base_url().'account/profile">Profile</a></li>';
					echo '<li role="separator" class="divider"></li>';
					echo '<li><a href="'.base_url().'signout">Sign Out</a></li>';
					echo '</ul>';
					echo '<script>';
					echo '$("#sign-account").addClass("dropdown");';
		      echo '</script>';
				}else{
					echo '<a href="'.base_url().'signin" class="btn btn-primary">Sign In</a>';
				}
	}

}
