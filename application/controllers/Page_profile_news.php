<?php

class Page_profile_news extends CI_Controller{

	var $meta_title = 'News';
	var $approve_email = 'pana@easybranches.com';

	function index()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$page = $this->input->get('page');
		if($page == false){$page = 1;}

		$status = $this->input->get('status');
		if($status == '' || $status == 1){
			$status = '1';
		}else{
			$status = '0';
		}

		$member = $this->Member->get_id($sess[0]->member_id);
		$news = $this->News->get_member_id($sess[0]->member_id, $status, $page);



		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['news'] = $news;
		$data['meta_title']  = $this->meta_title;
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		$data['page'] = $page;
		$data['status'] = $status;
	$data['searchKeyword'] = $this->input->get('q');

		$this->load->view('page.profile.news.php', $data);
	}

	function preview()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$id = $this->input->get('id');
		$member = $this->Member->get_id($sess[0]->member_id);
		$content = $this->News->get_member_and_id($member[0]->member_id, $id);
		$category = $this->News_category->get_id($content[0]->category_id);
		$topstory = $this->News->get_topstory('');

		$data['meta_title'] = 'Preview: '.$content[0]->title;
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['content'] = $content;
		$data['category'] = $category;
		$data['topstory_footer'] = $topstory;
		//$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.news.preview.php', $data);
	}

	function submit()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$member = $this->Member->get_id($sess[0]->member_id);
		$topstory = $this->News->get_topstory('');

		if($this->input->post('save')){
			$id = $this->input->post('id');

			$arr['title'] = $this->input->post('title');
			$arr['category_id'] = $this->input->post('category_id');
			$arr['keywords'] = $this->input->post('keywords');
			$arr['description'] = $this->input->post('description');
			$arr['hashtags'] = $arr['keywords'];
			$arr['email'] = $member[0]->email;
			$arr['member_id'] = $member[0]->member_id;
			$arr['editor'] = $member[0]->firstname. ' ' .$member[0]->lastname;
			$arr['detail'] = $this->input->post('detail');
			$arr['video'] = $this->input->post('video');

			if($id == false){

				$category = $this->News_category->get_id($arr['category_id']);
				$id = $this->News->insert($arr);

				if(is_file($_FILES['photo']['tmp_name'])){

					@mkdir('uploads/news/'.date('Y'));
					@mkdir('uploads/news/'.date('Y').'/'.date('m'));

					$filename = 'uploads/news/'.date('Y').'/'.date('m').'/'.$this->News->slug($arr['title']).'-'.$id.'.jpg';
					$slug = $arr['title'].'-'.$id;

					move_uploaded_file($_FILES['photo']['tmp_name'], $filename);

					$this->News->update($id, array('thumbnail'=>$filename, 'slug'=>$slug));

				}


				$headers = "Content-type:text/html; charset=utf-8\r\n";
				$subject = '[America.Easybranches.com] - News submited';

				$approve = base_url().'news/approve/'.md5($slug);
				$delete = base_url().'news/delete/'.md5($slug);

				$message = '<div style="width:650px; margin:auto;">';
				$message .= '<h1>'.$arr['title'].'</h1>';
				$message .= '<h2>'.$arr['description'].'</h2>';
				$message .= '<h3>By '.$arr['editor'].' <a href="mailto:'.$arr['email'].'">'.$arr['email'].'</a> in News/'.$category[0]->category_title.'</h3>';
				$message .= '<img src="'.base_url().$filename.'" style="width:100%">';
				if($arr['video']){
					$message .= '<p><a href="'.$arr['video'].'" target="_blank">'.$arr['video'].'</a></p>';
				}
				$message .= $arr['detail'];

				$message .= '<center>';
				$message .= 'What you want to do with this story? <br><br>';
				$message .= '<a href="'.$approve.'" style="display:block; padding:15px; background:#64dd17; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Approve</strong> </a>';
				$message .= '<br>';
				$message .= '<br>';
				$message .= '<a href="'.$delete.'" style="display:block; padding:15px; background:#d50000; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Delete</strong> </a>';
				$message .= '</center>';
				$message .= '</div>';

				mail($this->approve_email, $subject, $message, $headers);

				header('location:'.base_url().'account/news/preview?save=1&id=' . $id);
				exit;

			}else{
				$this->News->update($id, $arr);

				if(is_file($_FILES['photo']['tmp_name'])){

					@mkdir('uploads/news/'.date('Y'));
					@mkdir('uploads/news/'.date('Y').'/'.date('m'));

					$filename = 'uploads/news/'.date('Y').'/'.date('m').'/'.$this->News->slug($arr['title']).'-'.$id.'.jpg';
					$slug = $this->News->slug($arr['title']).'-'.$id;

					move_uploaded_file($_FILES['photo']['tmp_name'], $filename);

					$this->News->update($id, array('thumbnail'=>$filename, 'slug'=>$slug));
				}

				header('location:'.base_url().'account/news/submit?save=1&id=' . $id);
				exit;
			}
		}

		$id = $this->input->get('id');

		$content = $this->News->get_member_and_id($member[0]->member_id, $id);
		$category = $this->News_category->get();

		$data['sess'] = $sess;
		$data['meta_title']  = $this->meta_title;
		$data['meta_url'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$data['member'] = $member;
		$data['category'] = $category;
		$data['content'] = $content;
		$data['save'] = $this->input->get('save');
		$data['topstory_footer'] = $topstory;
		//$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.news.form.php', $data);

	}

}
