<?php
class Page_youtube extends CI_Controller{
	
	var $credit = 'YouTube';
	var $category_id = 10;
	
	function keyword($keyword)
	{
		
		$keyword = urlencode($keyword);
		
		if($category_id == false){
			$category_id = $this->category_id;
		}
				
		$content = $this->Youtube->video($keyword);
		$data = json_decode($content,true);
					
		foreach($data['items'] as $index=>$value){

			$src = $value['snippet']['thumbnails']['high']['url'];

			if(isset($value['id']['videoId'])){

				$arr['title'] = $value['snippet']['title'];
				$arr['description'] = $value['snippet']['description'];
				$arr['thumbnail'] = $src;
				$arr['contentdata'] = '<p>'.$arr['description'].'</p>';
				$arr['guid'] = $value['id']['videoId'];

				$id = $this->Rss->insert($arr);			
			}else{
				$id = false;
			}
							
			if($id != false){
				
				$news['video'] = 'https://www.youtube.com/watch?v='.$arr['guid'];

				$news['category_id'] = $category_id;
				$news['title'] = $arr['title'];
				$news['description'] = strip_tags($arr['description']);
				$news['detail'] = $arr['description'];
				$news['editor'] = $value['snippet']['channelTitle'];
				$news['youtube_channel'] = $value['snippet']['channelId'];
				$credit = array('domain'=>$this->credit,'url'=>$news['video']);
				$news['credit'] = json_encode($credit);
				$news_id = $this->News->insert($news);
				
				$year_path = 'uploads/news/'.date('Y');
				if(is_dir($year_path) == false){
					mkdir($year_path,0777);
				}

				$month_path = $year_path.'/'.date('m');
				if(is_dir($month_path) == false){
					mkdir($month_path,0777);
				}
				
				$filename = $month_path.'/'.$this->News->slug($arr['guid']).'.jpg';
				$content_photo = @file_get_contents($src);
				if($content_photo){
					file_put_contents($filename, $content_photo);
					$filename2 = 'uploads/news/'.date('Y').'/'.date('m').'/'.$this->News->slug($arr['guid']).'.jpg';
					$news_update['thumbnail'] = $filename2;
				}

				$news_update['status'] = 1;
				$news_update['slug'] = '';
				if($arr['title']){
					$news_update['slug'] .= $arr['title'].'-'; 
				}
				$news_update['slug'] .= $news_id;

				$this->News->update($news_id, $news_update);
				unset($news_update);				
			}
		}
	}
	
	function channel($channelId)
	{
		
		$category_id = $this->input->get('category_id');
		
		if($category_id == false){
			$category_id = $this->category_id;
		}
				
		$content = $this->Youtube->channel($channelId);
		$data = json_decode($content,true);
				
		foreach($data['items'] as $index=>$value){
						
			$src = $value['snippet']['thumbnails']['high']['url'];

			if(isset($value['id']['videoId'])){

				$arr['title'] = $value['snippet']['title'];
				$arr['description'] = $value['snippet']['description'];
				$arr['thumbnail'] = $src;
				$arr['contentdata'] = '<p>'.$arr['description'].'</p>';
				$arr['guid'] = $value['id']['videoId'];

				$id = $this->Rss->insert($arr);			
			}else{
				$id = false;
			}
							
			if($id != false){
				
				$news['video'] = 'https://www.youtube.com/watch?v='.$arr['guid'];

				$news['category_id'] = $category_id;
				$news['title'] = $arr['title'];
				$news['description'] = strip_tags($arr['description']);
				$news['detail'] = $arr['description'];
				$news['editor'] = $value['snippet']['channelTitle'];
				$news['youtube_channel'] = $value['snippet']['channelId'];
				$credit = array('domain'=>$this->credit,'url'=>$news['video']);
				$news['credit'] = json_encode($credit);
				$news_id = $this->News->insert($news);
				
				$year_path = 'uploads/news/'.date('Y');
				if(is_dir($year_path) == false){
					mkdir($year_path,0777);
				}

				$month_path = $year_path.'/'.date('m');
				if(is_dir($month_path) == false){
					mkdir($month_path,0777);
				}
				
				//get images 
				$filename = $month_path.'/'.$this->News->slug($arr['guid']).'.jpg';
				$content_photo = @file_get_contents($src);
				if($content_photo){

					file_put_contents($filename, $content_photo);

					$filename2 = 'uploads/news/'.date('Y').'/'.date('m').'/'.$this->News->slug($arr['guid']).'.jpg';
					$news_update['thumbnail'] = $filename2;
					
				}

				$slug = $arr['title']. '-'.$news_id;
				$slug = str_replace('/','',$slug);

				$news_update['status'] = 1;
				$news_update['slug'] = $slug;

				$this->News->update($news_id, $news_update);
				unset($news_update);				
						
			}
		}
	}	
}