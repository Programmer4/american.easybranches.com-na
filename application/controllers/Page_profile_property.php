<?php

class Page_profile_property extends CI_Controller{

	var $module = 'property';
	var $meta_title = 'Property';
	var $approve_email = 'pana@easybranches.com';
	var $paypal = 'advertising@easybranches.com';

	function index()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$page = $this->input->get('page');
		if($page == false){$page = 1;}
		$status = $this->input->get('status');
		if($status == '' || $status == 1){
			$status = 1;
		}else{
			$status = 0;
		}
		$property = $this->Property->get_member_id($sess[0]->member_id, $status);

		$data['page'] = $page;
		$data['sess'] = $sess;
		$data['status'] = $status;
		$data['searchKeyword'] = $this->input->get('q');
		$data['property'] = $property;
		$data['meta_title'] = $this->meta_title;
		$topstory = $this->News->get_topstory('');

		$this->load->view('page.profile.property.php', $data);

	}

	function upgrade()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$member = $this->Member->get_id($sess[0]->member_id);

		$id = $this->input->get('id');

		$data['module'] = $this->module;
		$data['paypal'] = $this->paypal;
		$data['content'] = $this->Property->get_id($id);
		$data['member'] = $member;

		$this->load->view('page.profile.property.upgrade.php', $data);
	}

	function preview()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$id = $this->input->get('id');
		$content = $this->Property->get_id_by_member($sess[0]->member_id, $id);

		$google_api = $this->Google->api();
		$data['sess'] = $sess;
		$data['content'] = $content;
		$data['country'] = $this->Country->get();
		$data['location'] = $this->Property->get_location();
		$data['google_api'] = $google_api;
		$data['photo'] = $this->Property->get_photo($id);
		$data['meta_title'] = 'Property - '. $content[0]->title;

		$topstory = $this->News->get_topstory('');
		$data['topstory_footer'] = $topstory;
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.property.preview.php', $data);

	}

	function submit()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		if($this->input->post('save')){

			$id = $this->input->post('id');

			$arr['member_id'] = $sess[0]->member_id;
			$arr['title'] = $this->input->post('title');
			$arr['type'] = $this->input->post('type');
			$arr['want_to'] = $this->input->post('want_to');
			$arr['address'] = $this->input->post('address');
			$arr['city'] = $this->input->post('city');
			$arr['state'] = $this->input->post('state');
			$arr['zipcode'] = $this->input->post('zipcode');
			$arr['country'] = $this->input->post('country');
			$arr['website'] = $this->input->post('website');
			$arr['phone'] = $this->input->post('phone');
			$arr['fax'] = $this->input->post('fax');
			$arr['email'] = $this->input->post('email');
			$arr['location_id'] = $this->input->post('location_id');
			$arr['property_price'] = $this->input->post('property_price');
			$arr['land_size'] = $this->input->post('land_size');
			$arr['ownership'] = $this->input->post('ownership');
			$arr['view'] = $this->input->post('view');
			$arr['video'] = $this->input->post('video');
			$arr['building_size'] = $this->input->post('building_size');
			$arr['beds'] = $this->input->post('beds');
			$arr['baths'] = $this->input->post('baths');
			$arr['parking'] = $this->input->post('parking');
			$arr['description'] = $this->input->post('description');
			$arr['detail'] = $this->input->post('detail');
			$arr['keywords'] = $this->input->post('keywords');
			//$arr['hashtags'] = $this->input->post('hashtags');
			$arr['lat'] = $this->input->post('lat');
			$arr['lng'] = $this->input->post('lng');
			$arr['geo'] = $this->input->post('geo_location');
			$arr['geo_zoom'] = $this->input->post('geo_location_zoom');
			$arr['note'] = $this->input->post('note');

			if($id == false){

				$arr['entered'] = date('Y-m-d H:i:s');

				$id = $this->Property->insert($arr);
				$arr_sign = array('/');
				$slug = str_replace($arr_sign, '', $arr['title']).'-'.$id;

				$this->Property->update($id, array('slug'=>$slug));

				$path = 'uploads/property/'.sprintf('%07d',$id);
				if(is_dir($path) == false){
					mkdir($path, 0777);
				}

				if(is_file($_FILES['thumbnail']['tmp_name'])){

					$filename = $path.'/thumbnail.jpg';
					move_uploaded_file($_FILES['thumbnail']['tmp_name'], $filename);

					$this->Property->update($id, array('thumbnail'=>$filename));
				}

				if(is_file($_FILES['floorplan']['tmp_name'])){

					$filename = $path.'/floorplan.jpg';
					move_uploaded_file($_FILES['floorplan']['tmp_name'], $filename);

					$this->Property->update($id, array('floorplan'=>$filename));
				}

				for($i=0; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){

						$filename = $path.'/'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $filename);

						$this->Property->insert_photo(array('id'=>$id, 'num'=>$i, 'filepath'=>$path.'/'.$i.'.jpg'));
					}
				}

				// sendmail

				$approve = base_url().'property/approve/'.md5($slug);
				$delete = base_url().'property/delete/'.md5($slug);

				$mapimg = 'https://maps.googleapis.com/maps/api/staticmap?center='.$arr['geo'].'&markers=color:red|'.$arr['lat'].','.$arr['lng'].'&zoom='.$arr['geo_zoom'].'&size=450x200&key='.$google_api;

				$headers = "Content-type:text/html; charset=utf-8\r\n";
				$subject = '[America.Easybranches.com] - New property has been submit';
				$message = '<div style="width:650px; margin:auto;">';

				$message .= '<h3 align="center">New Property Submit</h3>';

				$message .= '<table>';
				$message .= '<tr>';
				$message .= '<td nowrap><strong>Name of property:</strong> </td>';
				$message .= '<td>'.$arr['title'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Beds:</strong> </td>';
				$message .= '<td>'.$arr['beds'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Baths:</strong> </td>';
				$message .= '<td>'.$arr['baths'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Parking:</strong> </td>';
				$message .= '<td>'.$arr['parking'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Price:</strong> </td>';
				$message .= '<td>'.$arr['property_price'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Want to:</strong> </td>';
				$message .= '<td>'.$arr['want_to'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Description:</strong> </td>';
				$message .= '<td>'.$arr['description'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Detail:</strong> </td>';
				$message .= '<td>'.$arr['detail'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Map:</strong></td>';
				$message .= '<td>';
				$message .= '<img src="'.$mapimg.'">';
				$message .= '</td>';
				$message .= '</tr>';


				$message .= '<tr>';
				$message .= '<td nowrap><strong>Address:</strong> </td>';
				$message .= '<td>'.$arr['address'].' '.$arr['city']. ' '.$arr['state'] . ' '.$arr['country']. ' ' . $arr['zipcode'] . '</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Email:</strong> </td>';
				$message .= '<td><a href="mailto:'.$arr['email'].'">'.$arr['email'].'</a></td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Phone:</strong> </td>';
				$message .= '<td>'.$arr['phone'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td nowrap><strong>Fax:</strong> </td>';
				$message .= '<td>'.$arr['fax'].'</td>';
				$message .= '</tr>';

				$message .= '</table>';

				$message .= '<br>';

				$message .= '<center>';
				$message .= 'What you want to do with this property? <br><br>';
				$message .= '<a href="'.$approve.'" style="display:block; padding:15px; background:#64dd17; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Approve</strong> </a>';
				$message .= '<br>';
				$message .= '<br>';
				$message .= '<a href="'.$delete.'" style="display:block; padding:15px; background:#d50000; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Delete</strong> </a>';
				$message .= '</center>';
				$message .= '</div>';

				mail($this->approve_email, $subject, $message, $headers);

				if($this->input->post('price') > 0){
					header('location:'.base_url().'account/property/upgrade?id=' . $id);
					exit;
				}else{
					header('location:'.base_url().'account/property/preview?id=' . $id);
					exit;
				}

			}else{
				$this->Property->update($id, $arr);
				$slug = $this->News->slug($arr['title'].'-'.$id);

				$path = 'uploads/property/'.$slug;
				if(is_dir($path) == false){
					mkdir($path, 0777);
				}

				if(is_file($_FILES['thumbnail']['tmp_name'])){
					$filename = $path.'/floorplan.jpg';
					move_uploaded_file($_FILES['thumbnail']['tmp_name'], $filename);
					$this->Property->update($id, array('thumbnail'=>$filename));
				}

				if(is_file($_FILES['floorplan']['tmp_name'])){
					$filename = $path.'/floorplan.jpg';
					move_uploaded_file($_FILES['floorplan']['tmp_name'], $filename);
					$this->Property->update($id, array('floorplan'=>$filename));
				}

				for($i=0; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){

						$filename = $path.'/'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $filename);

						$photo = array('num'=>$i, 'filepath'=>$path.'/'.$i.'.jpg');
						$this->Property->insert_photo($photo);
					}
				}
			}

			header('location:'.base_url().'account/property/submit?id=' . $id.'&save=1');
			exit;
		}

		$id = $this->input->get('id');
		$country = $this->Country->get();
		$content = $this->Property->get_id_by_member($sess[0]->member_id, $id);
		$member = $this->Member->get_id($sess[0]->member_id);

		$data['sess'] = $sess;
		$data['save'] = $this->input->get('save');
		$data['google_api'] = $google_api;
		$data['meta_title'] = $this->meta_title;
		$data['social'] = $this->Member->get_social();
		$data['country'] = $country;
		$data['content'] = $content;
		$data['member'] = $member;
		$data['location'] = $this->Property->get_location();

		$topstory = $this->News->get_topstory('');
		$data['topstory_footer'] = $topstory;
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.property.form.php', $data);

	}

}
