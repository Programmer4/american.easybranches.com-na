<?php

class Page_search extends CI_Controller{

	var $module = 'search';
	var $cache = 15;

	public function index()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$topstory = $this->News->get_topstory('');

		//$data['event_footer'] = $this->Event->get_latest();
		$data['topstory'] = $topstory;
		$data['topstory_footer'] = $topstory;

		#$this->output->cache($this->cache);
		$this->load->view('page.search.php', $data);
	}

}
