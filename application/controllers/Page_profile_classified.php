<?php

class Page_profile_classified extends CI_Controller{
	
	var $approve_email = 'pana@easybranches.com';

	public function index()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		
		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$page = $this->input->get('page');
		if($page == false){
			$page = 1;
		}
		
		$status = $this->input->get('status');
		if($status == ''){
			$status = 1;
		}
		
		$data['page'] = $page;
		$data['classified'] = $this->Classified->get_member($sess[0]->member_id, $status);
		$data['category'] = $this->Classified->get_category_array();
		$data['sess'] = $sess;
		$data['status'] = $status;
		$data['searchKeyword'] = $this->input->get('q');
		
		$this->load->view('page.profile.classified.php', $data);
	}

	public function get_child()
	{
		$parent_id = $this->input->get('parent_id');
		$category = $this->Classified->get_category_list($parent_id);
		$child_id = $this->input->get('child_id');
		echo '<option value=""> - Select - </option>';
		foreach($category as $index=>$value){
			if($child_id == $value->category_id){
				$select = ' selected';
			}else{
				$select = '';
			}
			echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
		}
	}

	public function submit()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();
		
		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		if($this->input->post('save')){
			
			$id = $this->input->post('id');
			
			$arr['category_id'] = $this->input->post('category_id');
			$arr['child_id'] = $this->input->post('child_id');
			$arr['title'] = $this->input->post('title');
			$arr['description'] = $this->input->post('description');
			$arr['detail'] = $this->input->post('detail');
			$arr['member_id'] = $sess[0]->member_id;
			$arr['conditions'] = $this->input->post('conditions');
			$arr['email'] = $this->input->post('email');
			$arr['phone'] = $this->input->post('phone');
			$arr['price'] = trim($this->input->post('price'));
			$arr['lat'] = $this->input->post('lat');
			$arr['lng'] = $this->input->post('lng');			
			$arr['geo_location'] = $this->input->post('geo_location');
			$arr['geo_location_zoom'] = $this->input->post('geo_location_zoom');
			$arr['contact'] = $this->input->post('contact');
			$arr['address'] = $this->input->post('address');
			$arr['city'] = $this->input->post('city');
			$arr['location_id'] = $this->input->post('location_id');
			$arr['zipcode'] = $this->input->post('zipcode');
			$arr['note'] = $this->input->post('note');
			
			if($id == false){
				$arr['entered'] = date('Y-m-d H:i:s');
				$id = $this->Classified->insert($arr);
				
				$slug = $arr['title']. '-'.$id;
				
				$pathdir = 'uploads/classified/'.sprintf('%05d',$id);
				mkdir($pathdir,0777);
				
				if(is_file($_FILES['photo']['tmp_name'][0])){
					$filename = $pathdir.'/thumbnail.jpg';
					move_uploaded_file($_FILES['photo']['tmp_name'][0], $filename);
					$arr2 = array('slug'=>$slug, 'thumbnail'=>$filename);
				}
				
				$this->Classified->update($id, $arr2);
				
				for($i=1; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){
												
						$photo =  $pathdir.'/photo-'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $photo);
						
						$arr1['id'] = $id;
						$arr1['num'] = $i;
						$arr1['filepath'] = $photo;
						
						$this->Classified->insert_photo($arr1);
					}
				}
				
				# send mail to approve
				$location = $this->Classified->get_location_id($arr['location_id']);
				
				$headers = "Content-type:text/html; charset=utf-8\r\n";
				$subject = '[America.Easybranches.com] - Classified submited';
				
				$approve = base_url().'classified/approve/'.md5($slug);
				$delete = base_url().'classified/delete/'.md5($slug);
				
				$mapimg = 'https://maps.googleapis.com/maps/api/staticmap?center='.$arr['geo_location'].'&markers=color:red|'.$arr['lat'].','.$arr['lng'].'&zoom='.$arr['geo_location_zoom'].'&size=450x200&key='.$google_api;
				
				$message = '<div style="width:650px; margin:auto;">';
				$message .= '<h3 align="center">New classfidied Submit</h3>';
				$message .= '<table>';
				$message .= '<tr>';
				$message .= '<td colspan="2" nowrap><img src="'.base_url().$filename.'"> </td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Name of item:</strong> </td>';
				$message .= '<td>'.$arr['title'].'</td>';
				$message .= '</tr>';
				
				$message .= '<tr>';
				$message .= '<td nowrap><strong>Address:</strong> </td>';
				$message .= '<td>'.$arr['address'].' '.$arr['city']. ' '.$location[0]->title . ' ' . $arr['zipcode'] . '</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Email:</strong> </td>';
				$message .= '<td><a href="mailto:'.$arr['email'].'">'.$arr['email'].'</a></td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Phone:</strong> </td>';
				$message .= '<td>'.$arr['phone'].'</td>';
				$message .= '</tr>';
				
				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Short description of item:</strong> </td>';
				$message .= '<td>'.$arr['description'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Detail of item:</strong> </td>';
				$message .= '<td>'.$arr['detail'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Map: </strong></td>';
				$message .= '<td><a href="https://www.google.co.th/maps/@'.$arr['geo_location'].','.$arr['geo_location_zoom'].'z?hl=en" target="_blank"><img src="'.$mapimg.'"></a></td>';
				$message .= '</tr>';

				$message .= '</table>';
				
				$message .= '<br>';
				
				$message .= '<center>';
				$message .= 'What you want to do with this item? <br><br>';
				$message .= '<a href="'.$approve.'" style="display:block; padding:15px; background:#64dd17; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Approve</strong> </a>';
				$message .= '<br>';
				$message .= '<br>';
				$message .= '<a href="'.$delete.'" style="display:block; padding:15px; background:#d50000; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Delete</strong> </a>';
				$message .= '</center>';				
				$message .= '</div>';
				
				mail($this->approve_email, $subject, $message, $headers);
				
			}else{
				
				$arr['slug'] = $arr['title'].'-'.$id;

				$this->Classified->update($id, $arr);
				
				if(is_file($_FILES['photo']['tmp_name'][0])){
					$filename = $pathdir.'/thumbnail.jpg';
					move_uploaded_file($_FILES['photo']['tmp_name'][0], $filename);
					$arr2['thumbnail'] = $filename;
				}
				
				for($i=1; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){
						
						$this->Classified->clear_photo($id,$i);
												
						$photo =  $pathdir.'/photo-'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $photo);
						
						$arr1['id'] = $id;
						$arr1['num'] = $i;
						$arr1['filepath'] = $photo;
						
						$this->Classified->insert_photo($arr1);
					}
				}
			}
			
			header('location: '.base_url().'account/classified/preview?id='.$id.'&save=1');
			exit;
		}
		
		if($this->input->post('delete')){
			$id = $this->input->post('id');
			$this->Classified->delete($id);			
		}

		$id = $this->input->get('id');
		$category = $this->Classified->get_category_list(0);
		$member = $this->Member->get_id($sess[0]->member_id);

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['province'] = $this->Classified->get_th_province_list();
		$data['content'] = $this->Classified->get_id_by_member($id, $sess[0]->member_id);
		$data['category'] = $category;
		$data['google_api'] = $google_api;
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['event_footer'] = $this->Event->get_latest();		

		$this->load->view('page.profile.classified.form.php', $data);
	
	}
	
	function preview()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		
		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$id = $this->input->get('id');
		$content = $this->Classified->get_id_by_member($id, $sess[0]->member_id);
		$photo = $this->Classified->get_photo($content[0]->id);
		
		#echo '<pre>';
		#print_r($content);
		#echo '</pre>';

		$data['sess'] = $sess;
		$data['content'] = $content;
		$data['photo'] = $photo;
		$data['parent_category'] = $this->Classified->get_category_id($content[0]->category_id);
		$data['province'] = $this->Classified->get_location_id($content[0]->location_id);
		$data['child_category'] = $this->Classified->get_category_id($content[0]->child_id);
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.classified.preview.php', $data);
	
	}

}