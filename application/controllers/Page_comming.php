<?php

class Page_comming extends CI_Controller{

	var $module = 'comming';
	var $cache = 15;

	function index()
	{
		$topstory = $this->News->get_topstory('');
		
		$data['topstory'] = $topstory;
		
		$this->Cachecontrol->output($this->cache);
		$this->load->view('page.comming.php', $data);
	}

}