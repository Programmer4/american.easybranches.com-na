<?php

class Page_contribute extends CI_Controller{

	var $module = 'contribute';
	var $approve_email = 'pana@easybranches.com';

	public function index()
	{

		$data['module'] = $this->module;
		$this->load->view('page.contribute.php', $data);

	}

	public function post()
	{

		if($this->input->post('save')){

			$id = $this->input->post('id');

			$arr['category_id'] = $this->input->post('category_id');
			$arr['title'] = $this->input->post('title');
			$arr['keywords'] = $this->input->post('keywords');
			$arr['description'] = $this->input->post('description');
			$arr['detail'] = $this->input->post('detail');
			$arr['editor'] = $this->input->post('contact');
			$arr['company'] = $this->input->post('company');
			$arr['email'] = $this->input->post('email');
			$arr['lat'] = $this->input->post('lat');
			$arr['lng'] = $this->input->post('lng');
			$arr['zoom'] = $this->input->post('zoom');

			if($id == false){

				$id = $this->News->insert($arr);

				if(isset($_FILES['photo']['tmp_name'])){

					$year_path = 'uploads/news/'.date('Y');
					if(is_dir($year_path) == false){
						@mkdir($year_path,0777);
					}
					$month_path = $year_path.'/'.date('m');
					if(is_dir($month_path) == false){
						@mkdir($month_path,0777);
					}

					$filename = $month_path.'/'.$id.'.jpg';

					move_uploaded_file($_FILES['photo']['tmp_name'], $filename);
					$arr2['thumbnail'] = $filename;
				}

				$slug = str_replace('/','-',$arr['title']).'-'.$id;
				$arr2['slug'] = $slug;
				$this->News->update($id, $arr2);

				header('location:'.base_url().'contribute/preview/'.md5($slug));
				exit;
			}else{

				if(isset($_FILES['photo']['tmp_name'])){

					$year_path = 'uploads/news/'.date('Y');
					if(is_dir($year_path) == false){
						@mkdir($year_path,0777);
					}
					$month_path = $year_path.'/'.date('m');
					if(is_dir($month_path) == false){
						@mkdir($month_path,0777);
					}

					$filename = $month_path.'/'.$id.'.jpg';

					move_uploaded_file($_FILES['photo']['tmp_name'], $filename);
					$arr['thumbnail'] = $filename;
				}

				$this->News->update($id, $arr);

				$s = $this->input->get('s');
				header('location:'.base_url().'contribute/preview/'.$s);
				exit;

			}
		}

		$data['category'] = $this->News_category->get();

		$s = $this->input->get('s');
		$content = $this->News->md5_slug($s);

		if(isset($content[0]->id)){

			$data['category_id'] = $content[0]->category_id;
			$data['title'] = $content[0]->title;
			$data['description'] = $content[0]->description;
			$data['keywords'] = $content[0]->keywords;
			$data['detail'] = $content[0]->detail;

			$data['editor'] = $content[0]->editor;
			$data['email'] = $content[0]->email;
			$data['company'] = $content[0]->company;
			$data['editor'] = $content[0]->editor;
			$data['lat'] = $content[0]->lat;
			$data['lng'] = $content[0]->lng;
			$data['zoom'] = $content[0]->zoom;
			$data['id'] = $content[0]->id;
		}else{
			$data['lat'] = 49.763800;
			$data['lng'] = 11.562581;
			$data['zoom'] = 4;
		}

		$data['module'] = $this->module;
		$this->load->view('page.contribute.post.php', $data);

	}

	public function preview($slug)
	{

		if($this->input->post('save')){


			$content = $this->News->md5_slug($slug);

			$headers = "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: no-reply@easybranches.com" . "\r\n";
			$headers .= "Reply-to: " . $email . "\r\n";
			$subject = '[America.Easybranches.com] - Payment Notice for promote your content';

			$url = base_url().'contribute/paypal?id='.$content[0]->id;

			$text = '<!doctype html>';
			$text .= '<html>';
			$text .= '<head>';
			$text .= '<meta content="width=device-width,initial-scale=1" name=viewport>';
			$text .= '<meta charset="utf-8"><title>Untitled Document</title>';
			$text .= '</head>';
			$text .= '<body style="background:#f9f9f9;padding-top: 60px;padding-bottom: 60px;">';
			$text .= '<center>';
			$text .= '<a href="http://america.easybranches.com" target="_blank"><img src="'.base_url().'assets/images/logo-email.png"></a>';
			$text .= '</center>';
			$text .= '<br>';
			$text .= '<div style="background: #ffffff;margin:auto;border-radius:5px;padding-top:15px;padding-left:30px; padding-right:30px;">';
			$text .= '<h1 align="center" style="margin-top:15px;font-size: 16px;font-family: Verdana, \'Microsoft Sans Serif\'"> Please pay for promoting your content</h1>';
			$text .= '<table width="100%" align="center">';
			$text .= '<tr>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Title: </td>';
			$text .= '<td  style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$content[0]->title.'</td>';
			$text .= '</tr>';
			$text .= '<tr>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Description: </td>';
			$text .= '<td  style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$content[0]->description.'</td>';
			$text .= '</tr>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Email:</td>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'"><a href="mailto:'.$content[0]->email.'">'.$content[0]->email.'</a></td>';
			$text .= '</tr>';
			$text .= '<tr>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Preview: </td>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'"><a href="'.base_url().'contribute/preview/'.$slug.'">click</a></td>';
			$text .= '</tr>';
			$text .= '<tr>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Payment Link: </td>';
			$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'"><a href="'.$url.'">'. $url .'</a></td>';
			$text .= '</tr>';
			$text .= '<tr>';

			$text .= '</table>';
			$text .= '<p>&nbsp;</p>';
			$text .= '</div>';
			$text .= '<p align="center" style="font-size: 11px; color: #999; margin-top: 30px; font-family: Verdana, \'Microsoft Sans Serif\'">2017 Easy Branches Co., Ltd. All rights reserved.</p>';
			$text .= '</body>';
			$text .= '</html>';

			mail($content[0]->email, $subject, $text, $headers);

			$id = $this->input->post('id');
			header('location:'.base_url().'contribute/paypal?id='.$id);
			exit;
		}

				$data['content'] = $this->News->md5_slug($slug);
				$data['module'] = $this->module;


		$this->load->view('page.contribute.preview.php', $data);

	}

	public function paypal()
	{
		$id = $this->input->get('id');
		$content = $this->News->get_id($id);

		$data['content'] = $content;

		$this->load->view('page.contribute.paypal.php', $data);
	}

	public function paypal_ipn()
	{
		if($this->input->get('id') && $this->input->post('payment_status') == 'Completed'){

			$id = $this->input->get('id');
			$arr['sponsor'] = 10;
			$arr['status'] = '0';
			$this->News->update($id, $arr);

			$content = $this->News->get_id($id);

			$headers = 'MIME-Version: 1.0'."\n";
			$headers .= 'Content-type: text/html; charset=utf-8'."\n";

			$approve = base_url().'news/approve/'.md5($content[0]->slug);
			$reject = base_url().'news/reject/'.md5($content[0]->slug);

			$message = '<table width="650" align="center">';
			$message .= '<tr>';
			$message .= '<td valign="top">Title:</td>';
			$message .= '<td>'.$content[0]->title.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Description:</td>';
			$message .= '<td>'.$content[0]->description.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Photo:</td>';
			$message .= '<td><img src="'.base_url().$content[0]->thumbnail.'" style="width:100%"></td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Detail:</td>';
			$message .= '<td>'.$content[0]->detail.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Contact:</td>';
			$message .= '<td>'.$content[0]->editor.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Email:</td>';
			$message .= '<td>'.$content[0]->email.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Company:</td>';
			$message .= '<td>'.$content[0]->company.'</td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Approve:</td>';
			$message .= '<td><a href="'.$approve.'">'.$approve.'</a></td>';
			$message .= '</tr>';
			$message .= '<tr>';
			$message .= '<td valign="top">Reject:</td>';
			$message .= '<td><a href="'.$reject.'">'.$reject.'</a></td>';
			$message .= '</tr>';
			$message .= '</table>';
			mail($this->approve_email, '[America.Easybranches.com] - paid contribute post submited', $message, $headers);

		}
	}

	public function complete()
	{

		$data['module'] = $this->module;
		$this->load->view('page.contribute.thankyou.php', $data);

	}

}
