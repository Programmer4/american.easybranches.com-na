<?php

class Page_email extends CI_Controller{
	
	var $contactus = 'pana@easybranches.com';

	function contactus(){
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);		
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('email');
		$message = $this->input->post('message');
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL) && $name && $phone && $message){

			$headers = "From:".$email. "\r\n";
			$subject = '[America.Easybranches.com] - Business owner contact';

			$text = 'Name: '.$name."\n";
			$text .= 'Phone: '.$phone."\n";
			$text .= 'Email: '.$email."\n";
			$text .= 'Message: '.$message;
			
			mail($this->contactus, $subject, $text, $headers);
			echo 'ok';
			exit;
		}
	}

	function send()
	{
		
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);		
		
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		
		if(filter_var($email, FILTER_VALIDATE_EMAIL) && $subject && $message){
			$member = $this->Member->get_id($sess[0]->member_id);		
			if(isset($member[0]->email)){
				$headers = "From: " .$member[0]->email. "\r\n";
				$headers .= "Reply-to: " . $member[0]->email . "\r\n";
			}else{
				$headers = "From: no-replay@easybranches.com". "\r\n";
			}

			$text = $message;
											
			mail($email, $subject, $text, $headers);
			echo 'ok';
			exit;
		}
	}

	function share()
	{
		$data['url'] = $this->input->get('u');
		$this->load->view('page.email.php', $data);	
	}

}