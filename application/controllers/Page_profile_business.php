<?php

class Page_profile_business extends CI_Controller{

	var $meta_title = 'Business';
	var $approve_email = 'pana@easybranches.com';
	var $paypal = 'advertising@easybranches.com';

	function index()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$page = $this->input->get('page');
		if($page == false){$page = 1;}
		$status = $this->input->get('status');
		if($status == ''){
			$status = 1;
		}
		$member = $this->Member->get_id($sess[0]->member_id);
		$business = $this->Business->get_member_id($member[0]->member_id, $status);

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['business'] = $business;
		$data['meta_title']  = $this->meta_title;
		$data['page'] = $page;
		$data['status'] = $status;
		$data['searchKeyword'] = $this->input->get('q');
		
		$topstory = $this->News->get_topstory('');
		$data['topstory_footer'] = $topstory;
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.business.php', $data);

	}

	function preview()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$id = $this->input->get('id');
		$member = $this->Member->get_id($sess[0]->member_id);
		$content = $this->Business->get_business_id_by_member($member[0]->member_id, $id);

		if(empty($content[0]->id)){
			exit;
		}

		$category = $this->Business->get_category_id($content[0]->category_id);
		$country = $this->Country->get_code($content[0]->country);
		$google_api = $this->Google->api();
		$photo = $this->Business->get_photo($content[0]->id);
		$topstory = $this->News->get_topstory('');

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['content'] = $content;
		$data['category'] = $category;
		$data['country'] = $country;
		$data['google_api'] = $google_api;
		$data['photo'] = $photo;
		$data['social'] = $this->Member->get_social();
		$data['topstory_footer'] = $topstory;
		$data['event_footer'] = $this->Event->get_latest();

		$this->load->view('page.profile.business.preview.php', $data);
	}

	function upgrade()
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		$id = $this->input->get('id');
		$member = $this->Member->get_id($sess[0]->member_id);
		$content = $this->Business->get_business_id_by_member($member[0]->member_id, $id);

		$data['content'] = $content;
		$data['sess'] = $sess;
		$data['paypal'] = $this->paypal;
		$data['member'] = $member;
		$topstory = $this->News->get_topstory('');
		$data['topstory_footer'] = $topstory;


		$this->load->view('page.profile.business.upgrade.php', $data);
	}

	function submit()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$google_api = $this->Google->api();

		if(empty($sess[0]->id)){
			header('location:'.base_url().'signin');
			exit;
		}

		if($this->input->post('save')){

			$arr['member_id'] = $sess[0]->member_id;
			$arr['title'] = $this->input->post('title');
			$arr['category_id'] = $this->input->post('category_id');
			$arr['description'] = $this->input->post('description');
			$arr['detail'] = $this->input->post('detail');
			$arr['address'] = $this->input->post('address');
			$arr['city'] = $this->input->post('city');
			$arr['state'] = $this->input->post('state');
			$arr['country'] = $this->input->post('country');
			$arr['zipcode'] = $this->input->post('zipcode');
			$arr['lat'] = $this->input->post('lat');
			$arr['lng'] = $this->input->post('lng');
			$arr['meta'] = json_encode($this->input->post('meta'));
			$arr['geo'] = $this->input->post('geo_location');
			$arr['zoom'] = $this->input->post('geo_location_zoom');
			$arr['location'] = $this->input->post('location');
			$arr['hours'] = $this->input->post('hours');
			$arr['socailmedia'] = json_encode($this->input->post('socailmedia'));
			$arr['phone'] = $this->input->post('phone');
			$arr['fax'] = $this->input->post('fax');
			$arr['website'] = $this->input->post('website');
			$arr['email'] = $this->input->post('email');
			$arr['keywords'] = $this->input->post('keywords');
			#$arr['hashtags'] = $this->input->post('hashtags');
			$arr['hours'] = $this->input->post('hours');
			$arr['entered'] = date('Y-m-d H:i:s');

			$id = $this->input->post('id');

			if($id == false){
				$id = $this->Business->insert($arr);

				$slug = $this->Business->slug($arr['title']).'-'.$id;
				$this->Business->update($id, array('slug'=>$slug));

				$pathdir = 'uploads/business/'.$slug;
				@mkdir($pathdir);

				//move_uploaded_file();

				$headers = "Content-type:text/html; charset=utf-8\r\n";
				$subject = '[America.Easybranches.com] - Business submited';

				$approve = base_url().'business/approve/'.md5($slug);
				$delete = base_url().'business/delete/'.md5($slug);

				$mapimg = 'https://maps.googleapis.com/maps/api/staticmap?center='.$arr['geo'].'&markers=color:red|'.$arr['lat'].','.$arr['lng'].'&zoom='.$arr['zoom'].'&size=450x200&key='.$google_api;

				$message = '<div style="width:650px; margin:auto;">';
				$message .= '<h3 align="center">New Business Submit</h3>';
				$message .= '<table>';
				$message .= '<tr>';
				$message .= '<td nowrap><strong>Name of business:</strong> </td>';
				$message .= '<td>'.$arr['title'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Address:</strong> </td>';
				$message .= '<td>'.$arr['address'].' '.$arr['city']. ' '.$arr['state'] . ' '.$arr['country']. ' ' . $arr['zipcode'] . '</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td> </td>';
				$message .= '<td>'.$arr['location'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Email:</strong> </td>';
				$message .= '<td><a href="mailto:'.$arr['email'].'">'.$arr['email'].'</a></td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Phone:</strong> </td>';
				$message .= '<td>'.$arr['phone'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap><strong>Fax:</strong> </td>';
				$message .= '<td>'.$arr['fax'].'</td>';
				$message .= '</tr>';
				$message .= '<tr>';
				$message .= '<td><strong>Website:</strong> </td>';
				$message .= '<td><a href="'.$arr['website'].'" target="_blank">'.$arr['website'].'</a></td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Short description of business:</strong> </td>';
				$message .= '<td>'.$arr['description'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Detail of business:</strong> </td>';
				$message .= '<td>'.$arr['detail'].'</td>';
				$message .= '</tr>';


				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Hours of business: </strong></td>';
				$message .= '<td>'.$arr['hours'].'</td>';
				$message .= '</tr>';

				$message .= '<tr>';
				$message .= '<td nowrap valign="top"><strong>Map: </strong></td>';
				$message .= '<td><a href="https://www.google.co.th/maps/@'.$arr['geo'].','.$arr['zoom'].'z?hl=en" target="_blank"><img src="'.$mapimg.'"></a></td>';
				$message .= '</tr>';

				$message .= '</table>';

				$message .= '<br>';

				$message .= '<center>';
				$message .= 'What you want to do with this business? <br><br>';
				$message .= '<a href="'.$approve.'" style="display:block; padding:15px; background:#64dd17; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Approve</strong> </a>';
				$message .= '<br>';
				$message .= '<br>';
				$message .= '<a href="'.$delete.'" style="display:block; padding:15px; background:#d50000; color:white; width:250px;border-radius: 5px;text-decoration: none;"> <strong>Delete</strong> </a>';
				$message .= '</center>';
				$message .= '</div>';

				mail($this->approve_email, $subject, $message, $headers);

				if(is_file($_FILES['photo']['tmp_name'][0])){
					$thumbnail =  $pathdir.'/thumbnail.jpg';
					move_uploaded_file($_FILES['photo']['tmp_name'][0], $thumbnail);
					$this->Business->update($id, array('thumbnail'=>$thumbnail));
				}

				for($i=1; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){

						$this->Business->clear_photo($id, $i);

						$photo =  $pathdir.'/photo-'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $photo);

						$arr1['id'] = $id;
						$arr1['num'] = $i;
						$arr1['filepath'] = $photo;

						$this->Business->insert_photo($arr1);
					}
				}

				if($this->input->post('price') > 0){
					header('location: '.base_url().'account/business/upgrade?id=' . $id);
					exit;
				}else{
					header('location: '.base_url().'account/business/preview?id=' . $id);
					exit;
				}
			}else{

				$this->Business->update($id, $arr);

				$slug = $this->Business->slug($arr['title']).'-'.$id;

				$pathdir = 'uploads/business/'.$slug;
				@mkdir($pathdir);

				if(is_file($_FILES['photo']['tmp_name'][0])){
					$thumbnail =  $pathdir.'/thumbnail.jpg';
					move_uploaded_file($_FILES['photo']['tmp_name'][0], $thumbnail);
					$this->Business->update($id, array('thumbnail'=>$thumbnail));
				}

				for($i=1; $i<=9; $i++){
					if(is_file($_FILES['photo']['tmp_name'][$i])){

						unset($arr1);

						$this->Business->clear_photo($id, $i);

						$photo =  $pathdir.'/photo-'.$i.'.jpg';
						move_uploaded_file($_FILES['photo']['tmp_name'][$i], $photo);

						$arr1['id'] = $id;
						$arr1['num'] = $i;
						$arr1['filepath'] = $photo;

						$this->Business->insert_photo($arr1);
					}
				}


				header('location: '.base_url().'account/business/submit?save=1&id='. $id );
				exit;

			}

		}

		$id = $this->input->get('id');
		$category = $this->Business->get_category_list();
		$member = $this->Member->get_id($sess[0]->member_id);
		$country = $this->Country->get();
		$social = $this->Member->get_social();
		$content = $this->Business->get_business_id_by_member($member[0]->member_id, $id);

		$topstory = $this->News->get_topstory('');
		$data['topstory_footer'] = $topstory;
		$data['event_footer'] = $this->Event->get_latest();

		$data['sess'] = $sess;
		$data['member'] = $member;
		$data['category'] = $category;
		$data['country'] = $country;
		$data['social'] = $social;
		$data['google_api'] = $google_api;
		$data['meta_title']  = $this->meta_title;
		$data['save'] = $this->input->get('save');
		$data['content'] = $content;

		$this->load->view('page.profile.business.form.php', $data);

	}

}
