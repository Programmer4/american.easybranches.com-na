<?php

class Page_account extends CI_Controller{

	var $module = 'account';

	function index()
	{
	
		$session_id = $this->Session->getcode();
		$sess = $this->Sess->sess($session_id);
	
		$data['module'] = $this->module;
		$data['sess'] = $sess;
	
		$this->load->view('page.account.php', $data);
	
	}

}