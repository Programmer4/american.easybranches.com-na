<?php

class Page_property extends CI_Controller{

	var $cache = 15;

	function rss()
	{

		$page = 1;
		$property = $this->Property->get_active($page);

		$n = 0;
		foreach($property['rows'] as $value){

			$item[$n]['title'] = $value->title;
			$item[$n]['link'] = base_url().'property/'.$value->slug;
			$item[$n]['description'] = substr(strip_tags($value->description),0,255);
			$item[$n]['category'] = ucfirst($value->want_to);
			$item[$n]['thumbnail'] = base_url().$value->thumbnail;
			$item[$n]['updated'] = $value->entered;
			$n++;
		}
		$data['item'] = $item;

		$this->output->cache($this->cache);
		$this->load->view('rss.php',$data);
	}

	function map()
	{
		$this->map_pages(1);
	}

	function map_pages($page)
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$topstory = $this->News->get_topstory('');

		$data['sess'] = $sess;
		$data['topstory_footer'] = $topstory;
		$data['property_location'] = $this->Property->get_location();
		$data['property'] = $this->Property->get_active($page);
		$data['page'] = $page;
		$data['event_footer'] = $this->Event->get_latest();
		$data['want_to'] = 'sale';
		$data['breadcrumb'] = 'sale';
		$data['google_api'] = $this->Google->api();

		$this->output->cache($this->cache);
		$this->load->view('page.property.map.php', $data);
	}

	function search()
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$page = $this->input->get('page');
		if($page == false){ $page = 1;}

		$want_to = $this->input->get('want_to');
		$price_from = $this->input->get('price_from');
		$geo = $this->input->get('geo');
		$type = $this->input->get('type');

		$data['sess'] = $sess;
		$data['meta_title'] = 'Phuket News Local properties, land rent sale lease';
		$data['meta_desctiption'] = 'Phuket News Property - Phuket\'s properties daily Your business sharing  in social media, hotels, resorts, restaurants, condo\'s, appartments, villa\'s';
		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['property_location'] = $this->Property->get_location();
		$data['event_footer'] = $this->Event->get_latest();
		$data['property'] = $this->Property->get_search_active($page);
		$data['page'] = $page;
		$data['want_to'] = urldecode($want_to);
		$data['price_from'] = urldecode($price_from);
		$data['geo'] = urldecode($geo);
		$data['type'] = urldecode($type);
		$data['google_api'] = $this->Google->api();

		$this->load->view('page.property.search.php', $data);

	}

	function sale()
	{
		$this->sale_pages(1);
	}

	function sale_pages($page)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$topstory = $this->News->get_topstory('');

		$data['sess'] = $sess;
		$data['topstory_footer'] = $topstory;
		$data['property_location'] = $this->Property->get_location();
		$data['property'] = $this->Property->get_sale_active($page);
		$data['page'] = $page;
		$data['event_footer'] = $this->Event->get_latest();
		$data['want_to'] = 'sale';
		$data['breadcrumb'] = 'sale';
		$data['google_api'] = $this->Google->api();

		$this->output->cache($this->cache);
		$this->load->view('page.property.sale.php', $data);
	}

	function rental()
	{
		$this->rental_pages(1);
	}

	function rental_pages($page)
	{
		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		$topstory = $this->News->get_topstory('');

		$data['sess'] = $sess;
		$data['topstory_footer'] = $topstory;
		$data['property_location'] = $this->Property->get_location();
		$data['property'] = $this->Property->get_rental_active($page);
		$data['page'] = $page;
		$data['event_footer'] = $this->Event->get_latest();
		$data['want_to'] = 'rental';
		$data['breadcrumb'] = 'Rental';
		$data['google_api'] = $this->Google->api();

		$this->output->cache($this->cache);
		$this->load->view('page.property.php', $data);

	}


	function index(){

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);

		$data['sess'] = $sess;
		$data['meta_title'] = 'ซื้อขาย บ้าน บ้านเดี่ยว คอนโด ที่ดิน ทาวน์เฮ้าส์ อพาร์ทเม้นท์';
		$data['meta_description'] = 'รวมประกาศซื้อขายบ้านมือสอง คอนโด ที่ดิน ทาวน์เฮ้าส์-ทาวน์โฮม อพาร์ทเม้นท์ให้เช่า และโครงการบ้านใหม่ คอนโดใหม่ พร้อมรีวิวโครงการบ้านจัดสรร หลากทำเล หลายรายการ ครบทุกความต้องการของคุณ';

		$data['google_api'] = $this->Google->api();

		//$this->output->cache($this->cache);
		$this->load->view('page.property.php', $data);

	}


	function approve($slug)
	{

		$rs = $this->Property->md5_slug($slug);

		echo '<pre>';
		echo $slug.'<br>';
		print_r($rs);
		echo '</pre>';

		if(isset($rs[0]->id)){
			if($rs[0]->status == 0){

				echo '<h1 align="center">'.$rs[0]->title.' has been approve</h1>';
				echo '<p align="center"><a href="'.base_url().'property/'.urlencode($rs[0]->slug).'" target="_blank">View</a>';

				$this->Property->update($rs[0]->id, array('status'=>1));

				$subject = '[America.Easybranches.com] - Your property has been approved!';

				$message = 'Hi '.$rs[0]->email.','."\n";
				$message .= "Your property has been approved on America.Easybranches.com. \n";
				$message .= "You can view it online here: \n\n";
				$message .= base_url().'property/'.$rs[0]->slug."\n\n";
				$message .= "Thanks for your high quality submission. Keep up the awesome work! \n\n";
				$message .= "Regards,\n";
				$message .= "Easy Branches Team";

				mail($rs[0]->email, $subject, $message);

			}
		}
	}

	function printout()
	{

		$id = $this->input->get('id');
		$content = $this->Property->get_id($id);
		if(isset($content)){

			$photo = $this->Property->get_photo($content[0]->id);
			$location = $this->Property->get_location_id($content[0]->location_id);
			$agent = $this->Member->get_id($content[0]->member_id);

			$data['content'] = $content;
			$data['photo'] = $photo;
			$data['location'] = $location;
			$data['agent'] = $agent;

			$this->output->cache($this->cache);
			$this->load->view('page.property.print.php', $data);
		}
	}

	function contact(){

		$id = $this->input->post('id');
		$content = $this->Property->get_id($id);

		if(isset($content[0]->id)){

			$email = $this->input->post('email');
			$name = $this->input->post('name');
			$areacode = $this->input->post('areacode');
			$phone = $this->input->post('phone');
			$message = $this->input->post('message');

			if(filter_var($email, FILTER_VALIDATE_EMAIL) && $name && $areacode && $phone){

				$headers = "Content-type: text/html; charset=UTF-8" . "\r\n";
				$headers .= "From: no-reply@easybranches.com" . "\r\n";
				$headers .= "Reply-to: " . $email . "\r\n";
				$subject = '[America.Easybranches.com] - New Register Interest';

				$url = base_url().'property/'.$content[0]->slug;

				if($content[0]->note){
					$note = $content[0]->note;
				}else{
					$note = '-';
				}

				$text = '<!doctype html>';
				$text .= '<html>';
				$text .= '<head>';
				$text .= '<meta charset="utf-8"><title>Untitled Document</title>';
				$text .= '</head>';
				$text .= '<body style="background:#f9f9f9;padding-top: 60px;padding-bottom: 60px;">';
				$text .= '<center>';
				$text .= '<a href="http://America.Easybranches.com" target="_blank"><img src="'.base_url().'assets/images/logo-email.png"></a>';
				$text .= '</center>';
				$text .= '<br>';
				$text .= '<div style="width:600px; background: #ffffff;margin:auto;border-radius:5px;padding-top:15px;padding-left:30px; padding-right:30px;">';
				$text .= '<h1 align="center" style="margin-top:15px;font-size: 16px;font-family: Verdana, \'Microsoft Sans Serif\'"> New Register Interest </h1>';
				$text .= '<table width="100%" align="center">';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Property: </td>';
				$text .= '<td  style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$content[0]->title.'</td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">URL: </td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'"><a href="'.$url.'">'. $url .'</a></td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Contact person: </td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$name.'</td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Email:</td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'"><a href="mailto:'.$email.'">'.$email.'</a></td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Phone:</td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$areacode.' '.$phone.'</td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Message: </td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$message.'</td>';
				$text .= '</tr>';
				$text .= '<tr>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">Note: </td>';
				$text .= '<td style="font-family: Verdana, \'Microsoft Sans Serif\'">'.$note.'</td>';
				$text .= '</tr>';
				$text .= '</table>';
				$text .= '<p>&nbsp;</p>';
				$text .= '</div>';
				$text .= '<p align="center" style="font-size: 11px; color: #999; margin-top: 30px; font-family: Verdana, \'Microsoft Sans Serif\'">2017 Easy Branches Co., Ltd. All rights reserved.</p>';
				$text .= '</body>';
				$text .= '</html>';

				mail($content[0]->email, $subject, $text, $headers);
				echo 'ok';
				exit;
			}
		}
	}

	function delete($slug)
	{

		$rs = $this->Property->md5_slug($slug);

		if(isset($rs[0]->id)){
			if($rs[0]->status == 0){

				echo '<h1 align="center">'.$rs[0]->title.' has been delete</h1>';

				$this->Property->delete($rs[0]->id);

				$subject = '[America.Easybranches.com] - Your property has been reject!';

				$message = 'Hi '.$rs[0]->email.','."\n";
				$message .= "Your property has been reject on America.Easybranches.com. \n";
				$message .= "Thanks for your submission. \n\n";
				$message .= "Regards,\n";
				$message .= "Easy Branches Team";

				mail($rs[0]->email, $subject, $message);

			}
		}
	}

	function info($slug)
	{

		$session_id = $this->Session->getcode();
		$sess = $this->Session->sess($session_id);
		if(isset($sess[0]->member_id)){
			$member = $this->Member->get_id($sess[0]->member_id);
			$data['member'] = $member;
		}

		if(isset($member[0]->member_id)){
			$member_areacode = $this->Areacode->get_code($member[0]->country);
			$data['member_areacode'] = $member_areacode;
		}

		$content = $this->Property->get_slug(urldecode($slug));

		$data['topstory_footer'] = $this->News->get_topstory('');
		$data['topstory'] = $data['topstory_footer'];
		$data['event_footer'] = $this->Event->get_latest();

		if(isset($content[0]->id)){
			$google_api = $this->Google->api();
			$location = $this->Property->get_location_id($content[0]->location_id);
			$areacode = $this->Areacode->get();
			$agent = $this->Member->get_id($content[0]->member_id);
			$property_location = $this->Property->get_location();

			$data['content'] = $content;
			$data['sess'] = $sess;
			$data['agent'] = $agent;
			$data['photo'] = $this->Property->get_photo($content[0]->id);
			$data['google_api'] = $google_api;
			$data['meta_title'] = $content[0]->title;
			$data['meta_description'] = $content[0]->description.' in the phuket news real estate property section';
			$data['meta_image'] = base_url().'resize/?image=/'.$content[0]->thumbnail.'&width=600&height=315';
			$data['areacode'] = $areacode;
			$data['property_location'] = $property_location;

			$this->Cachecontrol->output($this->cache);
			$this->load->view('page.property.info.php', $data);

		}else{
			$this->load->view('page.404.php', $data);
		}
	}

}
