<?php

class Page_banner extends CI_Controller{

	function index()
	{
		// find user location
		$ip_addr = $_SERVER['REMOTE_ADDR'];
		if(strpos($_SERVER['DOCUMENT_ROOT'],':')){
			$ip_addr = '58.10.129.218';
		}

		$user = $this->Ip->LookUp($ip_addr);

		// get banner
		$banner = $this->Banner->get($user[0]->latitude, $user[0]->longitude, 2);
		$data['banner']	= $banner;
		$this->load->view('page.banner.php',$data);

	}

	function callback($account)
	{
		$banner = $this->Banner->get($account);

		if(isset($banner['url'])){

			$arr['ip'] = $_SERVER['REMOTE_ADDR'];
			$arr['url'] = $banner['url'];

			$this->Banner->track($arr);

			header('location:'.$arr['url'].'?ref=easybranches.com');
			exit;
		}

	}

}
