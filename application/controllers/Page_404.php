<?php

class Page_404 extends CI_Controller{
	
	var $module = 404;
	var $cache = 15;

	function index(){
		
		
		header('location:'.base_url());
		exit;
		
		$session_id = $this->Session->getcode();		
		$sess = $this->Session->sess($session_id);
		$topstory = $this->News->get_topstory('');
		
		$data['module'] = $this->module;
		$data['sess'] = $sess;

		$this->Cachecontrol->output($this->cache);		
		$this->load->view('page.404.php', $data);
		
	}

}