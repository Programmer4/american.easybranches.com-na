<?php
class Page_twitter extends CI_Controller{

	var $news = 'phuketnews_news';
	var $business = 'phuketnews_business';
	var $property = 'phuketnews_property';
	var $event = 'phuketnews_events';
	var $yacht = 'phuketnews_yacht';
	var $classified = 'phuketnews_classified';

	function news()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->news ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();

		$uri = base_url().'twitterapi/news.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}

	function business()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->business ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$uri = base_url().'twitterapi/business.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}

	function property()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->property ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$uri = base_url().'twitterapi/property.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}

	function event()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->event ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$uri = base_url().'twitterapi/event.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}

	function yacht()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->yacht ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$uri = base_url().'twitterapi/yacht.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}

	function classified()
	{
		$sql = "SELECT * ";
		$sql .= "FROM $this->classified ";
		$sql .= "WHERE status = 1 ";
		$sql .= "AND twitter = 0 ";
		$sql .= "ORDER BY id DESC ";
		$sql .= "LIMIT 0, 1 ";
		$query = $this->db->query($sql);
		$rs = $query->result();
		$uri = base_url().'twitterapi/classified.php?id='.$rs[0]->id;
		$content = file_get_contents($uri);
	}
}
