<?php

class Page_business_import extends CI_Controller{

	var $category_id = 23;
	var $data_table = 'factory';
	var $table = 'phuketnews_business';
	var $member_id = 1;
	var $limit = 50;
	function index()
	{
		
		set_time_limit(30000);
		
		$sql = 'SELECT * FROM '.$this->data_table.' WHERE import = 0 LIMIT 0, '.$this->limit;
		$query = $this->db->query($sql);
		$rs = $query->result();
		$entered = date('Y-m-d H:i:s');

		$items = array();
		foreach($rs as $value){
			
			if(trim($value->Name) == false){
				$title = str_replace('\\','',$value->Contact);
			}else{
				$title = str_replace('\\','',$value->Name);
			}
			
			$value->Business = str_replace('\\','',$value->Business);
			
			$sql = "INSERT INTO ".$this->table." ";
			$sql .= "SET ";
			$sql .= "member_id = '1', ";
			$sql .= "category_id = '$this->category_id', ";
			$sql .= "title = '$title', ";
			$sql .= "address = \"$value->NO $value->M $value->Soi $value->Rd $value->City\", ";
			$sql .= "city = '$value->Area', ";
			$sql .= "state = '$value->Province', ";
			$sql .= "zipcode = '$value->Zipcode', ";
			$sql .= "country = 'TH', ";
			$sql .= "phone = '$value->Tel', ";
			$sql .= "found = '$value->Found', ";
			$sql .= "labor = '$value->Labor', ";
			$sql .= "description = '$value->Business', ";
			$sql .= "detail = '$value->Business', ";
			$sql .= "contact = '$value->Contact', ";
			$sql .= "entered = NOW() ";
			$this->db->query($sql);
			$id = $this->db->insert_id();

			$sql = "UPDATE ".$this->table." ";
			$sql .= "SET ";
			$sql .= "slug = '".trim($title).'-'.$id."', ";
			$sql .= "status = 1 ";
			$sql .= "WHERE id = '$id' ";
			$this->db->query($sql);

			$sql = "UPDATE ".$this->data_table." SET import = 1 WHERE FactoryID = '$value->FactoryID' ";
			$this->db->query($sql);

		}

		if(count($rs) > 0){
			echo '<script>window.location.href="'.base_url().'business/import"</script>';
		}else{
			echo 'done';
		}	
	}
}