<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['nav'] = 'Page_nav';

$route['default_controller'] = 'Page_home';
$route['about'] = 'Page_static/about';
$route['terms'] = 'Page_static/terms';
$route['advertise'] = 'Page_static/advertise';
$route['rss'] = 'Page_static/rss';
$route['search'] = 'Page_search';
$route['google-news-sitemap.xml'] = 'Page_news_sitemap/google';
$route['live'] = 'Page_counter';
$route['addbanner'] = 'Page_addbanner';

$route['contribute'] = 'Page_contribute';
$route['contribute/post'] = 'Page_contribute/post';
$route['contribute/preview/(:any)'] = 'Page_contribute/preview/$1';
$route['contribute/paypal'] = 'Page_contribute/paypal';
$route['contribute/paypal_ipn'] = 'Page_contribute/paypal_ipn';
$route['contribute/thankyou'] = 'Page_contribute/complete';

$route['news/sitemap'] = 'Page_news_sitemap/index';
$route['news/sitemap/(:num)'] = 'Page_news_sitemap/index_pages/$1';

$route['twitter/news'] = 'Page_twitter/news';
$route['twitter/business'] = 'Page_twitter/business';
$route['twitter/property'] = 'Page_twitter/property';
$route['twitter/event'] = 'Page_twitter/event';
$route['twitter/yacht'] = 'Page_twitter/yacht';
$route['twitter/classified'] = 'Page_twitter/classified';

$route['resize'] = 'Image_resize';
$route['news/autotags'] = 'Page_news_api_auto_tags';

$route['news'] = 'Page_news/index';
$route['news/clips'] = 'Page_news/clips';
$route['news/clips/(:num)'] = 'Page_news/clips_pages/$1';
$route['news/editor'] = 'Page_news/editors';
$route['news/editor/(:any)'] = 'Page_news/editor/$1';
$route['news/editor/(:any)/(:num)'] = 'Page_news/editor_pages/$1/$2';
$route['news/tag'] = 'Page_news/tag_index';
$route['news/tag/(:any)'] = 'Page_news/tag/$1';
$route['news/tag/(:any)/(:num)'] = 'Page_news/tag_pages/$1/$2';
$route['news/rss/(:any)'] = 'Page_news_rss/index/$1';
$route['news/approve/(:any)'] = 'Page_news/approve/$1';
$route['news/delete/(:any)'] = 'Page_news/delete/$1';
$route['news/(:num)'] = 'Page_news/pages/$1';
$route['news/(:any)'] = 'Page_news/category/$1';
$route['news/(:any)/(:num)'] = 'Page_news/category_pages/$1/$2';
$route['movies'] = 'Page_movie/index';
$route['movies/(:num)'] = 'Page_movie/index_pages/$1';
$route['movies/(:any)'] = 'Page_movie/info/$1';

$route['story/views'] = 'Page_news/views';
$route['story/(:any)'] = 'Page_news/content/$1';

$route['business'] = 'Page_comming/index';
//$route['business'] = 'Page_business/index';
$route['business/import'] = 'Page_business_import';
$route['business/p/(:any)'] = 'Page_business/province/$1';
$route['business/p/(:any)/(:any)'] = 'Page_business/province_list/$1/$2';
$route['business/p/(:any)/(:any)/(:num)'] = 'Page_business/province_list_pages/$1/$2/$3';
$route['business/p/(:any)/(:any)/(:any)'] = 'Page_business/info/$3';

$route['business/rss'] = 'Page_business/rss';
$route['business/sitemap'] = 'Page_business/sitemap';
$route['business/sitemap/(:num)'] = 'Page_business/sitemap_pages/$1';
$route['business/(:num)'] = 'Page_business/index_pages/$1';
$route['business/search'] = 'Page_business/search';
$route['business/map'] = 'Page_business/map';
$route['business/map/(:num)'] = 'Page_business/map_pages/$1';
$route['business/approve/(:any)'] = 'Page_business/approve/$1';
$route['business/delete/(:any)'] = 'Page_business/reject/$1';

$route['business/contactus'] = 'Page_email/contactus';
$route['business/paypal/(:any)'] = 'Page_business/paypal/$1';
$route['business/paypal_ipn'] = 'Page_business/paypal_ipn';
$route['business/thankyou'] = 'Page_business/complete';

$route['business/contact'] = 'Page_business/contact';
$route['business/c/(:any)'] = 'Page_business/category/$1';
$route['business/c/(:any)/(:num)'] = 'Page_business/category_pages/$1/$2';
$route['business/v/(:any)'] = 'Page_business/views/$1';
$route['business/get/citylist/(:any)'] = 'Page_business/generate_city_by_state/$1';

$route['business/(:any)'] = 'Page_business/info/$1';
$route['business/(:any)/edit'] = 'Page_business/edit/$1';


$route['property'] = 'Page_comming/index';
//$route['property'] = 'Page_property';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)'] = 'Page_property/province/$1';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:num)'] = 'Page_property/province_pages/$1/$2';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)'] = 'Page_property/province_city/$1/$2';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)/(:num)'] = 'Page_property/province_city_pages/$1/$2/$3';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)/%E0%B8%82%E0%B8%B2%E0%B8%A2'] = 'Page_property/property_city_page_sell/$1/$2';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)/%E0%B8%82%E0%B8%B2%E0%B8%A2/(:num)'] = 'Page_property/property_city_page_sell_pages/$1/$2';

$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)/%E0%B8%82%E0%B8%B2%E0%B8%A2'] = 'Page_property/property_city_page_sell/$1/$2';
$route['property/%E0%B8%88%E0%B8%B1%E0%B8%87%E0%B8%AB%E0%B8%A7%E0%B8%B1%E0%B8%94/(:any)/(:any)/%E0%B8%82%E0%B8%B2%E0%B8%A2/(:num)'] = 'Page_property/property_city_page_sell_pages/$1/$2';


$route['property/rss'] = 'Page_property/rss';
$route['property/(:num)'] = 'Page_property/pages/$1';
$route['property/import'] = 'Page_property/import';
$route['property/map'] = 'Page_property/map';
$route['property/map/(:num)'] = 'Page_property/map_pages/$1';
$route['property/print'] = 'Page_property/printout';
$route['property/contact'] = 'Page_property/contact';
$route['property/approve/(:any)'] = 'Page_property/approve/$1';
$route['property/delete/(:any)'] = 'Page_property/delete/$1';
$route['property/search'] = 'Page_property/search';
$route['property/sale'] = 'Page_property/sale';
$route['property/sale/(:num)'] = 'Page_property/sale_pages/$1';
$route['property/rental'] = 'Page_property/rental';
$route['property/rental/(:num)'] = 'Page_property/rental_pages/$1';
$route['property/(:any)'] = 'Page_property/info/$1';

$route['event'] = 'Page_comming/index';
//$route['event'] = 'Page_event';
$route['event/rss'] = 'Page_event/rss';
$route['event/map'] = 'Page_event/map';
$route['event/map/(:num)'] = 'Page_event/map_pages/$1';
$route['event/search'] = 'Page_event/search';
$route['event/(:num)'] = 'Page_event/pages/$1';
$route['event/c/(:any)'] = 'Page_event/category/$1';
$route['event/c/(:any)/(:num)'] = 'Page_event/category_page/$1/$2';
$route['event/c/(:any)/(:any)'] = 'Page_event/province_list/$1/$2';
$route['event/c/(:any)/(:any)/(:num)'] = 'Page_event/province_list_paging/$1/$2/$3';

$route['event/c/(:any)/(:any)/(:any)'] = 'Page_event/city_list/$1/$2/$3';
$route['event/c/(:any)/(:any)/(:any)/(:num)'] = 'Page_event/city_list_paging/$1/$2/$3/$4';

$route['event/approve/(:any)'] = 'Page_event/approve/$1';
$route['event/contact'] = 'Page_event/contact';
$route['event/delete/(:any)'] = 'Page_event/delete/$1';
$route['event/user'] = 'Page_event/worldnews';
$route['event/(:any)'] = 'Page_event/info/$1';
$route['event/api/(:any)/(:num)'] = 'Page_event_api/index/$1/$2';
$route['event/api/venue'] = 'Page_event_api/venue';


$route['classified'] = 'Page_comming/index';
//$route['classified'] = 'Page_classified/index';
$route['classified/(:num)'] = 'Page_classified/index_pages/$1';
$route['classified/map'] = 'Page_classified/map';
$route['classified/map/(:num)'] = 'Page_classified/map_pages/$1';
$route['classified/search'] = 'Page_classified/search';
$route['classified/approve/(:any)'] = 'Page_classified/approve/$1';
$route['classified/delete/(:any)'] = 'Page_classified/delete/$1';


$route['classified/c/(:any)'] = 'Page_classified/category/$1';
$route['classified/c/(:any)/(:num)'] = 'Page_classified/category_pages/$1/$2';

$route['classified/p/(:any)'] = 'Page_classified/province/$1';
$route['classified/p/(:any)/(:num)'] = 'Page_classified/province_pages/$1/$2';

$route['classified/p/(:any)/(:any)'] = 'Page_classified/province_category/$1/$2';
$route['classified/p/(:any)/(:any)/(:num)'] = 'Page_classified/province_category_pages/$1/$2/$3';



$route['classified/contact'] = 'Page_classified/contact';
$route['classified/seller/(:any)'] = 'Page_classified/seller/$1';
$route['classified/seller/(:any)/(:num)'] = 'Page_classified/seller_pages/$1/$2';
$route['classified/(:any)'] = 'Page_classified/info/$1';

$route['yachts'] = 'Page_comming/index';
//$route['yachts'] = 'Page_yachts';
$route['yachts/rss'] = 'Page_yachts/rss';
$route['yachts/map'] = 'Page_yachts/map';
$route['yachts/map/(:num)'] = 'Page_yachts/map_pages/$1';
$route['yachts/search'] = 'Page_yachts/search';
$route['yachts/(:num)'] = 'Page_yachts/index_pages/$1';
$route['yachts/c/(:any)'] = 'Page_yachts/category/$1';
$route['yachts/contact'] = 'Page_yachts/contact';
$route['yachts/seller/(:any)'] = 'Page_yachts/seller/$1';
$route['yachts/seller/(:any)/(:num)'] = 'Page_yachts/seller_pages/$1/$2';
$route['yachts/c/(:any)/(:num)'] = 'Page_yachts/category_pages/$1/$2';
$route['yachts/approve/(:any)'] = 'Page_yachts/approve/$1';
$route['yachts/delete/(:any)'] = 'Page_yachts/delete/$1';
$route['yachts/p/(:any)'] = 'Page_yachts/province_list/$1';
$route['yachts/p/(:any)/(:num)'] = 'Page_yachts/province_list_paging/$1/$2';
$route['yachts/p/(:any)/(:any)'] = 'Page_yachts/category_list/$1/$2';
$route['yachts/p/(:any)/(:any)/(:num)'] = 'Page_yachts/category_list_paging/$1/$2/$3';
$route['yachts/p/(:any)/(:any)/(:any)'] = 'Page_yachts/want_to_list/$1/$2/$3';
$route['yachts/p/(:any)/(:any)/(:any)/(:num)'] = 'Page_yachts/want_to_list_paging/$1/$2/$3/$4';
$route['yachts/(:any)'] = 'Page_yachts/info/$1';

$route['comments'] = 'Page_comments/index';
$route['comments/form'] = 'Page_comments/form';
$route['comments/submit'] = 'Page_comments/submit';
$route['comments/approve'] = 'Page_comments/approve';
$route['comments/reject'] = 'Page_comments/reject';

$route['email/share'] = 'Page_email/share';
$route['email/share/send'] = 'Page_email/send';

$route['banner'] = 'Page_banner';
$route['banner/callback/(:any)'] = 'Page_banner/callback/$1';

$route['signin'] = 'Page_signin';
$route['forgot'] = 'Page_signin/forgot';
$route['signup'] = 'Page_signup';
$route['signup/complete'] = 'Page_signup/complete';
$route['signup/paypal/(:any)'] = 'Page_signup/paypal/$1';
$route['signup/paypal_ipn'] = 'Page_signup/paypal_ipn';


$route['signout'] = 'Page_signout';
$route['sign'] = 'Page_profile/sign';

$route['account/profile'] = 'Page_profile';
$route['account/dashboard'] = 'Page_profile/dashboard';
$route['account/news'] = 'Page_profile_news';
$route['account/news/submit'] = 'Page_profile_news/submit';
$route['account/news/preview'] = 'Page_profile_news/preview';

$route['account/business'] = 'Page_profile_business';
$route['account/business/submit'] = 'Page_profile_business/submit';
$route['account/business/preview'] = 'Page_profile_business/preview';
$route['account/business/upgrade'] = 'Page_profile_business/upgrade';

$route['account/property'] = 'Page_profile_property';
$route['account/property/submit'] = 'Page_profile_property/submit';
$route['account/property/preview'] = 'Page_profile_property/preview';
$route['account/property/upgrade'] = 'Page_profile_property/upgrade';

$route['account/event'] = 'Page_profile_event';
$route['account/event/submit'] = 'Page_profile_event/submit';
$route['account/event/preview'] = 'Page_profile_event/preview';
$route['account/event/upgrade'] = 'Page_profile_event/upgrade';

$route['account/classified'] = 'Page_profile_classified';
$route['account/classified/submit/getchild'] = 'Page_profile_classified/get_child';
$route['account/classified/submit'] = 'Page_profile_classified/submit';
$route['account/classified/preview'] = 'Page_profile_classified/preview';

$route['account/yachts'] = 'Page_profile_yachts';
$route['account/yachts/submit/getchild'] = 'Page_profile_yachts/get_child';
$route['account/yachts/submit'] = 'Page_profile_yachts/submit';
$route['account/yachts/preview'] = 'Page_profile_yachts/preview';

$route['newsapi'] = 'Page_news_api';
$route['newsapi/abcnews_alabama'] = 'Page_news_api_abcnews_alabama';
$route['newsapi/abcnews_alaska'] = 'Page_news_api_abcnews_alaska';
$route['newsapi/alabamanews'] = 'Page_news_api_alabamanews';
$route['newsapi/omaha'] = 'Page_news_api_omaha';
$route['newsapi/alaskapublic'] = 'Page_news_api_alaskapublic';
$route['newsapi/abcnews_arizona'] = 'Page_news_api_abcnews_arizona';
$route['newsapi/tucson'] = 'Page_news_api_abcnews_tucson';
$route['newsapi/abcnews_arkansas'] = 'Page_news_api_abcnews_arkansas';
$route['newsapi/abcnews_colorado'] = 'Page_news_api_abcnews_colorado';
$route['newsapi/abcnews_california'] = 'Page_news_api_abcnews_california';
$route['newsapi/coloradodaily'] = 'Page_news_api_coloradodaily';
$route['newsapi/abcnews_connecticut'] = 'Page_news_api_abcnews_connecticut';
$route['newsapi/delaware_times'] = 'Page_news_api_delaware_times';
$route['newsapi/abcnews_florida'] = 'Page_news_api_abcnews_florida';
// $route['newsapi/nbc_losangeles'] = 'Page_news_api_nbc_losangeles';
// $route['newsapi/nbc_miami'] = 'Page_news_api_nbc_miami';
$route['newsapi/abcnews_georgia'] = 'Page_news_api_abcnews_georgia';
$route['newsapi/abcnews_hawaii'] = 'Page_news_api_abcnews_hawaii';
$route['newsapi/abcnews_idaho'] = 'Page_news_api_abcnews_idaho';
$route['newsapi/eastidahonews'] = 'Page_news_api_eastidahonews';
// $route['newsapi/arktimes'] = 'Page_news_api_arktimes';
// $route['newsapi/nbc_connecticut'] = 'Page_news_api_nbc_connecticut';
// $route['newsapi/wtoc'] = 'Page_news_api_wtoc';
$route['newsapi/abcnews_illinois'] = 'Page_news_api_abcnews_illinois';
$route['newsapi/abcnews_indiana'] = 'Page_news_api_abcnews_indiana';
$route['newsapi/abcnews_iowa'] = 'Page_news_api_abcnews_iowa';
$route['newsapi/abcnews_kansas'] = 'Page_news_api_abcnews_kansas';
$route['newsapi/abcnews_kentucky'] = 'Page_news_api_abcnews_kentucky';
$route['newsapi/abcnews_louisiana'] = 'Page_news_api_abcnews_louisiana';
$route['newsapi/abcnews_maine'] = 'Page_news_api_abcnews_maine';
$route['newsapi/abcnews_maryland'] = 'Page_news_api_abcnews_maryland';
// $route['newsapi/washingtonpost'] = 'Page_news_api_washingtonpost';
$route['newsapi/abcnews_massachusetts'] = 'Page_news_api_abcnews_massachusetts';
$route['newsapi/boston'] = 'Page_news_api_boston';
$route['newsapi/abcnews_michigan'] = 'Page_news_api_abcnews_michigan';

// $route['newsapi/salem_gazette'] = 'Page_news_api_salem_gazette';
// $route['newsapi/bangordailynews'] = 'Page_news_api_bangordailynews';
// $route['newsapi/theadvocate'] = 'Page_news_api_theadvocate';
// $route['newsapi/nola'] = 'Page_news_api_nola';
// $route['newsapi/kshb'] = 'Page_news_api_kshb';
// $route['newsapi/thesouthern'] = 'Page_news_api_thesouthern';
$route['newsapi/huskers'] = 'Page_news_api_huskers';
$route['newsapi/abcnews_nebraska'] = 'Page_news_api_abcnews_nebraska';
$route['newsapi/nbc_montana'] = 'Page_news_api_nbc_montana';
$route['newsapi/missoulian'] = 'Page_news_api_missoulian';
$route['newsapi/khq_montana'] = 'Page_news_api_khq_montana';
$route['newsapi/abcnews_nevada'] = 'Page_news_api_abcnews_nevada';
$route['newsapi/njb'] = 'Page_news_api_njb';
$route['newsapi/abcnews_new_mexico'] = 'Page_news_api_abcnews_new_mexico';
$route['newsapi/newyorkpost'] = 'Page_news_api_newyorkpost';
$route['newsapi/abcnews_north_carolina'] = 'Page_news_api_abcnews_north_carolina';
$route['newsapi/abcnews_north_dakota'] = 'Page_news_api_abcnews_north_dakota';
$route['newsapi/abcnews_ohio'] = 'Page_news_api_abcnews_ohio';
$route['newsapi/abcnews_oklahoma'] = 'Page_news_api_abcnews_oklahoma';
$route['newsapi/abcnews_oregon'] = 'Page_news_api_abcnews_oregon';
$route['newsapi/abcnews_pennsylvania'] = 'Page_news_api_abcnews_pennsylvania';
$route['newsapi/abcnews_rhode_island'] = 'Page_news_api_abcnews_rhode_island';
$route['newsapi/nhpr'] = 'Page_news_api_nhpr';
$route['newsapi/santafe_new_mexican'] = 'Page_news_api_santafe_new_mexican';
$route['newsapi/wcnc'] = 'Page_news_api_wcnc';
$route['newsapi/dnrwi'] = 'Page_news_api_dnrwi';
$route['newsapi/abcnews_washington'] = 'Page_news_api_abcnews_washington';
$route['newsapi/vasun'] = 'Page_news_api_vasun';
$route['newsapi/trp'] = 'Page_news_api_trp';
$route['newsapi/wv_public'] = 'Page_news_api_wv_public';
$route['newsapi/deseretnews'] = 'Page_news_api_deseretnews';
$route['newsapi/ohsun'] = 'Page_news_api_ohsun';
$route['newsapi/nevadaappeal'] = 'Page_news_api_nevadaappeal';
$route['newsapi/bouldercity_review'] = 'Page_news_api_bouldercity_review';

//14-08-17
$route['newsapi/helenair'] = 'Page_news_api_helenair';
$route['newsapi/fremonttribune'] = 'Page_news_api_fremonttribune';
$route['newsapi/mesquitelocalnews'] = 'Page_news_api_mesquitelocalnews';

$route['(:any)'] = 'Page_news/category/$1';
$route['(:any)/(:num)'] = 'Page_news/category_pages/$1/$2';
$route['(:any)/(:any)'] = 'Page_news/content_news/$1/$2';
$route['newspic/(:any)/(:any)/(:any)/(:any)'] = 'Image_create_news/index/$1/$2/$3/$4';


$route['404_override'] = 'Page_404';
$route['translate_uri_dashes'] = FALSE;
