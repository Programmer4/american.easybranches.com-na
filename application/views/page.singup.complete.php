<?php include('tpl.meta.php');?>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-xs-offset-3  col-xs-6">
      <div class="card">
        <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="164" height="60" alt=""/></a></p>
        <h1 class="text-center">ขอบคุณสำหรับการลงทะเบียนมาเป็นสมาชิกกับเรา</h1>
        <p><br>
        ข้อมูลสมาชิกของคุณจะได้รับการตรวจสอบ ใน <a href="http://worldnews.easybranches.com" target="_blank">America.Easybranches.com</a><br>
        และจะทำการยืนยันบัญชีสมาชิกของคุณโดยเร็วที่สุด
        <br>
        <br>
        ขอแสดงความนับถือ,<br>
        Easy Branches Team</p>

        <br>
        <br>
        <p class="text-center"><a href="<?php echo base_url()?>signin">ล็อคอิน</a></p>
      </div>
    </div>
  </div>

</div>
</body>
</html>
