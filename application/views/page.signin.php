<?php include('tpl.meta.php');?>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="business-card">
      	<div class="business-card-body-full">
        <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="easybranches News" align="center" class="img-responsive"></a></p>
        <br>

        <form id="form-signin" action="<?php echo $this->config->item('signin');?>" method="post">
          <?php if(isset($error)){?>
          <div id="signup-error" class="alert alert-danger alert-dismissible text-center" role="alert"> <strong>Sorry!</strong> email of Password niet correct </div>
          <?php }?>
          <div class="form-group">
            <input name="email" class="form-control" id="register-email" placeholder="Email" aria-required="true" required="" type="email">
          </div>
          <div class="form-group">
            <input name="password" class="form-control" id="password" placeholder="Password" aria-required="true" required="" type="password">
          </div>
          <div class="form-group">
            <input name="save" type="submit" class="btn btn-primary btn-lg btn-block" value="Sign in">
            <input name="save" value="1" type="hidden">
          </div>
          <div class="form-group">
            <ul class="list-unstyled">
              <li><a href="<?php echo base_url();?>forgot" class="forgot-password btn btn-link">Forgot Password</a> </li>
              <li><a href="<?php echo base_url();?>signup" class="forgot-password btn btn-link">Don't have an account?</a> </li>
            </ul>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
