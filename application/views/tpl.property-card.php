<?php
$url = base_url().'property/'.$value->slug;
#$agentname = $value->firstname. ' '.$value->lastname;

echo '<div class="property-block">';
echo '<div class="property-block-header">';
echo '<figure>';
echo '<a href="'.$url.'">';
echo '<img src="'.base_url().$value->thumbnail.'" class="img-responsive">';
echo '</a>';
echo '</figure>';
echo '</div>';
echo '<div class="property-block-body">';
echo '<div class="property-block-info">';
echo '<h3><a href="'.$url.'">'.$value->title.'</a></h3>';
echo '<p>'.$value->address. ' ' . $value->city. ' ' .$value->state . ' '. $value->zipcode.'</p>';

echo '<hr>';
echo '<ul class="list-inline">';
if($value->beds){
	echo '<li> <i class="fa fa-fw fa-bed"></i> '.$value->beds.'</li>';
}
if($value->baths){
	echo '<li> <i class="fa fa-fw fa-shower"></i> '.$value->baths.'</li>';
}
if($value->parking){
	echo '<li> <i class="fa fa-fw fa-car"></i> '.$value->parking.'</li>';
}
echo '</ul>';
echo '</div>';
echo '<div class="property-block-agent">';
/*
echo '<div class="row">';
echo '<div class="col-xs-4"><img src="'.$avatar.'" class="avatar-block"></div>';
echo '<div class="col-xs-8">';
echo '<h3>'.$agentname.'</h3>';
echo '<hr>';
echo number_format($value->property_price);
echo '</div>';
echo '</div>';
*/
echo '</div>';
echo '</div>';
echo '</div>';
?>