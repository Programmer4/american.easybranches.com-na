<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/event">กิจกรรม</a></li>
        <li class="active">
          <?php if(isset($content[0]->title)){ echo $content[0]->title; } ?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <?php 
				if(isset($content[0]->status)){
					if($content[0]->status == 0){
						echo '<div class="alert alert-danger text-center"> กิจกรรมของคุณกำลังอยู่ภายใต้การตรวจสอบ</div>';
					}else{
						echo '<div class="alert alert-success text-center"> กิจกรรมของคุณได้รับการเผยแพร่แล้ว </div>';
					}
				}
				?>
          <div class="profile">
            <h2><span>ข้อมูลกิจกรรม</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อกิจกรรมของคุณ</label>
                <p>
                  <?php if(isset($content[0]->title)){ echo $content[0]->title; }?>
                </p>
              </div>
              <div class="form-group">
                <label>ประเภทของกิจกรรมของคุณ</label>
                <p>
                  <?php if(isset($category[0]->title)){ echo $category[0]->title;}?>
                </p>
              </div>
              <div class="form-group">
                <label>ที่อยู่</label>
                <p>
                  <?php if(isset($content[0]->address)){ echo $content[0]->address;}?>
                </p>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>เมือง</label>
                    <p>
                      <?php if(isset($content[0]->city)){ echo $content[0]->city;}?>
                    </p>
                  </div>
                  <div class="col-sm-4">
                    <label>จังหวัด</label>
                    <p>
                      <?php if(isset($content[0]->province)){ echo $content[0]->province;}?>
                    </p>
                  </div>
                  <div class="col-sm-4">
                    <label>รหัสไปรษณีย์</label>
                    <p>
                      <?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">โทรศัพท์
                    <p>
                      <?php if(isset($content[0]->phone)){ echo $content[0]->phone;}?>
                    </p>
                  </div>
                  <div class="col-sm-4">
                    <label>แฟกซ์</label>
                    <p>
                      <?php if(isset($content[0]->fax)){ echo $content[0]->fax;}?>
                    </p>
                  </div>
                  <div class="col-sm-4">
                    <label>Email</label>
                    <p>
                      <?php if(isset($content[0]->email)){ echo $content[0]->email;}?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>เว็บไซด์</label>
                <p>
                  <?php if(isset($content[0]->website)){ echo $content[0]->website;}?>
                </p>
              </div>
              <div class="form-group">
                <label>คำอธิบายโดยย่อของกิจกรรมของคุณ</label>
                <p>
                  <?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
                </p>
              </div>
              <div class="form-group">
                <label>รายละเอียดของกิจกรรม</label>
                <?php if(isset($content[0]->detail)){ echo $content[0]->detail; }?>
              </div>
              <?php
if(isset($content[0]->time_start)){
	$startdate = $content[0]->time_start;
}else{
	$startdate = date('Y-m-d H:i:s');
}
if(isset($content[0]->time_end)){
	$startdate2 = $content[0]->time_end;
}else{
	$startdate2 = date('Y-m-d H:i:s');
}
?>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label>เริ่ม</label>
                    <p><?php echo $startdate; ?></p>
                  </div>
                  <div class="col-xs-6">
                    <label>จบ</label>
                    <p><?php echo $startdate2; ?></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>Free Event?</label>
                    <p>
                      <?php if(isset($content[0]->free)){ if($content[0]->free == 1){ echo 'Yes';}else{ echo 'No';}}?>
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <div id="price" <?php if(isset($content[0]->free)){ if($content[0]->free == 1){ echo 'style="display:none;"' ;}}?>>
                      <label>กิจกรรมฟรีหรือไม่?</label>
                      <p>
                        <?php if(isset($content[0]->price)){ echo $content[0]->price; }?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>โปสเตอร์งานของคุณ</label>
                <p>
                  <?php 
							if(isset($content[0]->thumbnail)){
								if($content[0]->thumbnail){
							    echo '<img src="'.base_url().$content[0]->thumbnail.'" class="img-responsive" /> ';
								}else{
									echo ' - ';
								}
							}?>
                </p>
              </div>
              <h2><span>แผนที่งานกิจกรรม</span></h2>
              <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '7.9442418';
	$lng = '98.345671';
	$zoom = 11;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->zoom;
}
?>
              <div id="google-map" style="min-height:350px; margin-top:-30px;border-left:0; border-right:0; border-top:0; border-bottom: solid 1px #eff2f5"></div>
            </form>
            <?php 
					if(isset($content[0]->status)){
						if($content[0]->status == 1){?>
            <br>
            <p class="text-center"> <a href="<?php echo base_url();?>account/event/submit?id=<?php echo $content[0]->id; ?>" class="btn btn-success"> 
            อัพเดทประกาศ
            </a> </p>
            <?php 
						}
					}?>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</section>
<script>

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: false,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();
		
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		
		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);
		
		$('input[name=geo]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){
	
	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=zoom]').val());
	
	initMap2(lat, lng, zoom)
});

</script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
