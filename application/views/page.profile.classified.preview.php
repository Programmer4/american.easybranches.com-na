<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Account</a></li>
        <li><a href="<?php echo base_url(); ?>account/classified">Classified</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'Compose';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <?php 
			if($content[0]->status == 0){
				echo '<div class="alert alert-danger text-center">';
				echo 'สิ่งนี้จัดอยู่ในขั้นตอนการตรวจสอบ คุณจะได้รับแจ้งเมื่อได้รับการอนุมัติ';
				echo '</div>';
			}else{
				echo '<div class="alert alert-success text-center">';
				echo 'สิ่งตีพิมพ์นี้ได้รับการตีพิมพ์แล้ว';
				echo '</div>';
			}
			?>
            
          <div class="profile">
            <h2><span>Headline</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อสินค้า</label>
                <p><?php echo $content[0]->title;?></p>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-4">
                    <label>ประเภทสินค้า</label>
                    <p><?php echo $parent_category[0]->category_title;?> / <?php echo $child_category[0]->category_title;?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>ราคา</label>
                    <p><?php 
										$content[0]->price = str_replace(',','',$content[0]->price);
										if($content[0]->price){echo number_format($content[0]->price,0,'',',');}else{ echo  '-';}?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>เงื่อนไข</label>
                    <p><?php echo ucfirst($content[0]->conditions);?></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-4">
                    <label>ที่อยู่</label>
                    <p><?php echo $content[0]->address;?> </p>
                  </div>
                	<div class="col-lg-4">
                    <label>เมือง</label>
                    <p><?php echo $content[0]->city;?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>จังหวัด</label>
                    <p><?php echo $province[0]->title;?></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
									<div class="col-lg-4">
                    <label>รหัสไปรษณีย์</label>
                    <p><?php echo $content[0]->zipcode;?> </p>
                  </div>                

									<div class="col-lg-4">
                    <label>Email</label>
                    <p><?php echo $content[0]->email;?> </p>
                  </div>                
									<div class="col-lg-4">
                    <label>Phone</label>
                    <p><?php echo $content[0]->phone;?> </p>
                  </div>                
                </div>
              </div>
              <div class="form-group">
                <label>คำอธิบาย</label>
                <p><?php echo $content[0]->description;?></p>
              </div>
              <h2><span>รายละเอียดสินค้า</span></h2>
              <div class="form-group"> <img src="<?php echo base_url().$content[0]->thumbnail;?>" class="img-responsive mb-15"> <?php echo $content[0]->detail;?> </div>
              
              <?php foreach($photo as $index=>$value){?>
              <div class="form-group"><img src="<?php echo base_url().$value->filepath;?>" class="img-responsive"></div>
              <?php }?>
              
              <?php if($content[0]->status == 1){?>
              <div class="form-group">
                <p class="text-center"> <a href="<?php echo base_url();?>account/classified/submit?id=<?php echo $content[0]->id;?>" class="btn btn-success">Update Your Classified</a> </p>
              </div>
              <?php }?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<?php include('tpl.footer.php');?>
