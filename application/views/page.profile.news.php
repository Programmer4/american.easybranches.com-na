<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Account</a></li>
        <li class="active">News</li>
      </ul>
      <br>
      <div class="row">
        <div class="col-lg-6">
          <form method="get" class="form-inline">
            <div class="form-group">
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <input name="q" type="text" class="form-control" placeholder="Keyword" value="<?php echo $searchKeyword; ?>">
              <button type="submit" class="btn btn-search btn-info" > <i class="fa fa-fw fa-search"></i> </button>
              &nbsp; <a href="<?php echo  base_url();?>account/news/submit" class="btn btn-success"> Compose </a> </div>
          </form>
        </div>
        <div class="col-lg-6">
          <div class="btn-group pull-right" role="group" aria-label="...">
            <a href="<?php echo base_url()?>account/news?status=1" class="btn btn-default <?php if($status == '1'){ echo 'active';}?>">Approved</a>
            <a href="<?php echo base_url()?>account/news?status=0" class="btn btn-default <?php if($status == '0'){ echo 'active';}?>">Waiting for approval</a>
          </div>
        </div>
      </div>
      <?php

      if(count($news['rows'])){
  			echo '<br><br>';
  			echo '<div class="profile">';
  			echo '<table class="table">';
  			echo '<thead>';
  			echo '<tr>';
  			echo '<th nowrap>Image</th>';
  			echo '<th>Description</th>';
  			echo '<th>DateTime</th>';
  			if($status == 1){
  				echo '<th></th>';
  			}
  			echo '</tr>';
  			echo '</thead>';
  			echo '<tbody>';
  			foreach($news['rows'] as $value){

  				$preview = base_url().'account/news/preview?id='.$value->id;

  				echo '<tr>';
  				echo '<td>';
  				if(is_file($value->thumbnail)){
  					echo '<img src="'.base_url().'resize?image=/'.$value->thumbnail.'&width=90&height=90&cropratio=1:1" alt="'.$value->title.'" class="img-responsive">';
  				}else{
  					echo ' - None -';
  				}
  				echo '</td>';
  				echo '<td>';
  				echo '<table width="100%">';
  				echo '<tr>';
  				echo '<td width="150x"><strong>Title:</strong></td>';
  				echo '<td>'.$value->title.'</td>';
  				echo '</tr>';
  				echo '<tr>';
  				echo '<td width="150x"><strong>Category:</strong></td>';
  				echo '<td>'.$value->category_title.'</td>';
  				echo '</tr>';
  				echo '<tr>';
  				echo '<td valign="top"><strong>Keywords:</strong></td>';
  				echo '<td>';
  				if($value->keywords){
  					echo $value->keywords;
  				}else{
  					echo ' - None -';
  				}
  				echo '</td>';
  				echo '</tr>';
  				echo '<tr>';
  				echo '<td valign="top"><strong>Description:</strong></td>';
  				echo '<td>'.$value->description.'<br> <a href="'.base_url().'story/'.urlencode($value->slug).'" target="_blank">[View]</a></td>';
  				echo '</tr>';
  				echo '</table>';
  				echo '</td>';
  				echo '<td nowrap>'.$this->Entered->time_elapsed_string($value->entered).'</td>';
  				if($status == 1){
  					echo '<td nowrap><a href="'.$preview.'" class="btn btn-success">Edit</a></td>';
  				}
  				echo '</tr>';
  			}
  			echo '</tbody>';
  			echo '</table>';
  			echo '</div>';

  		}else{
			echo '<br><br><br>';
			echo '<p class="text-center"> Not found any news on your profile yet. Try to submit today.</p>';
			echo '<br><br><br>';
		}

		if($news['pages'] > 1){
      echo $this->Paginate->loadmorestr(base_url().'account/news?status='.$status . ($searchKeyword !="" ? "&q=".$searchKeyword : ""), $page, $news['pages']);
		}

    ?>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
