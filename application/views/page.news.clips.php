<?php
$url = '';
?>
<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row section-heading">
        <div class="col-lg-6">
          <h1>คลิปข่าว คลิปกระแส คลิปล่าสุด</h1>
          <h2>อัพเดท คลิปข่าว คลิปกระแส คลิปล่าสุด ก่อนใครได้ที่นี่</h2>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url();?>news">News</a></li>
            <li class="active">คลิปข่าว</li>
          </ul>
        </div>
        <div class="col-lg-6"> <?php echo $this->Share->push(base_url().'news/clips', base_url().$news['rows'][0]->thumbnail);?> </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="row">
            <div class="col-lg-12">
              <div class="cate-latest mt-15">
                <?php

						if(isset($news['rows'][0]->id)){

							$news['rows'][0]->slug = urlencode($news['rows'][0]->slug);

							if(is_file($news['rows'][0]->thumbnail)){
								$img = base_url().'resize?image=/'.$news['rows'][0]->thumbnail.'&width=750&height=495&cropratio=7.50:4.95';
							}else{
								$img = base_url().'assets/images/default-news.png';
							}

							echo '<div class="latest">';
							echo '<div class="latest-cover">';
							echo '<div class="latest-cover-overlay"></div>';
							echo '<img src="'.$img.'" alt="'.$news['rows'][0]->title.' - คลิปข่าว">';
							echo '<div class="latest-caption">';
							echo '<h1><a href="'.base_url().'story/'.$news['rows'][0]->slug.'" target="_blank">'.$news['rows'][0]->title.'</a></h1>';
							echo '<p>'.$news['rows'][0]->description.'</p>';
							echo '<div class="latest-entered">';
							echo '<ul class="list-inline share-this share-min">';
							echo '<li>'.$this->Entered->time_elapsed_string($news['rows'][0]->entered).'</li>';
							echo '<li><a href="'.base_url().'email/share?u=" data-url="'.base_url().'story/'.$news['rows'][0]->id.'" class="button-envelope"><i class="fa fa-fw fa-envelope"></i></a></li>';
							echo '<li><a href="#" data-toggle="modal" data-target="#modal-share" data-url="'.base_url().'story/'.$news['rows'][0]->id.'" data-image="'.$img.'" class="button-share"><i class="fa fa-fw fa-share-alt"></i></a></li>';
							echo '</ul>';
							echo '</div>';
							echo '</div>';

							echo '</div>';
							echo '</div>';
						}

			?>
              </div>
            </div>
          </div>
          <div class="row">
            <?php

	   		$n = 1;
				while($n < count($news['rows'])){
					$content_url = base_url().'story/'.urlencode($news['rows'][$n]->slug);
?>
            <div class="col-lg-4">
              <div class="business-card">
                <figure> <a href="<?php echo $content_url;?>" target="_blank">
                  <?php
									if(is_file($news['rows'][$n]->thumbnail)){

										$g = getimagesize($news['rows'][$n]->thumbnail);

										if($g[0] > $g[1]){
											$w = 400;
											$h = 300;
											$c = '4:3';
										}else{
											$w = 300;
											$h = 400;
											$c = '3:4';
										}

										?>
                  <img src="<?php echo base_url().'resize?image=/'.$news['rows'][$n]->thumbnail;?>&width=<?php echo $w; ?>&height=<?php echo $h;?>&cropratio=<?php echo $c;?>" alt="<?php echo $news['rows'][$n]->title; ?> - คลิปข่าว">
                  <?php }else{?>
                  <img src="<?php echo base_url()?>assets/images/default-news.png" alt="<?php echo $news['rows'][$n]->title; ?> - คลิปข่าว">
                  <?php }?>
                  </a> </figure>
                <div class="business-card-body"><a href="<?php echo $content_url; ?>" target="_blank">
                  <h3 class="overflow ellipsis"><?php echo $news['rows'][$n]->title; ?></h3>
                  </a>
                  <h4><small><?php echo $this->Entered->time_elapsed_string($news['rows'][$n]->entered);?></small></h4>
                  <?php echo '<p class="hidden-sm hidden-xs">'.$news['rows'][$n]->description.'</p>'; ?> </div>
                <div class="business-card-footer">
                  <ul class="list-inline">
                    <li class="pull-left"><a href="<?php echo base_url().'news/'.$news['rows'][$n]->category_slug;?>"><?php echo $news['rows'][$n]->category_title; ?></a></li>
                    <li><a href="#" class="modal-email" data-message="Have a look at <?php echo base_url();?>story/<?php echo $news['rows'][$n]->slug; ?> :)" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
                    <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-url="<?php echo base_url().'story/'.$news['rows'][$n]->id; ?>" data-image="<?php echo base_url().$news['rows'][$n]->thumbnail;?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
                    <li></li>
                  </ul>
                </div>
              </div>
            </div>
            <?php
					$n++;
				}
				?>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.phuket.php');?>
          <?php include('tpl.news.banner.php');?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <?php
	if($news['pages'] > 1){
		$url = base_url().'news/clips';
		echo $this->Paginate->pages($url, $page, $news['pages']);
	}
?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
