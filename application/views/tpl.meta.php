<?php
if(isset($meta_title)){
	$meta_title = str_replace('"',"", $meta_title);
}else{
	$meta_title = 'America.Easybranches.com keeping you up to date with the latest news from america';
}
if(isset($meta_description)){
	$meta_description = trim(preg_replace('/\s\s+/', ' ', $meta_description));
}else{
	$meta_description = 'easybranches news - America.Easybranches.com keeping you up to date with the latest news from america';
}

if(isset($meta_keywords)){
	$meta_keywords = str_replace('"',"", $meta_keywords);
}else{
	$meta_keywords = 'easybranches news, america news international breaking newest information';
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php echo $meta_description?>" />
<meta name="keywords" content="<?php echo $meta_keywords;?>" />
<meta name="news_keywords" content="<?php echo $meta_keywords; ?>" />
<meta name="fb_title" content="<?php echo $meta_title?>" />
<meta property="og:site_name" content="America.Easybranches.com" />
<meta property="og:title" content="<?php echo $meta_title;?>" />
<meta property="og:description" content="<?php echo $meta_description;?>" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@EasyBranches" />
<meta property="twitter:title" content="<?php echo $meta_title;?>" />
<meta property="twitter:description" content="<?php echo $meta_description; ?>" />
<meta property="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>" rel="alternate" hreflang="nl" />
<link href="<?php echo base_url();?>assets/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/flag-icon.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<?php
if(isset($module)){
	if($module == 'home'){?>
<link href="<?php echo base_url();?>assets/css/home.css" rel="stylesheet">
<?php
	}
}
?>
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>assets/jwplayer-7.9.1/jwplayer.js"></script>
<script>jwplayer.key="fVMElcMwXIEFkRobACNvv6mrYcBJV5eek3Rwjg==";</script>
<script src="<?php echo base_url();?>assets/js/jquery.lazyload.min.js"></script>
<?php

$amp_items[] = '"@context": "http://schema.org"';
$amp_items[] = '"@context": "http://schema.org"';
if(isset($meta_title)){
	$amp_items[] = '"headline": "'.$meta_title.'"';
}
if(isset($meta_image)){
	$amp_items[] = '"image": ["'.$meta_image.'"]';
}
?>
<script type="application/ld+json">{<?php echo implode(',',$amp_items);?>}</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PP7WJQX');</script>
<!-- End Google Tag Manager -->


</head>
<body class="<?php if(isset($module)){ echo $module; }?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PP7WJQX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
