<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">หน้า</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/yachts">เรือยอชท์</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="profile">
            <h2><span>ข้อมูลเรือยอชท์</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" name="title" class="form-control" required value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6">
                    <label>ประเภท</label>
                    <select class="form-control" name="category_id" required>
                      <option value=""> - เลือก - </option>
                      <?php 
											foreach($category as $index=>$value){
												
												if(isset($content[0]->id)){
													if($value->category_id == $content[0]->category_id){
														$select = ' selected';
													}else{
														$select = '';
													}
												}
												
												echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
											}
											?>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label>ต้องการ</label>
                    <select class="form-control" name="want_to" required onChange="price_type(this.value)">
                      <option value=""> - เลือก - </option>
                      <option value="sale"<?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'sale'){echo 'selected';}}?>> ขาย </option>
                      <option value="charter" <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'charter'){echo 'selected';}}?>> ให้เช่า </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-3">
                    <label>ที่อยู่</label>
                    <input type="text" name="address" class="form-control" value="<?php if(isset($content[0]->address)){ echo $content[0]->address;}?>">
                  </div>
                  <div class="col-lg-3">
                    <label>เมือง</label>
                    <input type="text" name="city" class="form-control" value="<?php if(isset($content[0]->city)){ echo $content[0]->city;}?>">
                  </div>
                  <div class="col-lg-3">
                    <label>จังหวัด</label>
                    <select class="form-control" name="location_id" required>
                      <option value=""> - เลือก - </option>
                      <?php
											$select = '';
                      foreach($province as $index=>$value){
												if(isset($content[0]->location_id)){
													if($content[0]->location_id == $value->location_id){
														$select = ' selected';
													}else{
														$select = '';
													}
												}
												echo '<option value="'.$value->location_id.'"'.$select.'>'.$value->title.'</option>';
											}
											?>
                    </select>
                  </div>
                  <div class="col-lg-3">
                    <label>รหัสไปรษณีย์</label>
                    <input name="zipcode" class="form-control" type="text" value="<?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-3">
                    <label>ความยาวของเรือ</label>
                    <input type="text" name="length" value="<?php if(isset($content[0]->length)){ echo $content[0]->length;}?>" class="form-control">
                  </div>
                  <div class="col-lg-3">
                    <label>จำนวนห้องน้ำ</label>
                    <input type="text" name="bathrooms" value="<?php if(isset($content[0]->bathrooms)){ echo $content[0]->bathrooms;}?>" class="form-control">
                  </div>
                  <div class="col-lg-3">
                    <label>จำนวนห้องนอน</label>
                    <input type="text" name="accommodates" value="<?php if(isset($content[0]->accommodates)){ echo $content[0]->accommodates;}?>" class="form-control">
                  </div>
                  <div class="col-lg-3">
                    <label>จำนวนเคบิน</label>
                    <input type="text" name="cabin" value="<?php if(isset($content[0]->cabin)){ echo $content[0]->cabin;}?>" class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>คำอธิบาย</label>
                <textarea class="form-control" name="description" required><?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
</textarea>
              </div>
              <div id="activities">
                <h2><span>กิจกรรมในการเดินทาง</span></h2>
                <div class="form-group">
                  <?php

									$_activities = array();
									
									if(isset($content[0]->activity)){
										$_activities = explode(',', $content[0]->activity);
									}
                  
									$activities[0] = 'Snorkeling';
									$activities[] = 'Sailing';
									$activities[] = 'Cruising';
									$activities[] = 'Party';
									$activities[] = 'Gourmet';
									$activities[] = 'Diving';
									$activities[] = 'Expeditions';
									$activities[] = 'Wellness';
									$activities[] = 'Romantic';
									$activities[] = 'Events';
									$activities[] = 'Wakeboard';
									$activities[] = 'Surf';
									$activities[] = 'Windsurf';
									$activities[] = 'Kitesurf';
									
									$check = '';
									$r = 1;
									$rows = ceil(count($activities)/3);
									$x = 0;
									echo '<table width="100%">';
									while($r <= $rows){
										echo '<tr>';
										for($i=1; $i<=3; $i++){
											echo '<td width="33%">';
											if(isset($activities[$x])){
												$value = $activities[$x];
												
												if(in_array($value, $_activities)){
													$check = ' checked';
												}else{
													$check = '';
												}

												
											?>
                  <label>
                    <input name="activity[]" type="checkbox" value="<?php echo $value; ?>" <?php echo $check;?>>
                    <?php echo $value?> </label>
                  <?php
											echo '</td>';
											}
											$x++;
										}
										echo '</tr>';
										$r++;
									}
									echo '</table>';
									?>
                </div>
              </div>
              <div id="amenities">
                <h2><span>สิ่งอำนวยความสะดวก</span></h2>
                <div class="form-group">
                  <?php
                  
									$amenities[0] = 'Kitchen';
									$amenities[] = 'Washer';
									$amenities[] = 'Crew';
									$amenities[] = 'Living room';
									$amenities[] = 'TV';
									$amenities[] = 'Wifi';
									$amenities[] = 'Telephone';
									$amenities[] = 'Large outdoor area';
									$amenities[] = 'Air condition';
									$amenities[] = 'Dryer';
									$amenities[] = 'Fishing';
									$amenities[] = 'CD Player with iPad plug';
									$amenities[] = 'Generator and Air conditioning';
									$amenities[] = 'Toilet';
									$amenities[] = 'BBQ';
									$amenities[] = 'Hot and Cold pressurised water system';
									$amenities[] = 'Sound system';
									$amenities[] = 'Kayaks';
									
									$_amenities = array();
									
									if(isset($content[0]->amenities)){
										$_amenities = explode(',', $content[0]->amenities);
									}
									$check = '';
									$r = 1;
									$rows = ceil(count($amenities)/3);
									$x = 0;
									echo '<table width="100%">';
									while($r <= $rows){
										echo '<tr>';
										for($i=1; $i<=3; $i++){
											echo '<td width="33%">';
											if(isset($amenities[$x])){
												$value = $amenities[$x];
												
												if(in_array($value, $_amenities)){
													$check = ' checked';
												}else{
													$check = '';
												}
												
											?>
                  <label>
                    <input name="amenities[]" type="checkbox" value="<?php echo $value; ?>" <?php echo $check;?>>
                    <?php echo $value?> </label>
                  <?php
											echo '</td>';
											}
											$x++;
										}
										echo '</tr>';
										$r++;
									}
									echo '</table>';
									?>
                </div>
              </div>
              <h2><span>รายละเอียด</span></h2>
              <div class="form-group">
                <textarea class="form-control summernote" name="detail"><?php if(isset($content[0]->detail)){ echo $content[0]->detail;}?></textarea>
              </div>
              <div class="form-group">
                <label>เงื่อนไข</label>
                <textarea name="conditions" class="form-control"><?php if(isset($content[0]->conditions)){ echo $content[0]->conditions;}?></textarea>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6">
                    <label>ราคา</label>
                    <input name="price" type="text" class="form-control" value="<?php if(isset($content[0]->price)){ echo $content[0]->price;}?>">
                  </div>
                  <div class="col-lg-6">
                    <div id="price_type" style="<?php echo 'display:none;'; ?>">
                      <label>ราคาต่อ</label>
                      <select name="rent_per" class="form-control">
                        <option value="">- เลือก - </option>
                        <option value="day" <?php if(isset($content[0]->rent_per)){ if($content[0]->rent_per == 'day'){ echo ' selected'; }}?>>วัน</option>
                        <option value="week" <?php if(isset($content[0]->rent_per)){ if($content[0]->rent_per == 'week'){ echo ' selected'; }}?>>สัปดาห์</option>
                      </select>
                    </div>
                    <script>
                    	function price_type(val)
											{
												if(val == 'sale'){
													$('#price_type').hide();
													$('#activities').hide();
													$('#amenities').hide();
												}else{
													$('#price_type').show();
													$('#activities').show();
													$('#amenities').show();
												}
											}
											
											price_type($('select[name=want_to]').val());
                    </script> 
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="<?php if(isset($content[0]->email)){ echo $content[0]->email;}else{ echo $member[0]->email;}?>">
                  </div>
                  <div class="col-lg-6">
                    <label>โทรศัพท์</label>
                    <input type="text" name="phone" class="form-control" value="<?php if(isset($content[0]->phone)){ echo $content[0]->phone;}else{ echo $member[0]->phone;}?>">
                  </div>
                </div>
              </div>
              <h2><span>ภาพถ่าย</span></h2>
              <div class="form-group">
                <div class="photos">
                  <?php for($i=0; $i<=9; $i++){?>
                  <div>
                    <label>
                      <?php if($i == 0 ){ echo 'รูปสำคัญ';}else{ echo 'รูปที่ '.$i;}?>
                    </label>
                    <input type="file" name="photo[]" <?php if(empty($content[0]->id)){if($i == 0){ echo 'required';} }?>>
                  </div>
                  <?php }?>
                </div>
              </div>
              <h2><span>แผนที่และตำแหน่ง</span></h2>
              <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '13.7248946';
	$lng = '100.4930264';
	$zoom = 3;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->geo_location_zoom;
}
?>
              <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>ละติจูด</label>
                    <input type="text" name="lat" class="form-control" value="<?php echo $lat;?>" required>
                  </div>
                  <div class="col-sm-6">
                    <label>ลองจิจูด</label>
                    <input type="text" name="lng" class="form-control" value="<?php echo $lng;?>" required>
                  </div>
                </div>
              </div>
              <h2><span>หมายเหตุ </span></h2>
              <div class="form-group">
                <textarea name="note" rows="10" class="form-control"><?php  if(isset($content[0]->note)){ echo $content[0]->note; }?>
</textarea>
              </div>
              <div class="form-group">
                <p class="text-center">
                  <input type="submit" name="save" class="btn btn-lg btn-success" value="<?php if(empty($content[0]->id)){ echo 'ลงประกาศของคุณ';}else{ echo 'อัพเดทประกาศของคุณ';}?>">
                </p>
                <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id; }?>">
                <input type="hidden" name="geo_location" value="<?php if(isset($content[0]->geo_location)){ echo $content[0]->geo_location;}?>">
                <input type="hidden" name="geo_location_zoom" value="<?php if(isset($content[0]->geo_location_zoom)){echo $content[0]->geo_location_zoom;}else{ echo $zoom;}?>">
              </div>
            </form>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script> 
<script>
$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});
$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();
		
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		
		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);
		
		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}


function initMap2(lat, lng, zoom) {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: zoom,
	center: {lat: lat, lng: lng}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: lat, lng: lng}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();
		
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		
		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);
		
		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){
	
	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=geo_location_zoom]').val());
	
	initMap2(lat, lng, zoom)
});

</script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
