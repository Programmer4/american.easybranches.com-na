<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/yachts">เรือยอชท์</a></li>
        <li class="active">
          <?php  echo $content[0]->title; ?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="profile">
            <h2><span>ข้อมูลเรือยอชท์</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อ</label>
                <p><?php echo $content[0]->title;?></p>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-4">
                    <label>ประเภท</label>
                    <p><?php echo $parent_category[0]->category_title;?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>ราคา</label>
                    <p><?php 
										$content[0]->price = str_replace(',','',$content[0]->price);
										$content[0]->price = trim($content[0]->price);
										if($content[0]->price > 0) {echo number_format($content[0]->price);}else{ echo '-';}  ?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>ต้องการ</label>
                    <p><?php echo ucfirst($content[0]->want_to);?></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-4">
                    <label>ที่อยู่</label>
                    <p><?php echo $content[0]->address;?> </p>
                  </div>
                	<div class="col-lg-4">
                    <label>เมือง</label>
                    <p><?php echo $content[0]->city;?></p>
                  </div>
                	<div class="col-lg-4">
                    <label>ตำแหน่ง</label>
                    <p><?php echo $province[0]->title;?></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
									<div class="col-lg-4">
                    <label>รหัสไปรษณีย์</label>
                    <p><?php echo $content[0]->zipcode;?> </p>
                  </div>                

									<div class="col-lg-4">
                    <label>อัเมล</label>
                    <p><?php echo $content[0]->email;?> </p>
                  </div>                
									<div class="col-lg-4">
                    <label>โทรศัพท์</label>
                    <p><?php echo $content[0]->phone;?> </p>
                  </div>                
                </div>
              </div>
              <div class="form-group">
                <label>คำอธิบาย</label>
                <p><?php echo $content[0]->description;?></p>
              </div>
              
              <h2><span>รายละเอียด</span></h2>
              <div class="form-group"> <img src="<?php echo base_url().$content[0]->thumbnail;?>" class="img-responsive mb-15"> <?php echo $content[0]->detail;?> </div>
              
              <?php foreach($photo as $index=>$value){?>
              <div class="form-group"><img src="<?php echo base_url().$value->filepath;?>" class="img-responsive"></div>
              <?php }?>

              <h2><span>เงื่อนไข</span></h2>
              <div class="form-group">  <?php echo nl2br($content[0]->conditions);?> </div>
              
              <?php if($content[0]->status == 1){?>
              <div class="form-group">
                <p class="text-center"> <a href="<?php echo base_url();?>account/yachts/submit?id=<?php echo $content[0]->id;?>" class="btn btn-lg btn-success">อัพเดทประกาศ</a> </p>
              </div>
              <?php }?>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<?php include('tpl.footer.php');?>
