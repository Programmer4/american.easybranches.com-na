<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/classified">อยากซื้อ อยากขาย</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="profile">
            <h2><span>ต้องการขายอะไร</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อสินค้า</label>
                <input type="text" name="title" class="form-control" required value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6">
                    <label>ประเภทสินค้า</label>
                    <select class="form-control" name="category_id" required>
                      <option value=""> - เลือก - </option>
                      <?php
											foreach($category as $index=>$value){

												if(isset($content[0]->id)){
													if($value->category_id == $content[0]->category_id){
														$select = ' selected';
													}else{
														$select = '';
													}
												}

												echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
											}
											?>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label>หมวดหมู่</label>
                    <select class="form-control" name="child_id" required>
                      <option value=""> - เลือก - </option>
                    </select>


                    <script>
										<?php
										if(isset($content[0]->id)){
											$url = base_url().'account/classified/submit/getchild?parent_id='.$content[0]->category_id.'&child_id='.$content[0]->child_id;
										}else{
											$url = base_url().'account/classified/submit/getchild?parent_id= + value';
										}
										?>

										function child(category_id, child_id){

											if(child_id == undefined){
												child_id = 0;
											}

											var url = '<?php echo base_url();?>account/classified/submit/getchild?parent_id=' + category_id + '&child_id=' + child_id;

											$('select[name=child_id]').load(url);
										}

										<?php
										if(isset($content[0]->id)){
											echo 'child('.$content[0]->category_id.', '.$content[0]->child_id.');';
										}else{
											echo 'child(0, 0);';

										}
										?>


										$('select[name=category_id]').change(function(){
											child(this.value, 0);
										});

                    </script>


                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-3">
                  	<label>ที่อยู่</label>
                    <input type="text" name="address" class="form-control" value="<?php if(isset($content[0]->address)){ echo $content[0]->address;}?>">
                  </div>
                	<div class="col-lg-3">
                  	<label>เมือง</label>
                    <input type="text" name="city" class="form-control" value="<?php if(isset($content[0]->city)){ echo $content[0]->city;}?>">
                  </div>
                	<div class="col-lg-3">
                  	<label>จังหวัด</label>
                    <select class="form-control" name="location_id" required>
                      <option value=""> - เลือก - </option>
                      <?php
											$select = '';
                      foreach($province as $index=>$value){
												if(isset($content[0]->location_id)){
													if($content[0]->location_id == $value->location_id){
														$select = ' selected';
													}else{
														$select = '';
													}
												}
												echo '<option value="'.$value->location_id.'"'.$select.'>'.$value->title.'</option>';
											}
											?>
                    </select>
                  </div>
                	<div class="col-lg-3">
                  	<label>รหัสไปรษณีย์</label>
                    <input name="zipcode" class="form-control" type="text" value="<?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>">
                  </div>
                </div>

              </div>
              <div class="form-group">
                <label>คำอธิบายสิ้นค้า</label>
                <textarea class="form-control" name="description" required><?php if(isset($content[0]->description)){ echo $content[0]->description;}?></textarea>
              </div>
              <div class="form-group">
                <label>รายละเอียดสินค้า</label>
                <textarea class="form-control summernote" name="detail"><?php if(isset($content[0]->detail)){ echo $content[0]->detail;}?></textarea>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6">
                    <label>เงื่อนไข</label>
                    <select name="conditions" required class="form-control">
                      <option value=""> - Select - </option>
                      <option value="new"<?php if(isset($content[0]->conditions)){if($content[0]->conditions == 'new'){ echo ' selected';}}?>>New</option>
                      <option value="good"<?php if(isset($content[0]->conditions)){if($content[0]->conditions == 'good'){ echo ' selected';}}?>>Good</option>
                      <option value="used"<?php if(isset($content[0]->conditions)){if($content[0]->conditions == 'used'){ echo ' selected';}}?>>Used</option>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label>ราคา</label>
                    <input name="price" type="text" class="form-control" value="<?php if(isset($content[0]->price)){ echo $content[0]->price;}?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
              	<div class="row">
                	<div class="col-lg-4">
                  	<label>ผู้ขายชื่อ</label>
                    <input type="text" name="contact" class="form-control" value="<?php if(isset($content[0]->contact)){ echo $content[0]->contact;}?>">
                  </div>
                	<div class="col-lg-4">
                  	<label>Email</label>
                    <input type="email" name="email" class="form-control" value="<?php if(isset($content[0]->email)){ echo $content[0]->email;}else{ echo $member[0]->email;}?>">
                  </div>
                	<div class="col-lg-4">
                  	<label>โทรศัพท์</label>
                    <input type="text" name="phone" class="form-control" value="<?php if(isset($content[0]->phone)){ echo $content[0]->phone;}else{ echo $member[0]->phone;}?>">
                  </div>
                </div>
							</div>
              <h2><span>ภาพถ่าย</span></h2>
              <div class="form-group">
                <div class="photos">
                  <?php for($i=0; $i<=9; $i++){?>
                  <div>
                    <label>
                      <?php if($i == 0 ){ echo 'รูปสำคัญ';}else{ echo 'รูปที่ '.$i;}?>
                    </label>
                    <input type="file" name="photo[]" <?php if(empty($content[0]->id)){if($i == 0){ echo 'required';} }?>>
                  </div>
                  <?php }?>
                </div>
              </div>

              <h2><span>แผนที่และตำแหน่ง</span></h2>
              <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '13.7248946';
	$lng = '100.4930264';
	$zoom = 5;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->geo_location_zoom;
}
?>
              <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>ละติจูด</label>
                    <input type="text" name="lat" class="form-control" value="<?php echo $lat;?>" required>
                  </div>
                  <div class="col-sm-6">
                    <label>ลองจิจูด</label>
                    <input type="text" name="lng" class="form-control" value="<?php echo $lng;?>" required>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <p class="text-center">
                  <input type="submit" name="save" class="btn btn-success" value="<?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo 'อัพเดทประกาศ';}?>">
                </p>
                <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id; }?>">
                <input type="hidden" name="geo_location" value="<?php if(isset($content[0]->geo_location)){ echo $content[0]->geo_location;}?>">
                <input type="hidden" name="geo_location_zoom" value="<?php if(isset($content[0]->geo_location_zoom)){echo $content[0]->geo_location_zoom;}else{ echo $zoom;}?>">
              </div>
            </form>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>
$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});
$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}


function initMap2(lat, lng, zoom) {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: zoom,
	center: {lat: lat, lng: lng}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: lat, lng: lng}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){

	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=geo_location_zoom]').val());

	initMap2(lat, lng, zoom)
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>

<?php include('tpl.footer.php');?>
