<h3><span>Post Your Comment</span></h3>
<form id="form-comment" method="post" class="form-horizontal" action="<?php echo base_url()?>comments/submit">
  <div class="alert alert-success" style="display:none;"> Your message has been submit </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Name</label>
    <div class="col-sm-9">
      <input type="text" name="name" class="form-control" placeholder="Your name" required value="<?php if(isset($member[0]->firstname)){ echo $member[0]->firstname . ' '; } if(isset($member[0]->lastname)){ echo $member[0]->lastname; }
			?>">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Email</label>
    <div class="col-sm-9">
      <input type="email" name="email" class="form-control" placeholder="Your Email" required value="<?php if(isset($member[0]->email)){ echo $member[0]->email; } ?>">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-3 control-label">Message</label>
    <div class="col-sm-9">
      <textarea name="message" class="form-control" required placeholder="Your Message"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
      <input type="submit" name="save" class="btn btn-primary btn-block" value="Comment">
      <input type="hidden" name="uri" id="uri" value="<?php echo $uri; ?>">
      <input type="hidden" name="member_id" value="<?php if(isset($member[0]->member_id)){ echo $member[0]->member_id; }?>">
      <input type="hidden" name="comment" value="Comment">
    </div>
  </div>
</form>
<script>
$('#form-comment').validate({
  submitHandler: function(form) {

		$('input[name=save]').attr('disabled','disabled');

    $.ajax({
			type: "POST",
			url: '<?php echo base_url()?>comments/submit',
			data: $("#form-comment").serialize()
		}).done(function(msg){

			if(msg == 'ok'){

				alert('Your comment has been submit but to display your comment in this page we need to reviews it. Thank you');

				$('#form-comment input[name=name]').val('');
				$('#form-comment input[name=email]').val('');
				$('#form-comment textarea[name=message]').val('');
				$('input[name=save]').removeAttr('disabled');
			}
		});
  }
});
</script>
