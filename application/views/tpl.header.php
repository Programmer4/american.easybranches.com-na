<style>
nav {
	background: #4285F4;
	padding-top: 5px;
}
#globalnews > li {
	border-right: solid 1px rgba(255,255,255,0.2);
	padding-right: 10px !important;
}
#globalnews > li > ul > li > a {
	font-size: 11px;
}
#globalnews > li:last-child {
	border: none;
}
#globalnews > li > a {
	font-size: 11px;
	text-transform: uppercase;
	color: #FFF;
}

.dropdown-menu {
	min-width: 200px;
}
.dropdown-menu.columns-2 {
	min-width: 400px;
}
.dropdown-menu.more-sub {
	min-width: 600px;
	/*display: block !important;*/
}

@media (min-width: 1200px) {
	.dropdown-menu.more-sub{
		width: 890px;
		left:-750px;
	}
}

.dropdown-menu li a {
	padding: 5px 15px;
	font-weight: 300;
}
.multi-column-dropdown {
	list-style: none;
  margin: 0px;
  padding: 0px;
}

.multi-column-dropdown li a {
	display: block;
	clear: both;
	line-height: 3;
	color: #333;
	white-space: normal;
}
.multi-column-dropdown li a:hover {
	text-decoration: none;
	background-color: #eee;
}

@media (max-width: 767px) {
	.dropdown-menu.multi-column {
		min-width: 240px !important;
		overflow-x: hidden;
	}
}

@media (max-width: 1199px) {

	.multi-column-dropdown li {
	border-left: solid 1px #eff2f5;
	}

	.more{
		display: none !important;
	}
	.more-sub
	{
		display: block;
		margin-top: -1px !important;
	}
	.more-sub .col-xs-6{
		padding-left: 0;
		padding-right: 0;
	}

	.no-gutters {
	  margin-right: 0;
	  margin-left: 0;

	  > .col,
	  > [class*="col-"] {
	    padding-right: 0;
	    padding-left: 0;
	  }
	}

	.dropdown-menu li a {
		padding: 12px 15px;
	}

	#all-sites-container
	{
		display: none;
	}

 }

.mobile-menu{
	top:95px;
}

.mobile-menu > ul > li.main-item{
	width: 50%;
	float: left;
}

.mobile-menu > ul > li{
	float: left;
}

.global-header
{
	float: left;
	width: 220px;
}

#other-sites
{
	font-size: 11px;
	text-transform: uppercase;
	color: #FFF;
	line-height: 3;
	text-decoration: none;
}

@media (min-width: 1199px) {
	#other-sites-container
	{
		display: none;
	}
}

</style>
<nav></nav>
<script>
$('nav').load('<?php echo base_url();?>nav');
$(document).delegate("#other-sites", "click", function (event) {
	$("#all-sites-container").toggle();
});
</script>
<header>
  <div class="container">
    <div class="global">
      <div class="global-header"> <a href="http://www.easybranches.com" title="" class="logo"> <img src="<?php echo base_url();?>assets/images/logo.png" alt="easybranches News"> </a> </div>
      <div class="global-search hidden-xs hidden-sm hidden-md">
        <form method="get" action="<?php echo base_url();?>search">
          <div class="form-group">
            <input type="text" name="q" placeholder="Search">
          </div>
          <div class="form-group form-submit">
            <button type="submit"><i class="fa fa-fw fa-search"></i></button>
          </div>
        </form>
      </div>
      <div class="global-navigation hidden-xs hidden-sm hidden-md">
        <ul class="nav navbar-nav">
          <li><a href="<?php echo base_url();?>">Home</a></li>

          <li class="main-item"><a href="<?php echo base_url();?>alabama">alabama</a></li>
          <li class="main-item"><a href="<?php echo base_url();?>alaska">alaska</a></li>
          <li class="main-item"><a href="<?php echo base_url();?>nebraska">nebraska</a></li>
          <li class="main-item"><a href="<?php echo base_url();?>arizona">arizona</a></li>
          <li class="main-item"><a href="<?php echo base_url();?>arkansas">arkansas</a></li>
          <li class="main-item"><a href="<?php echo base_url();?>california">california</a></li>

					<li class="dropdown"><a href="<?php echo base_url();?>"  class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">More</a>

						<ul class="dropdown-menu multi-column more-sub">
							<div class="row no-gutters">
								<div class="col-xs-6 col-lg-3">
									<ul class="multi-column-dropdown">
										<li><a href="<?php echo base_url();?>colorado">colorado</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>connecticut">connecticut</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>delaware">delaware</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>florida">florida</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>georgia">georgia</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>hawaii">hawaii</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>idaho">idaho</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>illinois">illinois</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>indiana">indiana</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>iowa">iowa</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>kansas">kansas</a></li>
			              <li role="separator" class="divider"></li>

									</ul>
								</div>
								<div class="col-xs-6 col-lg-3">
									<ul class="multi-column-dropdown">
										<li><a href="<?php echo base_url();?>louisiana">louisiana</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>maine">maine</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>maryland">maryland</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>massachusetts">massachusetts</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>michigan">michigan</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>minnesota">minnesota</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>mississippi">mississippi</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>missouri">missouri</a></li>
			              <li role="separator" class="divider"></li>
			              <li><a href="<?php echo base_url();?>montana">montana</a></li>
			              <li role="separator" class="divider"></li>

										<li><a href="<?php echo base_url();?>nevada">nevada</a></li>
			              <li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>new-hampshire">new hampshire</a></li>
										<li role="separator" class="divider"></li>

									</ul>
								</div>
								<div class="col-xs-6 col-lg-3">
									<ul class="multi-column-dropdown">
										<li><a href="<?php echo base_url();?>new-mexico">new mexico</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>new-york">new york</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>north-carolina">north carolina</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>north-dakota">north dakota</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>ohio">ohio</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>oklahoma">oklahoma</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>oregon">oregon</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>pennsylvania">pennsylvania</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>rhode-island">rhode island</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>south-carolina">south carolina</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>tennessee">tennessee</a></li>
										<li role="separator" class="divider"></li>

									</ul>
								</div>

								<div class="col-xs-6 col-lg-3">
									<ul class="multi-column-dropdown">
										<li><a href="<?php echo base_url();?>utah">utah</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>vermont">vermont</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>virginia">virginia</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>south-dakota">south dakota</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>washington">washington</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>west-virginia">west virginia</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>wisconsin">wisconsin</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>wyoming">wyoming</a></li>
										<li role="separator" class="divider"></li>

										<li><a href="<?php echo base_url();?>kentucky">kentucky</a></li>
			              <li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url();?>new-jersey">new jersey</a></li>
										<li role="separator" class="divider"></li>

										<li><a href="<?php echo base_url();?>texas">texas</a></li>
										<li role="separator" class="divider"></li>
									</ul>
								</div>

							</div>
						</ul>
          </li>

          <li id="sign-account"><a href="<?php echo base_url()?>signin" class="btn btn-primary">Inloggen</a></li>
        </ul>
      </div>
      <div class="global-mobile visible-xs visible-sm visible-md hidden-lg pull-right">
        <ul>
          <li><a href="#" id="mobile-menu-toggle"> <span class="bar"></span> <span class="bar"></span> <span class="bar"></span> </a> </li>
        </ul>
      </div>
    </div>
  </div>
  <script>
$('#sign-account').load('<?php echo base_url();?>sign');
</script>
</header>
