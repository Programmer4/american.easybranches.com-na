<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>แหล่งรวมข้อมูลรายชื่อธุรกิจ รวมรายชื่อโรงงาน รวมข้อมูลบริษัท รวมผู้ประกอบการทุกอุตสาหกรรมทั่วประเทศ</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="แหล่งรวมข้อมูลรายชื่อธุรกิจ รวมรายชื่อโรงงาน รวมข้อมูลบริษัท รวมผู้ประกอบการทุกอุตสาหกรรมทั่วประเทศ ให้บริการค้นหาข้อมูลสินค้าและบริการได้ทันที ทุกรายชื่อธุรกิจได้รับการตรวจสอบยืนยันว่ามีตัวตนจริง" />
<meta name="keywords" content="ธุรกิจ, สมุดหน้าเหลือง, สมุดโทรศัพท์, โรงงาน, โรงพิมพ์, ร้านค้า, สั่งของ, โรงงานผลิต" />
<meta name="fb_title" content="แหล่งรวมข้อมูลรายชื่อธุรกิจ รวมรายชื่อโรงงาน รวมข้อมูลบริษัท รวมผู้ประกอบการทุกอุตสาหกรรมทั่วประเทศ" />
<meta property="og:site_name" content="America.Easybranches.com" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@EasyBranches" />
<meta property="twitter:title" content="แหล่งรวมข้อมูลรายชื่อธุรกิจ รวมรายชื่อโรงงาน รวมข้อมูลบริษัท รวมผู้ประกอบการทุกอุตสาหกรรมทั่วประเทศ" />
<meta property="twitter:description" content="แหล่งรวมข้อมูลรายชื่อธุรกิจ รวมรายชื่อโรงงาน รวมข้อมูลบริษัท รวมผู้ประกอบการทุกอุตสาหกรรมทั่วประเทศ ให้บริการค้นหาข้อมูลสินค้าและบริการได้ทันที ทุกรายชื่อธุรกิจได้รับการตรวจสอบยืนยันว่ามีตัวตนจริง" />
<meta property="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" hreflang="en">

<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<?php

$amp_items[] = '"@context": "http://schema.org"';
$amp_items[] = '"@context": "http://schema.org"';
if(isset($meta_title)){
	$amp_items[] = '"headline": "'.$meta_title.'"';
}
if(isset($meta_image)){
	$amp_items[] = '"image": ["'.$meta_image.'"]';
}

?>
<script type="application/ld+json">{<?php echo implode(',',$amp_items);?>}</script>
<?php include('google.tagmanager.js.php');?>
</head>
<body class="<?php if(isset($module)){ echo $module; }?>">
<?php include('google.tagmanager.html.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <?php if(isset($current_category[0]->category_id)){?>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Home</a></li>
          <li><a href="<?php echo base_url();?>business">Business</a></li>
          <li class="active"><?php echo $current_category[0]->category_title; ?></li>
        </ul>
        <?php }else{?>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Home</a></li>
          <li class="active">Business</li>
        </ul>
        <?php }?>
      </div>
      <div class="col-lg-6">
        <?php 
			if(isset($business['rows'][0]->thumbnail)){
				echo $this->Share->push('http://t.janj.eu/b', base_url().$business['rows'][0]->thumbnail );
			}else{
				echo $this->Share->push('http://t.janj.eu/b', '' );
			}?>
      </div>
    </div>
    <div class="row mt-30 mb-30">
      <div class="col-lg-8">
        <div class="view-type">
          <?php
	if(empty($current_category[0]->category_title)){ 
		echo '<h1>'.number_format($business['items']).'  most interested businesses in Phuket Thailand </h1>'; 
	} else { 
		echo '<h1>'.number_format($business['items']).'  '.$current_category[0]->category_title.' businesses in Phuket Thailand</h1>'; 
	} 
 ?>
          <h2 class="hidden">Promoting your business online in Phuket</h2>
          <ul class="list-inline">
            <li class="active"><a href="<?php echo base_url()?>business"><i class="fa fa-fw fa-list"></i> List</a></li>
            <li><a href="<?php echo base_url()?>business/map"><i class="fa fa-fw fa-map-marker"></i> Map</a></li>
          </ul>
        </div>
        <?php
					if(count($business['rows'])){
						
						$rows = ceil(count($business['rows'])/3);
						
						$r = 1;
						$x = 0;
						while($r <= $rows){
							echo '<div class="row">';
							for($i=1; $i<=3; $i++){
								if(isset($business['rows'][$x])){
									echo '<div class="col-lg-4">';
									
									$value = $business['rows'][$x];
									
								?>
        <div class="business-card">
          <figure class="full event"> <a href="<?php echo base_url();?>business/<?php echo $value->slug;?>">
            <?php if(is_file($value->thumbnail)){?>
            <img src="<?php echo base_url().'resize?image=/'.$value->thumbnail;?>&with=265&height=165&cropratio=2.65:1.65">
            <?php } else{
											echo '<img src="'.base_url().'assets/images/default-event.png">';
									}?>
            </a> </figure>
          <div class="business-card-body"> <a href="<?php echo base_url();?>business/<?php echo $value->slug;?>">
            <h3><?php echo $value->title; ?> </h3>
            </a>
            <h4><small><?php echo $value->location;?></small></h4>
            <p class="overflow ellipsis"><?php echo $value->address . ' '.$value->city. ' '.$value->state.' '.$value->zipcode ;?></p>
          </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url();?>business/c/<?php echo $value->category_slug; ?>" class="overflow ellipsis"><?php echo $value->category_title; ?></a></li>
              <li><a href="#" class="modal-location" data-toggle="modal" data-target="#modal-map" data-location="https://www.google.com/maps/embed/v1/place?key=<?php echo $google_api; ?>&amp;q=<?php echo $value->lat.','.$value->lng;?>"><i class="fa fa-fw fa-map-marker"></i></a></li>
              <li><a href="#" class="modal-email" data-message="Have a look at http://t.janj.eu/b/<?php echo $value->id; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-url="http://t.janj.eu/b/<?php echo $value->id; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
            </ul>
          </div>
        </div>
        <?php
								echo '</div>';
								}
								$x++;
							}
							echo '</div>';
							$r++;
						}
					}
					
					
					if($business['pages'] > 1){
						
						if(isset($current_category[0]->type_id)){
							$url = base_url().'business/c/'.$current_category[0]->slug;
						}else{
							$url = base_url().'business';						
						}
						
						echo '<div class="row">';
						echo '<div class="col-lg-12">';
						echo $this->Paginate->pages($url, $page, $business['pages']);
						echo '</div>';
						echo '</div>';
					}
					
?>
      </div>
      <div class="col-lg-4">
        <div class="property-search">
          <?php include('tpl.form-business.php');?>
        </div>
        <?php include('tpl.news.banner.php');?>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
