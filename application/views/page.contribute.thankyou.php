<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Preview Your Post</title>
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/contribute.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet" hreflang="en">

<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>

</head>

<body>
<br>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-lg-8">
    <div class="card">
      <h1>Thank you for Your Post</h1>
      <p><br>
Your contribute post has been reviewing process on <a href="http://worldnews.easybranches.com" target="_blank">worldnews.easybranches.com</a><br>
<br>
Thanks for your high quality submission. Keep up the awesome work!<br>
<br>
Regards,<br>
Easy Branches Team</p>
    </div>
  </div>
  </div>
</div>
<body>
</body>
</html>
