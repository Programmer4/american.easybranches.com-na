<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Connecting to PayPal</title>
</head>

<body>
<form id="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="cmd" value="_xclick">
  <input type="hidden" name="business" value="<?php echo $paypal;?>">
  <input type="hidden" name="item_name" value="Make Your Property Premium ">
  <input type="hidden" name="quantity" value="1">
  <input type="hidden" name="amount" value="1500">
  <input type="hidden" name="currency_code" value="THB">
  <input type="hidden" name="invoice" value="<?php echo $content[0]->id; ?>">
  <input type="hidden" name="first_name" value="<?php echo $member[0]->firstname; ?>">
  <input type="hidden" name="last_name" value="<?php echo $member[0]->lastname; ?>">
  <input type="hidden" name="address1" value="<?php echo $member[0]->address; ?>">
  <input type="hidden" name="address2" value="">
  <input type="hidden" name="city" value="<?php echo $member[0]->city; ?>">
  <input type="hidden" name="zip" value="<?php echo $member[0]->zipcode; ?>">
  <input type="hidden" name="country" value="<?php echo $member[0]->country;?>">
  <input type="hidden" name="email" value="<?php echo $member[0]->email; ?>" />
  <input type="hidden" name="notify_url" value="<?php echo base_url().'property/paypal/ipn?id=' . $content[0]->id.'&amount=1500';?>">
  <input type="hidden" name="return" value="<?php echo base_url().'thankyou'; ?>">
</form>
<script>
document.getElementById('paypal').submit();
</script>
</body>
</html>
