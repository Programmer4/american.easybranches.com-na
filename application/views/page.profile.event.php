<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li class="active">กิจกรรม</li>
      </ul>
      <br>
      <div class="row">
        <div class="col-lg-6">
          <form method="get" class="form-inline">
            <div class="form-group">
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <input name="q" type="text" class="form-control" placeholder="คำค้นหา" value="<?php echo $searchKeyword; ?>">
              <button name="" type="submit" class="btn btn-search btn-info" > <i class="fa fa-fw fa-search"></i> </button>
              &nbsp; <a href="<?php echo  base_url();?>account/event/submit" class="btn btn-success"> ลงประกาศ </a> </div>
          </form>
        </div>
        <div class="col-lg-6">
          <div class="btn-group pull-right" role="group" aria-label="...">
            <a href="<?php echo base_url()?>account/event?status=1" class="btn btn-default <?php if($status == '1'){ echo 'active';}?>">อนุมัติแล้ว</a>
            <a href="<?php echo base_url()?>account/event?status=0" class="btn btn-default <?php if($status == '0'){ echo 'active';}?>">รอการอนุมัติ</a>
          </div>
        </div>
      </div>
      <br>
      <br>
      <?php

  		if(count($event['rows'])){
  			echo '<br><br>';
  			echo '<div class="profile">';
  			echo '<table class="table">';
  			echo '<thead>';
  			echo '<tr>';
  			echo '<th>ภาพถ่าย</th>';
  			echo '<th>ชื่อกิจกรรม</th>';
        echo '<th>ประเภท</th>';
        echo '<th>ราคา</th>';
  			echo '<th>วันที่เริ่มกิจกรรม</th>';
  			echo '<th>ที่อยู่</th>';
  			echo '<th>เมือง</th>';
  			echo '<th>จังหวัด</th>';
  			echo '</tr>';
  			echo '</thead>';
  			echo '<tbody>';
  			foreach($event['rows'] as $value){

  				$url = base_url().'account/event/preview?id='.$value->id;

  				echo '<tr>';
  				echo '<td><img src="'.base_url().'resize?image=/'.$value->thumbnail.'&width=90&height=90&cropratio=1:1" alt="'.$value->title.'"></td>';
  				echo '<td>'.$value->event_title.'</td>';
  				echo '<td>'. $value->category_title.'</td>';

          if($value->free == 1)
          {
            echo '<td>ฟรี</td>';
          }
          else {
            echo '<td>'. $value->price .'THB</td>';
          }

          echo '<td>'.date("D, d M Y H:i", strtotime($value->time_start)).'</td>';
  				echo '<td>'.$value->address.'</td>';
  				echo '<td>'.$value->city.'</td>';
  				echo '<td>'.$value->province.'</td>';
  				if($status == 1){
  					echo '<td><a href="'.$url.'" class="btn btn-success">Edit</a></td>';
  				}
  				echo '</tr>';
  			}
  			echo '</tbody>';
  			echo '</table>';
  			echo '</div>';

  		}else{
			echo '<br><br><br>';
			echo '<p class="text-center"> ไม่พบข้อมูลกิจกรรมใด ๆ ในโปรไฟล์ของคุณ ลองส่งวันนี้ดูสิ</p>';
			echo '<br><br><br>';
		}

		if($event['pages'] > 1){
      echo $this->Paginate->loadmorestr(base_url().'account/event?status='.$status . ($searchKeyword !="" ? "&q=".$searchKeyword : ""), $page, $event['pages']);
		}
		?>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
