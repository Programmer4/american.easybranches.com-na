<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <p class="footer-headline">Latest News</p>
        <?php
				$rs = $this->News->get_latest_10();
				echo '<ul class="list-unstyled">';
				foreach($rs as $index=>$value){
					echo '<li class="overflow ellipsis"><a href="'.base_url().urlencode($value->category_slug).'/'.urlencode($value->slug).'">'.$value->title.'</a></li>';
				}
				echo '</ul>';
				?>
      </div>
      <div class="col-md-4">
        <p class="footer-headline">Massachusetts</p>
        <?php
				$rs = $this->News->get_category(1, 22);
				echo '<ul class="list-unstyled">';
				$n = 0;
        $total = 10;
        if(count($rs['rows']) <= 10){
          $total = count($rs['rows']) - 1;
        }

				while($n <= $total){
					$value = $rs['rows'][$n];
					echo '<li class="overflow ellipsis"><a href="'.base_url().urlencode($value->category_slug).'/'.urlencode($value->slug).'">'.$value->title.'</a></li>';
					$n++;
				}
				echo '</ul>';
				?>
      </div>
      <div class="col-md-4">
        <p class="footer-headline">Connect on Social Media with Easy Branches Network in the America News</p>
        <ul class="list-inline socail-footer">
          <li> <a target="_blank" href="http://foursquare.com/user/7932491" title="Foursquare Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-foursquare.png" alt="Foursquare Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://twitter.com/easyjanjansen" title="Twitter Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-twitter.png" alt="Twitter Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://plus.google.com/113570697076738285616" title="Google+ Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-google-plus-gray.png" alt="Google+ Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://plus.google.com/114119767742293202325" title="Google+_Page Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-google-plus-red.png" alt="Google+_Page Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://klout.com/easyjanjansen" title="Klout Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-klout.png" alt="Klout Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.facebook.com/authorjanjansen" title="Facebook Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-facebook.png" alt="Facebook Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.facebook.com/EasyBranchesCompany/" title="Facebook_Fanpage Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-facebook-fanpage.png" alt="Facebook_Fanpage Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="http://home.kred/easy_branches" title="Kred Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-kr.png" alt="Kred Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.linkedin.com/in/easybranches" title="Linkedin Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-linkedin.png" alt="Linkedin Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.instagram.com/authorjanjansen/" title="Instragram Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-instagram.png" alt="Instragram Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.flickr.com/people/janjanseneasybranches/" title="Flickr Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-flickr.png" alt="Flickr Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.pinterest.com/easybranches/" title="Pinterest Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-pinterest.png" alt="Pinterest Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://www.youtube.com/user/Easybranches/videos" title="Youtube Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-youtube.png" alt="Youtube Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="https://easybranchesnetwork.wordpress.com/" title="Wordpress Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-wordpress.png" alt="Wordpress Easy Branches Network"> </a> </li>
          <li> <a target="_blank" href="http://blogger.com/profile/01197246479391475227" title="Blogger Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-bloger.png" alt="Blogger Easy Branches Network"> </a> </li>
          <?php
					/*
					<li> <a target="_blank" href="<?php echo base_url();?>rss" title="Blogger Easy Branches Network"> <img src="<?php echo base_url();?>assets/images/icon-rss.png" alt="Rss Easy Branches Network"> </a> </li>
					*/?>
        </ul>
        <p class="footer-headline">EASY BRANCHES NETWORK REAL-TIME PAGE VISITORS 2016<br>Since 16 december 2016</p>
       <div id="visitors">
          <iframe src="http://visitors.easybranches.com/iframe" frameborder="0" style="height:60px;"></iframe>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p class="footer-headline">Easy News</p>
        <ul id="easy-news" class="list-inline">
         	<li><a href="http://worldnews.easybranches.com/" target="_blank">Daily World News</a></li>
         	<li><a href="http://www.america.easybranches.com/" target="_blank">America News</a></li>
         	<li><a href="http://www.easybranches.nl/" target="_blank">Nederlands News</a></li>
         	<li><a href="http://america.easybranches.com" target="_blank">U.S.A. News</a></li>
         	<li><a href="http://www.easybranches.asia/" target="_blank">中国新闻</a></li>
         	<li><a href="http://www.easybranches.biz/" target="_blank">أخبار موقع</a></li>
         	<li><a href="http://www.thainews.easybranches.com/" target="_blank">ข่าวไทย</a></li>
         	<li><a href="http://football-soccer.easybranches.com/" target="_blank">Football News</a></li>
          <li><a href="http://football-soccer.easybranches.com/th" target="_blank">ข่าวฟุตบอล</a></li>
         </ul>
      </div>
    </div>
    <div id="footer-about">
      <div class="row">
        <div class="col-md-12">
            <ul class="list-inline">
              <li><a href="http://www.easybranches.com" target="_blank">©<?php echo date('Y');?> Easy Branches.</a></li>
              <li><a href="http://www.easybranches.com" target="_blank">Create & Develop by Easy Branches Team</a> - <a href="http://www.janjansen.asia/" target="_blank">Jan Jansen</a></li>
              <li><a href="<?php echo base_url()?>contribute" target="_blank">Create Your Ads</a></li>
              <!--<li><a href="<?php echo base_url()?>contribute" target="_blank">ติดต่อโฆษณา</a></li>-->
              <li><a href="<?php echo base_url()?>about">About</a></li>
              <li><a href="<?php echo base_url()?>terms">Terms & Privacy</a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<div id="modal-map" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Location</h4>
      </div>
      <div class="modal-body">
        <iframe
  width="100%"
  height="450"
  frameborder="0" style="border:0" allowfullscreen> </iframe>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
<form id="share-email-modal" method="post" action="<?php echo base_url().'email/share/send';?>" class="validate">
  <div id="modal-email" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Share to Email</h4>
        </div>
        <div class="modal-body">
          	<div class="alert alert-success"<?php echo ' style="display:none;"';?>>Your message has been submit</div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-smile-o"></i></span>
                <input type="email" name="email" class="form-control input-lg" placeholder="Your Friend address" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-envelope-o"></i></span>
                <input type="text" name="subject" class="form-control input-lg" placeholder="Your Email address" value="Hi, I Shared Something to You!" required>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-comment-o"></i></span>
                <input type="text" name="message" class="form-control input-lg" placeholder="Your Message" value="" required>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-block btn-primary btn-lg">Share to Email</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
$('#share-email-modal').submit(function(){
	$('#share-email-moda button[type=submit]').attr('disabled','disabled');
	$.ajax({
		type: "POST",
		url: $(this).attr('action'),
		data: $(this).serialize()
	}).done(function(msg){
		if(msg == 'ok'){
			$('#share-email-modal .alert').show();
			$('#share-email-modal button[type=submit]').removeAttr('disabled');
		}
	});
	return false;
});

$('.btn-flag').click(function(){
	var number = $(this).attr('data-value');
	var flag = '<?php echo base_url()?>assets/flags/4x3/'+ $(this).attr('data-flag') + '.svg';
	$('input[name=areacode]').val(number);
	$('#areacode').html(number);
	$('img.flag-selected').attr('src', flag);
});
</script>
<div id="modal-share" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Share to friends or on Social Media</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group"> <br>
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-link"></i></span>
              <input type="text" id="share-url" class="form-control input-lg" value="" readonly>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="text-center">
          <?php
					echo $this->Share->all('','');
					?>
        </div>
      </div>
    </div>
  </div>
</div>


<script src="<?php echo base_url();?>assets/js/jquery.lazyload.min.js"></script>
<script>
  $(function() {
      $("img.lazy").lazyload({
      	threshold : 200
  		});
  });
</script>
<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
<script src="http://visitors.easybranches.com/track"></script>
</body>
</html>
