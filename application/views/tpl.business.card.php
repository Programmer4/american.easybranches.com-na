<?php
$url = 'http://t.janj.eu/b/'.urlencode($value->id);

$meta = json_decode($value->meta,true);

echo '<div class="business-card">';
if(count($meta)){
	if($meta['StarRating']){
		$star = $meta['StarRating'];
		echo '<div class="meta-data">';
		echo '<img src="'.base_url().'assets/images/icon-star-'.$star.'.png">';
		echo '</div>';
	}
}
echo '<figure>';
echo '<a href="'.base_url().'business/'.$value->slug.'">';
echo '<img src="'.base_url().'resize/?image=/'.$value->thumbnail.'&with=800&height=495&cropratio=8:4.95" alt="'.$value->title.' - Phuket Business Directory">';
echo '</a>';
echo '</figure>';

echo '<div class="business-card-body">';
echo '<a href="'.base_url().'business/'.$value->slug.'" class="overflow ellipsis"><h3>'.$value->title.'</h3></a>';
echo '<h4><small>'.$value->location.'</small></h4>';
echo '<p>'.$value->address. ' '.$value->city. ' '.$value->state.' '.$value->zipcode.'</p>';
echo '</div>';
echo '<div class="business-card-footer">';
echo '<ul class="list-inline">';
echo '<li class="pull-left"><a href="'.base_url().'business/c/'.$value->category_slug.'">'.$value->category_title.'</a></li>';
echo '<li><a href="#" class="modal-location" data-toggle="modal" data-target="#modal-map" data-location="https://www.google.com/maps/embed/v1/place?key='.$google_api.'&q='.$value->lat.','.$value->lng.'"><i class="fa fa-fw fa-map-marker"></i></a></li>';
echo '<li><a href="#" class="modal-email" data-message="Have a look at '.$url.'" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>';
echo '<li><a href="#" class="share-url" data-image="'.base_url().$value->thumbnail.'" data-toggle="modal" data-target="#modal-share" data-url="'.$url.'"><i class="fa fa-fw fa-share-alt"></i></a></li>';
echo '<li></li>';
echo '</ul>';
echo '</div>';
echo '</div>';
?>