<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Be a Contributor and Contributor Your content Post as a Journalist</title>
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/contribute.css" rel="stylesheet">
</head>

<body>
<br>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <h1>Contribute Your Post in Easy Branches Network</h1>
        <h2>Easy Branches allows you to share your post with our network in any country of the world.</h2>
        <div class="benefit">
            <h3>Share news, opinion and stories from your community</h3>
            <h4>Tell other people in your community about events happening</h4>
        </div>
        <ul class="list-inline">
          <li><a href="<?php echo base_url(); ?>contribute/post" class="btn btn-lg btn-success">Contribute Your Post</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</body>
</html>
