<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url();?>movies">Movies</a></li>
        <li class="active"><?php echo $content[0]->title;?></li>
      </ul>
      <div class="row">
        <div class="col-lg-8">
          <div id="story">
            <!--<div id="heading">
              <h1><?php echo $content[0]->title;?></h1>
              <div class="entered">
                <?php
									echo '<div class="row">';
									echo '<div class="col-sm-5">';

									echo '</div>';
									echo '<div class="col-sm-7">';
?>
                <?php echo $this->Share->push(base_url().'movies/'.$content[0]->slug, base_url().$content[0]->thumbnail)?>
                <?php
									echo '</div>';
									echo '</div>';
					?>
              </div>
            </div>-->
            <div id="jwplayer"></div>
            <?php
            $link = json_decode($content[0]->link,true);
						?>
            <script>
var playerInstance = jwplayer("jwplayer")
playerInstance.setup({
	image:'<?php echo $content[0]->thumbnail;?>',
	file: "<?php echo $link[1]['@attributes']['href'];?>",
	width: "100%",
	aspectratio: "16:9",
	autostart: true
});

</script>
            <div id="story-text">
              <table width="100%" border="0">
                <tbody>
                  <tr>
                    <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                          <tr valign="top" align="left">
                            <td width="120" valign="top" align="center"><a href="https://itunes.apple.com/th/movie/minions/id991371510?uo=2"><img alt="<?php echo $content[0]->title; ?>" src="<?php echo $content[0]->thumbnail;?>" border="0"></a></td>
                            <td width="10"><img alt="" src="https://s.mzstatic.com/images/spacer.gif" width="10" height="1"></td>
                            <td><b><a href="<?php echo $content[0]->id;?>"><?php echo $content[0]->title;?></a></b><br>
                              <?php echo $content[0]->imartist ;?> <font size="2" face="Helvetica,Arial,Geneva,Swiss,SunSans-Regular"> <br>
                              <b>Genre:</b>
                              <?php
														$category = json_decode($content[0]->category,true);

														echo $category['@attributes']['term'];


														?>
                              <br>
                              <b>Price:</b> <?php echo $content[0]->imprice;?> <br>
                              <?php if($content[0]->imrentalPrice){?>
                              <b>Rental Price:</b> <?php echo $content[0]->imrentalPrice ;?> <br>
                              <?php }?>
                              <b>Release Date:</b> <?php echo date('d F Y', strtotime($content[0]->imreleaseDate));?></font></td>
                            <td width="10">&nbsp;</td>
                            <td><?php echo $this->Share->push(base_url().'movies/'.$content[0]->slug, base_url().$content[0]->thumbnail)?></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                  <tr>
                    <td><font size="2" face="Helvetica,Arial,Geneva,Swiss,SunSans-Regular"><br>
                    <?php echo $content[0]->summary;?></font><br>
                      <font size="2" face="Helvetica,Arial,Geneva,Swiss,SunSans-Regular"> &copy; &copy; 2015 Universal Studios. All Rights Reserved.</font></td>
                  </tr>
                </tbody>
              </table>
               <br>
              <p class="text-center"> <a href="<?php echo $content[0]->id?>" class="btn btn-lg btn-success" target="_blank">Buy now from App Store</a> </p>
            </div>
          </div>
          <div id="comments">
            <p class="text-center"> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <br>
              <span>Loading...</span> <br>
              <br>
            </p>
          </div>
          <div id="comments-form">
            <p class="text-center"> <br>
              <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <br>
              <span>Loading...</span> <br>
              <br>
            </p>
          </div>
          <script>

				$('#comments').load('<?php echo base_url().'comments?uri='.urlencode('movies/'.$content[0]->slug);?>&page=1');
				$('#comments-form').load('<?php echo base_url().'comments/form?uri='.urlencode('movies/'.$content[0]->slug);?>');
        </script>

          <br>
          <br>
          <br>
          <?php


$zinio_arr[] = '<a href="http://www.dpbolvw.net/click-8348672-11116049" target="_blank">
<img src="http://www.lduhtrp.net/image-8348672-11116049" class="mobile-responsive" alt="US Weekly" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.tkqlhce.com/click-8348672-11013119" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-11013119" class="mobile-responsive"  alt="Save big on top tech magazines.   " border="0"/></a>';

$zinio_arr[] = '<a href="http://www.kqzyfj.com/click-8348672-10999522" target="_blank">
<img src="http://www.lduhtrp.net/image-8348672-10999522" class="mobile-responsive"  alt="Daily Deal" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.kqzyfj.com/click-8348672-11116048" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-11116048" class="mobile-responsive" alt="Businessweek" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.kqzyfj.com/click-8348672-12260829" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-12260829" class="mobile-responsive" alt="" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.kqzyfj.com/click-8348672-11164490" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-11164490" class="mobile-responsive" alt="" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.anrdoezrs.net/click-8348672-11116063" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-11116063" class="mobile-responsive" alt="Harvard Business Review" border="0"/></a>';

$zinio_arr[] = '<a href="http://www.kqzyfj.com/click-8348672-11116059" target="_blank">
<img src="http://www.tqlkg.com/image-8348672-11116059" class="mobile-responsive" alt="Popular Science" border="0"/></a>';

echo '<p class="text-center">';
echo $zinio_arr[rand(0,count($zinio_arr)-1)];
echo '</p>';

?>
        </div>
        <div class="col-lg-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.banner.php');?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
