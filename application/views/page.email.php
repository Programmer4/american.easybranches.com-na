<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Share</title>
<link href="<?php echo base_url();?>assets/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
</head>

<body>
<form id="share-email-modal" method="post" action="<?php echo base_url().'email/share/send';?>" class="validate">
  <div id="modal-email" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Deel via email</h4>
        </div>
        <div class="modal-body">

        	<div class="alert alert-success"<?php echo ' style="display:none;"';?>>
          	Uw bericht is ingediend
          </div>

          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-smile-o"></i></span>
              <input type="email" name="email" class="form-control input-lg" placeholder="Uw vriendadres" value="" required>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-envelope-o"></i></span>
              <input type="text" name="subject" class="form-control input-lg" placeholder="Your Email address" value="Hallo, ik heb iets met je gedeeld!" required>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-comment-o"></i></span>
              <input type="text" name="message" class="form-control input-lg" placeholder="Bericht" value="Less Meer <?php echo $url; ?>" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-block btn-primary btn-lg">Deel via email</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
$('#share-email-modal').submit(function(){
	$('#share-email-moda button[type=submit]').attr('disabled','disabled');
	$.ajax({
		type: "POST",
		url: $(this).attr('action'),
		data: $(this).serialize()
	}).done(function(msg){
		if(msg == 'ok'){
			$('#share-email-modal .alert').show();
			$('#share-email-modal button[type=submit]').removeAttr('disabled');
		}
	});
	return false;
});
</script>
</body>
</html>
