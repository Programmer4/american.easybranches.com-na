<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Account</a></li>
        <li><a href="<?php echo base_url(); ?>account/news">News</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'Compose';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <?php
			if($content[0]->status == 0){
				echo '<div class="alert alert-danger text-center">';
				echo 'This story is under reviewing process. You will get inform when approved';
				echo '</div>';
			}else{
				echo '<div class="alert alert-success text-center">';
				echo 'This story has been published';
				echo '</div>';
			}
			?>
          <div class="profile">
            <h2><span>Headline</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>Title</label>
                <p><?php echo $content[0]->title;?></p>
              </div>
              <div class="form-group">
                <label>Category</label>
                <p><?php echo $category[0]->category_title;?></p>
              </div>
              <div class="form-group">
                <label>Description</label>
                <p><?php echo $content[0]->description;?></p>
              </div>
              <div class="form-group">
                <label>Keywords</label>
                <p><?php echo $content[0]->keywords;?></p>
              </div>
              <div class="form-group">
                <label>Hashtags</label>
                <p><?php echo $content[0]->hashtags;?></p>
              </div>
              <h2><span>Detail Text</span></h2>
              <div class="form-group"> <img src="<?php echo base_url().$content[0]->thumbnail;?>" class="img-responsive mb-15"> <?php echo $content[0]->detail;?> </div>
              <?php if($content[0]->status == 1){?>
              <div class="form-group">
                <p class="text-center"> <a href="<?php echo base_url();?>account/news/submit?id=<?php echo $content[0]->id;?>" class="btn btn-success">Update Your Story</a> </p>
              </div>
              <?php }?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>
$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});
$('.validate').validate();
</script>
<?php include('tpl.footer.php');?>
