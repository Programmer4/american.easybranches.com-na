<?php echo '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:georss="http://www.georss.org/georss" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:media="http://search.yahoo.com/mrss/"
	>'; ?>
<channel>
<title>easybranches News - Easy Branches</title>
<atom:link href="<?php echo base_url(); ?>rss/<?php echo $slug;?>" rel="self" type="application/rss+xml" />
<link>
<?php echo base_url();?>
</link>
<description>
easybranches News Phuket's people speak daily online local Phuketnews, mice events, classifieds, business directory hotels, resorts, properties restaurants
</description>
<lastBuildDate><?php echo date('D, d M Y H:i:s');?>+0000</lastBuildDate>
<language>en</language>
<sy:updatePeriod>hourly</sy:updatePeriod>
<sy:updateFrequency>1</sy:updateFrequency>
<generator><?php echo base_url();?></generator>
<image>
  <url><?php echo base_url();?>assets/images/logo.png</url>
  <title>easybranches News</title>
  <link>
  <?php echo base_url();?>
  </link>
</image>
<?php
foreach($item['rows'] as $value){
?>
<item>
<title><?php echo htmlspecialchars($value->title);?></title>
<link>
<?php echo base_url().$value->category_slug;?>/<?php echo $value->slug; ?>
</link>
<pubDate><?php echo date('D, d M Y H:i:s',strtotime($value->updated));?> +0000</pubDate>
<dc:creator>
  <![CDATA[<?php echo $value->editor;?>]]>
</dc:creator>
<?php
	$keyword = explode(',', $value->keywords);
	foreach($keyword as $val){

?>
<category>
  <![CDATA[<?php echo trim($val); ?>]]>
</category>
<?php }?>
<guid isPermaLink="false"><?php echo base_url().$value->category_slug.'/'.$value->slug;?></guid>
<description>
  <![CDATA[<?php

	if(is_file($value->thumbnail)){
		echo '<img src="'.base_url().$value->thumbnail.'">';
	}
	echo '<p>';
	echo strip_tags($value->description);
	echo '</p>';
	?>]]>
</description>
	<?php if(is_file($value->thumbnail)){?>
<media:thumbnail url="<?php echo base_url().$value->thumbnail;?>" />
	<?php }?>
</item>
<?php
}
?>

</channel>
</rss>
