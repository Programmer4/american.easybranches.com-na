<form id="form-interest" method="post" action="<?php echo $action; ?>">
  <div class="modal fade" id="RegisterInterest" tabindex="-1" role="dialog" aria-labelledby="RegisterInterest">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">
            <?php if(isset($content[0]->title)){echo $content[0]->title; }?>
          </h4>
        </div>
        <div class="modal-body">

        	<div id="form-interest-confirm" class="alert alert-success" style="display:none">
          	Your message has been send already.
          </div>

        	<div id="form-interest-error" class="alert alert-danger" style="display:none">
          	Your message could not be send. Please check the form below.
          </div>

          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-envelope"></i></span>
              <input type="email" name="email" class="form-control input-lg" placeholder="Your Email address" value="<?php if(isset($member[0]->email)){ echo $member[0]->email; }?>">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
              <input type="text" name="name" class="form-control input-lg" placeholder="Your Name" value="<?php
							if(isset($member[0]->firstname)){
								echo $member[0]->firstname;
							}
							if(isset($member[0]->lastname)){
								echo ' ';
								echo $member[0]->lastname;
							}
							?>">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group"> <span class="input-group-addon flag">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img src="<?php
							if(isset($member_areacode[0]->code)){
								echo base_url().'assets/flags/4x3/'.$member_areacode[0]->code.'.svg';
							}else{
								echo base_url().'assets/flags/4x3/th.svg';
							}
							?>" width="32" class="flag-selected"> <span id="areacode">(+
              <?php
							if(isset($member_areacode[0]->area)){
								echo $member_areacode[0]->area;
							}else{
								echo '66';
							}
							?>
              )</span> <span class="caret"></span> </button>
              <ul class="dropdown-menu">
                <?php
								foreach($areacode as $index=>$value){
									echo '<li><a href="javascript:void(0);" data-value="+'.$value->area.'" data-flag="'.$value->code.'" class="btn-flag">';
									echo '<img src="'.base_url().'assets/flags/4x3/'.$value->code.'.svg" width="32" class="flag">';
									echo $value->country.' <span>(+'.$value->area.')</span></a></li>';
								}
								?>
              </ul>
              </span>
              <input type="text" name="phone" class="form-control input-lg" placeholder="Your Phone Number" value="<?php if(isset($member[0]->phone)){ echo $member[0]->phone;}?>">
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control input-lg" name="message" placeholder="ข้อความ"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success btn-block btn-lg">Register Interest</button>
          <input type="hidden" name="areacode" value="+<?php if(isset($member_areacode[0]->area)){ echo $member_areacode[0]->area; }else{ echo '66';}?>">
          <input type="hidden" name="id" value="<?php echo $content[0]->id; ?>">
        </div>
      </div>
    </div>
  </div>
</form>
