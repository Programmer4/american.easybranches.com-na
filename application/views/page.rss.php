<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="cate-list" style="height:auto; margin-top: 15px;">
            <div>
              <h1>easybranches News Rss</h1>
              Subscribe to EasyBranches's RSS (Really Simple Syndication) feeds to get news delivered directly to your desktop! <br>
              <br>
              <b>To view one of the Easy Branches feeds in your RSS Aggregator</b> (<a href="javascript:EasyBranches_openPopup('/services/rss/about/where.exclude.html','620x430','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=620,height=430')">About RSS Aggregators</a>): <br>
              <div style="padding-left:10px;"> 1.	Copy the URL/shortcut that corresponds to the topic that interests you.<br>
                2.	Paste the URL into your reader.<br>
              </div>
              <br>
              <b>My Yahoo! users:</b> <br>
              <div style="padding-left:10px;">1.	Click on the "Add to My Yahoo!" button.<br>
                2.	Follow the instructions for adding the feed to your My Yahoo! page.</div>
              <br>
              <span style="color:#990000;font-weight:bold;">NEW:</span> EasyBranches.com now offers <b>podcasting feeds</b>. <br>
              <br>
              <div style="font-family:Arial;font-size:10px;">Please note that by accessing Easy Branches RSS feeds, you agree to our <a href="<?php echo base_url();?>terms">terms of use</a>.</div>
              <br>
              <b><a href="javascript:EasyBranches_openPopup('/services/rss/about/whatis.exclude.html','620x430','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=620,height=430')">What is RSS?</a> | <a href="javascript:EasyBranches_openPopup('/services/rss/about/where.exclude.html','620x430','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=620,height=430')">How do I access RSS?</a></b>
              <ul class="list-unstyled">
                <li><a href="<?php echo base_url();?>news/rss/phuket" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Easybranches News Rss</a></li>
                <li><a href="<?php echo base_url();?>news/rss/" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Politics News Rss</a></li>
                <li><a href="<?php echo base_url();?>news/rss/sport" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Sport News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/entertainment" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Entertainment News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/lifestyle" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Lifestyle News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/business" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Business News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/thailand" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Thailand News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/buitenland" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Buitenland News</a></li>
                <li><a href="<?php echo base_url();?>news/rss/daily-poetry" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Daily Poetry</a></li>
                <li><a href="<?php echo base_url();?>business/rss" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Phuket Business Rss</a></li>
                <li><a href="<?php echo base_url();?>property/rss" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Phuket Property Rss</a></li>
                <li><a href="<?php echo base_url();?>event/rss" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Phuket Events Rss</a></li>
                <li><a href="<?php echo base_url();?>yachts/rss" target="_blank"> <i class="fa fa-fw fa-rss text-warning"></i> Phuket Yachts Rss</a></li>
              </ul>
              <br>
              <br>
              <a name="terms"></a>
              <div class="EasyBranchesMemberServicesRedHead">TERMS OF USE</div>
              RSS (really simple syndication) service is a means by which EasyBranches.com offers feeds of story headlines in XML format ("RSS Content") to visitors to EasyBranches.com (the "Easy Branches Site") who use RSS aggregators. These Terms of Use govern your use of the RSS service. The use of the RSS service also is subject to the terms and conditions of the <a href="<?php echo base_url()?>terms">Easy Branches Interactive Service Agreement</a>, which governs the use of EasyBranches's websites, information services and content. These Terms of Use and the Easy Branches Interactive Service Agreement may be changed by Easy Branches at any time without notice. <br>
              <br>
              <b>Use of RSS Feeds:</b><br>
              <div style="padding-left:10px;"> RSS is a free service offered by Easy Branches for non-commercial use. Any other uses, including without limitation the incorporation of advertising into or the placement of advertising associated with or targeted towards the RSS Content, are strictly prohibited. You must use the RSS feeds as provided by EasyBranches, and you may not edit or modify the text, content or links supplied by EasyBranches.  For web posting, reprint, transcript or licensing requests for Easy Branches material. </div>
              <br>
              <b>Link to Content Pages:</b><br>
              <div style="padding-left:10px;"> The RSS service may be used only with those platforms from which a functional link is made available that, when accessed, takes the viewer directly to the display of the full article on the Easy Branches Site. You may not display the RSS Content in a manner that does not permit successful linking to, redirection to or delivery of the applicable Easy Branches Site web page. You may not insert any intermediate page, splash page or other content between the RSS link and the applicable Easy Branches Site web page. </div>
              <br>
              <b>Ownership/Attribution:</b><br>
              <div style="padding-left:10px;"> Easy Branches retains all ownership and other rights in the RSS Content, and any and all Easy Branches logos and trademarks used in connection with the RSS Service. You must provide attribution to the appropriate Easy Branches website in connection with your use of the RSS feeds. If you provide this attribution using a graphic, you must use the appropriate Easy Branches website's logo that we have incorporated into the RSS feed. </div>
              <br>
              <b>Right to Discontinue Feeds:</b><br>
              <div style="padding-left:10px;"> Easy Branches reserves the right to discontinue providing any or all of the RSS feeds at any time and to require you to cease displaying, distributing or otherwise using any or all of the RSS feeds for any reason including, without limitation, your violation of any provision of these Terms of Use. Easy Branches assumes no liability for any of your activities in connection with the RSS feeds or for your use of the RSS feeds in connection with your website. </div>
              <br>
              Subscribe to EasyBranches's RSS (Really Simple Syndication) feeds to get news delivered directly to your desktop!

              To view one of the Easy Branches feeds in your RSS Aggregator (About RSS Aggregators):
              1. Copy the URL/shortcut that corresponds to the topic that interests you.
              2. Paste the URL into your reader.

              My Yahoo! users:
              1. Click on the "Add to My Yahoo!" button.
              2. Follow the instructions for adding the feed to your My Yahoo! page. </div>
          </div>
        </div>
        <div class="col-md-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.banner.php');?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
