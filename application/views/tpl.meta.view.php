<?php
if(isset($meta_title)){
	$meta_title = str_replace('"',"", $meta_title);
}else{
	$meta_title = 'easybranches news, Phuket\'s people speak daily online local Phuketnews, mice events, mclassifieds, business directory hotels, resorts, properties restaurants';
}
if(isset($meta_description)){
	$meta_description = trim(preg_replace('/\s\s+/', ' ', $meta_description));
	$meta_description = substr($meta_description,0,150);
}else{
	$meta_description = 'easybranches news - Phuket\'s news online, with daily easybranches news, Phuket events, Phuket classifieds, Phuket business directory hotels, Phuket resorts, Phuket restaurants';
}

if(isset($meta_keywords)){
	$meta_keywords = str_replace('"',"", $meta_keywords);
}else{
	$meta_keywords = 'Phuket, news, events, classifieds, jobs, businesses, gazette, hotels, resorts, Thailand, travel, holiday, weather, accommodation, spa, villas, tours, tour, island, car rental, marinas, property, golf, restaurant, bar, bars, diving, real estate, bungalows,floods, flood, gazette, tv, weather, stabbing, accident, today, now, daily, latest, Chinese, Thai, language, new, year, city, police, station, real, time, updates, update, eyewitness, reports, report, trusted, fastest, web, online, around, island, foreign, english, headlines, popular, destinations, destination, breaking, tourist, tourisme, source, sources, piracy, high, sea, seas, pirate, pirates, plague waters, south, water, intellectual, rise, hijack, oil, steal, recounts, recount, ordeal, capital, southeast, asia, learn, learning, malaysia, shipping, ship';
}

#$meta_title = substr($meta_title,0,60);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php echo $meta_description?>" />
<meta name="keywords" content="<?php echo $meta_keywords;?>" />
<meta name="news_keywords" content="<?php echo $meta_keywords; ?>" />
<meta name="fb_title" content="<?php echo $meta_title?>" />
<meta property="og:site_name" content="phuketnews.easybranches.com" />
<meta property="og:title" content="<?php echo $meta_title;?>" />
<meta property="og:description" content="<?php echo $meta_description;?>" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@EasyBranches" />
<meta property="twitter:title" content="<?php echo $meta_title;?>" />
<meta property="twitter:description" content="<?php echo $meta_description; ?>" />
<meta property="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/flag-icon.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" hreflang="en">
<?php
if(isset($module)){
	if($module == 'home'){?>
<link href="<?php echo base_url();?>assets/css/home.css" rel="stylesheet" hreflang="en">
<?php
	}
}
?>
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>assets/jwplayer-7.9.1/jwplayer.js"></script>
<script>jwplayer.key="fVMElcMwXIEFkRobACNvv6mrYcBJV5eek3Rwjg==";</script>
<?php

$amp_items[] = '"@context": "http://schema.org"';
$amp_items[] = '"@context": "http://schema.org"';
if(isset($meta_title)){
	$amp_items[] = '"headline": "'.$meta_title.'"';
}
if(isset($meta_image)){
	$amp_items[] = '"image": ["'.$meta_image.'"]';
}

?>
<script type="application/ld+json">{<?php echo implode(',',$amp_items);?>}</script>

<?php include('google.tagmanager.js.php');?>

</head>
<body class="<?php if(isset($module)){ echo $module; }?>">
<?php include('google.tagmanager.html.php');?>
