<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php if(isset($meta_title)){ echo $meta_title;}?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php echo $meta_description?>" />
<meta name="keywords" content="<?php echo $meta_keywords;?>" />
<meta name="news_keywords" content="<?php echo $meta_keywords; ?>" />
<meta name="fb_title" content="<?php echo $meta_title?>" />
<meta property="og:site_name" content="America.Easybranches.com" />
<meta property="og:title" content="<?php echo $meta_title;?>" />
<meta property="og:description" content="<?php echo $meta_description;?>" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@EasyBranches" />
<meta name="twitter:title" content="<?php echo $meta_title;?>" />
<meta name="twitter:description" content="<?php echo $meta_description; ?>" />
<meta name="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>" rel="alternate" hreflang="nl">
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<?php include('google.tagmanager.js.php');?>
</head>
<body class="home">
<?php include('google.tagmanager.html.php');?>
<?php include('tpl.header.php');?>
<section>
  <div class="container">
    <div class="row section-heading">
      <div class="col-lg-8">
        <h1>Latest America updates</h1>
        <h2>America News International Breaking Newest Information</h2>
      </div>
      <div class="col-lg-4"> <?php echo $this->Share->push(base_url(),base_url().$news[0]->thumbnail);?> </div>
    </div>
    <div class="row">
      <div class="col-lg-8">
        <?php
			if(isset($news)){

				echo '<div class="latest mt-15">';
				echo '<div class="latest-cover">';
				echo '<div class="latest-cover-overlay"></div>';
				if(is_file($news[0]->thumbnail)){

						$g = getimagesize($news[0]->thumbnail);

						if($g[0] > $g[1]){
							$w = 750;
							$h = 490;
							$c = '7.50:4.90';
						}else{
							$w = 490;
							$h = 750;
							$c = '4.90:7.50';
						}

          echo '<img class="lazy" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($news[0]->entered)).'/' .$w. 'x' .$h. '/' .$news[0]->id. '" alt="'.$news[0]->title.' - America News">';
				}else{
					echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[0]->title.' - America News">';
				}
				echo '<div class="latest-caption">';
        echo '<strong class="heading"><a href="'.base_url(). urlencode($news[0]->category_slug).'/'.urlencode($news[0]->slug).'">'.$news[0]->title.'</a></strong>';
				echo '<p class="hidden-sm hidden-xs">'.$news[0]->description.'</p>';
				echo '<div class="latest-entered">';
				echo '<ul class="list-inline share-this share-min">';
				echo '<li>';
				echo $this->Entered->time_elapsed_string($news[0]->entered);
				echo '</li>';
        echo '<li><a href="'.base_url().'email/share?u=" data-url="'.base_url().urlencode($news[0]->category_slug).'/'.$news[0]->slug.'" class="button-envelope"><i class="fa fa-fw fa-envelope"></i></a></li>';
        echo '<li><a href="#" data-toggle="modal" data-target="#modal-share" data-image="'.base_url().$news[0]->thumbnail.'" data-url="'.base_url().urlencode($news[0]->category_slug).'/'.urlencode($news[0]->slug).'" class="button-share"><i class="fa fa-fw fa-share-alt"></i></a></li>';
				echo '</ul>';
				echo '</div>';

				echo '</div>';

				echo '</div>';
				echo '</div>';
			}
			?>
      </div>
      <div class="col-lg-4 hidden-md hidden-sm hidden-xs">
        <?php include('tpl.news.topstory.php');?>
      </div>
    </div>
    <div class="row">
      <?php
			$n = 1;
			while($n<=4){
				$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
				?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}
					?>
            <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().''.$news[$n]->category_slug;?>"><?php echo $news[$n]->category_title; ?></a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
				$n++;
			}
			?>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <h3><a href="<?php echo base_url();?>new-york">New York</a></h3>
    <div class="row">
      <?php
$n = 0;
$news = $new_york['rows'];
while($n <= 7){
		$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

					?>
            <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().'new-york';?>">New York</a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	$n++;
}
?>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h4><a href="<?php echo base_url();?>germany">California</a></h4>
    <div class="row">
      <?php
$n = 0;
$news = $california['rows'];
while($n <= 7){
		$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

					?>
            <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().'california';?>">California</a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	$n++;
}
?>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <h4><a href="<?php echo base_url();?>florida">Florida</a></h4>
    <div class="row">
      <?php
$n = 0;
$news = $florida['rows'];
while($n <= 7){
		$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

					?>
            <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().'florida';?>">Florida</a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	$n++;
}
?>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h5><a href="<?php echo base_url();?>netherlands">Texas</a></h5>
    <div class="row">
      <?php
$n = 0;
$news = $texas['rows'];
while($n <= 7){
		$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

					?>
            <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().'texas';?>">Texas</a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	$n++;
}
?>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h6><a href="<?php echo base_url();?>hawaii">Hawaii</a></h6>
    <div class="row">
      <?php
$n = 0;
$news = $hawaii['rows'];
while($n <= 7){
		$url = base_url().urlencode($news[$n]->category_slug).'/'.urlencode($news[$n]->slug);
?>
      <div class="col-lg-3">
        <div class="business-card">
          <figure><a href="<?php echo $url;?>">
            <?php
					if(is_file($news[$n]->thumbnail)){

						$g = getimagesize($news[$n]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

					?>
          <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news[$n]->entered)).'/' .$w. 'x' .$h. '/' .$news[$n]->id; ?>" alt="<?php echo $news[$n]->title; ?> - America News">
            <?php
					}else{
						echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news[$n]->title.' - America News">';
					}?>
            </a></figure>
          <div class="business-card-body"> <a href="<?php echo $url; ?>" class="overflow ellipsis"> <strong><?php echo $news[$n]->title; ?></strong> </a> <small><?php echo $this->Entered->time_elapsed_string($news[$n]->entered);?></small> <?php echo '<p>'.$news[$n]->description.'</p>'; ?> </div>
          <div class="business-card-footer">
            <ul class="list-inline">
              <li class="pull-left"><a href="<?php echo base_url().'hawaii';?>">Hawaii</a></li>
              <li><a href="#" class="modal-email" data-message="Lees meer <?php echo $url; ?>" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
              <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$news[$n]->thumbnail; ?>" data-url="<?php echo $url; ?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <?php
	$n++;
}
?>
    </div>
  </div>
</section>

<section class="lastest">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <h7><a href="<?php echo base_url();?>washington">Washington</a></h7>
        <div class="business-card">
          <?php
				$url = base_url().urlencode($washington['rows'][0]->category_slug).'/'.urlencode($washington['rows'][0]->slug);
				echo '<figure>';
				echo '<a href="'.$url.'">';
				if($washington['rows'][0]->thumbnail){

						$g = getimagesize($washington['rows'][0]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

          echo '<img class="lazy img-responsive" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($washington['rows'][0]->entered)).'/' .$w. 'x' .$h. '/' .$washington['rows'][0]->id. '" alt="'.$washington['rows'][0]->title.' - America News">';

				}else{
					echo '<img class="lazy img-responsive" data-original="'.base_url().'assets/default-news.png" alt="'.$washington['rows'][0]->title.' - America News">';
				}
				echo '</a>';
				echo '</figure>';
				?>
          <div class="business-card-body">
            <ul class="list-unstyled">
              <?php
					$n = 0;
          $total = count($washington['rows']);
          if($total >= 9) { $total=9; }
					while($n <= $total){
						$value = $washington['rows'][$n];
						$url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
        		echo '<li><a href="'.$url.'" class="overflow ellipsis"><strong>'.$value->title.'</strong></a></li>';
						$n++;
					}
				?>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-lg-3">
        <h7><a href="<?php echo base_url();?>virginia">Virginia</a></h7>
        <div class="business-card">
          <?php
				$url = base_url().urlencode($virginia['rows'][0]->category_slug).'/'.urlencode($virginia['rows'][0]->slug);
				echo '<figure>';
				echo '<a href="'.$url.'">';
				if($virginia['rows'][0]->thumbnail){

          echo '<img class="lazy img-responsive" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($virginia['rows'][0]->entered)).'/' .$w. 'x' .$h. '/' .$virginia['rows'][0]->id. '" alt="'.$virginia['rows'][0]->title.' - America News">';
				}else{
					echo '<img class="lazy img-responsive" data-original="'.base_url().'assets/default-news.png" alt="'.$virginia['rows'][0]->title.' - America News">';
				}
				echo '</a>';
				echo '</figure>';
				?>
          <div class="business-card-body">

            <ul class="list-unstyled">
              <?php

              if(count($virginia['rows']) > 0)
              {
      					$n = 0;
                $total = 10;
                if(count($virginia['rows']) <= 10){
                  $total = count($virginia['rows']) - 1;
                }

      					while($n <= $total){
                    $value = $virginia['rows'][$n];
        						$url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
                		echo '<li><a href="'.$url.'" class="overflow ellipsis"><strong>'.$value->title.'</strong></a></li>';
      						$n++;
      					}
              }
				     ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <h7><a href="<?php echo base_url();?>colorado">Colorado</a></h7>
        <div class="business-card">
          <?php
				$url = base_url().urlencode($colorado['rows'][0]->category_slug).'/'.urlencode($colorado['rows'][0]->slug);
				echo '<figure>';
				echo '<a href="'.$url.'">';
				if($colorado['rows'][0]->thumbnail){

						$g = getimagesize($colorado['rows'][0]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}

          echo '<img class="lazy img-responsive" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($colorado['rows'][0]->entered)).'/' .$w. 'x' .$h. '/' .$colorado['rows'][0]->id. '" alt="'.$colorado['rows'][0]->title.' - America News">';
				}else{
					echo '<img class="lazy img-responsive" data-original="'.base_url().'assets/default-news.png" alt="'.$colorado['rows'][0]->title.' - America News">';
				}
				echo '</a>';
				echo '</figure>';
				?>
          <div class="business-card-body">
            <ul class="list-unstyled">
              <?php
					$n = 0;
          $total = count($colorado['rows']);
          if($total >= 9) { $total=9; }
					while($n <= $total){
						$value = $colorado['rows'][$n];
						$url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
        		echo '<li><a href="'.$url.'" class="overflow ellipsis"><strong>'.$value->title.'</strong></a></li>';
						$n++;
					}
				?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <h7><a href="<?php echo base_url();?>arizona">Arizona</a></h7>
        <div class="business-card">
<?php
					$url = base_url().urlencode($arizona['rows'][0]->category_slug).'/'.urlencode($arizona['rows'][0]->slug);
					echo '<figure>';
					echo '<a href="'.$url.'">';
					if(is_file($arizona['rows'][0]->thumbnail)){

						$g = getimagesize($arizona['rows'][0]->thumbnail);

						if($g[0] > $g[1]){
							$w = 265;
							$h = 165;
							$c = '2.65:1.65';
						}else{
							$w = 165;
							$h = 265;
							$c = '1.65:2.65';
						}
            echo '<img class="lazy img-responsive" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($arizona['rows'][0]->entered)).'/' .$w. 'x' .$h. '/' .$arizona['rows'][0]->id. '" alt="'.$arizona['rows'][0]->title.' - America News">';
					}else{
						echo '<img class="lazy img-responsive" data-original="'.base_url().'assets/images/default-news.png" alt="'.$arizona['rows'][0]->title.' - America News">';
					}
					echo '</a>';
					echo '</figure>';
					?>
          <div class="business-card-body">
            <ul class="list-unstyled">
              <?php
					$n = 0;
          $total = count($arizona['rows'])-1;
          if($total > 10)
            $total=10;

					while($n <= $total){
						$value = $arizona['rows'][$n];
						$url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
        		echo '<li><a href="'.$url.'" class="overflow ellipsis"><strong>'.$value->title.'</strong></a></li>';
						$n++;
					}
				?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
