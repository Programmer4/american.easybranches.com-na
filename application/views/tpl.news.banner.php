<?php
  $category_id = '';
  $currentUrl = $_SERVER['REQUEST_URI'];
  $queryString = '';
  if (strpos($currentUrl, "/business") !== false) {
    $queryString = 'category_name=business';
  }
  else if (strpos($currentUrl, "/property") !== false) {
    $queryString = 'category_name=property';
  }
  else if (strpos($currentUrl, '/event') !== false) {
    $queryString = 'category_name=event';
  }
  else if (strpos($currentUrl, '/yachts') !== false) {
    $queryString = 'category_name=yachting';
  }
  else if (strpos($currentUrl, '/classified') !== false) {
    $queryString = 'category_name=classified';
  }
else {
  if(isset($content[0]->category_id)){
    $category_id = $content[0]->category_id;
    $queryString = 'category_id='.$category_id;
  }
}

?>

<div id="news-banner">
  <ul class="list-unstyled">
    <li class="text-center">

    	<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <br>
    	Content from our sponsored near by you

    </li>
  </ul>
</div>
<script>
$('#news-banner').load('<?php echo base_url();?>banner?<?php echo $queryString; ?>');
</script>
