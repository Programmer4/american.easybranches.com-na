<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>
<?php if(isset($meta_title)){ echo $meta_title; }?>
</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php if(isset($meta_description)){echo $meta_description;} ?>" />
<meta name="keywords" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>" />
<meta name="news_keywords" content="<?php if(isset($meta_keywords)){echo $meta_keywords;} ?>" />
<meta name="fb_title" content="<?php if(isset($meta_title)){ echo $meta_title;}?>" />
<meta property="og:site_name" content="America.Easybranches.com" />
<meta property="og:title" content="<?php if(isset($meta_title)){echo $meta_title;}?>" />
<meta property="og:description" content="<?php if(isset($meta_description)){ echo $meta_description;}?>" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@EasyBranches" />
<meta property="twitter:title" content="<?php if(isset($meta_title)){echo $meta_title;}?>" />
<meta property="twitter:description" content="<?php if(isset($meta_description)){echo $meta_description;}?>" />
<meta property="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" hreflang="en">
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<?php

$amp_items[] = '"@context": "http://schema.org"';
$amp_items[] = '"@context": "http://schema.org"';
if(isset($meta_title)){
	$amp_items[] = '"headline": "'.$meta_title.'"';
}
if(isset($meta_image)){
	$amp_items[] = '"image": ["'.$meta_image.'"]';
}
?>
<script type="application/ld+json">{<?php echo implode(',',$amp_items);?>}</script>
<?php include('google.tagmanager.js.php');?>
</head>
<body class="<?php if(isset($module)){ echo $module; }?>">
<?php include('google.tagmanager.html.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row section-heading">
        <div class="col-lg-6">
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <!-- <li><a href="<?php echo base_url();?>news">News</a></li> -->
            <li class="active">Tags</li>
          </ul>
        </div>
        <div class="col-lg-6">
          <?php
				if(isset($news['rows'][0]->thumbnail)){
					echo $this->Share->push(base_url().'news/tag',base_url().$news['rows'][0]->thumbnail);
				}else{
					echo $this->Share->push(base_url().'news/tag','');
				}
				?>
        </div>
      </div>
      <div class="row mb-30">
        <div class="col-lg-12">
          <h1 class="text-center">News Tags</h1>
          <h2 class="text-center"><strong><?php echo number_format($news['items'])?> News</strong> op <strong>Easybranches News</strong> </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <?php

				foreach($news['rows'] as $index=>$value){

					$content_url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
					?>
          <div class="col-lg-4">
            <div class="business-card">
              <figure> <a href="<?php echo $content_url;?>">
                <?php
									if(is_file($value->thumbnail)){

										$g = getimagesize($value->thumbnail);

										if($g[0] > $g[1]){
											$w = 400;
											$h = 300;
											$c = '4:3';
										}else{
											$w = 300;
											$h = 400;
											$c = '3:4';
										}

										?>
								<img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($value->entered)).'/' .$w. 'x' .$h. '/' .$value->id; ?>" alt="<?php echo $value->title; ?> - easybranches News">
                <?php }else{?>
                <img class="lazy" data-original="<?php echo base_url()?>assets/images/default-news.png" alt="<?php echo $value->title; ?> - easybranches News">
                <?php }?>
                </a> </figure>
              <div class="business-card-body"><a href="<?php echo $content_url; ?>">
                <h3 class="overflow ellipsis"><?php echo $value->title; ?></h3>
                </a>
                <h4><small><?php echo $this->Entered->time_elapsed_string($value->entered);?></small></h4>
                <?php echo '<p class="hidden-sm hidden-xs">'.$value->description.'</p>'; ?> </div>
              <div class="business-card-footer">
                <ul class="list-inline">
                  <li class="pull-left"><a href="<?php echo base_url().''.$value->category_slug;?>"><?php echo $value->category_title; ?></a></li>
                  <li><a href="#" class="modal-email" data-message="Have a look at <?php echo base_url().$value->category_slug;?>/<?php echo $value->slug; ?> :)" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
                  <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-url="<?php echo base_url().$value->category_slug.'/'.$value->slug; ?>" data-image="<?php echo base_url().$value->thumbnail;?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
                  <li></li>
                </ul>
              </div>
            </div>
          </div>
          <?php

				}


				?>
        </div>
        <div class="col-lg-4">
          <?php include('tpl.news.banner.php');?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
