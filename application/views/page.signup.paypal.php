<?php
# info@easybranches.com EUR
# advertising@easybranches.com

?>

<h1 align="center">Verbinden met PayPal.</h1>
<form id="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="info@easybranches.com">
<input type="hidden" name="item_name" value="Lidmaatschap - <?php echo $member[0]->member_id?>">
<input type="hidden" name="quantity" value="1">
<input type="hidden" name="amount" value="10">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="invoice" value="<?php echo $member[0]->member_id . date("sa")?>">
<input type="hidden" name="first_name" value="<?php echo $member[0]->firstname;?>">
<input type="hidden" name="last_name" value="<?php echo $member[0]->lastname;?>">
<input type="hidden" name="address1" value="">
<input type="hidden" name="address2" value="">
<input type="hidden" name="city" value="">
<input type="hidden" name="zip" value="">
<input type="hidden" name="country" value="">
<input type="hidden" name="email" value="<?php echo $member[0]->email;?>" />
<input type="hidden" name="notify_url" value="<?php echo base_url().'signup/paypal_ipn?id='.$member[0]->member_id;?>">
<input type="hidden" name="return" value="<?php echo base_url();?>signup/complete">
</form>
<script>
document.getElementById('paypal').submit();
</script>
