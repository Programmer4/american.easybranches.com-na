<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Preview Your Post</title>
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/contribute.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet" hreflang="en">

<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>

</head>

<body>

<br>
<br>
<br>
<br>
<form method="post">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="card">
          <h1>Preview Your Post</h1>
          <div class="form-group">
            <label><strong>Title</strong></label>
            <div class="preview">
              <p> <?php echo htmlspecialchars($content[0]->title);?> </p>
            </div>
          </div>
          <div class="form-group">
            <label><strong>Short Description</strong></label>
            <div class="preview">
              <p> <?php echo htmlspecialchars($content[0]->description);?> </p>
            </div>
          </div>
          <div class="form-group">
            <label><strong>Detail</strong></label>
            <div class="preview">
              <img id="thumbnail" src="<?php echo base_url().'news/'.date("Y/m", strtotime($content[0]->entered)).'/750x495/' .$content[0]->id; ?>" class="img-responsive"> <?php echo $content[0]->detail;?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div id="target-audience" class="card">
          <p><strong>Your target audience</strong></p>
          <div id="google-map" style="height:150px; margin-bottom:15px;"></div>

          <div class="form-group">
            <label><strong>Keywords</strong></label>
            <div class="preview">
              <p> <?php echo htmlspecialchars($content[0]->keywords);?> </p>
            </div>
          </div>

          <div class="form-group">
            <label><strong>Contact</strong></label>
            <div class="preview"> <?php echo $content[0]->editor;?> </div>
          </div>
          <div class="form-group">
            <label><strong>Email</strong></label>
            <div class="preview"> <?php echo $content[0]->email;?> </div>
          </div>
          <div class="form-group">
            <label><strong>Company</strong></label>
            <div class="preview"> <?php echo $content[0]->company;?> </div>
          </div>
        </div>

        <div class="card">
	        <p><strong>Sponsor</strong></p>
          <div class="radio">
            <label>
              <input type="radio" name="post_type" checked id="post_type2" value="sponsored">
              Price <strong>10 EUR</strong>
            </label>
          </div>
          <div class="help-block">Our sponsored <strong> 10 EUR</strong> Your content will be displayed on various websites in Easy Branches Network </div>
          <div class="form-group">
          	<input type="submit" name="save" value="Submit Your Post" class="btn btn-success btn-block btn-lg">
          </div>
          <div class="form-group">
          	<a href="<?php echo base_url();?>contribute/post?s=<?php echo md5($content[0]->slug); ?>" class="btn btn-default btn-block btn-lg">Edit</a>
            <input type="hidden" name="id" value="<?php echo $content[0]->id;?>">
          </div>

          <small>When you press a button <strong>Submit Your Post</strong> You have accepted in our <a href="<?php echo base_url()?>terms" target="_blank">Terms of service</a></small>

          <table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark"></a></td></tr></table><!-- PayPal Logo

        </div>
      </div>
    </div>
  </div>
</form>
<script>
var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
		zoom: <?php echo $content[0]->zoom; ?>,
		center: {lat: <?php echo $content[0]->lat; ?>, lng: <?php echo $content[0]->lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: false,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $content[0]->lat; ?>, lng: <?php echo $content[0]->lng;?>}
	});
}

$('#post_type1').click(function(){
	$('#target-audience').slideUp();
});

$('#post_type2').click(function(){
	$('#target-audience').slideDown();
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyB1EJJFs5Dc1mh8E08jlpSJwQrXUi_b-so&callback=initMap"></script>
</body>
</html>
