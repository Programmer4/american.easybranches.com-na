<?php 
echo '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:georss="http://www.georss.org/georss" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#" xmlns:media="http://search.yahoo.com/mrss/"
	>'; ?>
<channel>
<title>Phuket Business - Easy Branches</title>
<link><?php echo base_url();?></link>
<description>
easybranches News Phuket's people speak daily online local Phuketnews, mice events, classifieds, business directory hotels, resorts, properties restaurants
</description>
<lastBuildDate><?php echo date('D, d M Y H:i:s');?>+0000</lastBuildDate>
<language>en</language>
<sy:updatePeriod>hourly</sy:updatePeriod>
<sy:updateFrequency>1</sy:updateFrequency>
<generator><?php echo base_url();?></generator>
<image>
  <url><?php echo base_url();?>assets/images/logo.png</url>
  <title>Phuket Business</title>
  <link><?php echo base_url();?></link>
</image>
<?php
foreach($item as $value){
?>
<item>
  <title><?php echo htmlspecialchars($value['title']);?></title>
  <link><?php echo $value['link']; ?></link>
  <pubDate><?php echo date('D, d M Y H:i:s',strtotime($value['updated']));?> +0000</pubDate>
  <dc:creator><![CDATA[ Easy Branches ]]></dc:creator>
  <category><![CDATA[ <?php echo $value['category']; ?> ]]></category>
  <guid isPermaLink="false"><?php echo $value['link'];?></guid>
  <description><![CDATA[ <?php 
	
	echo '<img src="'.$value['thumbnail'].'">';
	echo '<p>';
	echo $value['description'];
	echo '</p>';
	?> ]]></description>
  <media:thumbnail url="<?php echo $value['thumbnail'];?>" />
</item> 
<?php 
}
?>
</channel>
</rss>
