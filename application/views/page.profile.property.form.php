<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url();?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
      <li><a href="<?php echo base_url(); ?>account/property">อสังหาริมทรัพย์</a></li>
      <li class="active">
        <?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo $content[0]->title; }?>
      </li>
    </ul>
    <br>
    <br>
    <div class="row">
      <div class="col-lg-12">
        <?php if($save){?>
        <div class="alert alert-success text-center"> อัปเดตอสังหาริมทรัพย์ของคุณแล้ว </div>
        <?php }?>
        <div class="profile">
          <h2><span>ข้อมูลทรัพย์สิน</span></h2>
          <form method="post" class="validate" enctype="multipart/form-data">
            <div class="form-group">
              <label>ชื่ออสังหาริมทรัพย์</label>
              <input type="text" name="title" class="form-control" required value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>ประเภทอสังหาริมทรัพย์</label>
                  <select name="type" class="form-control" required>
                    <option value=""> - เลือก - </option>
                    <option value="condo" <?php if(isset($content[0]->type)){ if($content[0]->type == 'condo'){ echo ' selected'; }}?>>คอนโด / อพาร์ทเม้นท์</option>
                    <option value="house" <?php if(isset($content[0]->type)){ if($content[0]->type == 'house'){ echo ' selected'; }}?>>บ้าน / วิลล่า</option>
                    <option value="land" <?php if(isset($content[0]->type)){ if($content[0]->type == 'land'){ echo ' selected'; }}?>>ที่ดิน</option>
                  </select>
                </div>
                <script>
              function type(value){
								if(value == 'land'){
									$('#building-detail').hide();
								}else{
									$('#building-detail').show();
								}
							}

							$('select[name=type]').change(function(){
								type(this.value);
							});

							type($('select[name=type]').val());

              </script>
                <div class="col-sm-4">
                  <label>ต้องการ</label>
                  <select name="want_to" class="form-control" required>
                    <option value=""> - เลือก - </option>
                    <option value="sale" <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'sale'){ echo ' selected'; }}?>> ขาย </option>
                    <option value="rentout" <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'rentout'){ echo ' selected'; }}?>>ให้เช่า</option>
                  </select>
                </div>
                <script>
							function want_to(val)
							{
								if(val == 'sale'){
									$('#price-sale').show();
									$('#price-rent').hide();
								}else if(val == 'rentout'){
									$('#price-sale').hide();
									$('#price-rent').show();
								}else{
									$('#price-sale').show();
									$('#price-rent').hide();
								}
							}
              $('select[name=want_to]').change(function(){
								want_to(this.value);
							});
							want_to($('select[name=want_to]').val());
              </script>
                <div class="col-sm-4">
                  <div id="if-sale" <?php echo ' style="display:none; "';?>>
                    <label>ความเป็นเจ้าของ</label>
                    <select name="ownership" class="form-control">
                      <option value=""> - เลือก - </option>
                      <option value="freehold"<?php if(isset($content[0]->ownership)){ if($content[0]->ownership == 'freehold'){ echo ' selected';}}?>> ขายขาด  </option>
                      <option value="leasehold"<?php if(isset($content[0]->ownership)){ if($content[0]->ownership == 'leasehold'){ echo ' selected';}}?>> ขายแบบมีสัญญาอายุ </option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <script>
          function onwership(val){
						if(val == 'sale'){
							$('#if-sale').show();
						}else{
							$('#if-sale').hide();
						}
					}
					onwership($('select[name=want_to]').val());
					$('select[name=want_to]').change(function(){
						onwership(this.value);
					});
          </script>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>ที่อยู่</label>
                  <input type="text" name="address" class="form-control" value="<?php if(isset($content[0]->address)){ echo $content[0]->address;}?>">
                </div>
                <div class="col-sm-4">
                  <label>เมือง</label>
                  <input type="text" name="city" class="form-control" value="<?php if(isset($content[0]->city)){ echo $content[0]->city;}?>">
                </div>
                <div class="col-sm-4">
                  <label>จังหวัด</label>
                  <select name="state" class="form-control">
                  	<option value="" selected>- เลือกจังหวัด -</option>
                    <?php
										$state = $this->Yacht->get_th_province_list();
										$select = '';
										foreach($state as $value){
											if(isset($content[0]->state)){
												if($value->title == $content[0]->state){
													$select = ' selected';
												}else{
													$select = '';
												}
											}
											echo '<option value="'.trim($value->title).'" '.$select.'>'.$value->title.'</option>';
										}
										?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>รหัสไปรษณีย์</label>
                  <input type="text" name="zipcode" class="form-control" value="<?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>">
                </div>
                <div class="col-sm-4">
                  <label>ประเทศ</label>
                  <select class="form-control" name="country" required>
                    <option value=""> - เลือก - </option>
                    <?php
								$select = '';
								foreach($country as $index=>$value){
									if(isset($content[0]->country)){
										if($value->code == $content[0]->country){
											$select = ' selected';
										}else{
											$select = '';
										}
									}else{
										$select = '';
									}
									echo '<option value="'.$value->code.'"'.$select.'>'.$value->name.'</option>';
								}
								?>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label>เว็บไซด์</label>
                  <input type="text" name="website" class="form-control" value="<?php if(isset($content[0]->website)){ echo $content[0]->website;}?>">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>Email</label>
                  <input type="text" name="email" class="form-control" value="<?php if(isset($content[0]->email)){ echo $content[0]->email;}else{ echo $member[0]->email; }?>">
                </div>
                <div class="col-sm-4">
                  <label>โทรศัพท์</label>
                  <input type="text" name="phone" class="form-control" value="<?php if(isset($content[0]->phone)){ echo $content[0]->phone;}else{ echo $member[0]->phone; }?>">
                </div>
                <div class="col-sm-4">
                  <label>แฟกซ์</label>
                  <input type="text" name="fax" class="form-control" value="<?php if(isset($content[0]->fax)){ echo $content[0]->fax;}else{ echo $member[0]->fax; }?>">
                </div>
              </div>
            </div>
            <h2><span>รายละเอียดของอสังหาริมทรัพย์ของคุณ</span></h2>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-3">
                  <label>สถานที่ใกล้เคียง</label>
                  <input name="location_id" class="form-control" type="text" value="<?php if(isset($content[0]->location_id)){echo $content[0]->location_id;}?>">
                </div>
                <?php
							if(isset($content[0]->meta)){
								$meta = json_decode($content[0]->meta,true);
							}
							?>
                <div class="col-sm-3">
                  <label> <span id="price-sale">ราคา</span> <span id="price-rent" <?php echo ' style="display:none"';?>>ราคา/วัน</span> บาท</label>
                  <div class="input-group"> <span class="input-group-addon">฿</span>
                    <input type="text" class="form-control" name="property_price" value="<?php if(isset($content[0]->property_price)){echo $content[0]->property_price;}?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <label>พื้นที่โดยรวม</label>
                  <div class="input-group"> <span class="input-group-addon">m<sup>2</sup></span>
                    <input type="text" class="form-control" name="land_size" value="<?php if(isset($content[0]->land_size)){echo $content[0]->land_size;}?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <label>ล้อมรอบด้วย</label>
                  <input type="text" class="form-control" name="view" value="<?php if(isset($content[0]->view)){ echo $content[0]->view;}?>">
                </div>
              </div>
            </div>
            <div class="form-group" id="building-detail">
              <div class="row">
                <div class="col-sm-3">
                  <label>ขนาดอาคาร</label>
                  <div class="input-group"> <span class="input-group-addon">m<sup>2</sup></span>
                    <input type="text" name="building_size" class="form-control" value="<?php if(isset($content[0]->building_size)){ echo $content[0]->building_size;}?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <label>ห้องนอน</label>
                  <input type="text" name="beds" class="form-control" value="<?php if(isset($content[0]->beds)){ echo $content[0]->beds;}?>">
                </div>
                <div class="col-sm-3">
                  <label>ห้องน้ำ</label>
                  <input type="text" name="baths" class="form-control" value="<?php if(isset($content[0]->baths)){ echo $content[0]->baths;}?>">
                </div>
                <div class="col-sm-3">
                  <label>ที่จอดรถ</label>
                  <input type="text" name="parking" class="form-control" value="<?php if(isset($content[0]->parking)){ echo $content[0]->parking;}?>">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>คำอธิบายโดยย่อเกี่ยวกับอสังหาริมทรัพย์</label>
              <textarea class="form-control" name="description"><?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
</textarea>
            </div>
            <div class="form-group">
              <textarea name="detail" class="form-control summernote"><?php if(isset($content[0]->detail)){echo $content[0]->detail;}?>
</textarea>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-12">
                  <label>คำค้นหา</label>
                  <input type="text" name="keywords" class="form-control" value="<?php if(isset($content[0]->keywords)){echo $content[0]->keywords;}?>">
                </div>
                <!--<div class="col-sm-6">
                  <label>Hashtags</label>
                  <input type="text" name="hashtags" class="form-control" value="<?php if(isset($content[0]->hashtags)){echo $content[0]->hashtags;}?>">
                </div>-->
              </div>
            </div>
            <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '13.7248946';
	$lng = '100.4930264';
	$zoom = 4;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->geo_zoom;
}
?>
            <h2><span>แผนที่และเส้นทาง</span></h2>
            <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>ละติจูด</label>
                  <input type="text" name="lat" class="form-control" value="<?php echo $lat;?>">
                </div>
                <div class="col-sm-6">
                  <label>ลองจิจูด</label>
                  <input type="text" name="lng" class="form-control" value="<?php echo $lng;?>">
                </div>
              </div>
            </div>
            <h2><span>วิดีโอ </span></h2>
            <div class="form-group">
              <label> URL ของ YouTube หรือ Vimeo </label>
              <input type="text" name="video" class="form-control" value="<?php if(isset($content[0]->video)){ echo $content[0]->video;}?>">
            </div>
            <h2><span>ภาพถ่าย </span></h2>
            <div class="form-group">
              <div class="photos">
                <div>
                  <label> รูปสำคัญ </label>
                  <input type="file" name="thumbnail" <?php if(empty($content[0]->id)){ echo 'required';}?>>
                </div>
                <div>
                  <label> แปลน </label>
                  <input type="file" name="floorplan">
                </div>
                <?php for($i=1; $i<=10; $i++){?>
                <div>
                  <label> <?php echo 'รูปที่ '.$i; ?> </label>
                  <input type="file" name="photo[]" <?php if(empty($content[0]->id)){if($i == 0){ echo 'required';} }?>>
                </div>
                <?php }?>
              </div>
            </div>
              <h2><span>หมายเหตุ </span></h2>
              <div class="form-group">
                <textarea name="note" rows="10" class="form-control"><?php  if(isset($content[0]->note)){ echo $content[0]->note; }?></textarea>
              </div>
            <?php
						/*
					if(empty($content[0]->id)){

					?>
            <div class="form-group">
              <div class="well">
                <h3><i class=" icon-certificate icon-color-1"></i> Make Your Property Premium </h3>
                <p>โฆษณาระดับพรีเมียมช่วยให้คุณโปรโมตผลิตภัณฑ์หรือบริการของคุณได้ดีขึ้น
                    โฆษณาของคุณมองเห็นได้มากขึ้น
                    ผู้ซื้อและผุ้ขายเห็นสิ่งที่พวกเขาต้องการได้เร็วขึ้น</p>
                <div class="form-group">
                  <table class="table table-hover checkboxtable">
                    <tbody>
                      <tr>
                        <td><div class="radio">
                            <label>
                              <input name="price" id="optionsRadios1" value="0" type="radio" onClick="remark()">
                              <strong>ฟรีไม่มีค่าใช้จ่าย </strong> </label>
                          </div></td>
                        <td><div class="radio">
                            <p>฿0</p>
                          </div></td>
                      </tr>
                      <tr>
                        <td><div class="radio">
                            <label>
                              <input name="price" id="optionsRadios2" value="1500" checked type="radio" onClick="remark();">
                              <strong>พิเศษ </strong> </label>
                          </div></td>
                        <td><div class="radio">
                            <p>฿1500 / ปี</p>
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="remark" class="alert alert-danger text-center" style="display:none">
                <p class="text-center"> ข้อมูลบางอย่างจะไม่สามารถมองเห็นได้</p>
              </div>
            </div>
            <?php
					}
					*/
					?>
            <div class="form-group">
              <p class="text-center">
                <?php
							/*
							if(isset($content[0]->id)){
								if($content[0]->sponsor == 0){
									echo '<a href="'.base_url().'account/property/upgrade?id='.$content[0]->id.'" class="btn btn-success" target="_blank">';
									echo ' Make Your Business Premium';
									echo '</a>';
									echo ' ';
								}
							}
							*/
							?>
                <input type="submit" name="save"
              class="btn btn-lg btn-success" value="<?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo 'แก้ไขประกาศ';}?>">
                <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id; }?>">
                <input type="hidden" name="geo_location" value="<?php echo $lat; ?>,<?php echo $lng;?>">
                <input type="hidden" name="geo_location_zoom" value="<?php echo $zoom;?>">
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>

function remark()
{
	if($('#optionsRadios1').prop('checked') == true){
		$('#remark').show();
	}else{
		$('#remark').hide();
	}
}

$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});

$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}


function initMap2(lat, lng, zoom) {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: zoom,
	center: {lat: lat, lng: lng}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: lat, lng: lng}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){

	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=geo_location_zoom]').val());

	initMap2(lat, lng, zoom)
});


</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
