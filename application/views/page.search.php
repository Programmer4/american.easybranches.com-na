<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<style>
.gcsc-branding{
	display: none;
}
input.gsc-input, .gsc-input-box, .gsc-input-box-hover, .gsc-input-box-focus{
	margin-top:2px !important;
	padding: 15px;
}
.gstl_50 {
	margin-top:20px;
    left: 33px;
    position: absolute;
    top: 14px;
    width: 80% !important;
}
.gsc-search-button.gsc-search-button-v2{
	height: 30px !important;
}
.gsc-search-button:before{
	position:absolute;
	content: 'Search';
	color:#000;
	margin-left:10px;
	margin-top: 8px;
}
.gs-image {
    margin-left: 5px;
}
.gs-bidi-start-align.gs-snippet {
    margin-left: 5px;
}
#story{
	min-height: 550px;
}
</style>
<section class="light-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-8"> 
      	<div id="story">
<script>
  (function() {
    var cx = '014708444459923656655:g35jl33mj3o';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
        </div>
      </div>
<div class="col-lg-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.banner.php');?>
        </div>      
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
