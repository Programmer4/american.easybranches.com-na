<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row section-heading">
        <div class="col-lg-6">
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url();?>news">ข่าว</a></li>
            <li><a href="<?php echo base_url();?>news/editor">ผู้รวบรวม</a></li>
            <?php if(isset($member[0]->firstname)){?>
            <li class="active"><?php echo $member[0]->firstname. ' '.$member[0]->lastname; ?></li>
            <?php } ?>
          </ul>
        </div>
        <?php if(isset($member[0]->member_id)){?>
        <div class="col-lg-6"> <?php echo $this->Share->push(base_url().'news/editor'.$member[0]->slug,'');?> </div>
        <?php }?>
      </div>

      <div class="row">


      	<?php
				if(is_file($member[0]->thumbnail)){
					echo '<div class="text-center">';
					echo '<img src="'.base_url().$member[0]->thumbnail.'" class="avatar-news-editor">';
					echo '</div>';
				}
				?>
      	<?php if(isset($member[0]->firstname)){?>
      	<h1 class="text-center"><?php echo $member[0]->firstname; ?> <?php echo $member[0]->lastname; ?></h1>
        <h2 class="text-center"><?php echo number_format($news['items'])?> ลงประกาศโดย <?php echo $member[0]->firstname; ?> <?php echo $member[0]->lastname; ?>  บน easybranches News </h2>
        <?php }?>
        <?php
				$socialmedia = json_decode($member[0]->socialmedia,true);

				echo '<div id="member-social" class="text-center">';
				echo '<ul class="list-inline">';
				foreach($socialmedia as $index=>$value){
					if($value){
						echo '<li><a href="'.$value.'" target="_blank" class="'.$index.'"><i class="fa fa-fw fa-'.$index.'"></i></a></li>';
					}
				}
				echo '</ul>';
				echo '</div>';

				echo '<div class="col-lg-8 col-lg-offset-2">';

				if(count($news['rows'])){
					foreach($news['rows'] as $index=>$value){

						$content_url = base_url().$value->category_slug.'/'.urlencode($value->slug);

						echo '<div class="row">';
						echo '<div class="col-lg-8 col-lg-offset-4"></div>';

						echo '<div class="cate-list">';
						echo '<div class="row">';
						echo '<div class="col-lg-6">';
						echo '<figure class="full">';
						if($value->thumbnail){
							echo '<a href="'.$content_url.'">';
							echo '<img src="'.base_url().$value->thumbnail.'">';
							echo '</a>';
						}
						echo '</figure>';
						echo '</div>';
						echo '<div class="col-lg-6">';
						echo '<div class="cate-list-card">';
						echo '<h4 class="overflow ellipsis"><a href="'.$content_url.'">'.$value->title.'</a></h4>';
						echo $value->description;
						echo '<ul class="list-inline share-this share-min">';
						echo '<li><i class="glyphicon glyphicon-time"></i> '.$this->Entered->time_elapsed_string($value->entered).'</li>';
						echo '<li><i class="glyphicon glyphicon-signal"></i> '.number_format($value->views,0,'.',',').'</li>';
						echo '<li><a href="'.base_url().'email/share?u=" data-url="'.$content_url.'" class="button-envelope"><i class="fa fa-fw fa-envelope"></i></a></li>';
						echo '<li><a href="#" data-toggle="modal" data-target="#modal-share" data-url="'.$content_url.'" class="button-share"><i class="fa fa-fw fa-share-alt"></i></a></li>';
						echo '</ul>';
						echo '</div>';
						echo '</div>';
						echo '</div>';
						echo '</div>';


						echo '</div>';

					}
				}

				if($news['pages'] > 1){
					$url = base_url().'news/editor/'.$slug;
					echo $this->Paginate->pages($url, $page, $news['pages']);
				}

				echo '</div>';

				?>


      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
