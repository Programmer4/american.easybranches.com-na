<?php include('tpl.meta.php');?>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="business-card">
      	<div class="business-card-body-full">
        <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="easybranches News" align="center" class="img-responsive"></a></p>
        <br>
        <form id="form-signin" action="<?php echo $this->config->item('signin');?>" method="post">
          <?php if(isset($error)){?>
          <div class="alert alert-danger alert-dismissible text-center" role="alert"> <strong>ผิดพลาด!</strong> Email </div>
          <?php }?>
          <?php if(isset($send)){?>
          <div class="alert alert-success alert-dismissible text-center" role="alert"> <strong>met succes</strong> Systeem stuur Password al naar uw email </div>
          <?php }?>

          <div class="form-group">
            <input name="email" class="form-control" id="register-email" placeholder="Email" aria-required="true" required="" type="email">
          </div>
          <div class="form-group">
            <input name="save" type="submit" class="btn btn-primary btn-block" value="Vraag nieuw Password aan">
          </div>
          <div class="form-group">
            <p class="text-center"> <a href="<?php echo base_url();?>signin" class="forgot-password btn btn-link">Log in</a> </p>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
