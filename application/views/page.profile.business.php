<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li class="active">ธุรกิจ</li>
      </ul>
      <br>
      <div class="row">
        <div class="col-lg-6">
          <form method="get" class="form-inline">
            <div class="form-group">
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <input name="q" type="text" class="form-control" placeholder="คำค้น	" value="<?php echo $searchKeyword; ?>">
              <button name="" type="submit" class="btn btn-search btn-info" > <i class="fa fa-fw fa-search"></i> </button>
              &nbsp; <a href="<?php echo  base_url();?>account/business/submit" class="btn btn-success"> ลงประกาศ </a> </div>
          </form>
        </div>
        <div class="col-lg-6">
					<div class="btn-group pull-right" role="group" aria-label="...">
            <a href="<?php echo base_url()?>account/business?status=1" class="btn btn-default <?php if($status == '1'){ echo 'active';}?>">อนุมัติแล้ว</a>
            <a href="<?php echo base_url()?>account/business?status=0" class="btn btn-default <?php if($status == '0'){ echo 'active';}?>">รอการอนุมัติ</a>
          </div>
        </div>
      </div>
      <?php

		if(count($business['rows'])){

			echo '<br><br>';

			echo '<div class="profile">';
			echo '<table class="table">';
			echo '<thead>';
			echo '<tr>';
			echo '<th nowrap>ภาพถ่าย</th>';
			echo '<th>ชื่อธุรกิจ</th>';
			echo '<th>ผู้ติดต่อ</th>';
			echo '<th>ที่อยู่</th>';
			echo '<th>จังหวัด</th>';
			echo '<th nowrap>รหัสไปรษณีย์</th>';
			echo '<th>โทรศัพท์</th>';
			if($status == 1){
				echo '<th>&nbsp;</th>';
			}
			echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			foreach($business['rows'] as $value){
				echo '<tr>';
				echo '<td>';
				if($value->thumbnail){
					echo '<img src="'.base_url().'resize?image='.$value->thumbnail.'&width=90&height=90&cropratio=1:1" class="img-reponsive" alt="'.$value->title.'">';
				}else{
					echo ' - ไม่มี - ';
				}
				echo '</td>';
				echo '<td>'.$value->title.'</td>';
				echo '<td>'.$value->contact.'</td>';
				echo '<td>'.$value->address . ' '.$value->city.'</td>';
				echo '<td>'.$value->state.'</td>';
				echo '<td>'.$value->zipcode.'</td>';
				echo '<td>'.$value->phone.'</td>';
				if($value->status == 1){
					$url = base_url().'account/business/preview?id='.$value->id;
					echo '<td><a href="'.$url.'" class="btn btn-success">แก้ไข</a></td>';
				}
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
			echo '</div>';

		}else{

			echo '<br><br><br>';
			echo '<p class="text-center">ยังไม่พบธุรกิจใด ๆ ในโปรไฟล์ของคุณ ลองส่งวันนี้ดูซิ.</p>';
			echo '<br><br><br>';

		}

		if($business['pages'] > 1){
      echo $this->Paginate->loadmorestr(base_url().'account/business?status='.$status . ($searchKeyword !="" ? "&q=".$searchKeyword : ""), $page, $business['pages']);
		}

    ?>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
