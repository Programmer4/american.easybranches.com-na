<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url();?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
      <li><a href="<?php echo base_url(); ?>account/business">ธุรกิจ</a></li>
      <li class="active">
        <?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo $content[0]->title; }?>
      </li>
    </ul>
    <br>
    <br>
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <?php if($content[0]->status == 0){?>
        <div class="alert alert-danger text-center"> ธุรกิจนี้อยู่ในระหว่างการตรวจสอบ</div>
        <?php }?>
        <div class="profile">
          <h2><span>Headline</span></h2>
          <form method="post" class="validate" enctype="multipart/form-data">
            <div class="form-group">
              <label>ชื่อ</label>
              <p>
                <?php if(isset($content[0]->title)){ echo $content[0]->title; }?>
              </p>
            </div>
            <div class="form-group">
              <label>Category</label>
              <p><?php echo $category[0]->category_title;?> </p>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>Address</label>
                  <p><?php echo $content[0]->address; ?></p>
                </div>
                <div class="col-sm-4">
                  <label>City</label>
                  <p><?php echo $content[0]->city; ?></p>
                </div>
                <div class="col-sm-4">
                  <label>State/Province</label>
                  <p><?php echo $content[0]->state; ?></p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>Zipcode</label>
                  <p><?php echo $content[0]->zipcode; ?></p>
                </div>
                <div class="col-sm-4">
                  <label>Country</label>
                  <p>
                    <?php
								echo $country[0]->name;
								?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <label>Website</label>
                  <p><a href="<?php echo $content[0]->website; ?>" target="_blank"><?php echo $content[0]->website;?></a></p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>Email</label>
                  <p><a href="mailto:<?php echo $content[0]->email; ?>" target="_blank"><?php echo $content[0]->email;?></a></p>
                </div>
                <div class="col-sm-4">
                  <label>Phone</label>
                  <p><?php echo $content[0]->phone;?></p>
                </div>
                <div class="col-sm-4">
                  <label>Fax</label>
                  <p><?php echo $content[0]->fax;?></p>
                </div>
              </div>
            </div>
            <h2><span>Detail of Your Business</span></h2>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Nearest point of interesting from Your business</label>
                  <p>
                    <?php if(isset($content[0]->location)){ echo $content[0]->location;}?>
                  </p>
                </div>
                <div class="col-sm-6">
                  <label>How about your products/services price range</label>
                  <?php
								$meta = json_decode($content[0]->meta, true);
								$n = 1;
								echo $meta['StarRating'];
								echo '<p>';
								while($n <= $meta['StarRating']){
									echo '$';
									$n++;
								}
								echo '</p>';

								?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Short description of Your business</label>
              <p>
                <?php if(isset($content[0]->description)){echo $content[0]->description;}?>
              </p>
            </div>
            <div class="form-group">
              <p>
                <?php if(isset($content[0]->detail)){echo $content[0]->detail;}?>
              </p>
            </div>
            <div class="form-group">
              <label>Description of Your business office Hours</label>
              <p>
                <?php if(isset($content[0]->hours)){echo $content[0]->hours;}?>
              </p>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Keywords</label>
                  <p>
                    <?php if(isset($content[0]->keywords)){ echo $content[0]->keywords;}?>
                  </p>
                </div>
                <div class="col-sm-6">
                  <label>Hashtags</label>
                  <p>
                    <?php if(isset($content[0]->hashtags)){ echo $content[0]->hashtags;}?>
                  </p>
                </div>
              </div>
            </div>
            <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '7.9442418';
	$lng = '98.345671';
	$zoom = 11;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->zoom;
}
?>
            <h2><span>Map &amp; Directions</span></h2>
            <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Latitude</label>
                  <p><?php echo $lat;?></p>
                </div>
                <div class="col-sm-6">
                  <label>Longitude</label>
                  <p><?php echo $lng;?></p>
                </div>
              </div>
            </div>
            <h2><span>Photos </span></h2>
            <div class="form-group">
              <div class="photos">
                <ul class="list-unstyled">
                  <li><img src="<?php echo base_url().$content[0]->thumbnail;?>" class="img-responsive">
                    <p class="text-center">Hero shot</p>
                  </li>
                  <?php
				$i = 1;
				foreach($photo as $index=>$value){?>
                  <li> <img src="<?php echo base_url().$value->filepath;?>" class="img-responsive">
                    <p class="text-center">Photo <?php echo $i;?></p>
                  </li>
                  <?php
					$i++;
				}
				?>
                </ul>
              </div>
            </div>
            <?php
					$socialmedia = json_decode($content[0]->socailmedia,true);

					echo '<h2><span>Social Media</span></h2>';

					if(count($social)){
						$i = 1;
						foreach($social as $index=>$value){

							?>
            <div class="form-group">
              <label><?php echo ucfirst($index);?></label>
              <p><a href="<?php echo  $socialmedia[$index]; ?>" target="_blank">
                <?php
			if(isset($socialmedia[$index])){
				if($socialmedia[$index]){
					echo $socialmedia[$index];
				}else{
					echo '-';
				}
			}else{
				echo '-';
			}?>
                </a></p>
            </div>
            <?php
						}
					}

					if(isset($content[0]->sponsor)){
						if($content[0]->sponsor > 0){
							$sponsor = $content[0]->sponsor;
						}else{
							$sponsor = 0;
						}
					}else{
						$sponsor = 0;
					}
					if($sponsor == 0){
					?>
            <div class="form-group">
              <div class="well">
                <h3><i class=" icon-certificate icon-color-1"></i> Make Your Business Premium </h3>
                <p>Premium ads help You promote Your product or services by getting
                  Your ads more visibility with more
                  buyers and sell what they want faster</p>
                <div class="form-group">
                  <table class="table table-hover checkboxtable">
                    <tbody>
                      <tr>
                        <td><div class="radio"> <strong>Premium </strong> </div></td>
                        <td><div class="radio">
                            <p>฿1500 / year</p>
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div id="remark" class="alert alert-danger text-center" style="display:none">
                <p class="text-center"> Some of information will be invisible ranking in search engine will not guarantee </p>
              </div>
            </div>
            <?php
					}
					?>
            <div class="form-group">
              <p class="text-center">
                <?php
							if(isset($content[0]->id)){
								if($content[0]->sponsor == 0){
									echo '<a href="'.base_url().'account/business/upgrade?id='.$content[0]->id.'" class="btn btn-success" target="_blank">';
									echo ' Make Your Business Premium';
									echo '</a>';
									echo ' ';
								}
							}
							?>
                <?php if($content[0]->status == 1){?>
                <a href="<?php echo base_url();?>account/business/submit?id=<?php echo $content[0]->id; ?>" class="btn <?php if($content[0]->sponsor != 0){ echo 'btn-success'; }else{ echo ' btn-default';}?>">
                <?php  echo 'Update Your Business'; ?>
                </a>
                <?php }?>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>

function remark()
{

	if($('#optionsRadios1').prop('checked') == true){
		$('#remark').show();
	}else{
		$('#remark').hide();
	}

}

$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});

$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('click', toggleBounce);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
