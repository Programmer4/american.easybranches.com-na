<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/business">ธุรกิจ</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <?php if($save){?>
          <div class="alert alert-success text-center">ธุรกิจของคุณได้รับการอัปเดตแล้ว</div>
          <?php }?>
          <div class="profile">
            <h2><span>พาดหัว</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อธุรกิจ</label>
                <input type="text" name="title" class="form-control" required value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
              </div>
              <div class="form-group">
                <label>หมวดหมู่</label>
                <select name="category_id" class="form-control" required>
                  <option value=""> - เลือก - </option>
                  <?php
							$select = '';
							foreach($category as $index=>$value){
								if(isset($content[0]->category_id)){
									if($value->category_id == $content[0]->category_id){
										$select = ' selected';
									}else{
										$select = '';
									}
								}
								echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
							}
							?>
                </select>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>ที่อยู่</label>
                    <input type="text" name="address" class="form-control" required value="<?php if(isset($content[0]->address)){ echo $content[0]->address;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>ตำบล</label>
                    <input type="text" name="city" class="form-control" required value="<?php if(isset($content[0]->city)){ echo $content[0]->city;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>จังหวัด</label>
                    <input type="text" name="state" class="form-control" required value="<?php if(isset($content[0]->state)){ echo $content[0]->state;}?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>รหัสไปรษีย์</label>
                    <input type="text" name="zipcode" class="form-control" required value="<?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>ประเทศ</label>
                    <select class="form-control" name="country" required>
                      <option value=""> - เลือก - </option>
                      <?php
								$select = '';
								foreach($country as $index=>$value){
									if(isset($content[0]->country)){
										if($value->code == $content[0]->country){
											$select = ' selected';
										}else{
											$select = '';
										}
									}else{
										$select = '';
									}
									echo '<option value="'.$value->code.'"'.$select.'>'.$value->name.'</option>';
								}
								?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <label>เว็บไซด์</label>
                    <input type="text" name="website" class="form-control" value="<?php if(isset($content[0]->website)){ echo $content[0]->website;}?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" value="<?php if(isset($content[0]->email)){ echo $content[0]->email;}else{ echo $member[0]->email;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>โทรศัพท์</label>
                    <input type="text" name="phone" class="form-control" value="<?php if(isset($content[0]->phone)){ echo $content[0]->phone;}else{ echo $member[0]->phone;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>แฟกซ์</label>
                    <input type="text" name="fax" class="form-control" value="<?php if(isset($content[0]->fax)){ echo $content[0]->fax;}else{ echo $member[0]->fax;}?>">
                  </div>
                </div>
              </div>
              <h2><span id="result_box" lang="th">รายละเอียดของธุรกิจของคุณ</span></h2>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>จุดที่น่าสนใจที่สุดจากธุรกิจของคุณ</label>
                    <input type="text" name="location" class="form-control" required value="<?php if(isset($content[0]->location)){ echo $content[0]->location;}?>">
                  </div>
                  <?php
							if(isset($content[0]->meta)){
								$meta = json_decode($content[0]->meta,true);
							}
							?>
                  <div class="col-sm-6">
                    <label>ผลิตภัณฑ์และบริการของคุณมีราคาเท่าไร</label>
                    <select name="meta[StarRating]" class="form-control" required>
                      <option value=""> - เลือก - </option>
                      <option value="1.0"<?php if(isset($meta['StarRating'])){ if($meta['StarRating'] == 1){ echo ' selected';} } ?>>$</option>
                      <option value="2.0"<?php if(isset($meta['StarRating'])){ if($meta['StarRating'] == 2){ echo ' selected';} } ?>>$$</option>
                      <option value="3.0"<?php if(isset($meta['StarRating'])){ if($meta['StarRating'] == 3){ echo ' selected';} } ?>>$$$</option>
                      <option value="4.0"<?php if(isset($meta['StarRating'])){ if($meta['StarRating'] == 4){ echo ' selected';} } ?>>$$$$</option>
                      <option value="5.0"<?php if(isset($meta['StarRating'])){ if($meta['StarRating'] == 5){ echo ' selected';} } ?>>$$$$$</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>คำอธิบายโดยย่อเกี่ยวกับธุรกิจของคุณ</label>
                <textarea class="form-control" name="description"><?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
</textarea>
              </div>
              <div class="form-group">
                <textarea name="detail" class="form-control summernote"><?php if(isset($content[0]->detail)){echo $content[0]->detail;}?>
</textarea>
              </div>
              <div class="form-group">
                <label>คำอธิบายเกี่ยวกับเวลาทำการ</label>
                <textarea name="hours" class="form-control"><?php if(isset($content[0]->hours)){echo $content[0]->hours;}?>
</textarea>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-12">
                    <label>คำสำคัญ</label>
                    <input type="text" name="keywords" class="form-control" value="<?php if(isset($content[0]->keywords)){echo $content[0]->keywords;}?>">
                  </div>
                </div>
              </div>
              <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '7.9442418';
	$lng = '98.345671';
	$zoom = 11;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->zoom;
}
?>
              <h2><span>แผนที่และเส้นทาง</span></h2>
              <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>ละติจูด</label>
                    <input type="text" name="lat" class="form-control" value="<?php echo $lat;?>">
                  </div>
                  <div class="col-sm-6">
                    <label>ลองจิจูด</label>
                    <input type="text" name="lng" class="form-control" value="<?php echo $lng;?>">
                  </div>
                </div>
              </div>
              <h2><span>ภาพถ่าย </span></h2>
              <div class="form-group">
                <div class="photos">
                  <?php for($i=0; $i<=9; $i++){?>
                  <div>
                    <label>
                      <?php if($i == 0 ){ echo 'รูปสำคัญ';}else{ echo 'รูปที่ '.$i;}?>
                    </label>
                    <input type="file" name="photo[]" <?php if(empty($content[0]->id)){if($i == 0){ echo 'required';} }?>>
                  </div>
                  <?php }?>
                </div>
              </div>
              <h2><span>สื่อสังคม</span></h2>
              <?php

					if(isset($content[0]->socailmedia)){
						$socailmedia = json_decode($content[0]->socailmedia, true);
					}
					foreach($social as $index=>$value){?>
              <div class="form-group">
                <label><?php echo ucfirst($value);?></label>
                <input name="socailmedia[<?php echo $index;?>]" type="text" class="form-control" value="<?php if(isset($socailmedia[$index])){ echo $socailmedia[$index];}?>">
              </div>
              <?php
					}?>
              <?php
					if(empty($content[0]->id)){
					?>
            <!--
              <div class="form-group">
                <div class="well">
                  <h3>แสดงผลแบบพิเศษ</h3>
                  <p>โฆษณาระดับพรีเมียมช่วยให้คุณโปรโมตผลิตภัณฑ์หรือบริการของคุณได้ดีขึ้น
                    โฆษณาของคุณมองเห็นได้มากขึ้น
                    ผู้ซื้อและผุ้ขายเห็นสิ่งที่พวกเขาต้องการได้เร็วขึ้น</p>
                  <div class="form-group">
                    <table class="table table-hover checkboxtable">
                      <tbody>
                        <tr>
                          <td><div class="radio">
                              <label>
                                <input name="price" id="optionsRadios1" value="0" type="radio" onClick="remark()">
                                <strong>ฟรีไม่มีค่าใช้จ่าย </strong> </label>
                            </div></td>
                          <td><div class="radio">
                              <p>฿0</p>
                            </div></td>
                        </tr>
                        <tr>
                          <td><div class="radio">
                              <label>
                                <input name="price" id="optionsRadios2" value="1500" checked type="radio" onClick="remark();">
                                <strong>พิเศษ </strong> </label>
                            </div></td>
                          <td><div class="radio">
                              <p>฿1500 / ปี</p>
                            </div></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              -->
                <!--<div id="remark" class="alert alert-danger text-center" style="display:none">
                  <p class="text-center"> Some of information will be invisible ranking in search engine will not guarantee </p>
                </div>
              </div>-->
              <?php
					}
					?>
              <div class="form-group">
                <p class="text-center">
                  <?php
              /*
              if(isset($content[0]->id)){
								if($content[0]->sponsor == 0){
									echo '<a href="'.base_url().'account/business/upgrade?id='.$content[0]->id.'" class="btn btn-success" target="_blank">';
									echo ' Make Your Business Premium';
									echo '</a>';
									echo ' ';
								}
							}
              */
							?>
                  <input type="submit" name="save"
              class="btn btn-lg <?php if(isset($content[0]->id)){ if($content[0]->sponsor == 0){ echo 'btn-default';}else{ echo 'btn-success';} }else{ echo 'btn-success';}?>" value="<?php if(empty($content[0]->id)){ echo 'ลงประกาศ';}else{ echo 'อัพเดท';}?>">
                  <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id; }?>">
                  <input type="hidden" name="geo_location" value="<?php echo $lat; ?>,<?php echo $lng;?>">
                  <input type="hidden" name="geo_location_zoom" value="<?php echo $zoom;?>">
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
    <br>
  </div>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>

function remark()
{
	if($('#optionsRadios1').prop('checked') == true){
		$('#remark').show();
	}else{
		$('#remark').hide();
	}
}

$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});

$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}


function initMap2(lat, lng, zoom) {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: zoom,
	center: {lat: lat, lng: lng}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: lat, lng: lng}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){

	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=geo_location_zoom]').val());

	initMap2(lat, lng, zoom)
});


</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
