<?php
# info@easybranches.com EUR
# advertising@easybranches.com
if($content[0]->editor){
	@list($firstname, $lastname) = @explode(' ', $content[0]->editor);
}
?>
<h1 align="center">Connecting with PayPal.</h1>
<form id="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="cmd" value="_xclick">
  <input type="hidden" name="business" value="info@easybranches.com">
  <input type="hidden" name="item_name" value="contribute - <?php echo $content[0]->id?>">
  <input type="hidden" name="quantity" value="1">
  <input type="hidden" name="amount" value="10">
  <input type="hidden" name="currency_code" value="EUR">
  <input type="hidden" name="invoice" value="<?php echo $content[0]->id?>">
  <input type="hidden" name="first_name" value="<?php echo $firstname;; ?>">
  <input type="hidden" name="last_name" value="">
  <input type="hidden" name="address1" value="<?php echo $lastname;?>">
  <input type="hidden" name="address2" value="">
  <input type="hidden" name="city" value="">
  <input type="hidden" name="zip" value="">
  <input type="hidden" name="country" value="">
  <input type="hidden" name="email" value="<?php echo $content[0]->email;?>" />
  <input type="hidden" name="notify_url" value="<?php echo base_url().'contribute/paypal_ipn?id='.$content[0]->id;?>">
  <input type="hidden" name="return" value="<?php echo base_url();?>contribute/thankyou">
</form>
<script>
document.getElementById('paypal').submit();
</script>
