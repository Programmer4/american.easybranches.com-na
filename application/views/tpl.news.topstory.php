<?php
if(count($topstory)){
?>
<div class="topstory hidden-xs hiddn-sm hidden-md">
  <h5>TOP STORIES</h5>
  <?php
	foreach($topstory as $index=>$value){
		$content_url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
						?>
  <div class="row">
    <div class="col-md-12 col-lg-4">
      <figure> <a href="<?php echo $content_url;?>">
      <?php
			if(is_file($value->thumbnail)){
				?>
      <img src="<?php echo base_url().'newspic/'.date("Y/m", strtotime($value->entered)).'/300x300/' .$value->id; ?>" alt="<?php echo $value->title; ?> - America News">
      <?php }else{?>
      <img src="<?php echo base_url().'assets/images/default-news.png';?>" alt="<?php echo $value->title; ?> - America News">
			<?php }?>
      </a> </figure>
    </div>
    <div class="col-md-12 col-lg-8">
    	<div class="cate-list-card">
      <strong><a href="<?php echo $content_url;?>" class="overflow ellipsis"><?php echo $value->title;?></a></strong>
      <?php
					echo '<ul class="list-inline entered">';
					echo '<li>'.$this->Entered->time_elapsed_string($value->entered).'</li>';
					echo '</ul>';
				?>
       </div>
    </div>
  </div>
  <?php
	}
	?>
</div>
<?php
}
?>
