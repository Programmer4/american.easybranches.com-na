<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <?php if(isset($current_category[0]->category_id)){?>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Home</a></li>
          <li><a href="<?php echo base_url();?>business">Business</a></li>
          <li class="active"><?php echo $current_category[0]->category_title; ?></li>
        </ul>
        <?php }else{?>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Home</a></li>
          <li class="active">Business</li>
        </ul>
        <?php }?>
      </div>
      <div class="col-lg-6">
        <?php 
			if(isset($movie['rows'][0]->thumbnail)){
				echo $this->Share->push('http://t.janj.eu/b', '' );
			}else{
				echo $this->Share->push('http://t.janj.eu/b', '' );
			}?>
      </div>
    </div>
    <div class="row mt-30 mb-30">
      <div class="col-lg-4">
        <!--<div class="property-search">
          <?php #include('tpl.form-business.php');?>
        </div>-->
        <?php include('tpl.news.banner.php');?>
      </div>
      <div class="col-lg-8">
        <div class="view-type">
          <h1><?php echo number_format($movie['items']); ?> lists of most recent interesting movies from iTunes store</h1>
        </div>
        <?php
					if(count($movie['rows'])){
						
						$rows = ceil(count($movie['rows'])/2);
						
						$r = 1;
						$x = 0;
						while($r <= $rows){
							echo '<div class="row">';
							for($i=1; $i<=2; $i++){
								if(isset($movie['rows'][$x])){
									echo '<div class="col-lg-6">';
									
									$value = $movie['rows'][$x];
									$category = json_decode($value->category, true);
																		
								?>
        <div class="business-card">
          <div class="card-body">
            <table width="100%" border="0">
              <tbody>
                <tr>
                  <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr valign="top" align="left">
                          <td width="62" valign="top" align="center"><a href="<?php echo base_url();?>movies/<?php echo $value->slug;?>"><img alt="<?php echo $value->title;?>" src="<?php echo $value->thumbnail;?>" border="0"></a></td>
                          <td width="10"><img alt="" src="https://s.mzstatic.com/images/spacer.gif" width="10" height="1"></td>
                          <td width="229"><b><a href="<?php echo base_url().'movies/'.$value->slug;?>"><?php echo $value->title;?></a></b><br>
                            <?php echo $value->imartist; ?> <font size="2" face="Helvetica,Arial,Geneva,Swiss,SunSans-Regular"> <br>
                            <b>Genre:</b> <?php echo $category['@attributes']['term']?> <br>
                            <?php if($value->imprice){?>
                            <b>Price:</b> <?php echo $value->imprice?>
                            <?php }?>
                            <br>
                            <?php if($value->imrentalPrice){?>
                            <b>imrentalPrice:</b> <?php echo $value->imrentalPrice.' <br>'; 	}?>
                            <b>Release Date:</b>
                            <?php 
														echo date('d F Y',strtotime($value->imreleaseDate));?>
                            </font></td>
                        </tr>
                      </tbody>
                    </table></td>
                </tr>
              </tbody>
            </table>
            <?php #echo $value->content;?>
          </div>
        </div>
        <?php
								echo '</div>';
								}
								$x++;
							}
							echo '</div>';
							$r++;
						}
					}
					
					
					if($movie['pages'] > 1){
						
						$url = base_url().'movies';						
						
						echo '<div class="row">';
						echo '<div class="col-lg-12">';
						echo $this->Paginate->pages($url, $page, $movie['pages']);
						echo '</div>';
						echo '</div>';
					}
					
?>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
