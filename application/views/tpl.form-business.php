<form method="get" action="<?php echo base_url().'business/search'?>">
  <div class="form-group">
    <label>คำค้นหา</label>
    <input type="text" name="keyword" class=" form-control" value="<?php if(isset($keyword)){echo $keyword;}?>">
  </div>
  <div class="form-group">
    <label>ประเภทธุรกิจ</label>
    <select name="category_id" class="form-control">
      <option value=""> - ทั้งหมด -</option>
      <?php 
			$select = '';
			$rs = $this->Business->get_category_list();
			foreach($rs as $value){
				
				if($this->input->get('category_id')){
					if($value->category_id == $this->input->get('category_id')){
						$select = ' selected';
					}else{
						$select = '';
					}
				}
				
				?>
				<option value="<?php echo $value->category_id; ?>" <?php echo $select; ?>><?php echo $value->category_title; ?></option>
				<?php 
			}
			?>
    </select>
  </div>
  <div class="form-group">
  	<?php
    $rs = $this->Yacht->get_th_province_list()
		?>
    <label>จังหวัด</label>
    <select name="province" class="form-control">
      <option value=""> - ทุกจังหวัด - </option>
      <?php 
			foreach($rs as $value){
				if($this->input->get('province') == $value->title){
					$select = ' selected';
				}else{
					$select = '';
				}
			?>
      <option value="<?php echo $value->title; ?>"<?php echo $select;?>><?php echo $value->title; ?></option>
      <?php 
			}
			?>
    </select>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success btn-block" value="Search">
  </div>
</form>
