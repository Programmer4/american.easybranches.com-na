<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row section-heading">
        <div class="col-lg-6">
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li><a href="<?php echo base_url();?>news">News</a></li>
            <li class="active">Editor</li>
          </ul>
        </div>
        <div class="col-lg-6"> <?php echo $this->Share->push('http://t.janj.eu/n/e','');?> </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <h1 class="text-center">Our easybranches news Editors</h1>
          <h2 class="text-center">Visit our <?php echo number_format($editor['items']);?> editors profile on easybranches news </h2>
        </div>
      </div>
      <?php
			echo '<div class="row">';
      foreach($editor['rows'] as $index=>$value){

				if($value->firstname != '' || $value->lastname != ''){
					$url = base_url().'news/editor/'.$value->slug;
					$socialmedia = json_decode($value->socialmedia,true);

					echo '<div class="col-lg-3 text-center">';
					if($value->thumbnail){
						echo '<a href="'.$url.'">';
						echo '<img src="'.base_url().'resize?image=/'.$value->thumbnail.'&with=265&height=165&cropratio=2.65:1.65" alt="'.$value->title.' - easybranches News" class="avatar-news-editor">';
						echo '</a>';
					}else{
						echo '<a href="'.$url.'">';
						echo '<img src="'.base_url().'assets/images/avatar.jpg" class="avatar-news-editor">';
						echo '</a>';
					}
					echo '<h4><a href="'.$url.'">'.$value->firstname.' ' .$value->lastname. '</a></h4>';

					if(count($socialmedia)){
						echo '<ul class="list-inline">';
						foreach($socialmedia as $ii=>$vv){
							if($vv){
								echo '<li><a href="'.$vv.'" target="_blank" class="'.$ii.'"><i class="fa fa-fw fa-'.$ii.'"></i></a></li>';
							}else{
								echo '<li><a href="#" class="'.$ii.' gray"><i class="fa fa-fw fa-'.$ii.'"></i></a></li>';
							}
						}
						echo '</ul>';
					}
					echo '</div>';
				}

      }
			echo '</div>';

			if($editor['rows']){
			}

			?>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
