<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
<?php
foreach($items as $value){
	
	$datetime = gmdate(strtotime($value->entered));
	$datetime = date('Y-m-d H:i', $datetime);
	$datetime = str_replace(' ','T',$datetime).'+07:00';
	
	if($value->keywords){
		$keywords = $value->keywords;
	}else{
		$keywords = 'phuket, news, events, classifieds, jobs,job, work,working, businesses, gazette, hotels, hotel, resort, resorts, Thailand, travel, holiday, holidays, vacation, weather, accommodation, spa, villas,tour, tours, island, car, cars,royal, marina, marinas, property, golf, restaurant, bar, diving, real, estate, realestate, property, properties, land, bungalows, bungalow, condo, condos, flat, apartment, apartments, complex';
	}
	
	if($value->description){
		$description = $value->description;	
	}else{
		$description = $value->title;
	}
	
?>
<url>
	<loc><?php echo base_url().'story/'.$value->slug; ?></loc>
	<news:news>
		<news:publication>
			<news:name>easybranches News</news:name>
			<news:language>en</news:language>
		</news:publication>
		<news:publication_date><?php echo $datetime;?></news:publication_date>
		<news:title><?php echo $value->title; ?></news:title>
		<news:geo_locations>Phuket, Thailand</news:geo_locations>
		<news:keywords><?php echo $value->keywords; ?></news:keywords>
	</news:news>
  <image:image>
    <image:loc><?php echo base_url().$value->thumbnail;?></image:loc>
    <image:caption><?php echo $value->description;?></image:caption>
    <image:title><?php echo $value->title;?></image:title>
  </image:image>
</url>
<?php 
}
?>
</urlset>
