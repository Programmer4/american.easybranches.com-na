﻿<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li class="active">อยากซื้อ อยากขาย</li>
      </ul>
      <br>
      <div class="row">
        <div class="col-lg-6">
          <form method="get" class="form-inline">
            <div class="form-group">
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <input name="q" type="text" class="form-control" placeholder="คำค้นหา" value="<?php echo $searchKeyword; ?>">
              <button name="" type="submit" class="btn btn-search btn-info" > <i class="fa fa-fw fa-search"></i> </button>
              &nbsp; <a href="<?php echo  base_url();?>account/classified/submit" class="btn btn-success"> ลงประกาศ </a> </div>
          </form>
        </div>
        <div class="col-lg-6">
          <div class="btn-group pull-right" role="group" aria-label="...">
              <a href="<?php echo base_url()?>account/classified?status=1" class="btn btn-default <?php if($status == '1'){ echo 'active';}?>">อนุมัติแล้ว</a>
              <a href="<?php echo base_url()?>account/classified?status=0" class="btn btn-default <?php if($status == '0'){ echo 'active';}?>">รอการอนุมัติ</a>
          </div>
        </div>
      </div>
      <?php

      if(count($classified['rows'])){
          echo '<br><br>';
          echo '<div class="profile">';
          echo '<table class="table">';
          echo '<thead>';
          echo '<tr>';
          echo '<th>ภาพถ่าย</th>';
          echo '<th>ชื่อสินค้า</th>';
          echo '<th>ประเภทสินค้า</th>';
          echo '<th>เงื่อนไข</th>';
          echo '<th>ราคา</th>';
          echo '<th>ที่อยู่</th>';
          echo '<th>เมือง</th>';
          echo '<th>จังหวัด</th>';
          echo '</tr>';
          echo '</thead>';
          echo '<tbody>';
          foreach($classified['rows'] as $value){

            $url = base_url().'account/classified/preview?id='.$value->id;

            echo '<tr>';
            echo '<td><img src="'.base_url().'resize?image=/'.$value->thumbnail.'&width=90&height=90&cropratio=1:1" alt="'.$value->title.'"></td>';
            echo '<td>'.$value->classified_title.'</td>';
            echo '<td>'. $value->category_title .'</td>';

            switch ($value->conditions) {
              case 'new':
                echo '<td>สินค้าใหม่</td>';
                break;
              case 'good':
                echo '<td>สภาพดี</td>';
                break;
              case 'used':
                echo '<td>มือสอง</td>';
                break;

              default:
                echo '<td></td>';
                break;
            }

            if($value->price){
              $value->price = str_replace(',','',$value->price);
              echo '<td>'. number_format($value->price) .' บาท</td>';
            }
            else {
              echo '<td>-</td>';
            }

            echo '<td>'.$value->address.'</td>';
            echo '<td>'.$value->city.'</td>';
            echo '<td>'.$value->location_title.'</td>';
            if($status == 1){
              echo '<td><a href="'.$url.'" class="btn btn-success">แก้ไข</a></td>';
            }
            echo '</tr>';
          }
          echo '</tbody>';
          echo '</table>';
          echo '</div>';

          }else{

			echo '<br><br><br>';
			echo '<p class="text-center">ไม่พบข้อมูลที่จัดไว้ในโปรไฟล์ของคุณ ลองส่งวันนี้ดูสิ</p>';
			echo '<br><br><br>';

		}

		if($classified['pages'] > 1){
      echo $this->Paginate->loadmorestr(base_url().'account/classified?status='.$status . ($searchKeyword !="" ? "&q=".$searchKeyword : ""), $page, $classified['pages']);
		}

    ?>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
