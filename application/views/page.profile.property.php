<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li class="active">อสังหาริมทรัพย์</li>
      </ul>
      <br>
      <div class="row">
        <div class="col-lg-6">
          <form method="get" class="form-inline">
            <div class="form-group">
              <input type="hidden" name="status" value="<?php echo $status; ?>" />
              <input name="q" type="text" class="form-control" placeholder="คำค้นหา"  value="<?php echo $searchKeyword; ?>">
              <button name="" type="submit" class="btn btn-search btn-info" > <i class="fa fa-fw fa-search"></i> </button>
              &nbsp; <a href="<?php echo  base_url();?>account/property/submit" class="btn btn-success"> ลงประกาศ </a> </div>
          </form>
        </div>
        <div class="col-lg-6">
          <div class="btn-group pull-right" role="group" aria-label="...">
            <a href="<?php echo base_url()?>account/property?status=1" class="btn btn-default <?php if($status == '1'){ echo 'active';}?>">อนุมัติแล้ว</a>
            <a href="<?php echo base_url()?>account/property?status=0" class="btn btn-default <?php if($status == '0'){ echo 'active';}?>">รอการอนุมัติ</a>
          </div>
        </div>
      </div>
      <?php

		if(count($property['rows'])){

			echo '<br><br>';
      echo '<div class="profile">';
      echo '<table class="table">';
      echo '<thead>';
      echo '<tr>';
      echo '<th>ภาพถ่าย</th>';
      echo '<th>ชื่ออสังหาริมทรัพย์</th>';
      echo '<th>ประเภท</th>';
      echo '<th nowrap>ต้องการ</th>';
      echo '<th>ที่อยู่</th>';
      echo '<th>เมือง</th>';
      echo '<th>จังหวัด</th>';
      if($status == 1){
				echo '<th></th>';
			}
      echo '</tr>';
      echo '</thead>';
      echo '<tbody>';
      foreach($property['rows'] as $value){

        $preview = base_url().'account/property/preview?id='.$value->id;

        echo '<tr>';
        echo '<td><img src="'.base_url().'resize?image=/'.$value->thumbnail.'&width=90&height=90&cropratio=1:1" alt="'.$value->title.'"></td>';
        echo '<td>'.$value->title.'<br> ';
				if($value->status == 1){
					echo '<a href="'.base_url().'/property/'.urlencode($value->slug).'" target="_blank">[แสดงผล]</a>';
				}
				echo '</td>';
        echo '<td>';
        if($value->type == 'condo'){
          echo 'คอนโด / อพาร์ทเม้นท์';
        }else{
          echo 'บ้าน / วิลล่า';
        }

        echo '</td>';
        echo '<td>';
        if($value->want_to == 'sale'){
          echo 'ขาย';
        }else{
          echo 'ให้เช่า';
        }
        echo '</td>';
        echo '<td>'.$value->address.'</td>';
        echo '<td>'.$value->city.'</td>';
        echo '<td>'.$value->state.'</td>';
        if($status == 1){
          echo '<td>';
          echo '<a href="'.$preview.'" class="btn btn-success">แก้ไข</a>';
          echo '</td>';
        }
        echo '</tr>';
      }
      echo '</tbody>';
      echo '</table>';
      echo '</div>';

      /*
			$rows = ceil(count($property['rows'])/4);

			$r = 1;
			$x = 0;
			while($r <= $rows){

				echo '<div class="row">';

				for($i=1; $i<=4; $i++){

					if(isset($property['rows'][$x]->id)){

						$value = $property['rows'][$x];

						$url = base_url().'account/property/preview?id='.$value->id;


						echo '<div class="col-md-3">';
						echo '<div class="business-card">';
						echo '<figure class="full">';
						echo '<a href="'.$url.'">';
						echo '<img src="'.base_url().$value->thumbnail.'">';
						echo '</a>';
						echo '</figure>';

						echo '<div class="business-card-body">';
						echo '<a href="'.$url.'"><h3>'.$value->title.'</h3></a>';
						echo '<h4><small>'.$value->want_to.'</small></h4>';
						echo '<p>'.$value->address. ' '.$value->city. ' '.$value->state.' '.$value->zipcode.'</p>';
						echo '</div>';
						echo '<div class="business-card-footer">';
						echo '<ul class="list-inline">';
						echo '<li class="pull-left"><a>'.$value->location.'</a></li>';
						echo '<li><a href="#" data-toggle="modal" data-target="#modal-map"><i class="fa fa-fw fa-map-marker"></i></a></li>';
						echo '<li><a href="#" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>';
						echo '<li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-url="'.$url.'"><i class="fa fa-fw fa-share-alt"></i></a></li>';
						echo '<li></li>';
						echo '</ul>';
						echo '</div>';
						echo '</div>';
						echo '</div>';

					}

					$x++;

				}

				echo '</div>';

				$r++;
			}
      */
		}else{

			echo '<br><br><br>';
			echo '<p class="text-center"> ไม่พบข้อมูลอสังหาริมทรัพย์ใด ๆ ในโปรไฟล์ของคุณ ลองส่งวันนี้ดูสิ</p>';
			echo '<br><br><br>';

		}

		if($property['pages'] > 1){
      echo $this->Paginate->loadmorestr(base_url().'account/property?status='.$status . ($searchKeyword !="" ? "&q=".$searchKeyword : ""), $page, $property['pages']);
		}

    ?>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
