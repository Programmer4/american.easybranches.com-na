<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Account</a></li>
        <li class="active">Profile</li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <?php
				if($save){
					?>
          <div class="alert alert-success text-center"> Your profile has been update </div>
          <?php
				}
				?>
          <div class="profile">
            <h2><span>Your Account</span></h2>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" name="email" readonly value="<?php echo $member[0]->email;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="password" value="<?php echo $member[0]->password;?>">
                </div>
              </div>
              <h2><span>Your Personal Information</span></h2>
              <div class="form-group">
                <label class="col-sm-4 control-label">Title</label>
                <div class="col-sm-8">
                  <select name="title" class="form-control">
                    <option value="Mr." <?php if(isset($member[0]->title)){ if($member[0]->title == 'Mr.'){ echo ' selected';}}?>>Mr.</option>
                    <option value="Mrs." <?php if(isset($member[0]->title)){ if($member[0]->title == 'Mrs.'){ echo ' selected';}}?>>Mrs.</option>
                    <option value="Miss" <?php if(isset($member[0]->title)){ if($member[0]->title == 'Miss'){ echo ' selected';}}?>>Miss</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">First Name</label>
                <div class="col-sm-8">
                  <input name="firstname" class="form-control" required value="<?php echo $member[0]->firstname;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Last Name</label>
                <div class="col-sm-8">
                  <input name="lastname" class="form-control" required value="<?php echo $member[0]->lastname;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Address</label>
                <div class="col-sm-8">
                  <input name="address" class="form-control" value="<?php echo $member[0]->address;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">City</label>
                <div class="col-sm-8">
                  <input name="city" class="form-control" value="<?php echo $member[0]->city;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Province/State</label>
                <div class="col-sm-8">
                  <input name="state" class="form-control" value="<?php echo $member[0]->state;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Zipcode</label>
                <div class="col-sm-8">
                  <input name="zipcode" class="form-control" value="<?php echo $member[0]->zipcode;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Country</label>
                <div class="col-sm-8">
                  <select class="form-control" name="country" required>
                    <option value="">Select</option>
                    <?php
									$select = '';
									foreach($country as $index=>$value){
										if(isset($member[0]->country)){
											if($member[0]->country == $value->code){
												$select = ' selected';
											}else{
												$select = '';
											}
										}

										echo '<option value="'.$value->code.'"'.$select.'>'.$value->name.'</option>';
									}
									?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>
                <div class="col-sm-8">
                  <input type="text" name="phone" class="form-control" value="<?php echo $member[0]->phone;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Fax</label>
                <div class="col-sm-8">
                  <input type="text" name="fax" class="form-control" value="<?php echo $member[0]->fax;?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Website</label>
                <div class="col-sm-8">
                  <input type="text" name="website" class="form-control" value="<?php echo $member[0]->website;?>">
                </div>
              </div>
              <h2><span>Photo</span></h2>
              <?php if(is_file($member[0]->thumbnail)){?>
              <center>
                <img src="<?php echo base_url().$member[0]->thumbnail;?>">
              </center>
              <br>
              <br>
              <?php }?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Personal Photo</label>
                <div class="col-sm-8">
                  <input type="file" name="thumbnail">
                </div>
              </div>
              <h2><span>Your Social Media</span></h2>
              <?php
					$socialmedia = json_decode($member[0]->socialmedia, true);

					foreach($social as $index=>$value){?>
              <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo ucfirst($value);?></label>
                <div class="col-sm-8">
                  <input type="text" name="social[<?php echo $index; ?>]" class="form-control" value="<?php if(isset($socialmedia[$index])){ echo $socialmedia[$index]; } //echo $member[0]->phone;?>">
                </div>
              </div>
              <?php
					}
					?>
              <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4 ">
                  <input type="submit" name="save" class="btn btn-success" value="Update Profile">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <br>
      <br>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
