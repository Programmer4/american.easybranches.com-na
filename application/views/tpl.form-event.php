<form method="get" action="<?php echo base_url().'event/search'?>">
  <div class="form-group">
    <label>ประเภท</label>
    <select name="type_id" class="form-control">
      <option value=""> - ทั้งหมด -</option>
      <?php
							$select = '';
							foreach($category as $index=>$value){

								if(isset($current_category[0]->type_id)){
									if($value->type_id == $current_category[0]->type_id){
										$select = ' selected';
									}else{
										$select = '';
									}
								}

								?>
              <option value="<?php echo $value->type_id; ?>" <?php echo $select; ?>><?php echo $value->title; ?></option>
              <?php
							}
							?>
    </select>
  </div>
  <div class="form-group">
    <label>วันที่เริ่มกิจกรรม</label>
    <input type="text" name="time_start" class="datepicker form-control" value="<?php if(isset($time_start)){echo $time_start;}?>">
  </div>
  <div class="form-group">
    <label>จังหวัด</label>
    <select name="province" class="form-control">
      <option value=""> - ทังหมด - </option>
      <?php
			$select = '';
			foreach($event_province as $index=>$value){
				if(isset($province)){
					if($value->province == $province){
						$select = ' selected';
					}else{
						$select = '';
					}
				}
				?>
      <option value="<?php echo $value->province; ?>"<?php echo $select;?>><?php echo $value->province; ?></option>
      <?php
			}
			?>
    </select>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success btn-block" value="Search">
  </div>
</form>
<script>
$('.datepicker').datepicker({
	format: 'yyyy-mm-dd',
	startDate: '<?php echo date('Y-m-d');?>',
	autoclose: true
});
</script>
