<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo base_url();?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
      <li><a href="<?php echo base_url(); ?>account/property">อสังหาริมทรัพย์</a></li>
      <li class="active"> รูปแบบ </li>
    </ul>
    <br>
    <br>
    <div class="row">
      <div class="col-lg-12">
        <?php if(isset($content[0]->status)){
				if($content[0]->status == 0){
				?>
        <div class="alert alert-danger text-center"> อสังหาริมทรัพย์ของคุณอยู่ระหว่างการตรวจสอบ</div>
        <?php }
			}?>
        <div class="profile">
          <h2><span>ข้อมูลทรัพย์สิน</span></h2>
          <form method="post" class="validate" enctype="multipart/form-data">
            <div class="form-group">
              <label>ชื่ออสังหาริมทรัพย์</label>
              <p>
                <?php if(isset($content[0]->title)){ echo $content[0]->title; }?>
              </p>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>ประเภทอสังหาริมทรัพย์</label>
                  <p>
                    <?php if(isset($content[0]->type)){ if($content[0]->type == 'condo'){ echo ' คอนโด/อพาร์ทเม้น '; }}?>
                    <?php if(isset($content[0]->type)){ if($content[0]->type == 'house'){ echo ' บ้าน/วิลล่า'; }}?>
                    <?php if(isset($content[0]->type)){ if($content[0]->type == 'land'){ echo ' ที่ดิน'; }}?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <label>ต้องการ</label>
                  <p>
                    <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'sale'){ echo 'ขาย'; }}?>
                    <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'rentout'){ echo 'ให้เช่า'; }}?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <div id="if-sale" <?php if(isset($content[0]->want_to)){if($content[0]->want_to == 'rentout'){ echo ' style="display:none; "'; }}?>>
                    <label>ความเป็นเจ้าของ</label>
                    <p>
                      <?php
										if(isset($content[0]->ownership)){
											if($content[0]->want_to == 'sale'){
												if($content[0]->ownership == 'freehold'){
													echo ' ขายขาด  ';
												}else if($content[0]->ownership == 'leasehold'){
													echo ' ขายแบบมีสัญญาอายุ ';
												}else{
													echo ' - ';
												}
											}
										}?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">ที่อยู่
                  <p>
                    <?php if(isset($content[0]->address)){ echo $content[0]->address;}?>
                  </p>
                </div>
                <div class="col-sm-4">เมือง
                  <p>
                    <?php if(isset($content[0]->city)){ echo $content[0]->city;}?>
                  </p>
                </div>
                <div class="col-sm-4">จังหวัด
                  <p>
                    <?php if(isset($content[0]->state)){ echo $content[0]->state;}?>
                  </p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">รหัสไปรษณีย์
                  <p>
                    <?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>
                  </p>
                </div>
                <div class="col-sm-4">ประเทศ
                  <p>
                    <?php
								$select = '';
								foreach($country as $index=>$value){
									if(isset($content[0]->country)){
										if($value->code == $content[0]->country){
											echo $value->name;
										}
									}
								}
								?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <label> เว็บไซด์ </label>
                  <p>
                    <?php if(isset($content[0]->website)){ echo $content[0]->website;}?>
                  </p>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-4">
                  <label>Email</label>
                  <p>
                    <?php if(isset($content[0]->email)){ echo $content[0]->email;}?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <label>โทรศัพท์</label>
                  <p>
                    <?php if(isset($content[0]->phone)){ if($content[0]->phone){ echo $content[0]->phone;}else{ echo '-';}}?>
                  </p>
                </div>
                <div class="col-sm-4">
                  <label>แฟกซ์</label>
                  <p>
                    <?php if(isset($content[0]->fax)){ if($content[0]->fax){ echo $content[0]->fax;}else{ echo '-';}}?>
                  </p>
                </div>
              </div>
            </div>
            <h2><span>ข้อมูลเบื้องต้นของอสังหาริมทรัพย์ของคุณ</span></h2>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-3">
                  <label>สถานที่ใกล้เคียง</label>
                  <p><?php echo $content[0]->location_id;?></p>
                </div>
                <?php
							if(isset($content[0]->meta)){
								$meta = json_decode($content[0]->meta,true);
							}
							?>
                <div class="col-sm-3">
                  <label> <span id="price-sale" <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'rentout'){ echo ' style="display:none;"';}}?>>ราคา</span> <span id="price-rent" <?php if(isset($content[0]->want_to)){ if($content[0]->want_to == 'sale'){echo ' style="display:none"';}}?>>ราคา/ต่อเดือน</span> บาท </label>
                  <p>
                    <?php if(isset($content[0]->property_price)){ echo number_format($content[0]->property_price); }?>
                  </p>
                </div>
                <div class="col-sm-3">
                  <label>พื้นที่โดยรวม</label>
                  (sqm)
                  <p>
                    <?php if(isset($content[0]->land_size)){ echo number_format($content[0]->land_size); }?>
                  </p>
                </div>
                <div class="col-sm-3">
                  <label>ล้อมรอบด้วย</label>
                  <p>
                    <?php if(isset($content[0]->view)){ echo $content[0]->view; }?>
                  </p>
                </div>
              </div>
            </div>
            <div class="form-group" id="building-detail">
              <div class="row">
                <div class="col-sm-3">
                  <label>ขนาดอาคาร</label>
                  (sqm)
                  <p>
                    <?php if(isset($content[0]->building_size)){ echo $content[0]->building_size;}?>
                  </p>
                </div>
                <div class="col-sm-3">
                  <label>ห้องนอน</label>
                  <p>
                    <?php if(isset($content[0]->beds)){ echo $content[0]->beds;}?>
                  </p>
                </div>
                <div class="col-sm-3">
                  <label>ห้องน้ำ</label>
                  <p>
                    <?php if(isset($content[0]->baths)){ echo $content[0]->baths;}?>
                  </p>
                </div>
                <div class="col-sm-3">
                  <label>ที่จอดรถ</label>
                  <p>
                    <?php if(isset($content[0]->parking)){ echo $content[0]->parking;}?>
                  </p>
                </div>
              </div>
            </div>
            <h2><span>คำอธิบายโดยย่อเกี่ยวกับอสังหาริมทรัพย์</span></h2>
            <div class="form-group">
              <p>
                <?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
              </p>
            </div>
             <h2><span>รายละเอียดเกี่ยวกับอสังหาริมทรัพย์</span></h2>
            <div class="form-group">
              <?php if(isset($content[0]->detail)){echo $content[0]->detail;}?>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>คำค้นหา</label>
                  <p>
                    <?php if(isset($content[0]->keywords)){ if($content[0]->keywords){echo $content[0]->keywords;}else{ echo '-';}}?>
                  </p>
                </div>
              </div>
            </div>
            <h2><span>แผนที่และเส้นทาง</span></h2>
            <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px;"></div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>ละติจูด</label>
                  <p>
                    <?php if(isset($content[0]->lat)){echo $content[0]->lat;}?>
                  </p>
                </div>
                <div class="col-sm-6">
                  <label>ละติจูด</label>
                  <p>
                    <?php if(isset($content[0]->lng)){echo $content[0]->lng;}?>
                  </p>
                </div>
              </div>
            </div>
            <h2><span>ภาพถ่าย</span></h2>
            <div class="form-group">
              <div class="photos">
                <?php

							echo '<figure>';
							echo '<img src="'.base_url().$content[0]->thumbnail.'" class="img-responsive">';
							echo '<p class="text-center"> รูปสำคัญ</p>';
							echo '</figure>';

							if(is_file($content[0]->floorplan)){
								echo '<figure>';
								echo '<img src="'.base_url().$content[0]->floorplan.'" class="img-responsive">';
								echo '<p class="text-center"> แปลน </p>';
								echo '</figure>';
							}

							foreach($photo as $index=>$value){

								echo '<figure>';
								echo '<img src="'.base_url().$value->filepath.'" class="img-responsive">';
								echo '<p class="text-center"> รูปที่ '.($value->num+1).' </p>';
								echo '</figure>';

							}
							?>
              </div>
            </div>
            <?php
					if(isset($content[0]->status)){
						if($content[0]->status == 1){
					?>
            <div class="form-group">
              <p class="text-center">

                <a href="<?php echo base_url();?>account/property/submit?id=<?php echo $content[0]->id;?>" class="btn btn-lg btn-success"> อัฟเดทประกาศ </a> </p>
            </div>
            <?php
						}
					}
				?>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $content[0]->geo_zoom; ?>,
	center: {lat: <?php echo $content[0]->lat; ?>, lng: <?php echo $content[0]->lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: false,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $content[0]->lat; ?>, lng: <?php echo $content[0]->lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=geo_location_zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}


</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
