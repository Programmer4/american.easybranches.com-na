<?php include('tpl.meta.php');?>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="business-card">
        <div class="business-card-body-full">
          <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="easybranches News" class="img-responsive"/></a></p>
          <br>
          <form id="form-signup" method="post">
            <?php if(isset($error)){?>
            <div class="alert alert-danger alert-dismissible text-center" role="alert"> <strong>Sorry!</strong> Please check Email and Password again. </div>
            <?php }?>
            <div class="form-group">
              <input name="firstname" class="form-control" placeholder="Firstname" aria-required="true" >
            </div>
            <div class="form-group">
              <input name="lastname" class="form-control" placeholder="Lastname" aria-required="true" >
            </div>

            <div class="form-group">
              <input name="email" class="form-control" placeholder="Email" aria-required="true" required type="email">
            </div>
            <div class="form-group">
              <input name="password" id="password" class="form-control" placeholder="Password" aria-required="true" required type="password">
            </div>
            <div class="form-group">
              <input name="password_again" id="password_again" class="form-control" placeholder="Password Again" aria-required="true" required type="password">
            </div>
            <div class="form-group">
              <p>P.S. Annual membership - € 10 / year</p>
            </div>
            <div class="form-group">
              <input name="save" type="submit" class="btn btn-primary btn-lg btn-block" value="Sign Up">
            </div>
            <div class="form-group">
              <p class="text-center"> <a href="<?php echo base_url();?>signin" class="forgot-password btn btn-link">Already have an account, sign-in</a> </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$( "#form-signup" ).validate({
  rules: {
    password: "required",
    password_again: {
      equalTo: "#password"
    }
  }
});
</script>
</body>
</html>
