<?php
  $uri = $_SERVER["REQUEST_URI"];
  $postUrl = 'yachts/search';
  if (strpos($uri, '/yachts/map') !== false) {
      $postUrl = 'yachts/map';
  }
?>

<form method="get" action="<?php echo base_url().$postUrl; ?>">
  <div class="form-group">
    <label>ประเภทเรือ</label>
    <select name="category_id" class="form-control">
      <option value=""> - ทั้งหมด -</option>
      <?php
							$select = '';
							foreach($categories as $index=>$value){

								if(isset($current_category[0]->category_id)){
									if($value->category_id == $current_category[0]->category_id){
										$select = ' selected';
									}else{
										$select = '';
									}
								}else{
									if(isset($category_id)){
										if($category_id == $value->category_id){
											$select = ' selected';
										}else{
											$select = '';
										}
									}
								}

								?>
      <option value="<?php echo $value->category_id; ?>" <?php echo $select; ?>><?php echo $value->category_title; ?></option>
      <?php
							}
							?>
    </select>
  </div>
  <div class="form-group">
    <label>ต้องการ</label>
    <select name="want_to" class="form-control">
    	<option value=""> - ทั้งหมด - </option>
    	<option value="sale"<?php if(isset($want_to)){ if($want_to == 'sale'){ echo ' selected';}}?>>ขาย</option>
    	<option value="charter"<?php if(isset($want_to)){ if($want_to == 'charter'){ echo ' selected';}}?>>ให้เช่า</option>
    </select>
  </div>
  <div class="form-group">
    <label>จังหวัด</label>
    <select name="location_id" class="form-control">
      <option value=""> - ทั้งหมด - </option>
      <?php
			$select = '';
			foreach($location as $index=>$value){
				if(isset($location_id)){
					if($value->location_id == $location_id){
						$select = ' selected';
					}else{
						$select = '';
					}
				}
				?>
      <option value="<?php echo $value->location_id; ?>"<?php echo $select;?>><?php echo $value->title; ?></option>
      <?php
			}
			?>
    </select>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success btn-block" value="Search">
  </div>
</form>
<script>
$('.datepicker').datepicker({
	format: 'yyyy-mm-dd',
	startDate: '<?php echo date('Y-m-d');?>',
	autoclose: true
});
</script>
