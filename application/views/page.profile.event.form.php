<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>account/event">กิจกรรม</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'ส่งประกาศ';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="profile">
            <h2><span>ข้อมูลกิจกรรม</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>ชื่อกิจกรรมของคุณ</label>
                <input type="text" name="title" class="form-control" required value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
              </div>
              <div class="form-group">
                <label>ประเภทของกิจกรรมของคุณ</label>
                <select name="type_id" class="form-control" required>
                  <option value=""> - เลือก - </option>
                  <?php 
								$select = '';
								foreach($type as $value){
									if(isset($content[0]->type_id)){
										if($content[0]->type_id == $value->type_id){
											$select = ' selected';
										}else{
											$select = '';
										}
									}
									echo '<option value="'.$value->type_id.'" '.$select.'>'.$value->title.'</option>';
								}?>
                </select>
              </div>
              <div class="form-group">
                <label>ที่อยู่</label>
                <input type="text" name="address" class="form-control" required value="<?php if(isset($content[0]->address)){ echo $content[0]->address;}?>">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>เมือง</label>
                    <input type="text" name="city" class="form-control" required value="<?php if(isset($content[0]->city)){ echo $content[0]->city;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>จังหวัด</label>
                    <input type="text" name="province" class="form-control" required value="<?php if(isset($content[0]->province)){ echo $content[0]->province;}?>">
                  </div>
                  <div class="col-sm-4">
                    <label>รหัสไปรษณีย์</label>
                    <input type="text" name="zipcode" class="form-control" required value="<?php if(isset($content[0]->zipcode)){ echo $content[0]->zipcode;}?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label>โทรศัพท์</label>
                    <input type="text" name="phone" class="form-control" value="<?php if(isset($content[0]->phone)){ echo $content[0]->phone; }else if(isset($member[0]->phone)){ echo $member[0]->phone; }?>" required>
                  </div>
                  <div class="col-sm-4">
                    <label>แฟกซ์</label>
                    <input type="text" name="fax" class="form-control" value="<?php if(isset($content[0]->fax)){ echo $content[0]->fax; }else if(isset($member[0]->fax)){ echo $member[0]->fax; }?>">
                  </div>
                  <div class="col-sm-4">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" value="<?php  if(isset($content[0]->email)){ echo $content[0]->email; }else if(isset($member[0]->email)){ echo $member[0]->email; }?>" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>เว็บไซด์</label>
                <input type="text" name="website" class="form-control" value="<?php if(isset($content[0]->website)){ echo $content[0]->website;}?>">
              </div>
              <div class="form-group">
                <label>คำอธิบายโดยย่อของกิจกรรมของคุณ</label>
                <textarea name="description" class="form-control" required><?php if(isset($content[0]->description)){ echo $content[0]->description;}?>
</textarea>
              </div>
              <div class="form-group">
                <label>รายละเอียดของกิจกรรม</label>
                <textarea name="detail" class="form-control summernote"><?php if(isset($content[0]->detail)){ echo $content[0]->detail; }?>
</textarea>
              </div>
              <?php
if(isset($content[0]->time_start)){
	$startdate = $content[0]->time_start;
}else{
	$startdate = date('Y-m-d H:i:s');
}
if(isset($content[0]->time_end)){
	$startdate2 = $content[0]->time_end;
}else{
	$startdate2 = date('Y-m-d H:i:s');
}
?>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label>เริ่ม</label>
                    <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" name="time_start" required value="<?php echo $startdate; ?>" />
                      <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                  </div>
                  <div class="col-xs-6">
                    <label>จบ</label>
                    <div class='input-group date' id='datetimepicker2'>
                      <input type="text" name="time_end" class="form-control" required value="<?php echo $startdate2; ?>" />
                      <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>กิจกรรมฟรีหรือไม่?</label>
                    <select name="free" class="form-control" required>
                      <option value="1" <?php if(isset($content[0]->free)){ if($content[0]->free == 1){ echo ' selected';}}?>>ใช่</option>
                      <option value="0" <?php if(isset($content[0]->free)){ if($content[0]->free == 0){ echo ' selected';}}?>>ไม่ใช่</option>
                    </select>
                  </div>
                  <div class="col-sm-6">
                    <div id="price" <?php echo ' style="display:none;" ';?>>
                      <label>ราคา</label>
                      <input type="text" name="price" class="form-control" required value="<?php if(isset($content[0]->price)){ echo $content[0]->price; }?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>โปสเตอร์งานของคุณ</label>
                <input type="file" name="thumbnail" <?php if(empty($content[0]->id)){ echo 'required';}?>>
              </div>
              <h2><span>แผนที่งานกิจกรรม</span></h2>
              <?php

if(empty($content[0]->lat) && empty($content[0]->lng)){
	$lat = '13.7248946';
	$lng = '100.4930264';
	$zoom = 5;
}else{
	$lat = $content[0]->lat;
	$lng = $content[0]->lng;
	$zoom = $content[0]->zoom;
}
?>
              <div id="google-map" style="min-height:350px; margin-top:-30px; margin-bottom:15px; border-left:0; border-right:0; border-top:0; border-bottom: solid 1px #eff2f5"></div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6">
                    <label>ละติจูด</label>
                    <input type="text" name="lat" class="form-control" value="<?php echo $lat;?>">
                  </div>
                  <div class="col-sm-6">
                    <label>ลองจิจูด</label>
                    <input type="text" name="lng" class="form-control" value="<?php echo $lng;?>">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <p class="text-center">
                  <input type="submit" name="save" class="btn btn-lg btn-success" value="ลงประกาศของคุณ">
                </p>
              </div>
              <input type="hidden" name="geo">
              <input type="hidden" name="zoom" value="<?php echo $zoom; ?>">
              <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id;}?>">
            </form>
          </div>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script> 
<script>
$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});
$('.validate').validate();

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: <?php echo $zoom; ?>,
	center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();
		
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		
		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);
		
		$('input[name=geo]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}


function initMap2(lat, lng, zoom) {
	var map = new google.maps.Map(document.getElementById('google-map'), {
	zoom: zoom,
	center: {lat: lat, lng: lng}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: lat, lng: lng}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();
		
		var lat = event.latLng.lat();
		var lng = event.latLng.lng();
		
		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);
		
		$('input[name=geo]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
	$('input[name=zoom]').val(map.getZoom());
	});

	marker.addListener('click', toggleBounce);
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

$('input[name=lat], input[name=lng]').change(function(){
	
	var lat = eval($('input[name=lat]').val());
	var lng = eval($('input[name=lng]').val());
	var zoom = eval($('input[name=zoom]').val());
	
	initMap2(lat, lng, zoom)
});


$('#datetimepicker1').datetimepicker({'autoclose':true, 'setStartDate': '<?php echo $startdate2; ?>'});
$('#datetimepicker2').datetimepicker({'autoclose':true, 'setStartDate': '<?php echo $startdate2; ?>'});

function price(val){
	if(val == 0){
		$('#price').show()
	}else{
		$('#price').hide();
	}
}

$('select[name=free]').change(function(){
	price(this.value);
});

price($('select[name=free]').val());
</script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo $google_api; ?>&callback=initMap"></script>
<?php include('tpl.footer.php');?>
