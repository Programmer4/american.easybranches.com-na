<p>
<a href="<?php echo base_url();?>contribute" class="pull-right" target="_blank">Post your ads</a>
<strong>Sponsored</strong>
</p>
<ul class="list-unstyled">
	<?php
	foreach($banner as $value){
		$url = base_url().urlencode($value->category_slug).'/'.urlencode($value->slug);
	?>
  <li>
<?php
		if(is_file($value->thumbnail)){

			$g = getimagesize($value->thumbnail);

			if($g[0] > $g[1]){
				$w = 265;
				$h = 165;
				$c = '2.65:1.65';
			}else{
				$w = 300;
				$h = 400;
				$c = '3:4';
			}


			echo '<p><a href="'.$url.'" target="_blank"><img src="'.base_url().'/newspic/'.date("Y/m", strtotime($value->entered)).'/' .$w. 'x' .$h. '/' .$value->id. '" alt="'.$value->title.' - easybranches News"></a></p>';

		}

?>
  	

  	<a href="<?php echo $url; ?>" target="_blank" class="overflow ellipsis"><?php echo $value->title;?></a>
    <p class="description"><?php echo trim($value->description); ?></p>
	</li>
  <?php
	}?>
</ul>
