<form method="get" action="<?php echo base_url().'property/search'?>">
  <div class="form-group">
    <label>จังหวัด</label>
    <select name="province" class="form-control">
      <option value=""> - ทั้งหมด - </option>
      <?php
			$rs = $this->Yacht->get_th_province_list();
			foreach($rs as $index=>$value){

					if($this->input->get('province') == $value->title){
						$select = ' selected';
					}
          else if($province == $value->title)
          {
            	$select = ' selected';
          }
          else{
						$select = '';
					}
				?>
      <option value="<?php echo $value->title; ?>"<?php echo $select; ?>><?php echo $value->title;?></option>
      <?php
			}
			?>
    </select>
  </div>
  <div class="form-group">
    <label>ประเภท</label>
    <select name="type" class="form-control">
      <option value=""> - ทั้งหมด - </option>
      <option value="condo"<?php if(isset($type)){if($type == 'condo'){ echo ' selected';}}?>>คอนโด / อพาร์ทเม้นท์</option>
      <option value="house"<?php if(isset($type)){if($type == 'house'){ echo ' selected';}}?>>บ้าน / วิวล่า</option>
      <option value="land"<?php if(isset($type)){if($type == 'land'){ echo ' selected';}}?>>ที่ดิน</option>
    </select>
  </div>
  <div class="form-group">
    <label>ต้องการ</label>
    <select name="want_to" class="form-control">
      <option value=""> - ทั้งหมด - </option>
      <option value="sale" <?php if(isset($want_to)){if($want_to == 'sale'){ echo ' selected';}}?>>ขาย</option>
      <option value="rentout" <?php if(isset($want_to)){if($want_to == 'rentout'){ echo ' selected';}}?>>ให้เช่า</option>
    </select>
  </div>
  <div class="form-group">
    <label>ราคา</label>
    <select name="price_from" class="form-control">
      <option value=""> - ทั้งหมด - </option>
      <option value="less than 2M" <?php if(isset($price_from)){if($price_from == 'less than 2M'){ echo ' selected';}}?>> น้อยกว่า 2 ล้านบาท</option>
      <option value="between 2M and 5M" <?php if(isset($price_from)){if($price_from == 'between 2M and 5M'){ echo ' selected';}}?>> ตั้งแต่ 2ล้าน ถึง 5ล้าน</option>
      <option value="between 5M and 10M" <?php if(isset($price_from)){if($price_from == 'between 5M and 10M'){ echo ' selected';}}?>> ตั้งแต่ 5ล้าน ถึง 10ล้าน</option>
      <option value="between 10M and 20M" <?php if(isset($price_from)){if($price_from == 'between 10M and 20M'){ echo ' selected';}}?>> ตั้งแต่ 10ล้าน ถึง 5ล้าน</option>
      <option value="between 20M and 50M" <?php if(isset($price_from)){if($price_from == 'between 20M and 50M'){ echo ' selected';}}?>> ตั้งแต่ 2ล้าน ถึง 5ล้าน</option>
      <option value="more than 50M" <?php if(isset($price_from)){if($price_from == 'more than 50M'){ echo ' selected';}}?>>มากกว่า 50 ล้าน</option>
    </select>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success btn-block" value="Search">
  </div>
</form>
