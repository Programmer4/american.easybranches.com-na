<div class="topstory">
  <form method="get" action="<?php echo base_url();?>property/search">
    <h5>Search for property in Phuket</h5>
    <div class="form-group">
      <label>Looking for</label>
      <select class="form-control">
        <option value=""> - All - </option>
        <option value="condo"> Condo / Apartment </option>
        <option value="house">House / Villa</option>
        <option value="land">Land</option>
      </select>
    </div>
    <div class="form-group">
      <label>To</label>
      <select class="form-control">
        <option value=""> - Rent &amp; Buy - </option>
        <option value="sale">Buy</option>
        <option value="rentout">Rent</option>
      </select>
    </div>
    <div class="form-group">
      <label>Where you looking for</label>
      <select class="form-control">
        <option> - All Locations in Phuket - </option>
        <?php 
						foreach($property_location as $index=>$value){
							echo '<option value="'.$value->location_id.'">'.$value->title.'</option>';
						}
						?>
      </select>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-success btn-block"> Search</button>
      <p class="text-center mt-30">
      <a href="<?php echo base_url();?>signup" class="text-center">List Your property with us</a>
      </p>
    </div>
  </form>
</div>
