<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>account/dashboard">Account</a></li>
        <li><a href="<?php echo base_url(); ?>account/news">News</a></li>
        <li class="active">
          <?php if(empty($content[0]->id)){ echo 'Compose';}else{ echo $content[0]->title; }?>
        </li>
      </ul>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <?php if($save){?>
          <div class="alert alert-success text-center"> Your story has been update </div>
          <?php }?>
          <div class="profile">
            <h2><span>Headline</span></h2>
            <form method="post" class="validate" enctype="multipart/form-data">
              <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control" required placeholder="Title of your story" value="<?php if(isset($content[0]->title)){ echo $content[0]->title; }?>">
              </div>
              <div class="form-group">
                <label>Category</label>
                <select name="category_id" class="form-control" required>
                  <option value=""> - Select - </option>
                  <?php
							$select = '';
							foreach($category as $index=>$value){
								if($value->category_id == $content[0]->category_id){
									$select = ' selected';
								}else{
									$select = '';
								}
								echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
							}
							?>
                </select>
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control" maxlength="160" rows="2" required placeholder="Short description for your story maximum 160 charectors"><?php
						if(isset($content[0]->description)){ echo $content[0]->description;}
						?>
</textarea>
              </div>
              <div class="form-group">
                <label>Keywords</label>
                <input type="text" name="keywords" class="form-control" placeholder="Keywords, Business, Branches" value="<?php if(isset($content[0]->keywords)){ echo $content[0]->keywords;}?>" required>
              </div>
              <div class="form-group">
                <label>Hashtags</label>
                <input type="text" name="hashtags" class="form-control" placeholder="#YourTags, #Business, #Branches" value="<?php if(isset($content[0]->hashtags)){echo $content[0]->hashtags;}?>">
              </div>
              <div class="form-group">
                <label>Video</label>
                <input type="text" name="video" class="form-control" placeholder="URL of YouTube or URL of Vimeo" value="<?php if(isset($content[0]->video)){echo $content[0]->video;}?>">
              </div>
              <div class="form-group">
                <label>Photo</label>
                <input type="file" name="photo" <?php if(empty($content[0]->id)){ echo 'required'; }?>>
              </div>
              <h2><span>Detail Text</span></h2>
              <div class="form-group">
                <textarea name="detail" class="form-control summernote"><?php if(isset($content[0]->detail)){echo $content[0]->detail;}?>
</textarea>
              </div>
              <div class="form-group">
                <p class="text-center">
                  <input type="submit" name="save" class="btn btn-success" value="<?php if(empty($content[0]->id)){ echo 'Compose Your News';}else{ echo 'Update';}?>">
                  <input type="hidden" name="id" value="<?php if(isset($content[0]->id)){ echo $content[0]->id; }?>">
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <br>
</section>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
<script>
$('.summernote').summernote({
	height: 250,
	theme: 'monokai'
});
$('.validate').validate();
</script>
<?php include('tpl.footer.php');?>
