<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Be a Contributor and Contribute Your content Post as a Journalist</title>
<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/css/contribute.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet" hreflang="en">
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/localization/messages_th.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/summernote/summernote.min.js"></script>
</head>
<body>
<br>
<br>
<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <h1>Contribute Your Post in Easy Branches Network</h1>
        <form method="post" enctype="multipart/form-data">
          <div class="personal">
		        <h2>Tell us 'Easy Branches' about You</h2>
            <div class="form-group">
              <label>Contact Person</label>
              <input type="text" name="contact" class="form-control required" value="<?php if(isset($editor)){echo $editor;} ?>">
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control required" value="<?php if(isset($email)){echo $email;}?>">
            </div>
            <div class="form-group">
              <label>Company Name</label>
              <input type="text" name="company" class="form-control required" value="<?php if(isset($company)){echo $company;}?>">
            </div>

            <div class="form-group">
	            <label>Define Your Target Audience</label>
            	<div id="google-map" style="height:350px;"></div>
            </div>

            <div class="form-group">
            	<input id="continue-1" class="btn btn-lg btn-success" type="button" value="Continue">
            	<input id="back-1" class="btn btn-lg btn-default" type="button" value="Go Back" onClick="window.location.href='<?php echo base_url();?>contribute'">
              <input type="hidden" name="lng" value="<?php echo $lng;?>">
              <input type="hidden" name="lat" value="<?php echo $lat;?>">
              <input type="hidden" name="geo_location_zoom" value="<?php echo $zoom;?>">
              <input type="hidden" name="id" value="<?php if(isset($id)){ echo $id;}?>">

            </div>
          </div>
          <div class="contents" <?php  echo ' style="display:none; "';?>>
	          <h2>Your Post</h2>
            <div class="row">
            	<div class="col-lg-12">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" required name="category_id">
                  	<option value=""> - Select - </option>
                    <?php
										foreach($category as $value){
											if(isset($category_id)){
												if($category_id == $value->category_id){
													$select = ' selected';
												}else{
													$select = '';
												}
											}
											echo '<option value="'.$value->category_id.'"'.$select.'>'.$value->category_title.'</option>';
										}
										?>
                  </select>
                </div>
              </div>
            </div>
          	<div class="form-group">
              <label>Title</label>
              <input type="text" name="title" class="form-control" required value="<?php if(isset($title)){echo $title;} ?>">
            </div>
          	<div class="form-group">
              <label>Keywords</label>
              <input type="text" name="keywords" class="form-control" required value="<?php if(isset($keywords)){ echo $keywords;}?>">
              <div class="help-block"> Separate keywords with commas.</div>
            </div>
          	<div class="form-group">
              <label>Short Description</label>
              <textarea name="description" class="form-control" required maxlength="255"><?php if(isset($description)){ echo $description;}?></textarea>
            </div>
          	<div class="form-group">
              <label>Detail</label>
              <textarea name="detail" id="summernote" class="html" required><?php if(isset($detail)){ echo $detail; } ?></textarea>
            </div>
          	<div class="form-group">
              <label>Photo</label>
              <input type="file" name="photo" <?php if(empty($id)){ echo 'required';}?>>
              <div class="help-block"> The image should be approximately 1024x768 pixels and the file size should not over 150Kb.</div>
            </div>
          	<div class="form-group">
            	<input id="continue-2" class="btn btn-lg btn-success" name="save" type="submit" value="Preview" onClick="$(this).attr('disabled')">
            	<input id="back-2" class="btn btn-lg btn-default" type="button" value="Go Back">
              <input type="hidden" name="lat" value="<?php echo $lat;?>">
              <input type="hidden" name="lng" value="<?php echo $lng;?>">
              <input type="hidden" name="zoom" value="<?php echo $zoom;?>">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>

$('form').validate({
	custom: {
		summernote: function($el){
			if($el.code().length<=11) {
				return false
			}	else{
				return true
			}
		}
	}
});

$('#continue-1').click(function(){

	var error = 0;
	$('.personal .required').each(function(){
		if(this.value == false){
			error++;
		}
	});

	if(error == 0){
		$('.personal').hide();
		$('.contents').fadeIn();
		$('.preview').hide();
	}else{
		alert('Please complete the form below.');
	}
});

$('#back-1').click(function(){
		$('.personal').fadeIn();
		$('.contents').hide();
});

$('#back-2').click(function(){
		$('.personal').fadeIn();
		$('.contents').hide();
});

$('.html').summernote({
	height: 250
});

var marker;

function initMap() {
	var map = new google.maps.Map(document.getElementById('google-map'), {
		zoom: <?php echo $zoom; ?>,
		center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng;?>}
	});

	marker.addListener('dragend', function(event){
		address = marker.getPlace();

		var lat = event.latLng.lat();
		var lng = event.latLng.lng();

		$('input[name=lat]').val(lat);
		$('input[name=lng]').val(lng);

		$('input[name=geo_location]').val(lat + "," + lng);
	});

	map.addListener('zoom_changed', function() {
		$('input[name=zoom]').val(map.getZoom());
	});
}

function sub_cate(val)
{
	$('#sub_category').html('<option value=""> Loading... </option>');

	if(val > 0){
		var url = '<?php echo base_url();?>contribute/subcategory?parent_id=' + val;
		$('#sub_category').load(url);
	}else{
		$('#sub_category').html('<option value=""> - Select - </option>');
	}
}

sub_cate($('select[name=parent_category_id]').val());
$('select[name=parent_category_id]').change(function(){
	sub_cate(this.value);
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyB1EJJFs5Dc1mh8E08jlpSJwQrXUi_b-so&callback=initMap"></script>
</body>
</html>
