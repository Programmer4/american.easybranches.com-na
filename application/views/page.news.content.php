<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php if(isset($meta_title)){ echo $meta_title; } ?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="<?php if(isset($meta_description)){ echo $meta_description; }?>" />
<meta name="keywords" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>" />
<meta name="news_keywords" content="<?php if(isset($meta_keywords)){ echo $meta_keywords; }?>" />
<meta name="fb_title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>" />
<meta property="og:site_name" content="America.Easybranches.com" />
<meta property="og:title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>" />
<meta property="og:description" content="<?php if(isset($meta_description)){echo $meta_description;}?>" />
<meta property="og:url" content="<?php if(isset($meta_url)){echo $meta_url;}?>" />
<meta property="og:type" content="article" />
<meta id="og-image" property="og:image" content="<?php if(isset($meta_image)){echo $meta_image;}?>" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="twitter:site" content="@EasyBranches" />
<meta property="twitter:title" content="<?php if(isset($meta_title)){ echo $meta_title; } ?>" />
<meta property="twitter:description" content="<?php if(isset($meta_description)){ echo $meta_description;}?>" />
<meta property="twitter:url" content="<?php if(isset($meta_url)){ echo $meta_url;}?>" />
<meta name="twitter:image" content="<?php if(isset($meta_image)){ echo $meta_image;}?>" />
<link href="<?php echo base_url();?>" rel="alternate" hreflang="nl" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/base_style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" hreflang="en">
<link href="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.css" rel="stylesheet" hreflang="en">

<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/fancybox-3.0/dist/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>assets/jwplayer-7.9.1/jwplayer.js"></script>
<script>jwplayer.key="fVMElcMwXIEFkRobACNvv6mrYcBJV5eek3Rwjg==";</script>
<?php

$amp_items[] = '"@context": "http://schema.org"';
$amp_items[] = '"@context": "http://schema.org"';
if(isset($meta_title)){
	$amp_items[] = '"headline": "'.$meta_title.'"';
}
if(isset($meta_image)){
	$amp_items[] = '"image": ["'.$meta_image.'"]';
}

?>
<script type="application/ld+json">{<?php echo implode(',',$amp_items);?>}</script>
<?php include('google.tagmanager.js.php');?>
</head>
<body class="<?php if(isset($module)){ echo $module; }?>">
<?php include('google.tagmanager.html.php');?>
<?php include('tpl.header.php');?>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>">Home</a></li>
        <li><a href="<?php echo base_url();?><?php echo $category[0]->category_slug;?>"><?php echo $category[0]->category_title; ?></a></li>
        <li class="active"><?php echo $content[0]->title;?></li>
      </ul>
      <div class="row">
        <div class="col-lg-8">
          <div id="story">
            <?php
					if($content[0]->video){


								if (strpos($content[0]->video, 'static.rtl.nl/embed') !== false || strpos($content[0]->video, 'abcnews.go.com/video/embed') !== false) {
									?>
								    <iframe src="<?php echo $content[0]->video; ?>" allowfullscreen="allowfullscreen" style="height: 350px;width:100%"></iframe>
								  <?php }
									else {
						?>

				            <div id="jwplayer"></div>
				            <script>
											var playerInstance = jwplayer("jwplayer")
											playerInstance.setup({
												image:'<?php echo base_url().$content[0]->thumbnail;?>',
												file: "<?php echo $content[0]->video;?>",
												width: "100%",
												aspectratio: "16:9",
												autostart: true
											});

											<?php

												$sql = "SELECT * FROM phuketnews_news WHERE video != '' AND status = '1' AND id < '".$content[0]->id."' AND category_id = '".$content[0]->category_id."' ORDER BY id DESC LIMIT 0,1";
												$query = $this->db->query($sql);
												$rs = $query->result();
												if(isset($rs[0]->slug)){
											?>

											playerInstance.onReady(function() {
											    playerInstance.onComplete(function() {
												    window.location.href = '<?php echo base_url().urlencode($rs[0]->category_slug).'/'.urlencode($rs[0]->slug); ?>';
											    });
											    playerInstance.play();
											});
											<?php
												}

											?>
											</script>
            <?php
											}
					}else{
						if(is_file($content[0]->thumbnail)){
						?>
            <figure class="full"> <a href="<?php echo base_url().$content[0]->thumbnail;?>" data-fancybox data-caption="<?php echo $content[0]->title;?>"> <img src="<?php echo base_url().$content[0]->thumbnail;?>" alt="<?php echo $content[0]->title;?> - easybranches News"> </a> </figure>
            <?php
						}
					}
					?>
          <div id="heading">
              <h1><?php echo $content[0]->title;?></h1>
              <div class="entered">
                <?php
									echo '<div class="row">';
									echo '<div class="col-sm-5">';
									if($content[0]->credit){

										$credit = explode('"',$content[0]->credit);

										echo '<div class="credit">';
										echo 'credit: <a href="'.stripslashes($credit[7]).'" target="_blank">'.$credit[3].'</a>';
										#echo 'credit: '.$credit[3];
										echo '</div>';
									}else{

										echo '<div class="credit">';
										if($content[0]->editor){
											if(isset($editor[0]->slug)){
												echo 'credit: '.$content[0]->editor;
												//echo 'credit: <a href="'.base_url().'news/editor/'.strtolower(urlencode($editor[0]->slug)).'" target="_blank">'.$content[0]->editor.'</a>';
											}else{
												echo 'credit: '.$content[0]->editor.'';
											}
										}
										echo '</div>';

									}


									echo '<ul class="list-inline">';
									echo '<li>'.$this->Entered->time_elapsed_string($content[0]->entered).'</li>';
									echo '<li><i class="glyphicon glyphicon-signal"></i> <span id="counter-views">'.number_format($content[0]->views,'0','',',').'</span> views</li>';
									echo '</ul>';
									echo '</div>';
									echo '<div class="col-sm-7">';
?>
                <script>
$('#counter-views').load('<?php echo base_url()?>story/views?id=<?php echo $content[0]->id;?>');
</script>
                <?php
								$share = 'http://us.janj.eu/n/'.($content[0]->id);
								echo $this->Share->push($share, base_url().$content[0]->thumbnail);
								echo '</div>';
								echo '</div>';
					?>
              </div>
            </div>
            <div id="story-text">
              <?php
					if($content[0]->member_id == 0){
						echo $content[0]->detail;
					}else{
						echo $content[0]->detail;
					}

?>
<?php
					$tags = explode(',', $content[0]->keywords);
					$arr = array('#','/','[',']', '“','”', '(', ')','"');

					foreach($tags as $index=>$value){
						$value = trim($value);
						$value = str_replace($arr,'',$value);
						if(strlen($value) > 1){
							$items[] = '<a href="'.base_url().'news/tag/'.urlencode($value).'"> </i>'.$value.'</a> ';
						}
					}

					echo '<div class="hashtags">';
					echo '<h3>Follow Us</h3>';
					echo '<p>';
					echo '<a href="https://twitter.com/BesteNews" class="twitter-follow-button" data-size="large" data-show-screen-name="false" data-dnt="true" data-show-count="false">Follow @BesteNews</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>';
					echo '</p>';
					echo '</div>';

					if(isset($items)){
						echo '<div class="hashtags">';
						echo '<h3><span>Tags</span></h3>';
						echo implode(' ', $items);
						echo '</div>';
					}

					?>
            </div>
          </div>
          <div id="comments">
            <p class="text-center">
            	<br>
              <br><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <br>
              <span>Loading.....</span> <br>
              <br>
            </p>
          </div>
          <div id="comments-form">
            <p class="text-center"> <br>
              <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <br>
              <span>Loading...</span> <br>
              <br>
            </p>
          </div>
          <script>
        // $('img.lazy').each(function (){
				// 	var src = $(this).attr('data-src');
				// 	$(this).removeAttr('srcset');
				// 	$(this).attr('src', src);
				// });

				$('#story-text img').each(function(){
					if($(this).attr('height')){
						$(this).removeAttr('height');
					}
				});

				$("[data-fancybox]").fancybox({});

				$('#comments').load('<?php echo base_url().'comments?uri='.urlencode('story/'.$slug);?>&page=1');
				$('#comments-form').load('<?php echo base_url().'comments/form?uri='.urlencode('story/'.$slug);?>');

        </script>
          <?php
				if(count($related) > 0){
					$rows = ceil(count($related)/3);
					$r = 1;
					$n = 0;
					echo '<div id="related">';
					echo '<h2>Related Stories</h2>';


					while($r <= $rows){
					?>
          <div class="row">
            <?php
						for($i=1; $i<=3; $i++){
							if(isset($related[$n]->id)){
								$content_url = base_url().$related[$n]->category_slug.'/'.urlencode($related[$n]->slug);
						?>
            <div class="col-lg-4">
              <div class="business-card">
                <figure><a href="<?php echo $content_url;?>">
                	<?php
								if(is_file($related[$n]->thumbnail)){

									$g = getimagesize($related[$n]->thumbnail);

									if($g[0] > $g[1]){
										$w = 265;
										$h = 165;
										$c = '2.65:1.65';
									}else{
										$w = 165;
										$h = 265;
										$c = '1.65:2.65';
									}

									?>
                <img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($related[$n]->entered)).'/' .$w. 'x' .$h. '/' .$related[$n]->id; ?>" alt="<?php echo $related[$n]->title; ?> - easybranches News">
								<?php
								}else{
									echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png"  alt="'.$related[$n]->title.' - easybranches News">';
								}?>
                 </a></figure>
                <div class="business-card-body"><a href="<?php echo $content_url;?>" class="overflow ellipsis"> <strong><?php echo $related[$n]->title;?></strong> </a>
                	<small><?php echo $this->Entered->time_elapsed_string($related[$n]->entered); ?></small>
                  <p><?php echo $related[$n]->description; ?></p>
                </div>
								<?php
									$content_url = 'http://us.janj.eu/n/'.$related[$n]->id;
								?>
                <div class="business-card-footer">
                  <ul class="list-inline">
                    <li class="pull-left"><a href="<?php echo base_url().''.$related[$n]->category_slug;?>"><?php echo $related[$n]->category_title; ?></a></li>
                    <li><a href="#" class="modal-email" data-message="ดูที่ <?php echo $content_url;?> หน่อยสิ" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
                    <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-image="<?php echo base_url().$related[$n]->thumbnail;?>"
                    data-url="<?php echo $content_url;?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
                    <li></li>
                  </ul>
                </div>
              </div>
            </div>
            <?php
							}
							$n++;
						}?>
          </div>
          <?php
						$r++;
					}
					echo '</div>';
				}
?>
        </div>
        <div class="col-lg-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.phuket.php');?>
          <?php include('tpl.news.banner.php');?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
