<?php include('tpl.meta.php');?>
<?php include('tpl.header.php');?>
<style>
h1{
	font-size: 16px;
}
h1 span{
	diplay:block;
}
</style>
<section class="light-gray">
  <div id="container-content">
    <div class="container">
      <div class="row section-heading">
        <div class="col-lg-6">

        	<h1>Comprehensive <?php echo $category[0]->category_title; ?> up to the minute head lines live updates <span> compelling articles and comments from features up to date news</span> </h1>
          <h2>Latest breaking update <?php echo $category[0]->category_title; ?> News.</h2>
          <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>">Home</a></li>
            <li class="active"><?php echo $category[0]->category_title; ?></li>
          </ul>
        </div>
        <div class="col-lg-6"> <?php echo $this->Share->push(base_url().''.$category[0]->category_slug, base_url().$news['rows'][0]->thumbnail);?> </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-lg-8">

        	<div class="row">
          	<div class="col-lg-12">

          <div class="cate-latest mt-15">
            <?php

if(isset($news['rows'][0]->id)){

							$news['rows'][0]->slug = urlencode($news['rows'][0]->slug);

							echo '<div class="latest">';
							echo '<div class="latest-cover">';
							echo '<div class="latest-cover-overlay"></div>';
							if(is_file($news['rows'][0]->thumbnail)){

								echo '<img class="lazy" data-original="'.base_url().'newspic/'.date("Y/m", strtotime($news['rows'][0]->entered)).'/750x495/' .$news['rows'][0]->id. '" alt="'.$news['rows'][0]->title.' - easybranches News">';
							}else{
								echo '<img class="lazy" data-original="'.base_url().'assets/images/default-news.png" alt="'.$news['rows'][0]->title.' - easybranches News">';
							}
							echo '<div class="latest-caption">';
							echo '<h1><a href="'.base_url().$news['rows'][0]->category_slug.'/'.$news['rows'][0]->slug.'">'.$news['rows'][0]->title.'</a></h1>';
							echo '<p class="hidden-sm hidden-xs">'.$news['rows'][0]->description.'</p>';
							echo '<div class="latest-entered">';
							echo '<ul class="list-inline share-this share-min">';
							echo '<li>'.$this->Entered->time_elapsed_string($news['rows'][0]->entered).'</li>';
							echo '<li><a href="'.base_url().'email/share?u=" data-url="'.base_url().$news['rows'][0]->category_slug.'/'.$news['rows'][0]->slug.'" class="button-envelope"><i class="fa fa-fw fa-envelope"></i></a></li>';
							echo '<li><a href="#" data-toggle="modal" data-target="#modal-share" data-url="'.base_url().$news['rows'][0]->category_slug.'/'.$news['rows'][0]->slug.'" data-image="'.base_url().$news['rows'][0]->thumbnail.'" class="button-share"><i class="fa fa-fw fa-share-alt"></i></a></li>';
							echo '</ul>';
							echo '</div>';
							echo '</div>';

							echo '</div>';
							echo '</div>';
}

			?></div>
          </div>
          </div>

          <div class="row">
            <?php

	   		$n = 1;
				while($n < count($news['rows'])){
					$content_url = base_url().$news['rows'][$n]->category_slug.'/'.urlencode($news['rows'][$n]->slug);
?>
            <div class="col-lg-4">
              <div class="business-card">
                <figure> <a href="<?php echo $content_url;?>">
                  <?php
									if(is_file($news['rows'][$n]->thumbnail)){

										$g = getimagesize($news['rows'][$n]->thumbnail);

										if($g[0] > $g[1]){
											$w = 400;
											$h = 300;
											$c = '4:3';
										}else{
											$w = 300;
											$h = 400;
											$c = '3:4';
										}
										?>
									<img class="lazy" data-original="<?php echo base_url().'newspic/'.date("Y/m", strtotime($news['rows'][$n]->entered)).'/' .$w. 'x' .$h. '/' .$news['rows'][$n]->id; ?>" alt="<?php echo $news['rows'][$n]->title; ?> - easybranches News">
                  <?php }else{?>
                  <img class="lazy" data-original="<?php echo base_url()?>assets/images/default-news.png" alt="<?php echo $news['rows'][$n]->title; ?> - easybranches News">
                  <?php }?>
                  </a> </figure>
                <div class="business-card-body"><a href="<?php echo $content_url; ?>">
                  <h3 class="overflow ellipsis"><?php echo $news['rows'][$n]->title; ?></h3>
                  </a>
                  <h4><small><?php echo $this->Entered->time_elapsed_string($news['rows'][$n]->entered);?></small></h4>
                  <?php echo '<p>'.$news['rows'][$n]->description.'</p>'; ?> </div>
                <div class="business-card-footer">
                  <ul class="list-inline">
                    <li class="pull-left"><a href="<?php echo base_url().''.$category[0]->category_slug;?>"><?php echo $category[0]->category_title; ?></a></li>
                    <li><a href="#" class="modal-email" data-message="Have a look at <?php echo base_url() . $news['rows'][$n]->category_slug ;?>/<?php echo $news['rows'][$n]->slug; ?> หน่อยสิ :)" data-toggle="modal" data-target="#modal-email"><i class="fa fa-fw fa-envelope-o"></i></a></li>
                    <li><a href="#" class="share-url" data-toggle="modal" data-target="#modal-share" data-url="<?php echo base_url() . $news['rows'][$n]->category_slug;?>/<?php echo $news['rows'][$n]->slug; ?>" data-image="<?php echo base_url().$news['rows'][$n]->thumbnail;?>"><i class="fa fa-fw fa-share-alt"></i></a></li>
                    <li></li>
                  </ul>
                </div>
              </div>
            </div>
            <?php
					$n++;
				}
				?>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <?php include('tpl.news.topstory.php');?>
          <?php include('tpl.news.phuket.php');?>
          <?php

					$content = $category;
					include('tpl.news.banner.php');
					?>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <?php
	if($news['pages'] > 1){
		$url = base_url().''.$slug;
		echo $this->Paginate->pages($url, $page, $news['pages']);
	}
?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('tpl.footer.php');?>
