<?php
if(count($comments['rows'])){

	echo '<h3>Comments</h3>';

	echo '<div class="cover">';
	foreach($comments['rows'] as $index=>$value){

		echo '<div class="row">';
		echo '<div class="col-xs-3">';
		if(is_file('uploads/profile/'.sprintf('%05d',$value->member_id).'.jpg')){
			echo '<img src="'.base_url().'uploads/profile/'.sprintf('%05d',$value->member_id).'.jpg"  class="avatar-75">';
		}else{
			echo '<img src="'.base_url().'assets/images/avatar.jpg" class="avatar-75">';
		}
		echo '<strong>'.$value->name.'</strong>';
		echo '</div>';
		echo '<div class="col-xs-9">'.nl2br($value->message).'</div>';
		echo '</div>';
	}
	echo '</div>';

	if($comments['pages'] > 1){

		$url = base_url().'comments?uri='.urlencode($uri);

		echo '<div class="row">';
		echo '<div class="col-xs-12">';

		echo $this->Paginate->loadmore($url, $page, $comments['pages']);

		echo '</div>';
		echo '</div>';
	}


}else{

	echo '<br>';
	echo '<p class="text-center"><i class="fa fa-comments-o fa-2x text-primary"></i> Be the first comment on this page</p>';
	echo '<br>';

}
?>
